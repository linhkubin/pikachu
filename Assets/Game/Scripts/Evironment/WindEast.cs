﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEast : MonoBehaviour
{
    private int timeStart;

    private int timeRemain;

    private float timer = 0;

    private float speed = 0.2f;

    bool isLife = false;

    bool updateWind = false;

    List<Node> list;

    public GameObject wind;


    private void Start()
    {
        //gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
        list = GameManager.Instance.m_Node;
    }

    private void Update()
    {
        if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_pause && !TimeManager.Instance.b_gameOver)
        {
            timer += Time.deltaTime;

            if (!isLife && timer > timeStart)
            {
                //gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                wind.SetActive(true);
                isLife = true;
                StartCoroutine(Check());
                timer = 0;
            }
            if (isLife && timer > timeRemain)
            {
                SimplePool.Despawn(gameObject);
            }
            if (TimeManager.Instance.b_gameOver)
            {
                SimplePool.Despawn(gameObject);
            }
        }
    }

    public IEnumerator Check()
    {
        

        //for (int j = 0; j < Constant.TOTAL_ITEM_NUMBER; j = j + Constant.NUMBER_ROW)
        //{
        //    int index = j + Constant.NUMBER_ROW - 2;

        //    for (int i = index; i > j + 1; i--)
        //    {
        //        if (list[i].GetComponent<Node>().IsNodeEmpty() && !IsBlockId(list[i - 1].GetComponent<Node>().I_id) && !list[i-1].GetComponent<Node>().IsNodeEmpty())
        //        {
        //            GameManager.Instance.SwapNode(i, i - 1);
        //            list[i - 1].GetComponent<Node>().SetAnim("windwest");
        //            list[i ].GetComponent<Node>().SetAnim("windwest");
        //            updateWind = true;
        //        }

        //    }

        //}

        for (int row = Constant.NUMBER_ROW - 1; row < Constant.TOTAL_ITEM_NUMBER; row += Constant.NUMBER_ROW)
        {
            for (int i = row; i > row - Constant.NUMBER_ROW + 2; i--)
            {
                if (list[i].IsNodeEmpty())
                {
                    for (int j = i - 1; j > row - Constant.NUMBER_ROW + 1; j--)
                    {
                        if (IsBlockId(list[j].I_id))
                        {
                            i = j;
                            break;
                        }
                        if (list[j].I_numberNode > 0 && !list[j].B_lockClick)
                        {
                            list[j].RemoveSelect();
                            list[j].SetAnim("idle");
                            list[j].SetAnim("windwest");
                            GameManager.Instance.SwapNode(i, j);
                            //list[i].SetAnim("windwest");
                            updateWind = true;
                            ElementManager.Instance.windLeft = false;
                            break;
                        }
                    }
                }

            }
        }

        yield return new WaitForSeconds(speed);

        if (updateWind)
        {
            updateWind = false;
        }

        if (!TimeManager.Instance.b_gameOver)
        {
            StartCoroutine(Check());
        }

    }

    public bool IsBlockId(int id)
    {
        if (id == Constant.ID_TYPE_BOX || id == Constant.ID_TYPE_BARRIER || id == Constant.ID_TYPE_ENVIRONMENT || id == Constant.ID_TYPE_NODE_LINK || id == Constant.ID_TYPE_NODE_LOCK || id == Constant.ID_NOTHING || id == Constant.ID_TYPE_NODE_WATER || id == Constant.ID_TYPE_NODE_FREEZE || id == Constant.ID_KEY || id == Constant.ID_HAT || id == Constant.ID_TYPE_NODE_CLOUD)
        {
            return true;
        }

        return false;
    }

    public void SetTime(int timeStart, int timeRemain)
    {
        isLife = false;
        timer = 0;
        this.timeStart = timeStart;
        this.timeRemain = timeRemain;
        wind.SetActive(false);
    }
}
