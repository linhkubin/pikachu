﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCloud3 : MonoBehaviour
{
    List<Node> list; 

    public void SetCloud()
    {
        list = GameManager.Instance.m_Node;
        for (int i = 80; i < 110; i++)
        {
            if (list[i].I_id == Constant.ID_TYPE_NODE && list[i].I_numberNode > 0)
            {
                if (Random.Range(0, 4) == 1)
                {
                    list[i].I_id = Constant.ID_TYPE_NODE_CLOUD;
                    list[i].SetFace(SpriteManager.Instance.g_cloud.anim);
                    list[i].g_faceId.GetComponent<AffectNode>().SetIndex(i);
                }
            }
        }
    }
}
