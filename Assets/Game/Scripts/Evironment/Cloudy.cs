﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloudy : MonoBehaviour
{
    private int timeStart;

    private int timeRemain;

    private float timer = 0;

    private float speed = 0.2f;

    bool isLife = false;

    bool updateWind = false;

    public Animator[] anim;

    private void Update()
    {
        if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_pause)
        {
            timer += Time.deltaTime;

            if (!isLife && timer > timeStart)
            {
                isLife = true;
                if (ElementManager.Instance.windLeft)
                {
                    Check("left");
                }
                Check("right");

                timer = 0;
            }
            
            if (TimeManager.Instance.b_gameOver)
            {
                SimplePool.Despawn(gameObject);
            }
        }
    }

    private void Check(string direction)
    {
        for (int i = 0; i < anim.Length; i++)
        {
            anim[i].SetTrigger(direction);
            anim[i].speed = (float)Random.Range(0, 4) / 10 + 1;
        }
        StartCoroutine(Disable());
    }

    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(7f);
        SimplePool.Despawn(gameObject);
    }

    public void SetTime(int timeStart)
    {
        this.timeStart = timeStart;
        timer = 0;
        isLife = false;
    }


}
