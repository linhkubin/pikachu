﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarTest : MonoBehaviour
{
    // Update is called once per frame
    //void Update()
    //{

    //    timer += Time.deltaTime;
    //    if (gameObject.transform.position == target.transform.position && !reset)
    //    {
    //        reset = true;
    //        Debug.Log("==");
    //        StartCoroutine(Reset());
    //    }

    //}

    //bool reset = false;
    //public IEnumerator Reset()
    //{
    //    yield return new WaitForSeconds(2f);
    //    Start();
    //    reset = false;
    //}

    //public IEnumerator PreMove()
    //{

    //public static void MoveCurvel(Transform tfMove, Vector3 target, float speed, Action done = null, float delay = 0f, bool randomDirection = false)
    //{
    //    if (!monoBehaviour)
    //    {
    //        Debug.Log("------------------------->> monoBehaviour null!!!");
    //        return;
    //    }
    //    monoBehaviour.StartCoroutine(IEMoveCurvel(tfMove, target, speed, done, delay, randomDirection));
    //}

    //static IEnumerator IEMoveCurvel(Transform tfMove, Vector3 target, float speed, Action done, float delay, bool randomDirection)
    //{
    //    if (delay > 0)
    //    {
    //        yield return new WaitForSeconds(delay);
    //    }
    //    var speedMove = 0f;
    //    var stopMove = false;
    //    if (randomDirection)
    //    {
    //        tfMove.eulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
    //    }
    //    Quaternion to;
    //    while (tfMove && !stopMove)
    //    {

    //        Vector2 direction = target - tfMove.position;
    //        to = Quaternion.FromToRotation(Vector3.up, direction);
    //        tfMove.Translate(speed  speedMove  tfMove.up, Space.World);
    //        tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, speedMove * speed);
    //        speedMove += Thanh.GameManager.instance.deltaTime / 5;
    //        stopMove = direction.sqrMagnitude < 0.1f;
    //        yield return null;
    //    }
    //    tfMove.transform.position = target;
    //    done?.Invoke();
    //}
    private IEnumerator IEMoveCurvel(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true)
    {
        target.x += Random.Range(-1, 2) * 0.3f;

        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }
        var speedMove = 0f;
        var stopMove = false;
        if (randomDirection)
        {
            tfMove.eulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
        }
        Quaternion to;
        while (tfMove && !stopMove)
        {

            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < 0.1f;
            yield return null;
        }
        tfMove.transform.position = target;


        if (effect != "null")
        {
            GameObject go = SimplePool.Spawn(effect, target, Quaternion.identity);

            yield return new WaitForSeconds(.4f);

            SimplePool.Despawn(go);

        }

        SimplePool.Despawn(gameObject);

    }

    private IEnumerator IEMoveCurvelNoFix(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true)
    {
        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }
        var speedMove = 0f;
        var stopMove = false;
        if (randomDirection)
        {
            tfMove.eulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
        }
        Quaternion to;
        while (tfMove && !stopMove)
        {

            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < 0.1f;
            yield return null;
        }
        tfMove.transform.position = target;


        if (effect != "null")
        {
            GameObject go = SimplePool.Spawn(effect, target, Quaternion.identity);

            yield return new WaitForSeconds(.4f);

            SimplePool.Despawn(go);

        }

        SimplePool.Despawn(gameObject);

    }

    private IEnumerator IEMoveKeyPreBooster(Transform tfMove, Vector3 target, Node effect, float speed, float delay, bool randomDirection = true)
    {

        float distance = (target - tfMove.position).magnitude;

        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }
        var stopMove = false;
        var speedMove = 0f;

        if (randomDirection)
        {
            Vector3 targetDir = target - tfMove.position;
            float angle = Vector3.Angle(targetDir, Vector3.up);
            if (Vector3.Angle(targetDir, Vector3.right) < 90)
            {
                angle = Vector3.Angle(targetDir, Vector3.down) + 180f;
            }

            angle = Random.Range(angle - 45f, angle + 45f);

            tfMove.eulerAngles = new Vector3(0, 0, angle);
        }

        Quaternion to;

        while (tfMove && !stopMove)
        {

            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, 3* speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < 0.2f;
            yield return null;
        }
        tfMove.transform.position = target;

        if (effect != null)
        {
            effect.RemoveFace();

            yield return new WaitForSeconds(.1f);

        }

        SimplePool.Despawn(gameObject);

    }

    public Animator anim;
    private IEnumerator IEMoveUpStar(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true, int star = 0)
    {
        target.x += (star - 1) * 0.3f;

        anim.SetTrigger("appear");

        yield return new WaitForSeconds(0.6f);

        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }
        var speedMove = 0f;
        var stopMove = false;
        if (randomDirection)
        {
            Vector3 targetDir = target - tfMove.position;
            float angle = Vector3.Angle(targetDir, Vector3.up);
            if (Vector3.Angle(targetDir, Vector3.right) < 90)
            {
                angle = Vector3.Angle(targetDir, Vector3.down) + 180f;
            }

            angle = Random.Range(angle - 60f, angle + 60f);

            tfMove.eulerAngles = new Vector3(0, 0, angle);
        }
        Quaternion to;
        while (tfMove && !stopMove)
        {

            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < 0.2f;
            yield return null;
        }
        tfMove.transform.position = target;


        if (effect != "null")
        {
            ScoreManager.Instance.UpdateStarInMode();

            GameObject go = SimplePool.Spawn(effect, target, Quaternion.identity);

            yield return new WaitForSeconds(.4f);

            SimplePool.Despawn(go);

        }

        SimplePool.Despawn(gameObject);

    }

    private IEnumerator IEMoveBuyDiamond(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true)
    {
        if (delay > 0)
        {
            yield return new WaitForSeconds(Random.Range(0, (float)delay));
        }
        var speedMove = 0f;
        var stopMove = false;
        if (randomDirection)
        {
            tfMove.eulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
        }
        Quaternion to;
        while (tfMove && !stopMove)
        {

            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < 0.1f;
            yield return null;
        }
        tfMove.transform.position = target;


        if (effect != "null")
        {
            GameObject go = SimplePool.Spawn(effect, target, Quaternion.identity);

            yield return new WaitForSeconds(.6f);

            SimplePool.Despawn(go);

        }

        SimplePool.Despawn(gameObject);

    }

    //-------------------set---------------------------
    public void SetTarget(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true)
    {
        StartCoroutine(IEMoveCurvel(gameObject.transform, target.position, effect, speed, delay, randomDirection));
    }

    public void SetTargetNoFix(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true)
    {
        StartCoroutine(IEMoveCurvelNoFix(gameObject.transform, target.position, effect, speed, delay, randomDirection));
    }

    public void SetTargetBuyDiamond(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true)
    {
        StartCoroutine(IEMoveBuyDiamond(gameObject.transform, target.position, effect, speed, delay, randomDirection));
    }

    public void SetTargetPrebooster(Transform target, Node effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true)
    {
        StartCoroutine(IEMoveKeyPreBooster(gameObject.transform, target.position, effect, speed, delay, randomDirection));
    }

    public void SetTargetUpStar(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true, int star = 0)
    {
        StartCoroutine(IEMoveUpStar(gameObject.transform, target.position, effect, speed, delay, randomDirection,star));
    }
}
