﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarModeRequest : MonoBehaviour
{
    private IEnumerator IEMoveCurvel(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true)
    {
        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }

        var speedMove = 0f;
        var stopMove = false;
        Quaternion to;
        Vector2 direction = target - tfMove.position;
        
        if (randomDirection)
        {
            tfMove.eulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(-90, 90));
        }
        while (tfMove && !stopMove && speedMove < 1f)
        {

            direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, 5 * speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < speedMove * 5;
            yield return null;
        }
        tfMove.transform.position = target;


        if (effect != "null")
        {
            GameObject go = SimplePool.Spawn(effect, target, Quaternion.identity);

            yield return new WaitForSeconds(.4f);

            SimplePool.Despawn(go);

        }

        //Debug.Log("end");

        SimplePool.Despawn(gameObject);

    }

    public void SetTarget(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true)
    {
        StartCoroutine(IEMoveCurvel(gameObject.transform, target.position, effect, speed, delay, randomDirection));
    }

}
