﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoThrough : MonoBehaviour {

    public Transform tf_end;

#region Cicle variable
    [Header("Cicle variable")]
    private Vector3 target, mousePos;
    private bool move = false;
    public float speed;
    public float coefficient;
    #endregion
    #region Throw variable 
    [Header("Throw variable")]
    public Rigidbody2D rigid;
    public Vector3 point;
    public float veloY, veloX;
    public float timer;
    private float time;
    private readonly float g = -9.8f;
    private Quaternion rotate;
    private bool runThrow;
#endregion
    // Use this for initialization
    void Start () {
        //InitThrow();
        transform.position = Vector3.zero;

        point = tf_end.position;
        veloY = Vector3.Distance(transform.position, point);
        InitThrow();
    }

    // Update is called once per frame
    void Update () {

        OnThrow();
    }

    private void InitThrow()
    {
        time = 0;
        rigid.gravityScale = 1;
        runThrow = true;        
        veloY = -veloY * veloY / 2 / g > point.y - transform.position.y ? veloY : Mathf.Sqrt(-g * 2 * (point.y - transform.position.y))+0.5f;
        timer = (-veloY - Mathf.Sqrt(veloY * veloY - 4 * (-point.y + transform.position.y) * g / 2)) / g;
        veloX = (point.x - transform.position.x) / timer;
        rigid.velocity = new Vector2(veloX, veloY);
    }

    private void OnThrow()
    {
        Debug.Log("throw");
        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        //    veloY = Vector3.Distance(transform.position, point);
        //    InitThrow();
        //}
        if (!runThrow)
            return;
        if (time >= timer)
        {
            rigid.gravityScale = 0;
            rigid.velocity = Vector2.zero;
            runThrow = false;
        }
        time += Time.deltaTime;
        rotate = Quaternion.LookRotation(rigid.velocity.normalized, Vector3.back);
        rotate.x = 0;
        rotate.y = 0;
        transform.rotation = rotate;
    }

    private void OnCicle()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        //    target = GetTarget(transform.position, mousePos);

        //    move = true;
        //}
        if (move)
        {
            transform.RotateAround(target, new Vector3(0, 0, -1), speed);
            var x = Vector3.Distance(transform.position, mousePos);
            move = x > 0.1f;
            //target = GetTarget(transform.position, mousePos);
        }
    }

    Vector3 GetTarget(Vector3 start, Vector3 end)
    {
        var difference = end - start;
        difference.Normalize();
        print(difference);
        speed = difference.x >= 0 ? Mathf.Abs(speed) : -Mathf.Abs(speed);
        var _difference = new Vector3(difference.y * difference.x > 0 ? Mathf.Abs(difference.y) : -Mathf.Abs(difference.y), -Mathf.Abs(difference.x)) * coefficient;
        print(_difference);
        var mid = (end + start) / 2 + _difference;
        print(mid);
        return mid;
    }
}
