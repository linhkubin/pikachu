﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearNode : MonoBehaviour
{

    public void SetAppear(Node node,int number)
    {
        StartCoroutine(Appear( node, number));
    }

    private IEnumerator Appear(Node node, int number)
    {
        yield return new WaitForSeconds(0.3f);
        if (TimeManager.Instance.b_gameStart)
        {
            node.NewNode(number);
        }
        yield return new WaitForSeconds(0.2f);
        SimplePool.Despawn(gameObject);
    }
}
