﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarNode : MonoBehaviour
{

    //public Transform tf_end;
    //public Transform tf_start;

    #region Cicle variable
    [Header("Cicle variable")]
    private Vector3 target, mousePos;
    private bool move = false;
    public float speed;
    public float coefficient;
    #endregion
    #region Throw variable 
    [Header("Throw variable")]
    public Rigidbody2D rigid;
    public Vector3 point;
    public float veloY, veloX;
    public float timer;
    private float time;
    private readonly float g = -9.8f;
    private Quaternion rotate;
    private bool runThrow;
    #endregion

    #region Effect
    private string effect;
    #endregion

    Node newNode;
    int numberNode;

    void Start()
    {
        //transform.position = tf_start.position;

        //point = tf_end.position;

        //veloY = Vector3.Distance(transform.position, point);

        //InitThrow();

        //OnThrow();

    }

    // Update is called once per frame
    void Update()
    {
        OnThrow();
    }

    private void InitThrow()
    {
        time = 0;
        rigid.gravityScale = 1;
        runThrow = true;
        veloY = -veloY * veloY / 2 / g > point.y - transform.position.y ? veloY : Mathf.Sqrt(-g * 2 * (point.y - transform.position.y)) + 0.5f;
        timer = (-veloY - Mathf.Sqrt(veloY * veloY - 4 * (-point.y + transform.position.y) * g / 2)) / g ;
        veloX = (point.x - transform.position.x) / timer;
        rigid.velocity = new Vector2(veloX, veloY);
     }

    private void OnThrow()
    {
        if (!runThrow)
            return;
        if (time >= timer || !TimeManager.Instance.b_gameStart)
        {
            rigid.gravityScale = 0;
            rigid.velocity = Vector2.zero;
            runThrow = false;
            CompaleteMove();
            return;
        }
        time += Time.deltaTime;

        rotate = Quaternion.LookRotation(rigid.velocity.normalized, Vector3.back);
        rotate.x = 0;
        rotate.y = 0;
        transform.rotation = rotate;

    }

    private void CompaleteMove()
    {
        //Debug.Log("stop ");

        if (!TimeManager.Instance.b_gameStart)
        {
            SimplePool.Despawn(gameObject);
            return;
        }

        GameObject go = SimplePool.Spawn("SpawnNode", newNode.GetTransform().position,Quaternion.identity);

        go.GetComponent<AppearNode>().SetAppear(newNode, numberNode);

        SimplePool.Despawn(gameObject);
    }


    public void SetTarget(Transform tf_end, string effect = null, Node node = null, int numberNode = 0)
    {
        this.effect = effect;

        newNode = node;

        this.numberNode = numberNode;

        point = tf_end.position;

        veloY = Vector3.Distance(transform.position, point);

        InitThrow();
    }
}
