﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeRequest : MonoBehaviour
{
    private static ModeRequest instance;
    public static ModeRequest Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<ModeRequest>(); return instance; } }

    private int complete = 0;


    public UIFindNode[] UI_findNodes;

    public List<FindNode> findNode;

    public void SetRequest(int numberNode,int numberRequest)
    {
        complete++;
        findNode.Add(new FindNode(numberNode, numberRequest));
        UI_findNodes[findNode.Count - 1].ui.SetActive(true);
        UI_findNodes[findNode.Count - 1].ui.SetActive(true);
    }

    public void ResetUI()
    {
        UI_findNodes[0].Reset();
        UI_findNodes[1].Reset();
        UI_findNodes[2].Reset();
    }

    public void Reset()
    {
        complete = 0;
        findNode.Clear();
        ResetUI();
    }

    public void CheckRequest(Vector3 positionInit, int numberNode)
    {
        if (!TimeManager.Instance.b_gameOver)
        {
            if (complete == 0)
            {
                return;
            }

            for (int i = 0; i < findNode.Count; i++)
            {
                if (findNode[i].numberNode == numberNode && !findNode[i].complete)
                {
                    // anim
                    GameObject go = SimplePool.Spawn("StarRequest", positionInit, Quaternion.identity);
                    go.GetComponent<StarModeRequest>().SetTarget(UI_findNodes[i].GetPosition(), "Connected", 1.2f, 0.2f, true);

                    findNode[i].numbeRequest--;
                    StartCoroutine(UpdateTxtNumber(i));

                    if (findNode[i].numbeRequest == 0)
                    {
                        complete--;

                        findNode[i].complete = true;

                        if (complete == 0)
                        {
                            Debug.Log("complete mode request");

                            StartCoroutine(WinRequest());
                        }
                    }

                }
            }
        }
    }

    private IEnumerator UpdateTxtNumber(int i)
    {
        yield return new WaitForSeconds(1.2f);

        if (findNode[i].numbeRequest == 0)
        {
            yield return new WaitForSeconds(0.2f);

            UI_findNodes[i].numberRequest.gameObject.SetActive(false);
            UI_findNodes[i].complete.SetActive(true);
        }
        else
        {
            UI_findNodes[i].numberRequest.text = findNode[i].numbeRequest.ToString();

        }

    }

    private IEnumerator WinRequest()
    {
        TimeManager.Instance.b_gameStart = false;

        yield return new WaitForSeconds(1.4f);

        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (GameManager.Instance.m_Node[i].I_numberNode > 0)
            {
                GameManager.Instance.m_Node[i].RemoveNode();
                yield return new WaitForSeconds(0.05f);
            }
        }
        yield return new WaitForSeconds(0.7f);

        StartCoroutine(UIManager.Instance.OpenWinPanel(TimeManager.Instance.GetTime(), true));
    }
}

[Serializable]
public class UIFindNode
{
    public GameObject ui;
    public GameObject complete;
    public Image image;
    public Text numberRequest;
    private Transform position;
    public Transform GetPosition()
    {
        if (position == null)
        {
            position = ui.transform;
        }
        return position;
    }

    public void Reset()
    {
        complete.SetActive(false);
        numberRequest.gameObject.SetActive(true);
        ui.SetActive(false);
        position = null;
    }

}

[Serializable]
public class FindNode
{
    public int numberNode;
    public int numbeRequest;
    public bool complete = false;

    public FindNode(int number,int request)
    {
        numberNode = number;
        numbeRequest = request;

    }
}