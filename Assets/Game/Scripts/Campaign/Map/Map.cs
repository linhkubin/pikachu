﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    //public int numberWall;
    public int numberMap;
    // Start is called before the first frame update

    public GameObject[] stars = new GameObject[3];

    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(ChooseMap);
        gameObject.GetComponent<Animator>().ResetTrigger("Normal");
        gameObject.transform.GetChild(0).GetComponent<Text>().text = numberMap.ToString();
        //numberWall = (numberMap - 1) / 20;
    }

    public void SetNumberMap(int num)
    {
        numberMap = num;
        gameObject.GetComponentInChildren<Text>().text = numberMap.ToString();
    }

    private void ChooseMap()
    {
        SoundManager.instance.PlayFxSound("Click");
        // Debug.Log(numberMap);
        // Debug.Log(PlayerPrefs.GetInt("Level" + numberMap));
        if (DataManager.Instance.GetCurrentEnergy() > 0)
        {
            //GameManager.Instance.NUMBER_WALL = numberWall;

            GameManager.MAP_LEVEL = numberMap;

            CUIManager.Instance.inGame.SetActive(true);
    
            GameManager.Instance.ResetStage();

            CUIManager.Instance.processMap.SetActive(false);

            UIManager.Instance.OpenPrePlayPanel();
        }
        else
        {
            Debug.Log("Not enough energy");
            CUIManager.Instance.OpenEnergyPack();
        }
    }
}
