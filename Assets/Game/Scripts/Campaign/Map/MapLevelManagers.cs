﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLevelManagers : MonoBehaviour
{

    [SerializeField]
    private WallMapLevel[] mapLevel;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < mapLevel[0].map.Length; i++)
        {
            mapLevel[0].map[i].GetComponent<Map>().numberMap = i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[Serializable]
public class WallMapLevel
{
    public Sprite backGround;

    public GameObject[] map;
}