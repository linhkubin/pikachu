﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CUIManager : MonoBehaviour
{
    public GameObject mc;

    //-----------------------Daily Gifts---------------------------------

    public GameObject dailyGiftsPopup;

    public GameObject dailyGiftsBg;
    public GameObject rewardPopup;
    public Text rewardNumber;
    public Text rewardName;
    public GameObject rewardImage;

    //-----------------------Scroll Process Map--------------------------

    public GameObject campaignPanel;
    public GameObject maskLayer;
    public GameObject mapContent;

    //------------------------Level------------------------------

    public Sprite lockedLevel;
    public Sprite unlockedLevel;

    //-----------------------Variables--------------------------

    public static CUIManager instance;
    public static CUIManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<CUIManager>(); return instance; } }

    [SerializeField]
    private GameObject settingContent;
    public GameObject settingImage;

    public GameObject inGame;

    public GameObject processMap;

    //-----------------------Sound and Music--------------------------

    private bool notSound = false;
    private bool notMusic = false;

    [SerializeField]
    private GameObject soundImage;

    [SerializeField]
    private GameObject musicImage;

    //------------------------Buy Pack--------------------------

    [SerializeField]
    private GameObject gemPack;

    [SerializeField]
    private GameObject energyPack;

    [SerializeField]
    private GameObject gemPackImage;

    [SerializeField]
    private GameObject energyPackImage;

    public Text energyNumberText;

    public Text gemNumberText;

    public Text FullLifePriceText;
    public Text TakeHeart;
    public Button FullLifeBtn;
    public GameObject FullLifeImg;
    public Text FullLifeText;

    //------------------------Challenge---------------------------

    [SerializeField]
    private GameObject challengePanel;

    [SerializeField]
    private GameObject challengePanelImage;

    [SerializeField]
    private GameObject selectedDailyMission;

    [SerializeField]
    private GameObject unselectedDailyMission;

    [SerializeField]
    private GameObject selectedAchievement;

    [SerializeField]
    private GameObject unselectedAchievement;

    //------------------------Setting buttons --------------------

    [SerializeField]
    private GameObject buttonFb;

    [SerializeField]
    private GameObject buttonHighScore;

    [SerializeField]
    private GameObject buttonSound;

    [SerializeField]
    private GameObject buttonMusic;

    [SerializeField]
    private GameObject buttonSetting;

    [SerializeField]
    private GameObject buttonFbImage;

    [SerializeField]
    private GameObject buttonHighScoreImage;

    [SerializeField]
    private GameObject buttonSoundImage;

    [SerializeField]
    private GameObject buttonMusicImage;

    //------------------------Star--------------------

    public Text totalStarText;

    //------------------------Refill energy--------------------

    [SerializeField]
    private Text energyRefillText;

    //------------------------------------------------------------
    
    public GameObject currentLvParticle;

    //---------------------------World---------------------------------

    public GameObject[] bricks = new GameObject[10];

    public Sprite[] worldBG = new Sprite[2];

    public bool openSetting = false;

    //-----------------------Functions--------------------------
    //-----------------------Setting animations--------------------------

    private void Awake()
    {
        Application.targetFrameRate = 60;

    }

    private void Start()
    {
        DisableBrick();
        bricks[0].GetComponent<WorldStar>().stage[0].GetComponent<Image>().sprite = unlockedLevel;
        
        // OpenDailyGiftsPopup();

        energyNumberText.text = DataManager.Instance.GetCurrentEnergy().ToString();
        gemNumberText.text = PlayerPrefs.GetInt("GemNumber").ToString();
        StartCoroutine("Jump");

        SoundManager.instance.PlayMusic(Random.Range(3,6));
    }

    private void Update()
    {
        energyRefillText.text = DataManager.Instance.GetNextLifeTime().ToString();
        energyNumberText.text = DataManager.Instance.GetCurrentEnergy().ToString();
    }

    //--------------------------------Worlds----------------------------------------

    public void DisableBrick()
    {
        for(int i = 0; i < bricks.Length; i++)
        {
            for (int j = 0; j < bricks[i].GetComponent<WorldStar>().stage.Length; j++)
            {
                bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Image>().sprite = lockedLevel;
                bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Button>().interactable = false;

                if(bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Map>().numberMap == PlayerPrefs.GetInt("CurrentLevel"))
                {
                    CUIManager.Instance.currentLvParticle.transform.position = bricks[i].GetComponent<WorldStar>().stage[j].gameObject.transform.position;
                    CUIManager.Instance.currentLvParticle.transform.parent = bricks[i].GetComponent<WorldStar>().stage[j].gameObject.transform;

                    // float mapContentYAxis = bricks[i].GetComponent<WorldStar>().stage[j].transform.position.y - bricks[i].GetComponent<WorldStar>().stage[j].transform.position.y;
                    // CUIManager.Instance.mapContent.transform.position -= new Vector3(0f, mapContentYAxis, 0f);

                    // Debug.Log(bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Map>().numberMap);
                }
            }
            bricks[i].GetComponent<WorldStar>().background.sprite = null;    
        }
    }

    //--------------------------------Setting buttons----------------------------------------

    bool delaySetting = false;

    public void OpenSetting()
    {
        if (!delaySetting)
        {
            StartCoroutine(DelaySetting());
            if (!settingContent.activeInHierarchy )
            {
                OpenSettingCourotine();
                Debug.Log("Open setting");

            }
            else
            {
                CloseSettingCourotine();
                Debug.Log("close setting");

            }
        }
        
    }
    private IEnumerator DelaySetting()
    {
        delaySetting = true;
        settingContent.transform.parent.GetComponent<Button>().interactable = false;
        yield return new WaitForSeconds(0.7f);
        delaySetting = false;
        settingContent.transform.parent.GetComponent<Button>().interactable = true;

    }

    public void OpenSettingCourotine()
    {
        if(openSetting == false)
        {
            settingContent.SetActive(true);

            SetSettingIconsActive();

            StartCoroutine("Jump");

            AnimateOpenPopup(settingImage.GetComponent<Animator>(), "OpenSetting");

            openSetting = true;
        }
        
        else
        {
            // SetSettingIconsDeactive();

            StartCoroutine("Jump");

            AnimateOpenPopup(settingImage.GetComponent<Animator>(), "CloseSetting");

            openSetting = false;
        }
        // AnimateOpenPopup(buttonMusic.GetComponent<Animator>(), "open music");

        // yield return new WaitForSeconds(0.16f);

        // AnimateOpenPopup(buttonSound.GetComponent<Animator>(), "open sound");
        
        // yield return new WaitForSeconds(0.14f);
        
        // AnimateOpenPopup(buttonHighScore.GetComponent<Animator>(), "open highscore");
        
        // yield return new WaitForSeconds(0.12f);
        
        // AnimateOpenPopup(buttonFb.GetComponent<Animator>(), "open fb");
    }

    public void SetDefaultSettingIcons()
    {
        buttonFb.transform.position = new Vector3(-199f, 181f, 0f);
        buttonHighScore.transform.position = new Vector3(-199f, 181f, 0f);
        buttonSound.transform.position = new Vector3(-199f, 181f, 0f);
        buttonMusic.transform.position = new Vector3(-199f, 181f, 0f);
        // buttonSetting.GetComponent<Button>().interactable = false;
        SetSettingIconsDeactive();
        settingContent.SetActive(false);
    }

    private void CloseSettingCourotine()
    {
        // AnimateOpenPopup(buttonFb.GetComponent<Animator>(), "close fb");
        // yield return new WaitForSeconds(0.04f);
        
        // AnimateOpenPopup(buttonHighScore.GetComponent<Animator>(), "close highscore");
        // yield return new WaitForSeconds(0.06f);
        
        // AnimateOpenPopup(buttonSound.GetComponent<Animator>(), "close sound"); 
        // yield return new WaitForSeconds(0.08f);
        
        // AnimateOpenPopup(buttonMusic.GetComponent<Animator>(), "close music");
        // yield return new WaitForSeconds(0.22f);

        SetSettingIconsDeactive();
        
        settingContent.SetActive(false);
    }

    private void SetSettingIconsActive()
    {
        buttonMusic.SetActive(true);
        buttonSound.SetActive(true);
        buttonHighScore.SetActive(true);
        buttonFb.SetActive(true);
    }

    private void SetSettingIconsDeactive()
    {
        buttonMusic.SetActive(false);
        buttonFb.SetActive(false);
        buttonHighScore.SetActive(false);
        buttonSound.SetActive(false);
    }

    //-----------------------Challenge---------------------------------

    public void OpenChallengePanel()
    {
        mc.SetActive(false);
        challengePanel.SetActive(true);

        AnimateOpenPopup(challengePanelImage.GetComponent<Animator>(), "open");
    }

    public void CloseChallengePanel()
    {
        mc.SetActive(true);
        StartCoroutine(CloseBuyPanel(challengePanelImage.GetComponent<Animator>(), challengePanel));
    }

    public void OpenChallengeTab()
    {
        if(!selectedDailyMission.activeInHierarchy)
        {
            selectedDailyMission.SetActive(true);
            unselectedDailyMission.SetActive(false);
            selectedAchievement.SetActive(false);
            unselectedAchievement.SetActive(true);
        }
        else
        {
            selectedDailyMission.SetActive(false);
            unselectedDailyMission.SetActive(true);
            selectedAchievement.SetActive(true);
            unselectedAchievement.SetActive(false);
        }
    }

    //--------------------Daily Gifts------------------------

    public void OpenDailyGiftsPopup()
    {
        mc.SetActive(false);

        dailyGiftsPopup.SetActive(true);

        AnimateOpenPopup(dailyGiftsBg.GetComponent<Animator>(), "open");
    }

    public void CloseDailyGiftsPopup()
    {
        mc.SetActive(true);
        StartCoroutine(CloseBuyPanel(dailyGiftsBg.GetComponent<Animator>(), dailyGiftsPopup));
    }

    public void OpenRewardPopup()
    {
        rewardPopup.SetActive(true);
    }

    public void CloseRewardPopup()
    {
        rewardPopup.SetActive(false);

        if(DailyGiftsManager.Instance.secondRewardPopup == 1)
        {
            DailyGiftsManager.Instance.OpenSecondRewardPopup();
        }
        
        DailyGiftsManager.Instance.secondRewardPopup = 0;
    }

    //-----------------------Pack--------------------------
    public void OpenEnergyPack()
    {
        mc.SetActive(false);

        if(gemPack.activeSelf)
        {
            CloseGemPack();
        }

        energyPack.SetActive(!energyPack.activeInHierarchy);

        AnimateOpenPopup(energyPackImage.GetComponent<Animator>(), "open");
    }

    public void CloseEnergyPack()
    {
        mc.SetActive(true);
        StartCoroutine(CloseBuyPanel(energyPackImage.GetComponent<Animator>(), energyPack));
    }

    public void OpenGemPack()
    {
        mc.SetActive(false);

        if(energyPack.activeSelf)
        {
            CloseEnergyPack();
        }

        gemPack.SetActive(!gemPack.activeInHierarchy);

        AnimateOpenPopup(gemPackImage.GetComponent<Animator>(), "open");
    }

    public void CloseGemPack()
    {
        mc.SetActive(true);
        StartCoroutine(CloseBuyPanel(gemPackImage.GetComponent<Animator>(), gemPack));
    }

    //------------------------------------------------------------------------------

    public void SetFullLivesBtn()
    {
        FullLifePriceText.text = "";
        TakeHeart.text = "";
        FullLifeImg.SetActive(false);
        FullLifeText.enabled = true;
    }

    public void SetNotFullLivesBtn(int m_MaxVlue, int m_CurrentValue)
    {
        FullLifePriceText.text = ((m_MaxVlue - m_CurrentValue)).ToString();
        TakeHeart.text = ((m_MaxVlue - m_CurrentValue)).ToString();
        FullLifeImg.SetActive(true);
        FullLifeText.enabled = false;
    }

    //-----------------------Sound and Music--------------------------

    public void EnableSound()
    {
        buttonSound.transform.position = new Vector3(202f, 79f, 0f);
        if(notSound)
        {
            soundImage.SetActive(false);
            notSound = false;
            SoundManager.instance.StartMusic();
        }
        else
        {
            soundImage.SetActive(true);
            notSound = true;
            SoundManager.instance.MuteMusic();

        }
    }

    public void EnableMusic()
    {
        if(notMusic)
        {
            SoundManager.instance.StartFx();
            musicImage.SetActive(false);
            notMusic = false;
        }
        else
        {
            SoundManager.instance.MuteFx();
            musicImage.SetActive(true);
            notMusic = true;
        }
    }

    //-----------------------Buttons jump--------------------------

    private IEnumerator Jump()
    {
        while(true)
        {
            buttonFb.GetComponent<Animator>().SetTrigger("Fb Jump");

            yield return new WaitForSeconds(0.35f);

            buttonHighScore.GetComponent<Animator>().SetTrigger("HighScore Jump");

            yield return new WaitForSeconds(0.35f);

            buttonSound.GetComponent<Animator>().SetTrigger("Sound Jump");

            yield return new WaitForSeconds(0.35f);

            buttonMusic.GetComponent<Animator>().SetTrigger("Music Jump");

            yield return new WaitForSeconds(3f);
        }
    }

    //-----------------------Animations--------------------------

    private void AnimateOpenPopup(Animator anim, string status)
    {
        anim.SetTrigger(status);
    }

    private void AnimateOpenPopup(Animator anim, bool setBool, string status)
    {
        anim.SetBool(status, setBool);
    }

    private void AnimateOpenPopupWithDelay(float delayTime, Animator anim, string status)
    {
        StartCoroutine(DelayAnimation(delayTime, anim, status));
    }

    private IEnumerator CloseBuyPanel(Animator anim, GameObject panel)
    {
        anim.SetTrigger("close");

        yield return new WaitForSeconds(0.2f);

        panel.SetActive(false);
    }


#region Buy Diamond
    [Header("Diamond")]
    public Transform tf_diamond;
    public Transform tf_manyDiamond;
    public Transform tf_largeDiamond;
    public Transform tf_targetDiamond;
    #endregion
    int indexDiamond;
    public void BuyDiamod(int diamond)
    {
        Vector3 initPos = Vector3.zero;
        float numberDiamond = 0;

        indexDiamond = PlayerPrefs.GetInt("GemNumber") - diamond;
        int deltaDiamondPerSec = 0;

        if (diamond == 5)
        {
            initPos = tf_diamond.position;
            numberDiamond = 5f;
            deltaDiamondPerSec = 1;
        }
        else if (diamond == 30)
        {
            initPos = tf_manyDiamond.position;
            numberDiamond = 15f;
            deltaDiamondPerSec = 2;
        }
        else if (diamond == 100)
        {
            initPos = tf_largeDiamond.position;
            numberDiamond = 50f;
            deltaDiamondPerSec = 2;
        }

        for (int i = 0; i < numberDiamond; i++)
        {
            StartCoroutine(SpawnDiamond(numberDiamond/10, initPos, deltaDiamondPerSec));
        }
    }

    private IEnumerator SpawnDiamond(float time, Vector3 initPos,int diamond)
    {
        yield return new WaitForSeconds(Random.Range(0, time));

        GameObject go = SimplePool.Spawn("Diamond", initPos, Quaternion.identity);
        go.GetComponent<StarTest>().SetTargetBuyDiamond(tf_targetDiamond, "DiamondBroken", 2f, 0, true);

        yield return new WaitForSeconds(1f);
        indexDiamond += diamond;
        CUIManager.Instance.gemNumberText.text = indexDiamond.ToString();
    }

    #region Buy Heart
    [Header("Heart")]
    public Transform tf_heart;
    public Transform tf_targetHeart;
    #endregion

    public void BuyHeart(int heart)
    {
        for (int i = 0; i < heart; i++)
        {
            GameObject go = SimplePool.Spawn("Heart", tf_heart.position, Quaternion.identity);
            go.GetComponent<StarTest>().SetTargetBuyDiamond(tf_targetHeart, "HeartBroken", 2f, 0f, true);
        }
    }

    //-----------------------------Process Map UI------------------------------------
    public void MoveOutProcessMapUI()
    {
        AnimateOpenPopup(campaignPanel.GetComponent<Animator>(), "MoveOut"); 
        AnimateOpenPopup(maskLayer.GetComponent<Animator>(), "MoveOut");
    }

    public void MoveInProcessMapUI()
    {
        AnimateOpenPopupWithDelay(0.2f, campaignPanel.GetComponent<Animator>(), "MoveIn");
        AnimateOpenPopupWithDelay(0.2f, maskLayer.GetComponent<Animator>(), "MoveIn");
    }

    IEnumerator DelayAnimation(float delayTime, Animator anim, string status)
    {
        yield return new WaitForSeconds(delayTime);
        anim.SetTrigger(status);
    }
}