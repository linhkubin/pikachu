﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public static Brick instance;
    public static Brick Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<Brick>(); return instance; } }

    public WorldStar[] worlds;
}
