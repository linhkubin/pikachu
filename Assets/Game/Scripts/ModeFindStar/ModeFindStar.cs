﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeFindStar : MonoBehaviour
{
    private static ModeFindStar instance;
    public static ModeFindStar Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<ModeFindStar>(); return instance; } }

    List<Hat> list_hats = new List<Hat>();

    public void newHat(GameObject hat)
    {
        list_hats.Add(hat.GetComponent<Hat>());
    }

    public int upStar = 0;

    public void StartMode()
    {
        RandomStar();
    }

    private void RandomStar()
    {

        //Debug.Log("list cout : " + list_hats.Count);
        for (int i = 0; i < 3; i++)
        {
            int ran = Random.Range(0, list_hats.Count);

            while (list_hats[ran].haveStar)
            {
                ran = ran <= 0 ? list_hats.Count - 1 : ran - 1;
            }

            list_hats[ran].haveStar = true;
        }
        Debug.Log("number hat : "+list_hats.Count);
    }

    public void Reset()
    {
        for (int i = 0; i < list_hats.Count; i++)
        {
            list_hats[i].haveStar = false;
        }
        list_hats.Clear();
        upStar = 0;
    }

}
