﻿using UnityEngine;
using UnityEngine.UI;

public class Arrow : MonoBehaviour
{
    public static Arrow instance;
    public static Arrow Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<Arrow>(); return instance; } }

    [SerializeField]
    private GameObject circleBorder;

    public int itemNumber;
    public string itemName;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Reward")
        {
            itemNumber = other.gameObject.GetComponent<WheelCell>().Factor;
            itemName = other.gameObject.GetComponent<Image>().sprite.name;
        }
    }
}