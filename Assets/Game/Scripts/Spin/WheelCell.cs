﻿using UnityEngine;
using UnityEngine.UI;

public class WheelCell : MonoBehaviour
{
	// private Sprite cellImage;
	private int factor;
	
	public Text number;

	public int Factor { get { return factor; } set { factor = value; } }
}