﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    public static LightManager instance;
    public static LightManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<LightManager>(); return instance; } }

    [SerializeField] GameObject[] lightArray = new GameObject[24];
    public float blinkTime = 0.2f;

    public IEnumerator Blink()
    {
        while(true)
        {
            for(int i = 0; i < lightArray.Length; i++)
            {
                if(lightArray[i].activeInHierarchy)
                {
                    lightArray[i].SetActive(false);
                }
                else
                {
                    lightArray[i].SetActive(true);
                }
            }

            yield return new WaitForSeconds(blinkTime);
        }
    }
}