﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FortuneWheelManager : MonoBehaviour
{
	private int randomBooster1;
	private int randomBooster2;
    private bool _isStarted;
    private float[] _sectorsAngles;
    private float _finalAngle;
    private float _startAngle = 20;
    private float _currentLerpRotationTime;
    public Button TurnButton;
    public GameObject Circle;

	private bool spinRewardAppear = false;

	[SerializeField] GameObject[] wheelCells = new GameObject[9];
	[SerializeField] Sprite[] wheelSprites = new Sprite[7];

	private float[] finalArray;

	private int level = 1;

	private void Start()
	{
		// _isStarted = true;
		if(level <= 10)
		{
			SetSpriteLv1_10();
		}
		else if(level <= 30)
		{
			SetSpriteLv11_30();
		}
		else if(level <= 100)
		{
			SetSpriteLv31_100();
		}
		else if(level <= 200)
		{
			SetSpriteLv101_200();
		}
		else
		{
			SetSpriteLv201();
		}
	}

	private void SetSpriteLv1_10()
	{
		SetSprite(wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[1], wheelSprites[1], wheelSprites[Random.Range(2, 6)]);
		SetFactor(1, 1, 2, 2, 3, 3, 1, 2, 1);
	}

	private void SetSpriteLv11_30()
	{
		SetSprite(wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[0], wheelSprites[1], wheelSprites[1], wheelSprites[Random.Range(2, 6)], wheelSprites[6]);
		SetFactor(1, 1, 2, 2, 3, 1, 2, 1, 1);
	}

	private void SetSpriteLv31_100()
	{
		RandomBooster();
		SetSprite(wheelSprites[1], wheelSprites[0], wheelSprites[1], wheelSprites[0], wheelSprites[randomBooster1], wheelSprites[0], wheelSprites[randomBooster2], wheelSprites[0], wheelSprites[6]);
		SetFactor(1, 2, 1, 2, 1, 4, 1, 4, 1);
	}

	private void SetSpriteLv101_200()
	{
		RandomBooster();
		SetSprite(wheelSprites[0], wheelSprites[1], wheelSprites[0], wheelSprites[1], wheelSprites[0], wheelSprites[randomBooster1], wheelSprites[6], wheelSprites[randomBooster2], wheelSprites[6]);
		SetFactor(1, 1, 1, 1, 3, 1, 1, 1, 1);
	}

	private void SetSpriteLv201()
	{
		SetSprite(wheelSprites[0], wheelSprites[1], wheelSprites[0], wheelSprites[1], wheelSprites[0], wheelSprites[1], wheelSprites[6], wheelSprites[Random.Range(2, 6)], wheelSprites[6]);
		SetFactor(1, 1, 1, 1, 3, 3, 1, 1, 1);
	}

	private void SetFactor(int factor0, int factor1, int factor2, int factor3, int factor4, int factor5, int factor6, int factor7, int factor8)
	{
		wheelCells[0].GetComponent<WheelCell>().Factor = factor0;
		wheelCells[0].GetComponent<WheelCell>().number.text = factor0.ToString();

		wheelCells[1].GetComponent<WheelCell>().Factor = factor1;
		wheelCells[1].GetComponent<WheelCell>().number.text = factor1.ToString();

		wheelCells[2].GetComponent<WheelCell>().Factor = factor2;
		wheelCells[2].GetComponent<WheelCell>().number.text = factor2.ToString();

		wheelCells[3].GetComponent<WheelCell>().Factor = factor3;
		wheelCells[3].GetComponent<WheelCell>().number.text = factor3.ToString();

		wheelCells[4].GetComponent<WheelCell>().Factor = factor4;
		wheelCells[4].GetComponent<WheelCell>().number.text = factor4.ToString();

		wheelCells[5].GetComponent<WheelCell>().Factor = factor5;
		wheelCells[5].GetComponent<WheelCell>().number.text = factor5.ToString();

		wheelCells[6].GetComponent<WheelCell>().Factor = factor6;
		wheelCells[6].GetComponent<WheelCell>().number.text = factor6.ToString();

		wheelCells[7].GetComponent<WheelCell>().Factor = factor7;
		wheelCells[7].GetComponent<WheelCell>().number.text = factor7.ToString();

		wheelCells[8].GetComponent<WheelCell>().Factor = factor8;
		wheelCells[8].GetComponent<WheelCell>().number.text = factor8.ToString();
	}

	private void SetSprite(Sprite sprite0, Sprite sprite1, Sprite sprite2, Sprite sprite3, Sprite sprite4, Sprite sprite5, Sprite sprite6, Sprite sprite7, Sprite sprite8)
	{
		wheelCells[0].GetComponent<Image>().sprite = sprite0;
		wheelCells[1].GetComponent<Image>().sprite = sprite1;
		wheelCells[2].GetComponent<Image>().sprite = sprite2;
		wheelCells[3].GetComponent<Image>().sprite = sprite3;
		wheelCells[4].GetComponent<Image>().sprite = sprite4;
		wheelCells[5].GetComponent<Image>().sprite = sprite5;
		wheelCells[6].GetComponent<Image>().sprite = sprite6;
		wheelCells[7].GetComponent<Image>().sprite = sprite7;
		wheelCells[8].GetComponent<Image>().sprite = sprite8;
	}

	public void TurnWheel()
    {
		DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_SPIN_ID, 1);
		//----------------------Blink Spin Light------------------------

		StartCoroutine("SpinBlinkFast");
		// StartCoroutine(LightManager.Instance.BlinkFast());

		// gameObject.transform.eulerAngles = new Vector3(0f, 0f, 20f);

		_currentLerpRotationTime = 0f;
	
		// Fill the necessary angles (for example if you want to have 12 sectors you need to fill the angles with 30 degrees step)
		_sectorsAngles = new float[] {20, 60, 100, 140, 180, 220, 260, 300, 340};
	
		int fullCircles = 4;
		finalArray = RandomChooseByLevel();
		//Debug.Log(finalArray);

		float randomFinalAngle = finalArray[Random.Range(0, finalArray.Length)];


		// Here we set up how many circles our wheel should rotate before stop
		_finalAngle = (fullCircles * 360 + randomFinalAngle);

		UIManager.Instance.spinIsStarted = true;

		StartCoroutine(Delay(5f));
    }

	private float[] RandomChooseByLevel()
	{
		int randomValue = Random.Range(1, 11);

		if(level <= 10)
		{
			if(randomValue <= 7)
			{
				return new float[] {20, 60, 100, 140, 180, 220};
			}
			else if(randomValue == 8)
			{
				return new float[] {260, 300};
			}
			else
			{
				return new float[] {340};
			}
		}
		else if(level <= 30)
		{
			if(randomValue <= 6)
			{
				return new float[] {20, 60, 100, 140, 180};
			}
			else if(randomValue <= 8)
			{
				return new float[] {220, 260};
			}
			else if(randomValue == 9)
			{
				return new float[] {300};
			}
			else
			{
				return new float[] {340};
			}
		}
		else if(level <= 100)
		{
			if(randomValue <= 5)
			{
				return new float[] {60, 140, 220, 300};
			}
			else if(randomValue <= 7)
			{
				return new float[] {20, 100};
			}
			else if(randomValue <= 9)
			{
				return new float[] {180, 260};
			}
			else
			{
				return new float[] {340};
			}
		}
		else if(level <= 200)
		{
			if(randomValue <= 5)
			{
				return new float[] {20, 100, 180};
			}
			else if(randomValue <= 7)
			{
				return new float[] {60, 140};
			}
			else if(randomValue == 8)
			{
				return new float[] {220, 300};
			}
			else
			{
				return new float[] {260, 340};
			}
		}
		else
		{
			if(randomValue <= 4)
			{
				return new float[] {20, 100, 180};
			}
			else if(randomValue <= 7)
			{
				return new float[] {60, 140, 220};
			}
			else if(randomValue == 8)
			{
				return new float[] {300};
			}
			else
			{
				return new float[] {260, 340};
			}
		}
	}

    void Update()
    {
		if(UIManager.Instance.spinRewardAppear)
		{
			UIManager.Instance.OpenSpinRewardPanel();
			// SpinReward.Instance.SetSpinReward();
		}

        // Make turn button non interactable if user has not enough money for the turn
    	if (UIManager.Instance.spinIsStarted)
		{
    	    TurnButton.interactable = false;
    	}
		else
		{
    	    TurnButton.interactable = true;
    	}

    	float maxLerpRotationTime = 4f;
    
    	// increment timer once per frame
    	_currentLerpRotationTime += Time.deltaTime;
    	if (_currentLerpRotationTime > maxLerpRotationTime || Circle.transform.eulerAngles.z == _finalAngle) {
    	    _currentLerpRotationTime = maxLerpRotationTime;
    	    // _isStarted = false;
    	    _startAngle = _finalAngle % 360;
    
    	    // GiveAwardByAngle ();
    	}
    
    	// Calculate current position using linear interpolation
    	float t = _currentLerpRotationTime / maxLerpRotationTime;
    
    	// This formulae allows to speed up at start and speed down at the end of rotation.
    	// Try to change this values to customize the speed
    	t = t * t * t * (t * (6f * t - 15f) + 10f);
    
    	float angle = Mathf.Lerp (_startAngle, _finalAngle, t);
		//Debug.Log(_startAngle);
		//Debug.Log(_finalAngle);
    	Circle.transform.eulerAngles = new Vector3 (0, 0, angle);
    }

	private IEnumerator Delay(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);

		UIManager.Instance.spinRewardAppear = true;
	}

	private IEnumerator SpinBlinkFast()
	{
		yield return new WaitForSeconds(0.5f);

		LightManager.Instance.blinkTime = 0.08f;

		yield return new WaitForSeconds(3f);

		LightManager.Instance.blinkTime = 0.2f;
	}

	private void RandomBooster()
	{
		randomBooster1 = Random.Range(2, 6);
		do
		{
			randomBooster2 = Random.Range(2, 6);
		} while (randomBooster2 == randomBooster1);
	}
}