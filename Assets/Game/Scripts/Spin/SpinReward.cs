﻿using UnityEngine;
using UnityEngine.UI;

public class SpinReward : MonoBehaviour
{
    public static SpinReward instance;
    public static SpinReward Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<SpinReward>(); return instance; } }

    public Text number;
    public Text reward;

    public GameObject rewardImage;

    public Sprite[] rewardImageItems = new Sprite[7];

    private void Update()
    {
        SetSpinReward();
        // return;
    }

    public void SetSpinReward()
    {
        StopCoroutine("Blink");
        number.text = "+" + Arrow.Instance.itemNumber.ToString();
        reward.text = Arrow.Instance.itemName.ToString();

        if(Arrow.Instance.itemName.Equals("Heart"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[0];
            reward.text = "Heart".ToString();
        }
        else if(Arrow.Instance.itemName.Equals("Gem"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[1];
            reward.text = "Gem".ToString();
        }
        else if(Arrow.Instance.itemName.Equals("Booster1"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[2];
            reward.text = "Time Booster".ToString();
        }
        else if(Arrow.Instance.itemName.Equals("Booster2"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[3];
            reward.text = "Hammer Booster".ToString();
        }
        else if(Arrow.Instance.itemName.Equals("Booster3"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[4];
            reward.text = "Mix Booster".ToString();
        }
        else if(Arrow.Instance.itemName.Equals("Booster4"))
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[5];
            reward.text = "Hint Booster".ToString();
        }
        else
        {
            rewardImage.GetComponent<SpriteRenderer>().sprite = rewardImageItems[6];
            reward.text = "Ticket".ToString();
        }
    }
}