﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AffectNode : MonoBehaviour
{
    public int index;
    public GameObject clear;
    public abstract void Attach();
    public abstract IEnumerator Check();

    public abstract void SetIndex(int index);

    public abstract void Move(Transform target);

    public abstract void Getkey();
}
