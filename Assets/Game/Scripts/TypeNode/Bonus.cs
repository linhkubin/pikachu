﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : AffectNode
{
    public GameObject anim;

    private Node m_Node;
    private Node GetNode()
    {
        return m_Node;
    }

    private int orderLayer;

    private IEnumerator Init()
    {
        yield return new WaitForSeconds(0.3f);
        GetNode().I_id = Constant.ID_TYPE_NODE_BONUS;
        GetNode().I_numberNode = 100;
        GetNode().B_lockClick = false;
        
        //Debug.Log(GetNode().currentOrder);
        //gameObject.transform.parent.GetComponent<AnimNode>().SetLayer(100);

    }

    private IEnumerator Disappear()
    {

        int timer = 0;
        while (gameObject.activeInHierarchy && timer < 10)
        {
            yield return new WaitForSeconds(1f);
            if (!TimeManager.Instance.b_pause)
            {
                timer++;
            }
            if (TimeManager.Instance.b_gameOver)
            {
                SimplePool.Despawn(gameObject);
            }
        }

        if (gameObject.activeInHierarchy)
        {
            GetNode().GetAnimNode().SetIdle();
            yield return new WaitForSeconds(0.3f);
            GetNode().RemoveNode();
        }
        
    }

    public override void Attach()
    {

    }

    public override IEnumerator Check()
    {
        yield return null;
    }

    public override void SetIndex(int index)
    {
        //Debug.Log(index);
        m_Node = GameManager.Instance.m_Node[index];
        GetNode().SetAnim(100);
        GetNode().GetAnimNode().SetLayer(GetNode().GetCurrentOrder);
        StartCoroutine(Init());
        StartCoroutine(Disappear());
    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}
