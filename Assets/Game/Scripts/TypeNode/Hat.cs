﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : AffectNode
{
    private List<Node> list;

    private Node parentNode;

    private int takeOfHat = 2;

    public Animator anim;

    public bool haveStar = false;

    public override IEnumerator Check()
    {

        //Debug.Log("check");

        if (list[index + 1].Connecting)
        {
            TakeOfHat();
        }
        if (list[index - 1].Connecting)
        {
            TakeOfHat();
        }
        if (list[index + Constant.NUMBER_ROW].Connecting)
        {
            TakeOfHat();
        }
        if (list[index - Constant.NUMBER_ROW].Connecting)
        {
            TakeOfHat();
        }

        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }

    }

    private void TakeOfHat()
    {
        takeOfHat--;

        if (takeOfHat == 1)
        {
            anim.SetTrigger("open1");

        }
        else if (takeOfHat <= 0)
        {
            anim.SetTrigger("open2");
            StopAllCoroutines();
            StartCoroutine(Disappear());
        }

    }

    //public void CheckStar()
    //{
    //    if (haveStar)
    //    {
    //        UpStar();
    //    }
    //}

    private IEnumerator Disappear()
    {
        yield return new WaitForSeconds(1.6f);
        parentNode.RemoveNode();

    }

    //private void UpStar()
    //{
        
    //    GameObject go = SimplePool.Spawn("StarTime", gameObject.transform.position, Quaternion.identity);
    //    go.GetComponent<StarTest>().SetTargetUpStar(TimeManager.Instance.target, "Connected", 2f, 0.2f, true, ++ModeFindStar.Instance.upStar);
    //}

    public override void Attach()
    {

        StopAllCoroutines();


        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void SetIndex(int index)
    {
        takeOfHat = 2;

        anim.ResetTrigger("open1");

        anim.ResetTrigger("open2");

        list = GameManager.Instance.m_Node;

        parentNode = list[index];

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        parentNode.B_lockClick = true;

        int inOrder = (1 + 10 * (index / Constant.NUMBER_ROW)); ;

        anim.GetComponent<MeshRenderer>().sortingOrder = inOrder;

        //gameObject.transform.position = list[index].transform.position;
    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}

