﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarInHat : MonoBehaviour
{
    [SerializeField]
    private Hat hat;
    
    public void CheckStar()
    {
        if (hat.haveStar)
        {
            UpStar();
        }
    }

    private void UpStar()
    {

        GameObject go = SimplePool.Spawn("StarTime", gameObject.transform.position, Quaternion.identity);
        go.GetComponent<StarTest>().SetTargetUpStar(TimeManager.Instance.target, "Connected", 2f, 0.2f, true, ModeFindStar.Instance.upStar++);
    }
}
