﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flood : AffectNode
{
    private List<GameObject> list;

    //check xung quanh o nao dinh kem
    private List<int> listAttach = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

        StartCoroutine(UnFlood());

        GameManager.Instance.l_lock.Add(gameObject);

        gameObject.transform.parent.GetComponent<Node>().B_lockClick = true;

        index = gameObject.transform.parent.GetComponent<Node>().i_index;

        list = GameManager.Instance.L_listNode;

        Attach();

    }

    public IEnumerator UnFlood()
    {
        yield return new WaitForSeconds(12);
        GameManager.Instance.l_lock.RemoveAt(GameManager.Instance.l_lock.IndexOf(gameObject));
        gameObject.transform.parent.GetComponent<Node>().RemoveFace();
    }

    public override void Attach()
    {
        listAttach.Clear();

        StopCoroutine(Check());

        if (list[index + 1].GetComponent<Node>().I_numberNode > 0)
        {
            listAttach.Add(index + 1);
        }
        if (list[index - 1].GetComponent<Node>().I_numberNode > 0)
        {
            listAttach.Add(index - 1);
        }
        if (list[index + Constant.NUMBER_ROW].GetComponent<Node>().I_numberNode > 0)
        {
            listAttach.Add(index + Constant.NUMBER_ROW);
        }
        if (list[index - Constant.NUMBER_ROW].GetComponent<Node>().I_numberNode > 0)
        {
            listAttach.Add(index - Constant.NUMBER_ROW);
        }

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }

    }

    public override IEnumerator Check()
    {
        if (gameObject.transform.parent.GetComponent<Node>().i_index != index)
        {
            index = gameObject.transform.parent.GetComponent<Node>().i_index;

            Attach();
        }
        yield return new WaitForSeconds(0.05f);

        for (int i = 0; i < listAttach.Count; i++)
        {
            if (list[listAttach[i]].GetComponent<Node>().Connecting)
            {
                gameObject.transform.parent.GetComponent<Node>().RemoveFace();
            }
        }

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void SetIndex(int index)
    {

    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}
