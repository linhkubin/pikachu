﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : AffectNode
{
    private List<Node> list;

    private Node parentNode;

    private float f_timeAction;

    public override void Attach()
    {
        //StopAllCoroutines();

        //if (gameObject.activeInHierarchy)
        //{
        //    StartCoroutine(Check());
        //    StartCoroutine(UnCloud());
        //}
    }

    public override IEnumerator Check()
    {
        if (list[index + 1].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index - 1].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index + Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index - Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveFace();
        }

        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void Getkey()
    {

    }

    public override void Move(Transform target)
    {

    }

    public override void SetIndex(int index)
    {
        list = GameManager.Instance.m_Node;

        parentNode = list[index];

        parentNode.SetSpeedAnim(0);

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        list[index].B_lockClick = true;

        gameObject.transform.position = list[index].transform.position;

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
            StartCoroutine(UnCloud());
        }
    }

    private IEnumerator UnCloud()
    {
        int timer = 0;
        while (gameObject.activeInHierarchy && timer < 10)
        {
            yield return new WaitForSeconds(1f);
            if (!TimeManager.Instance.b_pause)
            {
                timer++;
            }

        }

        parentNode.RemoveFace();
    }
}
