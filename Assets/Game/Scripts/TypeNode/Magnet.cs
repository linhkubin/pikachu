﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : AffectNode
{
    private Node parentNode;

    public Animator anim;

    //-------hut xung quanh-------
    public void GravityFall(int point)
    {
        //Debug.Log("magnet " + point);

        int index = point;
        //hut xung quanh
        //index = point + Constant.NUMBER_ROW + 1; //1
        //Gravite3Point(index, index + 1, index + Constant.NUMBER_ROW + 1, index + Constant.NUMBER_ROW);

        index = point + Constant.NUMBER_ROW;//2
        Gravite3Point(index, index + Constant.NUMBER_ROW);//+ 1, index + Constant.NUMBER_ROW, index + Constant.NUMBER_ROW - 1);

        //index = point + Constant.NUMBER_ROW - 1;//3
        //Gravite3Point(index, index + Constant.NUMBER_ROW, index + Constant.NUMBER_ROW - 1, index - 1);

        index = point - 1;//4
        Gravite3Point(index, index - 1);// + Constant.NUMBER_ROW - 1, index - 1, index - Constant.NUMBER_ROW - 1);

        //index = point - Constant.NUMBER_ROW - 1;//5
        //Gravite3Point(index, index - 1, index - Constant.NUMBER_ROW - 1, index - Constant.NUMBER_ROW);

        index = point - Constant.NUMBER_ROW;//6
        Gravite3Point(index, index - Constant.NUMBER_ROW);// + 1, index - Constant.NUMBER_ROW, index - Constant.NUMBER_ROW - 1);

        //index = point - Constant.NUMBER_ROW + 1;//7
        //Gravite3Point(index, index + 1, index - Constant.NUMBER_ROW + 1, index - Constant.NUMBER_ROW);

        index = point + 1;//8
        Gravite3Point(index, index + 1);// - Constant.NUMBER_ROW , index + 1, index + Constant.NUMBER_ROW + 1);
    }
   
    private void Gravite3Point(int index, int point1)//, int point2, int point3)
    {
        List<GameObject> list = GameManager.Instance.L_listNode;
        if (list[index].GetComponent<Node>().IsNodeEmpty())
        {
            if (list[point1].GetComponent<Node>().I_id == Constant.ID_TYPE_NODE && list[point1].GetComponent<Node>().I_numberNode > 0)
            {
                anim.SetTrigger("impact");
                GameManager.Instance.SwapNode(index, point1);
            }
        }
    }

    public override IEnumerator Check()
    {
        //Debug.Log("magnet");

        GravityFall(index);

        yield return new WaitForSeconds(0.3f);

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void Attach()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void SetIndex(int index)
    {
        parentNode = GameManager.Instance.m_Node[index];

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        //int inOrder = (8 + 10 * (index / Constant.NUMBER_ROW)) > 9 ? (8 + 10 * (index / Constant.NUMBER_ROW)) : 9;

        //anim.GetComponent<SkeletonMecanim>(). = inOrder;

        gameObject.transform.position = parentNode.transform.position;

    }

    public override void Move(Transform target)
    {
        bool stopMove = false;

        while (!stopMove)
        {
            Vector2 direction = target.position - gameObject.transform.position;
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.position, Time.deltaTime * 4f);
            stopMove = direction.sqrMagnitude < 0.01f;
        }

        index = parentNode.i_index;

    }

    public override void Getkey()
    {

    }
}
