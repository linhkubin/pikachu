﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nothing : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
        gameObject.transform.parent.GetComponent<SpriteRenderer>().color = Color.clear;
    }

}
