﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : AffectNode
{
    private int i_timeDuration;
    private float f_timeBoom = 35;

    Node parentNode;

    private IEnumerator CoolDownTime()
    {
        while (i_timeDuration > -0.1)
        {

            if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_pause)
            {
                CombatTextManager.Instance.CreateText(transform.position, i_timeDuration.ToString(), Color.red);
                i_timeDuration--;
            }
            yield return new WaitForSeconds(1f);

        }

        TimeManager.Instance.SubtractTime(f_timeBoom);
        parentNode.RemoveFace();

    }

    public override void Attach()
    {

    }

    public override IEnumerator Check()
    {
        yield return null;
    }

    public override void SetIndex(int index)
    {
        i_timeDuration = 30;

        parentNode = GameManager.Instance.m_Node[index];

        this.index = index;

        int inOrder = (1 + 10 * (index / Constant.NUMBER_ROW)) > 9 ? (1 + 10 * (index / Constant.NUMBER_ROW)) : 9;

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = inOrder;

        StartCoroutine(CoolDownTime());

    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}

