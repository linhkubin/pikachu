﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : AffectNode
{

    public GameObject g_key;

    private List<Node> list;
    //check xung quanh o nao dinh kem
    private Node parentNode;

    int key = 0;

    bool inited = false;

    public override IEnumerator Check()
    {

        yield return new WaitForSeconds(0.3f);

        //Debug.Log(listAttach.Count);

        if (key == 0)
        {
            parentNode.RemoveFace();
            StopAllCoroutines();
        }

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }


    public override void Attach()
    {
        if (!inited)
        {
            inited = true;

            StopAllCoroutines();

            if (list[index + 1].I_id == Constant.ID_TYPE_NODE && list[index + 1].I_numberNode > 0)
            {
                list[index + 1].SetFace(g_key);
                list[index + 1].I_id = Constant.ID_KEY;
                list[index + 1].g_faceId.GetComponent<Key>().SetParent(gameObject);
                key++;
                //listAttach.Add(index + 1);
            }
            if (list[index - 1].I_id == Constant.ID_TYPE_NODE && list[index - 1].I_numberNode > 0)
            {
                list[index - 1].SetFace(g_key);
                list[index - 1].I_id = Constant.ID_KEY;
                list[index - 1].g_faceId.GetComponent<Key>().SetParent(gameObject);
                key++;
                //listAttach.Add(index - 1);
            }

            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(Check());
            }
        }
    }

    public override void SetIndex(int index)
    {
        list = GameManager.Instance.m_Node;

        parentNode = list[index];

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        list[index].B_lockClick = true;

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = (1 + 10 * (index / Constant.NUMBER_ROW));

        gameObject.transform.position = parentNode.transform.position;

        parentNode.SetSpeedAnim(0);

        //--------------
        inited = false;
        key = 0;

    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

        key--;
    }
}
