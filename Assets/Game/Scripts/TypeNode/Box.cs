﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : AffectNode
{
    private List<Node> list;
    //check xung quanh o nao dinh kem
    private Node parentNode;


    public override IEnumerator Check()
    {

        if (index + 1 < Constant.TOTAL_ITEM_NUMBER && list[index + 1].Connecting)
        {
            parentNode.RemoveNode();
        }
        if (index - 1 > 0 && list[index - 1].Connecting)
        {
            parentNode.RemoveNode();
        }
        if (index + Constant.NUMBER_ROW < Constant.TOTAL_ITEM_NUMBER && list[index + Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveNode();
        }
        if (index - Constant.NUMBER_ROW > 0 && list[index - Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveNode();
        }

        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void Attach()
    {
        //Debug.Log("box");

        StopAllCoroutines();

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
        
    }

    public override void SetIndex(int index)
    {
        list = GameManager.Instance.m_Node;

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        parentNode = list[index];

        list[index].B_lockClick = true;

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = (1 + 10 * (index / Constant.NUMBER_ROW));

        gameObject.transform.position = list[index].transform.position;

    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}
