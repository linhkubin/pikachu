﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour
{
    private float f_timeAction;

    // Start is called before the first frame update
    void Start()
    {
        f_timeAction = UnityEngine.Random.Range(10, 15);
        StartCoroutine(UnHideNode());
    }

    private IEnumerator UnHideNode()
    {
        yield return new WaitForSeconds(f_timeAction);
        gameObject.transform.parent.GetComponent<Node>().RemoveFace();
    }
}
