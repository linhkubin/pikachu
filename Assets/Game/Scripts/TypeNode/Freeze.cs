﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freeze : AffectNode
{
    private List<Node> list;

    private Node parentNode;
   

    public override IEnumerator Check()
    {
        if (list[index + 1].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index - 1].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index + Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveFace();
        }
        if (list[index - Constant.NUMBER_ROW].Connecting)
        {
            parentNode.RemoveFace();
        }

        yield return new WaitForSeconds(0.1f);

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }

    }

    public override void Attach()
    {
        StopAllCoroutines();

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Check());
        }
    }

    public override void SetIndex(int index)
    {
        list = GameManager.Instance.m_Node;

        parentNode = list[index];

        parentNode.SetSpeedAnim(0);

        GameManager.Instance.l_lock.Add(gameObject);

        this.index = index;

        list[index].B_lockClick = true;

        int inOrder = (1 + 10 * (index / Constant.NUMBER_ROW)) > 9 ? (1 + 10 * (index / Constant.NUMBER_ROW)) : 9;

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = inOrder;

        gameObject.transform.position = list[index].transform.position;
    }

    public override void Move(Transform target)
    {

    }

    public override void Getkey()
    {

    }
}
