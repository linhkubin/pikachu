﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{

    AffectNode parent;
    
    public void GetKey()
    {
        parent.Getkey();
    }

    public void SetParent(GameObject game)
    {
        parent = game.GetComponent<AffectNode>();
        transform.position = new Vector3(transform.position.x - 0.15f, transform.position.y + 0.15f, 0);
    }

    public void Despawn()
    {
        StartCoroutine(Disappear());
    }

    private IEnumerator Disappear()
    {
        GetComponent<Animator>().SetTrigger("disappear");

        yield return new WaitForSeconds(0.5f);

        GetKey();

        SimplePool.Despawn(gameObject);
    }
}
