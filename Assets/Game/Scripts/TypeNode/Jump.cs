﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    private int index;

    private List<GameObject> list;

    // Start is called before the first frame update
    void Start()
    {
        list = GameManager.Instance.L_listNode;
        //gameObject.transform.parent.GetComponent<SpriteRenderer>().sortingOrder = 9;
        StartCoroutine(Jumping());
    }

    public IEnumerator Jumping()
    {
        
        int ranTime = Random.Range(4,8);

        yield return new WaitForSeconds(ranTime);

        index = gameObject.transform.parent.GetComponent<Node>().i_index;

        //tra ve vi tri random moi

        if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_gameOver)
        {
            List<GameObject> list = GameManager.Instance.L_listNode;

            int ran = UnityEngine.Random.Range(Constant.NUMBER_ROW + 1, Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW);

            while (!list[ran].GetComponent<Node>().IsNodeEmpty())
            {
                ran--;
                if (ran <= Constant.NUMBER_ROW)
                {
                    ran = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW;
                }
            }

            GameManager.Instance.SwapNode(index,ran);

            GameManager.Instance.RenewSuggest();
        }

        StartCoroutine(Jumping()); 
        
    }
}
