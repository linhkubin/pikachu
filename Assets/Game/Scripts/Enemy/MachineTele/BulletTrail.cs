﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTrail : MonoBehaviour
{
    private IEnumerator IEMoveTarget(Transform tfMove, Vector3 target, string effect, float speed, float delay, bool randomDirection = true, int nodeFrom = 0, int nodeTo = 0,int numberNode = -1)
    {

        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }
        var stopMove = false;
        var speedMove = 0f;

        if (randomDirection)
        {
            Vector3 targetDir = target - tfMove.position;
            float angle = Vector3.Angle(targetDir, Vector3.up);
            if (Vector3.Angle(targetDir, Vector3.right) < 90)
            {
                angle = Vector3.Angle(targetDir, Vector3.down) + 180f;
            }

            angle = Random.Range(angle - 45f, angle + 45f);

            tfMove.eulerAngles = new Vector3(0, 0, angle);
        }

        Quaternion to;

        while (tfMove && !stopMove && !TimeManager.Instance.b_gameOver)
        {
            Vector2 direction = target - tfMove.position;
            to = Quaternion.FromToRotation(Vector3.up, direction);
            tfMove.Translate(speed * speedMove * tfMove.up, Space.World);
            tfMove.rotation = Quaternion.Lerp(tfMove.rotation, to, 3 * speedMove * speed);
            speedMove += Time.deltaTime / 5;
            stopMove = direction.sqrMagnitude < speedMove * 5;
            yield return null;
        }
        tfMove.transform.position = target;

        if (effect != null && !TimeManager.Instance.b_gameOver)
        {
            GameObject go = SimplePool.Spawn(effect, gameObject.transform.position, Quaternion.identity);
            go.GetComponent<DespawnHole>().SetTimeMachineTeleAppear(nodeFrom, nodeTo, numberNode);
            yield return new WaitForSeconds(.3f);
            SimplePool.Despawn(gameObject);

        }
        else
        {
            yield return new WaitForSeconds(.3f);

            SimplePool.Despawn(gameObject);
        }


    }

    public void SetTarget(Transform target, string effect, float speed = 2f, float delay = 0.2f, bool randomDirection = true, int nodeFrom = 0, int nodeTo = 0, int numberNode = -1)
    {
        StartCoroutine(IEMoveTarget(gameObject.transform, target.position, effect, speed, delay, randomDirection, nodeFrom, nodeTo, numberNode));
    }
}
