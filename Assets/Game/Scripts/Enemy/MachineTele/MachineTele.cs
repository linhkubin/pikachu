﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineTele : Enemy
{

    List<Node> list;

    int minTimeCreate;
    int maxTimeCreate;

    int minCouple;
    int maxCouple;

    //public Transform posSpawn;

    public GameObject magicBall;
    public Transform tfMagicBall;

    // Start is called before the first frame update
    void Start()
    {
        list = GameManager.Instance.m_Node;
    }

    public override void SetPosition(int position)
    {
        I_index = position;

        transform.position = GameManager.Instance.L_position[I_index].transform.position;

        StartCoroutine(SetBarrier());

        timer = 0;

        magicBall.SetActive(false);

    }

    private IEnumerator SetBarrier()
    {
        yield return new WaitForSeconds(0.1f);

        list[I_index].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index + 1].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index - Constant.NUMBER_ROW].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index - Constant.NUMBER_ROW + 1].I_id = Constant.ID_TYPE_BARRIER;

        GameManager.Instance.L_spritePosition[I_index].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index + 1].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index - Constant.NUMBER_ROW + 1].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index - Constant.NUMBER_ROW].color = Color.clear;
    }

    private IEnumerator StartSpawn()
    {
        int ran = Random.Range(1, 3);
        //Debug.Log("ran : " + ran);


        yield return new WaitForSeconds(6f);

        if (!TimeManager.Instance.b_gameStart || TimeManager.Instance.b_pause)
        {
            StartCoroutine(StartSpawn());
        }
        else
        if (!TimeManager.Instance.b_gameOver)
        {

            StartCoroutine(Spawn(ran));

            StartCoroutine(StartSpawn());
        }

    }

    private void Update()
    {
        if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_pause && !TimeManager.Instance.b_gameOver)
        {
            timer += Time.deltaTime;
            if (timer > 6f)
            {
                timer = 0;
                StartCoroutine(Spawn(Random.Range(1, 3)));
            }
        }
    }

    private IEnumerator Spawn(int number)
    {

        for (int i = 0; i < number; i++)
        {
            if (!TimeManager.Instance.b_gameOver)
            {
                StartCoroutine(TeleRun());
                yield return new WaitForSeconds(0.1f);
            }
        }

    }

    private IEnumerator TeleRun()
    {
        magicBall.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        Run();
    }

    private void Run()
    {
        if (TimeManager.Instance.b_gameOver)
        {
            return;
        }

        int ran1 = Random.Range(Constant.NUMBER_ROW + 1, Constant.TOTAL_ITEM_NUMBER - 1);

        int check = ran1;

        while (list[ran1].I_id != Constant.ID_TYPE_NODE || list[ran1].I_numberNode <= 0 || list[ran1].B_lockClick)
        {
            ran1--;

            if (check == ran1)
            {
                Debug.Log("null 1");
                return;
            }

            if (ran1 <= Constant.NUMBER_ROW)
            {
                ran1 = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW;
            }
        }
        //L_listNode[ran].GetComponent<Node>().I_id = id;

        int ran2 = Random.Range(Constant.NUMBER_ROW + 1, Constant.TOTAL_ITEM_NUMBER);

        check = ran2;

        while (!list[ran2].IsNodeEmpty())
        {
            ran2++;

            if (check == ran2)
            {
                Debug.Log("null 2");
                return;
            }

            if (ran2 >= Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW)
            {
                ran2 = Constant.NUMBER_ROW;
            }
        }

        if (ran1 == GameManager.Instance.i_suggest1 || ran1 == GameManager.Instance.i_suggest2)
        {
            return;
        }


        if (!TimeManager.Instance.b_gameOver && !TimeManager.Instance.b_pause)
        {
            list[ran2].I_id = Constant.ID_TYPE_NODE_WATER;
            //list[ran2].I_numberNode = list[ran1].I_numberNode;
            list[ran1].B_lockClick = true;

            StartCoroutine(ApearBullet(ran1, ran2, list[ran1].I_numberNode));
        }

    }

    private IEnumerator ApearBullet(int nodeFrom,int nodeTo,int numberNode)
    {
        list[nodeFrom].B_lockClick = true;

        GameObject go = SimplePool.Spawn("BulletTrail", tfMagicBall.position, Quaternion.identity);
        go.GetComponent<BulletTrail>().SetTarget(list[nodeFrom].GetTransform(), "TeleHoleBegin", 4f, 0.3f, true, nodeFrom, nodeTo, numberNode);
        
        yield return new WaitForSeconds(1.4f);

        magicBall.SetActive(false);

    }


}
