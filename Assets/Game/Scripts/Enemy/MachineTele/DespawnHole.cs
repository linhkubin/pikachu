﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnHole : MonoBehaviour
{
    public Animator anim;

    List<Node> list;

    public void SetTimeDespawn(float time = 100)
    {
        StartCoroutine(SetTime(time));
    }

    private void Start()
    {
        list = GameManager.Instance.m_Node;
    }

    private IEnumerator SetTime(float time)
    {
        int timer = 0;
        while (!TimeManager.Instance.b_gameOver)
        {
            yield return new WaitForSeconds(1f);
            if (!TimeManager.Instance.b_pause)
            {
                timer++;
                if (timer > time)
                {
                    SimplePool.Despawn(gameObject);
                }
            }
        }

        SimplePool.Despawn(gameObject);
    }


    #region Appear
    public void SetTimeMachineTeleAppear(int nodeFrom, int nodeTo,int numberNode)
    {
        StartCoroutine(SetTimeAutoAppear(nodeFrom, nodeTo, numberNode));
    }

    private IEnumerator SetTimeAutoAppear(int nodeFrom, int nodeTo, int numberNode)
    {

        yield return new WaitForSeconds(0.3f);
        anim.SetTrigger("gravity");

        //int tempNode = list[nodeFrom].I_numberNode;

        if (list[nodeFrom].GetAnimNode() != null)
        {
            list[nodeFrom].SetSpeedAnim(1);
            list[nodeFrom].SetAnim("select");
            yield return new WaitForSeconds(0.56f);
            list[nodeFrom].SetAnim("idle");
            yield return new WaitForSeconds(0.1f);
            list[nodeFrom].ResetState();
        }

        GameObject go = SimplePool.Spawn("TeleHoleEnd", list[nodeTo].GetTransform().position, Quaternion.identity);
        go.GetComponent<DespawnHole>().SetTimeMachineTeleDisappear(nodeFrom, nodeTo, numberNode);

        anim.SetTrigger("destroy");

        yield return new WaitForSeconds(1f);
        SimplePool.Despawn(gameObject);
    }
    #endregion


    #region Disappear
    public void SetTimeMachineTeleDisappear(int nodeFrom, int nodeTo,int numberNode)
    {
        StartCoroutine(SetTimeAutoDisappear(nodeFrom, nodeTo, numberNode));
    }

    private IEnumerator SetTimeAutoDisappear(int nodeFrom, int nodeTo,int numberNode)
    {
        yield return new WaitForSeconds(0.3f);
        anim.SetTrigger("gravity");

        yield return new WaitForSeconds(0.25f);

        list[nodeTo].NewNode(numberNode);

        if (list[nodeTo].GetAnimNode() != null)
        {
            list[nodeTo].SetSpeedAnim(1);
            list[nodeTo].SetAnim("select");
            yield return new WaitForSeconds(0.56f);
            list[nodeTo].SetAnim("idle");
        }

        anim.SetTrigger("destroy");
        
        yield return new WaitForSeconds(1f);
        SimplePool.Despawn(gameObject);
    }
    #endregion

}
