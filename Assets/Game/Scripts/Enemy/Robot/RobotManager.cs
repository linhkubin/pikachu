﻿using System.Collections;
using UnityEngine;

public class RobotManager : MonoBehaviour
{
    private Transform tf;
    private float penaltyTime = 5f;
    private float waitingTimeJump = 4f;

    private void Start()
    {
        tf = GetComponent<Transform>();

        StartCoroutine("Jump");
    }

    private IEnumerator Jump()
    {
        yield return new WaitForSeconds(0.0000001f);

        tf.position = GameManager.Instance.L_listNode[8].transform.position;

        while(true)
        {
            int randomPos;

            do
            {
                randomPos = Random.Range(0, Constant.TOTAL_ITEM_NUMBER);
            } 
            while(GameManager.Instance.L_listNode[randomPos].GetComponent<Node>().I_id != Constant.ID_TYPE_ENVIRONMENT &&
            !GameManager.Instance.L_listNode[randomPos].GetComponent<Node>().IsNodeEmpty());
            
            tf.position = GameManager.Instance.L_listNode[randomPos].transform.position;
            yield return new WaitForSeconds(waitingTimeJump);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Line")
        {
            Debug.Log("Line");
            TimeManager.Instance.SubtractTime(penaltyTime);
        }
    }
}
