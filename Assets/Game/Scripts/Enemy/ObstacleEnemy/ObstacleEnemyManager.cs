﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleEnemyManager : MonoBehaviour
{
    public GameObject ObstacleEnemy;
    private List<GameObject> ObstacleEnemyList;
    private List<GameObject> nodes;

    private int index1, index2, index3;

    private void Start()
    {
        index1 = 32;
        index2 = 33;
        index3 = 34;
        ObstacleEnemyList = new List<GameObject>();
        nodes = new List<GameObject>();
        
        ObstacleEnemyList.Add(Instantiate(ObstacleEnemy) as GameObject);
        ObstacleEnemyList.Add(Instantiate(ObstacleEnemy) as GameObject);
        ObstacleEnemyList.Add(Instantiate(ObstacleEnemy) as GameObject);

        ObstacleEnemyList[0].transform.position = GameManager.Instance.L_listNode[index1].transform.position;
        ObstacleEnemyList[1].transform.position = GameManager.Instance.L_listNode[index2].transform.position;
        ObstacleEnemyList[2].transform.position = GameManager.Instance.L_listNode[index3].transform.position;

        nodes.Add(GameManager.Instance.L_listNode[index1] as GameObject);
        nodes.Add(GameManager.Instance.L_listNode[index2] as GameObject);
        nodes.Add(GameManager.Instance.L_listNode[index3] as GameObject);

        StartCoroutine("Blink");
    }

    private IEnumerator Blink()
    {
        while(true)
        {
            BlockNode();

            yield return new WaitForSeconds(3f);

            UnblockNode();

            yield return new WaitForSeconds(3f);
        }
    }

    private void BlockNode()
    {
        for(int i = 0; i < nodes.Count; i++)
        {
            nodes[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
            //nodes[i].GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.GetComponent<SpriteManager>().s_nothing.GetComponent<SpriteRenderer>().sprite;
            ObstacleEnemyList[i].SetActive(true);
        }
    }

    private void UnblockNode()
    {
        for(int i = 0; i < nodes.Count; i++)
        {
            nodes[i].GetComponent<Node>().RemoveNode();
            ObstacleEnemyList[i].SetActive(false);
        }
    }
}