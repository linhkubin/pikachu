﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotMini : Enemy
{

    List<Node> list;

    private List<PointLand> listPointLand = new List<PointLand>();

    private int NumberStep = 4;

    private float speed = 2f;

    public Transform TargetPosition { get; set; }


    public Transform tf_spawnBullet;


    // Start is called before the first frame update
    void Start()
    {
        list = GameManager.Instance.m_Node;
        anim = down.GetComponent<Animator>();
        //SetLayer(currentOrder(I_index));
    }

    // Update is called once per frame
    void Update()
    {
        if (!TimeManager.Instance.b_gameOver && !TimeManager.Instance.b_pause)
        {
            timer += Time.deltaTime;

            if (timer > timeAction)
            {
                timer = 0;

                CreateMap(I_index);
                RunOnMap();
            }

            transform.position = Vector3.MoveTowards(transform.position, TargetPosition.transform.position, speed * Time.deltaTime);
        }
    }

    public override void SetPosition(int position)
    {
        ResetState();

        i_pathIndex = I_index = position;

        transform.position = GameManager.Instance.L_position[I_index].transform.position;

        TargetPosition = gameObject.transform;

    }


    private void CreateMap(int point)
    {
        NumberStep = Random.Range(2, 5);
        int index = 0;
        PointLand pointLand = new PointLand(point, index);

        listPointLand.Add(pointLand);

        index++;

        CheckIndStep(listPointLand[0], point, index);

        do
        {
            for (int i = 0; i < listPointLand.Count; i++)
            {
                if (listPointLand[i].Step == index)
                {
                    CheckIndStep(listPointLand[i], listPointLand[i].Point, index + 1);
                }
            }
            index++;
        }
        while (index < NumberStep);
        //PrintListLand();
    }

    //ktra xem xung quanh co the di hay k && danh dau
    private void CheckIndStep(PointLand parent, int point, int index)
    {
        //up
        if ((point >= Constant.NUMBER_ROW) && !IsInMap(point - Constant.NUMBER_ROW))
        {
            if (!IsLand(point - Constant.NUMBER_ROW))
            {
                PointLand pointLand = new PointLand(point - Constant.NUMBER_ROW, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point - Constant.NUMBER_ROW, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //down
        if ((point < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) && !IsInMap(point + Constant.NUMBER_ROW))
        {
            if (!IsLand(point + Constant.NUMBER_ROW))
            {
                PointLand pointLand = new PointLand(point + Constant.NUMBER_ROW, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point + Constant.NUMBER_ROW, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //right
        if ((point % Constant.NUMBER_ROW) != (Constant.NUMBER_ROW - 1) && !IsInMap(point + 1))
        {
            if (!IsLand(point + 1))
            {
                PointLand pointLand = new PointLand(point + 1, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point + 1, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //left
        if ((point % Constant.NUMBER_ROW) != 0 && !IsInMap(point - 1))
        {
            if (!IsLand(point - 1))
            {
                PointLand pointLand = new PointLand(point - 1, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point - 1, index, parent);
                listPointLand.Add(pointLand);
            }
        }
    }

    // check xem la node empty 
    private bool IsLand(int point)
    {
        if (list[point].I_numberNode < 0)
        {
            return false;
        }
        if (list[point].IsNodeEmpty() || list[point].I_id == Constant.ID_TYPE_ENVIRONMENT)
        {
            return true;
        }
        return false;
    }

    // check xem trong diction co node day chua
    private bool IsInMap(int point)
    {
        for (int i = 0; i < listPointLand.Count; i++)
        {
            if (listPointLand[i].Point == point)
            {
                return true;
            }
        }
        return false;
    }

    private void RunOnMap()
    {
        List<PointLand> pointLands = new List<PointLand>();
        List<int> listLand = new List<int>();

        for (int i = 0; i < listPointLand.Count; i++)
        {
            if (listPointLand[i].Step == NumberStep)
            {
                pointLands.Add(listPointLand[i]);
            }
        }

        //Debug.Log("pointLands.Count : " + pointLands.Count);

        if (pointLands.Count == 0)
        {
            ResetState();
            return;
        }

        if (pointLands.Count > 1)
        {
            for (int i = 0; i < pointLands.Count; i++)
            {
                if (pointLands[i].Point == i_pathIndex)
                {
                    pointLands.RemoveAt(i);
                    i_pathIndex = I_index;
                    break;
                }
            }
        }

        int ran = Random.Range(0, pointLands.Count);

        PointLand point;

        point = pointLands[ran];

        I_index = point.Point;

        while (point != null)
        {
            listLand.Add(point.Point);
            point = point.Parent;
        }

        StartCoroutine(Run(listLand));

    }

    private void PrintListLand()
    {
        for (int i = 0; i < listPointLand.Count; i++)
        {
            Debug.Log("point : " + listPointLand[i].Point + " ______ index : " + listPointLand[i].Step);
        }
    }

    private void ResetState()
    {
        AnimDirection(0);
        listPointLand.Clear();
    }

    private IEnumerator Run(List<int> listLand)
    {
        for (int i = listLand.Count - 1; i >= 0; i--)
        {
            if (immortaling)
            {
                listLand.Clear();
                break;
            }

            Direction(TargetPosition, GameManager.Instance.L_position[listLand[i]].transform);
            //gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2 + 10 * listLand[i] / Constant.NUMBER_ROW;

            TargetPosition = GameManager.Instance.L_position[listLand[i]].transform;

            yield return new WaitForSeconds(0.3f);

        }

        ResetState();

    }

    private void Direction(Transform target, Transform pos)
    {
        // right
        if (target.position.x > pos.position.x)
        {
            //return 1;
            AnimDirection(1);
            return;
        }

        //left
        if (target.position.x < pos.position.x)
        {
            //return -1;
            AnimDirection(-1);
            return;
        }

        //up
        if (target.position.y > pos.position.y)
        {
            //return -2;
            AnimDirection(-2);
            return;
        }

        //down
        if (target.position.y < pos.position.y)
        {
            //return 2;
            AnimDirection(2);
            return;
        }

        AnimDirection(0);
        //return 0;
    }

    int past_direct;
    private void AnimDirection(int direct)
    {
        //if (direct == 2)
        //{
        //    SetLayer(-10);
        //}
        //if (direct == -2)
        //{
        //    SetLayer(10);
        //}

        if (direct == past_direct)
        {
            return;
        }
        else
        {
            past_direct = direct;
        }

        left.SetActive(false);
        up.SetActive(false);
        down.SetActive(false);

        switch (direct)
        {
            case 1:
                {
                    left.SetActive(true);
                    GetTransform().localScale = new Vector3(Mathf.Abs(GetTransform().localScale.x), GetTransform().localScale.y, GetTransform().localScale.z);
                    break;
                }

            case -1:
                {
                    left.SetActive(true);
                    GetTransform().localScale = new Vector3(- Mathf.Abs(GetTransform().localScale.x), GetTransform().localScale.y, GetTransform().localScale.z);
                    break;
                }

            case 2:
                {
                    up.SetActive(true);
                    break;
                }

            case -2:
                {
                    down.SetActive(true);
                    anim.SetTrigger("move");
                    break;
                }
            default:
                down.SetActive(true);
                anim.SetTrigger("idle");
                break;
        }
    }

    public void SetLayer(int orderNumber)
    {
        //Debug.Log("setlayer");
        //for (int i = 0; i < sortingLayer.Length; i++)
        //{
        //    sortingLayer[i].sortingOrder += orderNumber;
        //}
    }

    //int currentOrder(int index)
    //{
    //    return 10 * (index / Constant.NUMBER_ROW) - 5;
    //}

    bool immortaling = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Line")
        {
            if (!immortaling)
            {
                immortaling = true;
                StartCoroutine(Immortal());
                SoundManager.instance.Vibrate(2);
                GameObject go = SimplePool.Spawn("EnemyBullet", tf_spawnBullet.position, Quaternion.identity);
                go.GetComponent<ElecTrail>().SetTranform(TimeManager.Instance.tf_MC);
            }
           
        }
    }

    private IEnumerator Immortal()
    {
        yield return new WaitForSeconds(0.5f);
        immortaling = false;
    }

}
