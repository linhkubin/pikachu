﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarineAlien : MonoBehaviour
{
    private int pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9;
    private Transform tf;
    private List<GameObject> waterList;
    private List<int> waterPosList;
    private float timeBeforeAppear = 5f;
    private float timeBeforedisappear = 5f;

    private void Start()
    {

        tf = GetComponent<Transform>();
        waterList = new List<GameObject>();
        waterPosList = new List<int>();

        disappearMarineAlien();

        SetWaterPosition();
        SetWaterList();
        ElementManager.Instance.CreateWater3x3(waterList);
        StartCoroutine("SetAlienPosition");
    }
    
    private void SetWaterList()
    {
        waterList.Add(GameManager.Instance.L_listNode[pos1]);
        waterList.Add(GameManager.Instance.L_listNode[pos2]);
        waterList.Add(GameManager.Instance.L_listNode[pos3]);
        // waterList.Add(GameManager.Instance.L_listNode[pos4]);
        // waterList.Add(GameManager.Instance.L_listNode[pos5]);
        // waterList.Add(GameManager.Instance.L_listNode[pos6]);
        // waterList.Add(GameManager.Instance.L_listNode[pos7]);
        // waterList.Add(GameManager.Instance.L_listNode[pos8]);
        // waterList.Add(GameManager.Instance.L_listNode[pos9]);
    }

    private void SetWaterPosition()
    {
        pos1 = 32;
        pos2 = 33;
        pos3 = 34;
        // pos4 = 42;
        // pos5 = 43;
        // pos6 = 44;
        // pos7 = 52;
        // pos8 = 53;
        // pos9 = 54;
        waterPosList.Add(pos1);
        waterPosList.Add(pos2);
        waterPosList.Add(pos3);
        // waterPosList.Add(pos4);
        // waterPosList.Add(pos5);
        // waterPosList.Add(pos6);
        // waterPosList.Add(pos7);
        // waterPosList.Add(pos8);
        // waterPosList.Add(pos9);
    }

    private IEnumerator SetAlienPosition()
    {
        while(true)
        {
            yield return new WaitForSeconds(timeBeforeAppear);

            tf.position = GameManager.Instance.L_listNode[waterPosList[Random.Range(0, 3)]].transform.position;

            appearMarineAlien();

            yield return new WaitForSeconds(timeBeforedisappear);

            disappearMarineAlien();
        }
    }

    private void appearMarineAlien()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        this.gameObject.GetComponent<Collider2D>().enabled = true;
    }

    private void disappearMarineAlien()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
        this.gameObject.GetComponent<Collider2D>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Line")
        {
            Debug.Log("Crash");
            TimeManager.Instance.SubtractTimeByAlien();
        }
    }
}