﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public int i_index;
    public int I_index
    {
        get { return i_index; }
        set
        {
            if (value < 0)
            {
                i_index = 0;
            }
            else
            if (value > Constant.TOTAL_ITEM_NUMBER - 1)
            {
                i_index = Constant.TOTAL_ITEM_NUMBER - 1;
            }
            else i_index = value;

        }
    }

    public float timer = 0;

    public float timeAction = 5;

    public abstract void SetPosition(int position);

    public GameObject left;
    public GameObject up;
    public GameObject down;

    public SpriteMeshInstance[] sortingLayer;

    public Transform m_Transform;
    public Transform GetTransform()
    {
        if (m_Transform == null)
        {
            m_Transform = transform;
        }
        return m_Transform;
    }

    public Animator anim;

    public int i_pathIndex;
}
