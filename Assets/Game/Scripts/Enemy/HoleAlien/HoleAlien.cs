﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleAlien : MonoBehaviour
{
    public GameObject hole;
    private Transform tf;
    private Vector3 currentPos;
    private List<Vector3> listHolePos;
    public  List<Vector3> ListHolePos
    {
        get
        {
            return listHolePos;
        }
        set
        {
            listHolePos = value;
        }
    }

    private void Start()
    {
        tf = GetComponent<Transform>();
        listHolePos = new List<Vector3>();
        StartCoroutine("Jump");
    }

    private IEnumerator Jump()
    {
        while(true)
        {
            int randomPos;

            do
            {
                randomPos = Random.Range(0, Constant.TOTAL_ITEM_NUMBER);
            } 
            while(!GameManager.Instance.L_listNode[randomPos].GetComponent<Node>().IsNodeEmpty());
            gameObject.GetComponent<SpriteRenderer>().color = Color.clear;

            yield return new WaitForSeconds(1f);

            Instantiate(hole, GameManager.Instance.L_listNode[randomPos].transform.position, Quaternion.identity);

            yield return new WaitForSeconds(2f);

            gameObject.GetComponent<SpriteRenderer>().color = Color.white;

            tf.position = GameManager.Instance.L_listNode[randomPos].transform.position;
            currentPos = tf.position;

            yield return new WaitForSeconds(4f);
        }
    }

    private void CreateHole()
    {
        for(int i = 0; i < listHolePos.Count; i++)
        {
            if(tf.transform.position == ListHolePos[i])
            {
                break;
            }
        }
        Instantiate(hole, currentPos, Quaternion.identity);
        ListHolePos.Add(currentPos);
    }
}