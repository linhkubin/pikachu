﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine("RemoveHole");
    }
    private IEnumerator RemoveHole()
    {
        yield return new WaitForSeconds(10f);

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Line")
        {
            TimeManager.Instance.SubtractTime(5f);
        }
    }
}
