﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineSpawn : Enemy
{

    List<Node> list;

    int minTimeCreate;
    int maxTimeCreate;

    int minCouple;
    int maxCouple;

    public Transform posSpawn;

    // Start is called before the first frame update
    void Start()
    {
        list = GameManager.Instance.m_Node;
        GameManager.Instance.machineSpawn = GetComponent<MachineSpawn>();
    }

    public override void SetPosition(int position)
    {
        Constant.have_SPAWN_MACHINE = true;

        I_index = position;

        transform.position = GameManager.Instance.L_position[I_index].transform.position;

        StartCoroutine(SetBarrier());

        timer = 0;
    }

    private IEnumerator SetBarrier()
    {
        yield return new WaitForSeconds(0.1f);

        list[I_index].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index + 1].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index - Constant.NUMBER_ROW].I_id = Constant.ID_TYPE_BARRIER;
        list[I_index - Constant.NUMBER_ROW + 1].I_id = Constant.ID_TYPE_BARRIER;

        GameManager.Instance.L_spritePosition[I_index].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index + 1].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index - Constant.NUMBER_ROW + 1].color = Color.clear;
        GameManager.Instance.L_spritePosition[I_index - Constant.NUMBER_ROW].color = Color.clear;
    }

    private IEnumerator StartSpawn()
    {
        int ran = Random.Range(1, 3);
        //Debug.Log("ran : " + ran);


        yield return new WaitForSeconds(Random.Range(5f, 7f));

        if (!TimeManager.Instance.b_gameStart || TimeManager.Instance.b_pause)
        {
            StartCoroutine(StartSpawn());
        }
        else
        if (!TimeManager.Instance.b_gameOver)
        {
            
            StartCoroutine(Spawn(ran));

            StartCoroutine(StartSpawn());
        }
        
    }

    private void Update()
    {
        if (TimeManager.Instance.b_gameStart && !TimeManager.Instance.b_pause && !TimeManager.Instance.b_gameOver)
        {
            timer += Time.deltaTime;
            if (timer > 6.5f)
            {
                timer = 0;
                StartCoroutine(Spawn(Random.Range(1, 3)));
            }
        }
    }

    public void SubTimerSpawn()
    {
        timer += 1.5f;
    }

    private void Boil()
    {
        anim.SetTrigger("boil");
    }

    private IEnumerator Spawn(int number)
    {
        anim.SetTrigger("boil");

        yield return new WaitForSeconds(1.15f);

        for (int i = 0; i < number; i++)
        {
            int ran = Random.Range(0, SpriteManager.Instance.l_spritesType.Count);

            GenerateTwoNode(SpriteManager.Instance.l_spritesType[ran].number);
        }

    }

    private void GenerateTwoNode(int numberNode)
    {

        int ran1 = Random.Range(Constant.NUMBER_ROW + 1, (Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) / 2);

        int check = ran1;

        while (!list[ran1].IsNodeEmpty())
        {
            ran1--;
            if (check == ran1)
            {
                return;
            }
            if (ran1 <= Constant.NUMBER_ROW)
            {
                ran1 = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW;
            }
        }
        //L_listNode[ran].GetComponent<Node>().I_id = id;

        int ran2 = Random.Range((Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) / 2, Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW);

        check = ran2;

        while (!list[ran2].IsNodeEmpty() || ran2 == ran1)
        {
            ran2++;
            if (check == ran2)
            {
                return;
            }

            if (ran2 >= Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW )
            {
                ran2 = Constant.NUMBER_ROW;
            }
        }

        list[ran1].I_id = Constant.ID_TYPE_NODE_WATER;

        list[ran2].I_id = Constant.ID_TYPE_NODE_WATER;

        GameObject go = SimplePool.Spawn("StarNode", posSpawn.position, Quaternion.identity);
        go.GetComponent<StarNode>().SetTarget(list[ran1].GetTransform(), "SpawnNode", list[ran1], numberNode);

        go = SimplePool.Spawn("StarNode", posSpawn.position, Quaternion.identity);
        go.GetComponent<StarNode>().SetTarget(list[ran2].GetTransform(), "SpawnNode", list[ran2], numberNode);

    }


}
