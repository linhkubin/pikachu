﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEnemy : MonoBehaviour
{
    private Transform tf;
    private int cameraPos;
    private LineRenderer line;
    private int endNodePos;
    private int[] degreeArray = new int[4];
    private float rotateTime = 5f;
    public List<int> blockedNodes = new List<int>();

    void Start()
    {
        line = GetComponent<LineRenderer>();
        ConfigLine();

        degreeArray[0] = 0;
        degreeArray[1] = 90;
        degreeArray[2] = 180;
        degreeArray[3] = 270;

        tf = GetComponent<Transform>();
        SetCameraPos();

        StartCoroutine("RotateCamera");
    }

    private void Update()
    {
        if(line.enabled == true)
        {
            for(int i = 0; i < blockedNodes.Count; i++)
            {
                if(GameManager.Instance.L_listNode[blockedNodes[i]].GetComponent<Node>().IsNodeEmpty())
                {
                    TimeManager.Instance.SubtractTimeByAlien();
                    Destroy(gameObject);
                }
            }    
        }
    }

    private void ConfigLine()
    {
        line.startWidth = 0.1f;
        line.endWidth = 0.1f;
        line.material.color = new Color(255f, 0f, 0f);
    }

    private void DrawLine(int degree)
    {
        line.enabled = true;
        line.positionCount = 2;

        switch(degree)
        {
            case 0:
                endNodePos = cameraPos;

                for(int i = 0; endNodePos < Constant.TOTAL_ITEM_NUMBER - 20; i++)
                {
                    endNodePos = cameraPos + 10 * i;

                    if(GameManager.Instance.L_listNode[endNodePos].GetComponent<Node>().IsNodeEmpty() == false)
                    {
                        blockedNodes.Add(endNodePos);
                    }
                }
                break;

            case 90:
                endNodePos = cameraPos;

                for(int i = 0; endNodePos < (cameraPos / 10 * 10 + 8); i++)
                {
                    endNodePos = cameraPos + i;

                    if(GameManager.Instance.L_listNode[endNodePos].GetComponent<Node>().IsNodeEmpty() == false)
                    {
                        blockedNodes.Add(endNodePos);
                    }
                }
                break;

            case 180:
                endNodePos = cameraPos;

                for(int i = 0; endNodePos > 20; i++)
                {
                    endNodePos = cameraPos - 10 * i;

                    if(GameManager.Instance.L_listNode[endNodePos].GetComponent<Node>().IsNodeEmpty() == false)
                    {
                        blockedNodes.Add(endNodePos);
                    }
                }
                break;
                
            case 270:
                endNodePos = cameraPos;

                for(int i = 0; endNodePos > (cameraPos / 10 * 10 + 1); i++)
                {
                    endNodePos = cameraPos - i;
                    
                    if(GameManager.Instance.L_listNode[endNodePos].GetComponent<Node>().IsNodeEmpty() == false)
                    {
                        blockedNodes.Add(endNodePos);
                    }
                }
                break;
        }
        line.SetPosition(0, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -1f));
        line.SetPosition(1, new Vector3(GameManager.Instance.L_listNode[endNodePos].transform.position.x, GameManager.Instance.L_listNode[endNodePos].transform.position.y, -1f));
    }

    private void DisableLine()
    {
        line.enabled = false;
    }

    private IEnumerator RotateCamera()
    {
        while(true)
        {
            DisableLine();
            int degree = degreeArray[Random.Range(0, 4)];
            tf.eulerAngles = new Vector3(0, 0, degree);

            yield return new WaitForSeconds(2f);

            DrawLine(degree);

            yield return new WaitForSeconds(rotateTime);

            blockedNodes.Clear();
            DisableLine();

            yield return new WaitForSeconds(2f);
        }
    }

    private void SetCameraPos()
    {
        cameraPos = 43 ;
        tf.position = GameManager.Instance.L_listNode[cameraPos].transform.position;
        GameManager.Instance.L_listNode[cameraPos].GetComponent<Node>().RemoveNode();
        GameManager.Instance.L_listNode[cameraPos].GetComponent<Node>().I_id = Constant.ID_NOTHING;
    }
}