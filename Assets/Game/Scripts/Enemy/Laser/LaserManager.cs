﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserManager : MonoBehaviour
{
    public GameObject laser1;
    public GameObject laser2;
    private GameObject laser_1;
    private GameObject laser_2;

    private List<GameObject> blockedNodes = new List<GameObject>();

    private int laser_index_1;
    private int laser_index_2;
    public int Laser_index_1 {
        get { return laser_index_1; }
        set
        {
            if (value < 0)
            {
                laser_index_1 = 0;
            }
            else
            if (value > Constant.TOTAL_ITEM_NUMBER - 1)
            {
                laser_index_1 = Constant.TOTAL_ITEM_NUMBER - 1;
            }
            else laser_index_1 = value;

        }
    }
    public int Laser_index_2 {
        get { return laser_index_2; }
        set
        {
            if (value < 0)
            {
                laser_index_2 = 0;
            }
            else
            if (value > Constant.TOTAL_ITEM_NUMBER - 1)
            {
                laser_index_2 = Constant.TOTAL_ITEM_NUMBER - 1;
            }
            else laser_index_2 = value;

        }
    }
    private LineRenderer line;
    
    private void Start() 
    {
        line = GetComponent<LineRenderer>();
        line.startWidth = 0.07f;
        line.endWidth = 0.07f;

        SetLaserPostion();
        CreateLaser();

        Debug.Log(blockedNodes.Count);
    }

    private void SetLaserPostion()
    {
        // do
        // {
        //     Laser_index_1 = Random.Range(0, Constant.TOTAL_ITEM_NUMBER);
        // } while (GameManager.Instance.L_listNode[Laser_index_1].GetComponent<Node>().I_id != Constant.ID_TYPE_NODE);

        // do
        // {
        //     Laser_index_2 = Random.Range(0, Constant.TOTAL_ITEM_NUMBER);
        // } while (!CheckLaser2());
        Laser_index_1 = 32;
        Laser_index_2 = 37;

        // Laser_index_1 = 20;
        // Laser_index_2 = 23;
    }

    private bool CheckLaser2()
    {
        if(Laser_index_2 == Laser_index_1)
        {
            return false;
        }
        else if(GameManager.Instance.L_listNode[Laser_index_2] .GetComponent<Node>().I_id != Constant.ID_TYPE_NODE)
        {
            return false;
        }
        else if((Laser_index_2 % 10 != Laser_index_1 % 10) && (Laser_index_2 / 10 != Laser_index_1 / 10))
        {
            return false;
        }

        return true;
    }

    private void CreateLaser()
    {
        laser_1 = Instantiate(laser1);
        laser_2 = Instantiate(laser2);

        if((Laser_index_1 / Constant.NUMBER_ROW) == (Laser_index_2 / Constant.NUMBER_ROW))
        {
            for(int i = Laser_index_1; i < laser_index_2 + 1; i++)
            {
                if(i == Laser_index_1)
                {
                    GameManager.Instance.L_listNode[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
                    laser_1.transform.position = GameManager.Instance.L_listNode[Laser_index_1].transform.position;
                }
                else if(i == Laser_index_2)
                {
                    GameManager.Instance.L_listNode[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
                    laser_2.transform.position = GameManager.Instance.L_listNode[Laser_index_2].transform.position;
                }
                else
                {
                    blockedNodes.Add(GameManager.Instance.L_listNode[i]);
                }
            }
        }
        else if((Laser_index_1 % Constant.NUMBER_ROW) == (Laser_index_2 % Constant.NUMBER_ROW))
        {
            for(int i = Laser_index_1; i < laser_index_2 + Constant.NUMBER_ROW; i += Constant.NUMBER_ROW)
            {
                if(i == Laser_index_1)
                {
                    GameManager.Instance.L_listNode[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
                    laser_1.transform.position = GameManager.Instance.L_listNode[Laser_index_1].transform.position;
                }
                else if(i == Laser_index_2)
                {
                    GameManager.Instance.L_listNode[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
                    laser_2.transform.position = GameManager.Instance.L_listNode[Laser_index_2].transform.position;
                }
                else
                {
                    blockedNodes.Add(GameManager.Instance.L_listNode[i]);
                }
            }
        }

        for(int i = 0; i < blockedNodes.Count; i++)
        {
            blockedNodes[i].GetComponent<Node>().RemoveNode();
        }
        GameManager.Instance.L_listNode[Laser_index_1].GetComponent<Node>().RemoveNode();
        GameManager.Instance.L_listNode[Laser_index_1].GetComponent<Node>().I_id = Constant.ID_NOTHING;
        GameManager.Instance.L_listNode[Laser_index_2].GetComponent<Node>().RemoveNode();
        GameManager.Instance.L_listNode[Laser_index_2].GetComponent<Node>().I_id = Constant.ID_NOTHING;

        StartCoroutine(CreateLine(laser_1, laser_2));
    }

    private IEnumerator CreateLine(GameObject laser1, GameObject laser2)
    {
        line.positionCount = 2;
        line.SetPosition(0, new Vector3(laser1.transform.position.x, laser1.transform.position.y, -1f));
        line.SetPosition(1, new Vector3(laser2.transform.position.x, laser2.transform.position.y, -1f));

        while(true)
        {
            line.enabled = true;
            BlockNode();

            yield return new WaitForSeconds(3f);

            line.enabled = false;
            UnblockNode();

            yield return new WaitForSeconds(3f);
        }
    }

    private void BlockNode()
    {
        if((Laser_index_1 / Constant.NUMBER_ROW) == (Laser_index_2 / Constant.NUMBER_ROW) || (Laser_index_1 % Constant.NUMBER_ROW) == (Laser_index_2 % Constant.NUMBER_ROW))
        {
            Debug.Log("Same row called");
            for(int i = 0; i < blockedNodes.Count; i++)
            {
                blockedNodes[i].GetComponent<Node>().I_id = Constant.ID_NOTHING;
            }
        }
    }

    private void UnblockNode()
    {
        for(int i = 0; i < blockedNodes.Count; i++)
        {
            blockedNodes[i].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE;
            blockedNodes[i].GetComponent<Node>().I_numberNode = 0;
        }
    }
}