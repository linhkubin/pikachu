﻿using System.Collections;
using UnityEngine;

public class ChainManager : MonoBehaviour
{
    public GameObject chain;
    private GameObject chainer;
    private int chainNumber;
    private int chainDirection;
    private GameObject chainedNode;
    // private bool chained;
    private Transform tf;

    private void Start()
    {
        chainer = new GameObject();
        tf = GetComponent<Transform>();

        // directionArray = new int[-1, 1, -10, 10];

        chainedNode = new GameObject();
        chainNumber = 32;

        // while(true)
        // {
        //     chainNumber = Random.Range(11, 109);
        //     // chainNumber = 32;

        //     if(GameManager.Instance.L_listNode[chainNumber].GetComponent<Node>().I_id == Constant.ID_TYPE_NODE)
        //     {
        //         break;
        //     }
        // }

        // chainDirection = Random.Range(1, 5);

        CreateChain();
        StartCoroutine("SetChainingTime");
    }

    private void CreateChain()
    {
        tf.position = GameManager.Instance.L_listNode[chainNumber].transform.position;
        chainer = Instantiate(chain);
        SetChainedNode();
    }

    // private void SetChainedNode()
    // {
    //     chainDirection = Random.Range(0, 4);

    //     switch(chainDirection)
    //     {
    //         case 0:
    //             chainedNode = GameManager.Instance.L_listNode[chainNumber - 1];

    //             if(chainedNode.GetComponent<Node>().I_id != Constant.ID_TYPE_NODE)
    //             {
    //                 Debug.Log("Change direction");
    //                 chainedNode = GameManager.Instance.L_listNode[chainNumber + 1];
    //             }

    //             chainedNode.GetComponent<Node>().B_lockClick = true;
    //             chainer.transform.position = (chainedNode.transform.position + GameManager.Instance.L_listNode[chainNumber].transform.position) / 2;
    //             break;
    //         case 1:
    //             chainedNode = GameManager.Instance.L_listNode[chainNumber + 1];

    //             if(chainedNode.GetComponent<Node>().I_id != Constant.ID_TYPE_NODE)
    //             {
    //                 Debug.Log("Change direction");

    //                 chainedNode = GameManager.Instance.L_listNode[chainNumber - 1];
    //             }

    //             chainedNode.GetComponent<Node>().B_lockClick = true;
    //             chainer.transform.position = (chainedNode.transform.position + GameManager.Instance.L_listNode[chainNumber].transform.position) / 2;
    //             break;
    //         case 2:
    //             chainer.transform.Rotate(new Vector3(0f, 0f, 90));

    //             chainedNode = GameManager.Instance.L_listNode[chainNumber - 10];

    //             if(chainedNode.GetComponent<Node>().I_id != Constant.ID_TYPE_NODE)
    //             {
    //                 Debug.Log("Change direction");
    //                 chainedNode = GameManager.Instance.L_listNode[chainNumber + 10];
    //             }

    //             chainedNode.GetComponent<Node>().B_lockClick = true;
    //             chainer.transform.position = (chainedNode.transform.position + GameManager.Instance.L_listNode[chainNumber].transform.position) / 2;
    //             break;
    //         case 3:
    //             chainer.transform.Rotate(new Vector3(0f, 0f, 90));
    //             chainedNode = GameManager.Instance.L_listNode[chainNumber + 10];

    //             if(chainedNode.GetComponent<Node>().I_id != Constant.ID_TYPE_NODE)
    //             {
    //                 Debug.Log("Change direction");
    //                 chainedNode = GameManager.Instance.L_listNode[chainNumber - 10];
    //             }

    //             chainedNode.GetComponent<Node>().B_lockClick = true;
    //             chainer.transform.position = (chainedNode.transform.position + GameManager.Instance.L_listNode[chainNumber].transform.position) / 2;
    //             break;
    //     }
    // }

    private void SetChainedNode()
    {
        chainedNode = GameManager.Instance.L_listNode[chainNumber - 1];
        chainedNode.GetComponent<Node>().B_lockClick = true;
        chainer.transform.position = (chainedNode.transform.position + GameManager.Instance.L_listNode[chainNumber].transform.position) / 2;
    }

    private void RemoveChain()
    {
        chainedNode.GetComponent<Node>().B_lockClick = false;
        Destroy(chainer);
        GameManager.Instance.L_listNode[chainNumber].GetComponent<Node>().RemoveNode();
        Destroy(gameObject);
    }

    private IEnumerator SetChainingTime()
    {
        GameManager.Instance.L_listNode[chainNumber].GetComponent<Node>().I_id = Constant.ID_NOTHING;
        //GameManager.Instance.L_listNode[chainNumber].GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.GetComponent<SpriteManager>().s_nothing.GetComponent<SpriteRenderer>().sprite;
        yield return new WaitForSeconds(5f);
        RemoveChain();
    }
}