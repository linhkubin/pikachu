﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderRobo : MonoBehaviour
{
    private int i_index;
    public int I_index {
        get { return i_index; }
        set
        {
            if (value < 0)
            {
                i_index = 0;
            }
            else
            if (value > Constant.TOTAL_ITEM_NUMBER - 1)
            {
                i_index = Constant.TOTAL_ITEM_NUMBER - 1;
            }
            else i_index = value;

        }
    }

    private int i_pathIndex;

    List<GameObject> list;

    private List<PointLand> listPointLand = new List<PointLand>();

    public int NumberStep = 4;

    public float speed = 5f;

    public float timer = 0;

    private float timeDuration = 7f;

    [SerializeField]
    private GameObject webPrefabs;

    public Transform TargetPosition { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        i_pathIndex = I_index = 0;

        list = GameManager.Instance.L_listNode;
        TargetPosition = gameObject.transform;

        transform.position = GameManager.Instance.L_listNode[I_index].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > timeDuration)
        {
            timer = 0;

            CreateMap(I_index);
            RunOnMap();
        }

        transform.position = Vector3.MoveTowards(transform.position, TargetPosition.transform.position, speed * Time.deltaTime);
    }

    
    private void CreateMap(int point)
    {
        int index = 0;
        PointLand pointLand = new PointLand(point,index);

        listPointLand.Add(pointLand);

        index++;

        CheckIndStep(listPointLand[0],point, index);

        do
        {
            for (int i = 0; i < listPointLand.Count; i++)
            {
                if (listPointLand[i].Step == index)
                {
                    CheckIndStep(listPointLand[i],listPointLand[i].Point, index + 1);
                }
            }
            index++;
        }
        while (index < NumberStep);

    }

    //ktra xem xung quanh co the di hay k && danh dau
    private void CheckIndStep(PointLand parent, int point ,int index)
    {
        //up
        if ((point >= Constant.NUMBER_ROW) && !IsInMap(point - Constant.NUMBER_ROW))
        {
            if (!IsLand(point - Constant.NUMBER_ROW))
            {
                PointLand pointLand = new PointLand(point - Constant.NUMBER_ROW, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point - Constant.NUMBER_ROW, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //down
        if ((point < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) && !IsInMap(point + Constant.NUMBER_ROW))
        {
            if (!IsLand(point + Constant.NUMBER_ROW))
            {
                PointLand pointLand = new PointLand(point + Constant.NUMBER_ROW, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point + Constant.NUMBER_ROW, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //right
        if ((point % Constant.NUMBER_ROW) != (Constant.NUMBER_ROW - 1) && !IsInMap(point + 1))
        {
            if (!IsLand(point + 1))
            {
                PointLand pointLand = new PointLand(point + 1, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point + 1, index, parent);
                listPointLand.Add(pointLand);
            }
        }

        //left
        if ((point % Constant.NUMBER_ROW) != 0 && !IsInMap(point - 1))
        {
            if (!IsLand(point - 1))
            {
                PointLand pointLand = new PointLand(point - 1, -1);
                listPointLand.Add(pointLand);
            }
            else
            {
                PointLand pointLand = new PointLand(point - 1, index, parent);
                listPointLand.Add(pointLand);
            }
        }
    }

    // check xem la node empty hoac node moi truong
    private bool IsLand(int point)
    {
        if (list[point].GetComponent<Node>().I_id == Constant.ID_TYPE_ENVIRONMENT || list[point].GetComponent<Node>().IsNodeEmpty())
        {
            return true;
        }
        return false;
    }

    // check xem trong diction co node day chua
    private bool IsInMap(int point)
    {
        for (int i = 0; i < listPointLand.Count; i++)
        {
            if (listPointLand[i].Point == point)
            {
                return true;
            }
        }
        return false;
    }

    private void RunOnMap()
    {
        List<PointLand> pointLands = new List<PointLand>();
        List<int> listLand = new List<int>();

        for (int i = 0; i < listPointLand.Count; i++)
        {
            if (listPointLand[i].Step == NumberStep)
            {
                pointLands.Add(listPointLand[i]);
            }
        }

        if (pointLands.Count == 0)
        {
            return;
        }

        if (pointLands.Count > 1)
        {
            for (int i = 0; i < pointLands.Count; i++)
            {
                if (pointLands[i].Point == i_pathIndex)
                {
                    pointLands.RemoveAt(i);
                    i_pathIndex = I_index;
                    break;
                }
            }
        }

        int ran = Random.Range(0, pointLands.Count);

        PointLand point;

        point = pointLands[ran];

        I_index = point.Point;

        while (point != null)
        {
            listLand.Add(point.Point);
            point = point.Parent;
        }

        StartCoroutine(Run(listLand));

    }

    private void PrintListLand()
    {
        for (int i = 0; i < listPointLand.Count; i++)
        {
            Debug.Log("point : " + listPointLand[i].Point + " ______ index : " + listPointLand[i].Step);
        }
    }

    private void ResetState()
    {
        listPointLand.Clear();
    }

    private IEnumerator Run(List<int> listLand)
    {
        for (int i = listLand.Count - 1; i >= 0; i--)
        {

            TargetPosition = list[listLand[i]].transform;

            yield return new WaitForSeconds(0.12f);

            Instantiate(webPrefabs, TargetPosition.position, Quaternion.identity);

        }

        ResetState();
    }
}

public class PointLand
{
    public int Point { get; set; } //number node
    public int Step { get; set; } // step
    public PointLand Parent { get; set; } //parent

    public PointLand(int point, int step, PointLand parent = null)
    {
        this.Point = point;
        this.Step = step;
        this.Parent = parent;
    }
}
