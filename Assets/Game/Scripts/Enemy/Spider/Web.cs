﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RemoveWeb());   
    }

    private IEnumerator RemoveWeb()
    {
        yield return new WaitForSeconds(5f);

        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Line")
        {
            //Debug.Log( "colider");
            TimeManager.Instance.SubtractTimeByWeb(10f);
        }
    }
}
