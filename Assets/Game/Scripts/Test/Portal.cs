﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    private Rigidbody2D enterRigidbody;
    private float enterVelocity, exitVelocity;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        enterRigidbody = collision.gameObject.GetComponent<Rigidbody2D>();
        enterVelocity = enterRigidbody.velocity.x;

        if (gameObject.name == "BluePortal")
        {
            PortalControl.Instance.DisableCollider("orange");
            PortalControl.Instance.CreateClone("atOrange");
        }
        else if (gameObject.name == "OrangePortal") 
        {
            PortalControl.Instance.DisableCollider("blue");
            PortalControl.Instance.CreateClone("atBlue");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        exitVelocity = enterRigidbody.velocity.x;
        if (enterVelocity != exitVelocity)
        {
            Destroy(GameObject.Find("Clone"));
        }
        else if (gameObject.name != "Clone")
        {
            Destroy(collision.gameObject);
            PortalControl.Instance.EnableCollider();
            GameObject.Find("Clone").name = "Character";
        }
    }

}
