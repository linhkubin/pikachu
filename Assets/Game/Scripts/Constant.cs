﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    public static int NUMBER_COLUNM = 12;

    public static int NUMBER_ROW = 10;

    public static int TOTAL_ITEM_NUMBER = NUMBER_ROW * NUMBER_COLUNM;
    
    public static int NUMBER_TYPE_POKEMON = 5;
    //-------------------------------------

    public static int ID_NOTHING = -2;          //k co gi ca
    public static int ID_TYPE_BARRIER = -1;     // k chua node ,k link qua duoc.
    public static int ID_TYPE_ENVIRONMENT = 0;   // k chua node , link qua duoc.
    public static int ID_TYPE_NODE_WATER = 15;   // k chua node , link qua duoc.

    public static int ID_TYPE_NODE_BONUS = 1;   //node bonus
    public static int ID_TYPE_NODE_GIFT = 2;    //node gift
    public static int ID_TYPE_NODE_QUEST = 3;   //node quest
    public static int ID_TYPE_NODE = 4;         // chua node bthg ---------
    public static int ID_TYPE_NODE_FREEZE = 5;  //node dong bang
    public static int ID_TYPE_NODE_LOCK = 6;    //node khoa.
    public static int ID_TYPE_NODE_MAGNET = 7;  //node nam cham.
    public static int ID_TYPE_NODE_JUMP = 8;    //node jump.
    public static int ID_TYPE_NODE_CLOUD = 9;    //node lat.
    public static int ID_TYPE_NODE_LINK = 10;   //node dinh.
    public static int ID_TYPE_NODE_BOOM = 11;   //node bom

    public static int ID_TYPE_NODE_FLOOD = 12;  //node ngap lut.
    public static int ID_TYPE_BOX = 13;

    public static int ID_ENEMY = 14;

    public static int ID_KEY = 15;

    public static int ID_HAT = 16;

    public static int ID_STAR = 17;


    public static float COOLDOWN_COMBO_TIMER = 2f;

    public static float MAX_TIME_PLAY = 90f;

    public static float TIME_REMOVE_NODE = 0.1f;

    public static bool is_MODE_SOLO = false;

    public static bool is_MODE_REQUEST = false;

    public static bool is_MODE_FIND_STAR = false;

    public static bool have_SPAWN_MACHINE = false;



    //-----------------------------------------

}