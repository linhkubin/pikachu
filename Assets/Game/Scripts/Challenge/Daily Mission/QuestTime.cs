﻿using UnityEngine;
using System;

public class QuestTime
{
    public float questTimeLeft = 300f;
    public float giftTimeLeft = 10f;
    public float newTime;
    public float newYear;
    public float newMonth;
    public float newDay;

    public void Start()
    {
        // InitStartTime();

        SetNewDateTime();

        questTimeLeft = PlayerPrefs.GetFloat("TimeLeft");

        giftTimeLeft = PlayerPrefs.GetFloat("GiftTimeLeft");
        
        if(newYear != PlayerPrefs.GetFloat("oldYear") || newMonth != PlayerPrefs.GetFloat("oldMonth") || newDay != PlayerPrefs.GetFloat("oldDay"))
        {
            ResetQuestTimeLeft();
            ResetGiftTimeLeft();
        }
        else
        {
            if(newTime - PlayerPrefs.GetFloat("oldTime") >= questTimeLeft)
            {
                questTimeLeft = newTime - PlayerPrefs.GetFloat("oldTime") - PlayerPrefs.GetFloat("TimeLeft");
                questTimeLeft = 300 - (questTimeLeft % 300);
                PlayerPrefs.SetFloat("TimeLeft", questTimeLeft);

                giftTimeLeft = newTime - PlayerPrefs.GetFloat("oldTime") - PlayerPrefs.GetFloat("GiftTimeLeft");
                giftTimeLeft = 10 - (giftTimeLeft % 10);
                PlayerPrefs.SetFloat("GiftTimeLeft", giftTimeLeft);

                for(int i = 0; i < 7; i++)
                {
                    DataManager.Instance.questManager.QuestResetUI(i);
                }
            }
            else
            {
                questTimeLeft -= (newTime - PlayerPrefs.GetFloat("oldTime"));
                PlayerPrefs.SetFloat("TimeLeft", questTimeLeft);

                giftTimeLeft -= (newTime - PlayerPrefs.GetFloat("oldTime"));
                PlayerPrefs.SetFloat("GiftTimeLeft", giftTimeLeft);
            }
        }

        SaveTime();
    }

    public void Update()
    {
        SetNewDateTime();
        SaveTime();

        if(questTimeLeft < 0)
        {
            ResetQuestTimeLeft();
            for(int i = 0; i < 7; i++)
            {
                DataManager.Instance.questManager.QuestResetUI(i);
            }
        }

        if(giftTimeLeft < 0)
        {
            if(PlayerPrefs.GetInt("GiftClaim") == 0)
            {
                DailyGiftsManager.Instance.UnlockGift();
            }
            if(PlayerPrefs.GetInt("GiftClaim") == 1)
            {
                DailyGiftsManager.Instance.nextReward.SetActive(false);
                DailyGiftsManager.Instance.claimReward.SetActive(true);
            }
            ResetGiftTimeLeft();
        }

        questTimeLeft -= Time.deltaTime;
        PlayerPrefs.SetFloat("TimeLeft", questTimeLeft);

        giftTimeLeft -= Time.deltaTime;
        PlayerPrefs.SetFloat("GiftTimeLeft", giftTimeLeft);
        DailyGiftsManager.Instance.nextRewardTimeText.text = "Next Rewards" + "\n" + giftTimeLeft.ToString("F2");
        // DailyGiftsManager.Instance.nextRewardTimeText.text = questTimeLeft.ToString("F2");
    }

    public void InitStartTime()
    {
        PlayerPrefs.SetFloat("TimeLeft", 300f);
        InitLastTime();

        PlayerPrefs.SetFloat("GiftTimeLeft", 10f);
        InitLastTime();
    }

    public void InitLastTime()
    {
        PlayerPrefs.SetFloat("oldYear", DateTime.Now.Year);
        PlayerPrefs.SetFloat("oldMonth", DateTime.Now.Month);
        PlayerPrefs.SetFloat("oldDay", DateTime.Now.Day);
        PlayerPrefs.SetFloat("oldTime", DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second);
    }

    public void SetNewDateTime()
    {
        newTime = DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
        newYear = DateTime.Now.Year;
        newMonth = DateTime.Now.Month;
        newDay = DateTime.Now.Day;
    }

    public void SaveTime()
    {
        PlayerPrefs.SetFloat("oldTime", newTime);
        PlayerPrefs.SetFloat("oldYear", newYear);
        PlayerPrefs.SetFloat("oldMonth", newMonth);
        PlayerPrefs.SetFloat("oldDay", newDay);
    }

    public void ResetQuestTimeLeft()
    {
        questTimeLeft = 300f;
        PlayerPrefs.SetFloat("TimeLeft", questTimeLeft);
    }

    public void ResetGiftTimeLeft()
    {
        giftTimeLeft = 10f;
        PlayerPrefs.SetFloat("GiftTimeLeft", giftTimeLeft);
    }
}