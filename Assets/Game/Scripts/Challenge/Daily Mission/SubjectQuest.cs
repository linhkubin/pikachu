﻿using System.Collections.Generic;
using UnityEngine;

public class SubjectQuest
{
    List<QuestObserver> questObservers = new List<QuestObserver>();

    public void QuestNotify(int id, int amount)
    {
        //Debug.Log("Quest notify");
        int len = questObservers.Count;

        for (int i = 0; i < len; i++) {
            questObservers[i].OnNotify(id, amount);
        }
    }

    public void QuestSetUpUI(int id)
    {
        int len = questObservers.Count;

        for (int i = 0; i < len; i++) {
            questObservers[i].OnSetUpUI(id);
        }
    }

    public void QuestResetUI(int id)
    {
        int len = questObservers.Count;

        for (int i = 0; i < len; i++) {
            questObservers[i].OnResetUI(id);
        }
    }

    public void AddQuestObserver(QuestObserver questObserver)
    {
        questObservers.Add(questObserver);
    }

    //Remove observer from the list
    public void RemoveObserver(Observer observer)
    {
    }
}