﻿using UnityEngine;
using System.Collections;

public class Quest : QuestObserver
{
    public int id;
    public string title;
    public int currentProgress;
    public int maxProgress;
    public int rewardNumber;
    public int[] randomProgress = new int[3];
    public int canClaim;

    public Quest(int id)
    {
        this.id = id;
    }

    public void SetupProgress(string title, int currentProgress, int maxProgress, int rewardNumber)
    {
        this.title = title;
        this.currentProgress = currentProgress;
        this.maxProgress = maxProgress;
        this.rewardNumber = rewardNumber;
    }

    public override void OnNotify(int id, int amount)
    {

    }

    public override void OnSetUpUI(int id)
    {

    }

    public override void OnResetUI(int id)
    {

    }

    public virtual bool IsCompleted()
    {
        return currentProgress >= maxProgress ? true : false;
    }
}

public class QuestAds : Quest
{
    public QuestAds(int id)
    : base(id)
    {
        randomProgress[0] = 1;
        randomProgress[1] = 2;
        randomProgress[2] = 3;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestAdsProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_Ads.gameObject.SetActive(false);
        UIQuestManager.Instance.go_AdsReward.SetActive(false);
        UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.unlockedAds;
        UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.adsCompleted.SetActive(true);

        PlayerPrefs.SetInt("QuestAdsCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestAdsCanClaim");
            UIQuestManager.Instance.txt_AdsTitle.text = title.ToString();
            UIQuestManager.Instance.txt_AdsRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestAdsCompleted") == 0)
            {
                UIQuestManager.Instance.adsCompleted.SetActive(false);
                UIQuestManager.Instance.go_AdsReward.SetActive(true);
                UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.lockedAds;
                UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.adsCompleted.SetActive(true);
                UIQuestManager.Instance.go_AdsReward.SetActive(false);
                UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.unlockedAds;
                UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestAdsData();

            currentProgress = PlayerPrefs.GetInt("QuestAdsProgress");
            UIQuestManager.Instance.txt_AdsTitle.text = PlayerPrefs.GetString("QuestAdsTitle").ToString();
            UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.lockedAds;
            UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_Ads.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_AdsRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_AdsReward.SetActive(true);

            UIQuestManager.Instance.adsCompleted.SetActive(false);

            UIQuestManager.Instance.txt_AdsProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_AdsProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_AdsProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_AdsProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Ads.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_AdsRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.lockedAds;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_AdsProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_AdsProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Ads.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_AdsRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_AdsBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_Ads.sprite = UIQuestManager.Instance.unlockedAds;
        }
    }
}

public class Quest3Stars : Quest
{
    public Quest3Stars(int id)
    : base(id)
    {
        randomProgress[0] = 1;
        randomProgress[1] = 2;
        randomProgress[2] = 3;
        // maxProgress = randomProgress[Random.Range(0, randomProgress.Length)];`
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("Quest3StarsProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_3Stars.gameObject.SetActive(false);
        UIQuestManager.Instance.go_3starsReward.SetActive(false);
        UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.unlocked3Stars;
        UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.stars3Completed.SetActive(true);

        PlayerPrefs.SetInt("Quest3StarsCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("Quest3StarsCanClaim");
            UIQuestManager.Instance.txt_3StarsTitle.text = title.ToString();
            UIQuestManager.Instance.txt_3StarsRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("Quest3StarsCompleted") == 0)
            {
                UIQuestManager.Instance.stars3Completed.SetActive(false);
                UIQuestManager.Instance.go_3starsReward.SetActive(true);
                UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.locked3Stars;
                UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.lockedBg;
                
            }
            else
            {
                UIQuestManager.Instance.stars3Completed.SetActive(true);
                UIQuestManager.Instance.go_3starsReward.SetActive(false);
                UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.unlocked3Stars;
                UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuest3StarsData();

            currentProgress = PlayerPrefs.GetInt("Quest3StarsProgress");
            UIQuestManager.Instance.txt_3StarsTitle.text = PlayerPrefs.GetString("Quest3StarsTitle").ToString();
            UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.locked3Stars;
            UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_3Stars.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_3StarsRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_3starsReward.SetActive(true);

            UIQuestManager.Instance.stars3Completed.SetActive(false);

            UIQuestManager.Instance.txt_3StarsProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_3StarsProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_3StarsProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_3StarsProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_3Stars.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_3StarsRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.locked3Stars;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_3StarsProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_3StarsProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_3Stars.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_3StarsRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_3StarsBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_3Stars.sprite = UIQuestManager.Instance.unlocked3Stars;
        }
    }
}

public class QuestBooster : Quest
{
    public QuestBooster(int id)
    : base(id)
    {
        maxProgress = 3;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestBoosterProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public string RandomBooster(int booster)
    {
        if (booster == 3)
        {
            return "time";
        }
        else if (booster == 4)
        {
            return "hammer";
        }
        else if (booster == 5)
        {
            return "mix";
        }
        else if (booster == 6)
        {
            return "hint";
        }
        else
        {
            return "";
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_Booster.gameObject.SetActive(false);
        UIQuestManager.Instance.go_BoosterReward.SetActive(false);
        UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.unlockedBooster;
        UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.boosterCompleted.SetActive(true);

        PlayerPrefs.SetInt("QuestBoosterCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestBoosterCanClaim");
            UIQuestManager.Instance.txt_BoosterTitle.text = title.ToString();
            UIQuestManager.Instance.txt_BoosterRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestBoosterCompleted") == 0)
            {
                UIQuestManager.Instance.boosterCompleted.SetActive(false);
                UIQuestManager.Instance.go_BoosterReward.SetActive(true);
                UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.lockedBooster;
                UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.boosterCompleted.SetActive(true);
                UIQuestManager.Instance.go_BoosterReward.SetActive(false);
                UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.unlockedBooster;
                UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestBoosterData();

            currentProgress = PlayerPrefs.GetInt("QuestBoosterProgress");
            UIQuestManager.Instance.txt_BoosterTitle.text = PlayerPrefs.GetString("QuestBoosterTitle").ToString();
            UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.lockedBooster;
            UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_Booster.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_BoosterRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_BoosterReward.SetActive(true);

            UIQuestManager.Instance.boosterCompleted.SetActive(false);

            UIQuestManager.Instance.txt_BoosterProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_BoosterProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_BoosterProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_BoosterProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Booster.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_BoosterRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.lockedBooster;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_BoosterProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_BoosterProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Booster.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_BoosterRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_BoosterBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_Booster.sprite = UIQuestManager.Instance.unlockedBooster;
        }
    }
}

public class QuestHighScore : Quest
{
    public int level;
    public QuestHighScore(int id)
    : base(id)
    {
        level = Random.Range(6, 41);
        maxProgress = 1;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestHighScoreProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_HighScore.gameObject.SetActive(false);
        UIQuestManager.Instance.go_HighScoreReward.SetActive(false);
        UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.unlockedHighScore;
        UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.highScoreCompleted.SetActive(true);

        PlayerPrefs.GetInt("QuestHighScoreCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestHighScoreCanClaim");
            UIQuestManager.Instance.txt_HighScoreTitle.text = title.ToString();
            UIQuestManager.Instance.txt_HighScoreRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestHighScoreCompleted") == 0)
            {
                UIQuestManager.Instance.highScoreCompleted.SetActive(false);
                UIQuestManager.Instance.go_HighScoreReward.SetActive(true);
                UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.lockedHighScore;
                UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.highScoreCompleted.SetActive(true);
                UIQuestManager.Instance.go_HighScoreReward.SetActive(false);
                UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.unlockedHighScore;
                UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestHighScoreData();

            currentProgress = PlayerPrefs.GetInt("QuestHighScoreProgress");
            UIQuestManager.Instance.txt_HighScoreTitle.text = PlayerPrefs.GetString("QuestHighScoreTitle").ToString();
            UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.lockedHighScore;
            UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_HighScore.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_HighScoreRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_HighScoreReward.SetActive(true);

            UIQuestManager.Instance.highScoreCompleted.SetActive(false);

            UIQuestManager.Instance.txt_HighScoreProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_HighScoreProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_HighScoreProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_HighScoreProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_HighScore.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_HighScoreRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.lockedHighScore;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_HighScoreProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_HighScoreProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_HighScore.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_HighScoreRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_HighScoreBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_HighScore.sprite = UIQuestManager.Instance.unlockedHighScore;
        }
    }
}

public class QuestInvite : Quest
{
    public QuestInvite(int id)
    : base(id)
    {
        maxProgress = 1;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestInviteProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_Invite.gameObject.SetActive(false);
        UIQuestManager.Instance.go_InviteReward.SetActive(false);
        UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.unlockedInvite;
        UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.inviteCompleted.SetActive(true);

        PlayerPrefs.SetInt("QuestInviteCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestInviteCanClaim");
            UIQuestManager.Instance.txt_InviteTitle.text = title.ToString();
            UIQuestManager.Instance.txt_InviteRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestInviteCompleted") == 0)
            {
                UIQuestManager.Instance.inviteCompleted.SetActive(false);
                UIQuestManager.Instance.go_InviteReward.SetActive(true);
                UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.lockedInvite;
                UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.inviteCompleted.SetActive(true);
                UIQuestManager.Instance.go_InviteReward.SetActive(false);
                UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.unlockedInvite;
                UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestInviteData();

            currentProgress = PlayerPrefs.GetInt("InviteScoreProgress");
            UIQuestManager.Instance.txt_InviteTitle.text = PlayerPrefs.GetString("QuestInviteTitle").ToString();
            UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.lockedInvite;
            UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_Invite.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_InviteRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_InviteReward.SetActive(true);

            UIQuestManager.Instance.inviteCompleted.SetActive(false);

            UIQuestManager.Instance.txt_InviteProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_InviteProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_InviteProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_InviteProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Invite.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_InviteRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.lockedInvite;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_InviteProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_InviteProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Invite.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_InviteRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_InviteBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_Invite.sprite = UIQuestManager.Instance.unlockedInvite;
        }
    }
}

public class QuestSpin : Quest
{
    public QuestSpin(int id)
    : base(id)
    {
        randomProgress[0] = 1;
        randomProgress[1] = 2;
        randomProgress[2] = 3;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestSpinProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_Spin.gameObject.SetActive(false);
        UIQuestManager.Instance.go_SpinReward.SetActive(false);
        UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.unlockedSpin;
        UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.spinCompleted.SetActive(true);

        PlayerPrefs.SetInt("QuestSpinCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestSpinCanClaim");
            UIQuestManager.Instance.txt_SpinTitle.text = title.ToString();
            UIQuestManager.Instance.txt_SpinRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestSpinCompleted") == 0)
            {
                UIQuestManager.Instance.spinCompleted.SetActive(false);
                UIQuestManager.Instance.go_SpinReward.SetActive(true);
                UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.lockedSpin;
                UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.spinCompleted.SetActive(true);
                UIQuestManager.Instance.go_SpinReward.SetActive(false);
                UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.unlockedSpin;
                UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestSpinData();

            currentProgress = PlayerPrefs.GetInt("QuestSpinProgress");
            UIQuestManager.Instance.txt_SpinTitle.text = PlayerPrefs.GetString("QuestSpinTitle").ToString();
            UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.lockedSpin;
            UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_Spin.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_SpinRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_SpinReward.SetActive(true);

            UIQuestManager.Instance.spinCompleted.SetActive(false);

            UIQuestManager.Instance.txt_SpinProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_SpinProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_SpinProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_SpinProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Spin.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_SpinRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.lockedSpin;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_SpinProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_SpinProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_Spin.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_SpinRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_SpinBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_Spin.sprite = UIQuestManager.Instance.unlockedSpin;
        }
    }
}

public class QuestEarnStar : Quest
{
    public QuestEarnStar(int id)
    : base(id)
    {
        randomProgress[0] = 10;
        randomProgress[1] = 15;
        randomProgress[2] = 20;
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("QuestEarnStarProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id && currentProgress < maxProgress)
        {
            IncreaseProgress(amount);
            SetProgressUI();
        }
    }

    public void Complete()
    {
        UIQuestManager.Instance.btn_EarnStar.gameObject.SetActive(false);
        UIQuestManager.Instance.go_EarnStarReward.SetActive(false);
        UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.unlockedEarnStar;
        UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.unlockedBg;
        UIQuestManager.Instance.earnStarCompleted.SetActive(true);

        PlayerPrefs.SetInt("QuestEarnStarCompleted", 1);

        DataManager.Instance.ChangeGemNumber(rewardNumber);
    }

    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {
            SetProgressUI();
            canClaim = PlayerPrefs.GetInt("QuestEarnStarCanClaim");
            UIQuestManager.Instance.txt_EarnStarTitle.text = title.ToString();
            UIQuestManager.Instance.txt_EarnStarRewardNum.text = "+" + rewardNumber.ToString();

            if(PlayerPrefs.GetInt("QuestEarnStarCompleted") == 0)
            {
                UIQuestManager.Instance.earnStarCompleted.SetActive(false);
                UIQuestManager.Instance.go_EarnStarReward.SetActive(true);
                UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.lockedEarnStar;
                UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.lockedBg;
            }
            else
            {
                UIQuestManager.Instance.earnStarCompleted.SetActive(true);
                UIQuestManager.Instance.go_EarnStarReward.SetActive(false);
                UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.unlockedEarnStar;
                UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.unlockedBg;
            }
        }
    }
    
    public override void OnResetUI(int id)
    {
        if(this.id == id)
        {
            DataManager.Instance.InitQuestEarnStarData();

            currentProgress = PlayerPrefs.GetInt("QuestEarnStarProgress");
            UIQuestManager.Instance.txt_EarnStarTitle.text = PlayerPrefs.GetString("QuestEarnStarTitle").ToString();
            UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.lockedEarnStar;
            UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.btn_EarnStar.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_EarnStarRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.go_EarnStarReward.SetActive(true);

            UIQuestManager.Instance.earnStarCompleted.SetActive(false);

            UIQuestManager.Instance.txt_EarnStarProgress.text = "0" + "/" + maxProgress;
            UIQuestManager.Instance.bar_EarnStarProgress.transform.localScale = new Vector3(0f, 1f, 1f);
        }
    }

    public void SetProgressUI()
    {
        float barLength;

        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIQuestManager.Instance.txt_EarnStarProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_EarnStarProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_EarnStar.gameObject.SetActive(false);
            UIQuestManager.Instance.txt_EarnStarRewardNum.gameObject.SetActive(true);
            UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.lockedBg;
            UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.lockedEarnStar;
        }
        else
        {
            barLength = 1f;
            UIQuestManager.Instance.txt_EarnStarProgress.text = currentProgress + "/" + maxProgress;
            UIQuestManager.Instance.bar_EarnStarProgress.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIQuestManager.Instance.btn_EarnStar.gameObject.SetActive(true);
            UIQuestManager.Instance.txt_EarnStarRewardNum.gameObject.SetActive(false);
            UIQuestManager.Instance.img_EarnStarBg.sprite = UIQuestManager.Instance.unlockedBg;
            UIQuestManager.Instance.img_EarnStar.sprite = UIQuestManager.Instance.unlockedEarnStar;
        }
    }
}