﻿using UnityEngine;

public class QuestButton : MonoBehaviour
{
    public void ClaimAds()
    {
        DataManager.Instance.questAds.Complete();
    }

    public void UpAds()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_ADS_ID, 1);
    }  

    public void Claim3Stars()
    {
        DataManager.Instance.quest3Stars.Complete();
    }

    public void Up3Stars()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_3STARS_ID, 1);
    }

    public void ClaimBooster()
    {
        DataManager.Instance.questBooster.Complete();
    }
    
    public void UpBooster()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_BOOSTER_ID, 1);
    }

    public void ClaimHighScore()
    {
        DataManager.Instance.questHighscore.Complete();
    }
    
    public void UpHighScore()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_HIGHSCORE_ID, 1);
    }

    public void ClaimInvite()
    {
        DataManager.Instance.questInvite.Complete();
    }
    
    public void UpInvite()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_INVITE_ID, 1);
    }

    public void ClaimSpin()
    {
        DataManager.Instance.questSpin.Complete();
    }
    
    public void UpSpin()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_SPIN_ID, 1);
    }

    public void ClaimEarnStar()
    {
        DataManager.Instance.questEarnStar.Complete();
    }
    
    public void UpEarnStar()
    {
        DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_EARNSTAR_ID, 5);
    }
}
