﻿using UnityEngine;
using UnityEngine.UI;

public class UIQuestManager : MonoBehaviour
{
    private static UIQuestManager instance;
    public static UIQuestManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<UIQuestManager>(); return instance; } }

    [Header("Change Background")]
    public Sprite unlockedBg;
    public Sprite lockedBg;

    [Header("Ads Image")]
    public Sprite unlockedAds;
    public Sprite lockedAds;

    [Header("3Stars Image")]
    public Sprite unlocked3Stars;
    public Sprite locked3Stars;

    [Header("Booster Image")]
    public Sprite unlockedBooster;
    public Sprite lockedBooster;

    [Header("High score Image")]
    public Sprite unlockedHighScore;
    public Sprite lockedHighScore;

    [Header("Invite Image")]
    public Sprite unlockedInvite;
    public Sprite lockedInvite;

    [Header("Spin Image")]
    public Sprite unlockedSpin;
    public Sprite lockedSpin;

    [Header("EarnStar Image")]
    public Sprite unlockedEarnStar;
    public Sprite lockedEarnStar;

    [Header("Quest ADS")]
    public Text txt_AdsTitle;
    public Image img_Ads;
    public Text txt_AdsProgress;
    public Text txt_AdsRewardNum;
    public GameObject bar_AdsProgress;
    public Button btn_Ads;
    public Image img_AdsBg;
    public GameObject adsCompleted;
    public GameObject go_AdsReward;


    [Header("Quest Booster")]
    public Text txt_BoosterTitle;
    public Image img_Booster;
    public Text txt_BoosterProgress;
    public Text txt_BoosterRewardNum;
    public GameObject bar_BoosterProgress;
    public Button btn_Booster;
    public Image img_BoosterBg;
    public GameObject boosterCompleted;
    public GameObject go_BoosterReward;


    [Header("Quest High Score")]
    public Text txt_HighScoreTitle;
    public Image img_HighScore;
    public Text txt_HighScoreProgress;
    public Text txt_HighScoreRewardNum;
    public GameObject bar_HighScoreProgress;
    public Button btn_HighScore;
    public Image img_HighScoreBg;
    public GameObject highScoreCompleted;
    public GameObject go_HighScoreReward;


    [Header("Quest 3 Stars")]
    public Text txt_3StarsTitle;
    public Image img_3Stars;
    public Text txt_3StarsProgress;
    public Text txt_3StarsRewardNum;
    public GameObject bar_3StarsProgress;
    public Button btn_3Stars;
    public Image img_3StarsBg;
    public GameObject stars3Completed;
    public GameObject go_3starsReward;


    [Header("Quest Invite")]
    public Text txt_InviteTitle;
    public Image img_Invite;
    public Text txt_InviteProgress;
    public Text txt_InviteRewardNum;
    public GameObject bar_InviteProgress;
    public Button btn_Invite;
    public Image img_InviteBg;
    public GameObject inviteCompleted;
    public GameObject go_InviteReward;


    [Header("Quest Spin")]
    public Text txt_SpinTitle;
    public Image img_Spin;
    public Text txt_SpinProgress;
    public Text txt_SpinRewardNum;
    public GameObject bar_SpinProgress;
    public Button btn_Spin;
    public Image img_SpinBg;
    public GameObject spinCompleted;
    public GameObject go_SpinReward;


    [Header("Quest Earn Star")]
    public Text txt_EarnStarTitle;
    public Image img_EarnStar;
    public Text txt_EarnStarProgress;
    public Text txt_EarnStarRewardNum;
    public GameObject bar_EarnStarProgress;
    public Button btn_EarnStar;
    public Image img_EarnStarBg;
    public GameObject earnStarCompleted;
    public GameObject go_EarnStarReward;
}
