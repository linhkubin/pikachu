﻿using UnityEngine;

public abstract class QuestObserver
{
    public abstract void OnNotify(int id, int amount);
    public abstract void OnSetUpUI(int id);
    public abstract void OnResetUI(int id);
}
