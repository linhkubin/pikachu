﻿public class AchievementConstant
{
    //-----------------------Achievement ID--------------------------
    public static int ACHIEVEMENT_LEVEL_ID = 0;
    public static int ACHIEVEMENT_3STARS_ID = 1;
    public static int ACHIEVEMENT_BOOSTER_ID = 2;
    public static int ACHIEVEMENT_30DAYS_ID = 3;
    public static int ACHIEVEMENT_SCORE_ID = 4;
    public static int ACHIEVEMENT_FREE_ID = 5;
    public static int ACHIEVEMENT_INVITE_ID = 6;

    //-----------------------Achievement LEVEL--------------------------
    public static string ACHIEVEMENT_LEVEL50_TITLE = "COMBATANT";
    public static string ACHIEVEMENT_LEVEL100_TITLE = "GUARDIAN";
    public static string ACHIEVEMENT_LEVEL200_TITLE = "MASTER GUARDIAN";
    public static string ACHIEVEMENT_LEVEL50_DES = "PASS THROUGH LV.50";
    public static string ACHIEVEMENT_LEVEL100_DES = "PASS THROUGH LV.100";
    public static string ACHIEVEMENT_LEVEL200_DES = "PASS THROUGH LV.200";

    //-----------------------Achievement 3STARS--------------------------
    public static string ACHIEVEMENT_STARS10_TITLE = "BRONZE STAR";
    public static string ACHIEVEMENT_STARS30_TITLE = "IRON STAR";
    public static string ACHIEVEMENT_STARS60_TITLE = "SILVER STAR";
    public static string ACHIEVEMENT_STARS100_TITLE = "GOLDEN STAR";
    public static string ACHIEVEMENT_STARS200_TITLE = "DIAMOND STAR";
    public static string ACHIEVEMENT_STARS10_DES = "PASS 10 LEVELS WITH 3 STARS";
    public static string ACHIEVEMENT_STARS30_DES = "PASS 30 LEVELS WITH 3 STARS";
    public static string ACHIEVEMENT_STARS60_DES = "PASS 60 LEVELS WITH 3 STARS";
    public static string ACHIEVEMENT_STARS100_DES = "PASS 100 LEVELS WITH 3 STARS";
    public static string ACHIEVEMENT_STARS200_DES = "PASS 200 LEVELS WITH 3 STARS";

    //-----------------------Achievement BOOSTER--------------------------
    public static string ACHIEVEMENT_BOOSTER5_TITLE = "BRONZE BOOSTER";
    public static string ACHIEVEMENT_BOOSTER20_TITLE = "IRON BOOSTER";
    public static string ACHIEVEMENT_BOOSTER50_TITLE = "SILVER BOOSTER";
    public static string ACHIEVEMENT_BOOSTER100_TITLE = "GOLDEN BOOSTER";
    public static string ACHIEVEMENT_BOOSTER200_TITLE = "DIAMOND BOOSTER";
    public static string ACHIEVEMENT_BOOSTER5_DES = "USE 5 BOOSTERS IN SPECIFIC LEVEL";
    public static string ACHIEVEMENT_BOOSTER20_DES = "USE 20 BOOSTERS IN SPECIFIC LEVEL";
    public static string ACHIEVEMENT_BOOSTER50_DES = "USE 50 BOOSTERS IN SPECIFIC LEVEL";
    public static string ACHIEVEMENT_BOOSTER100_DES = "USE 100 BOOSTERS IN SPECIFIC LEVEL";
    public static string ACHIEVEMENT_BOOSTER200_DES = "USE 200 BOOSTERS IN SPECIFIC LEVEL";

    //-----------------------Achievement Skin-------------------------
    public static string ACHIEVEMENT_30DAYS_TITLE = "30 DAYS CHECK IN";
    public static string ACHIEVEMENT_30DAYS_DES = "CHECK IN 30 DAYS";

    //-----------------------Achievement Score--------------------------
    public static string ACHIEVEMENT_SCORE10K_TITLE = "SLAVE";
    public static string ACHIEVEMENT_SCORE15K_TITLE = "FARMER";
    public static string ACHIEVEMENT_SCORE20K_TITLE = "WARRIOR";
    public static string ACHIEVEMENT_SCORE30K_TITLE = "KNIGHT";
    public static string ACHIEVEMENT_SCORE50K_TITLE = "KING";
    public static string ACHIEVEMENT_SCORE10K_DES = "GET 10000 SCORES";
    public static string ACHIEVEMENT_SCORE15K_DES = "GET 15000 SCORES";
    public static string ACHIEVEMENT_SCORE20K_DES = "GET 20000 SCORES";
    public static string ACHIEVEMENT_SCORE30K_DES = "GET 30000 SCORES";
    public static string ACHIEVEMENT_SCORE50K_DES = "GET 50000 SCORES";

    //-----------------------Achievement Free--------------------------
    public static string ACHIEVEMENT_FREE100_TITLE = "PAWN";
    public static string ACHIEVEMENT_FREE500_TITLE = "HOOK";
    public static string ACHIEVEMENT_FREE2K_TITLE = "BISHOP";
    public static string ACHIEVEMENT_FREE5K_TITLE = "KNIGHT";
    public static string ACHIEVEMENT_FREE10K_TITLE = "KING";
    public static string ACHIEVEMENT_FREE100_DES = "FREE 100 COUPLE OF NODES";
    public static string ACHIEVEMENT_FREE500_DES = "FREE 500 COUPLE OF NODES";
    public static string ACHIEVEMENT_FREE2K_DES = "FREE 2000 COUPLE OF NODES";
    public static string ACHIEVEMENT_FREE5K_DES = "FREE 5000 COUPLE OF NODES";
    public static string ACHIEVEMENT_FREE10K_DES = "FREE 10000 COUPLE OF NODES";

    //-----------------------Achievement Invite--------------------------
    public static string ACHIEVEMENT_INVITE1_TITLE = "BRONZE INVITATION";
    public static string ACHIEVEMENT_INVITE3_TITLE = "IRON INVITATION";
    public static string ACHIEVEMENT_INVITE10_TITLE = "SILVER INVITATION";
    public static string ACHIEVEMENT_INVITE20_TITLE = "GOLDEN INVITATION";
    public static string ACHIEVEMENT_INVITE40_TITLE = "DIAMOND INVITATION";
    public static string ACHIEVEMENT_INVITE1_DES = "1 INVITED FRIEND";
    public static string ACHIEVEMENT_INVITE3_DES = "3 INVITED FRIENDS";
    public static string ACHIEVEMENT_INVITE10_DES = "10 INVITED FRIENDS";
    public static string ACHIEVEMENT_INVITE20_DES = "20 INVITED FRIENDS";
    public static string ACHIEVEMENT_INVITE40_DES = "40 INVITED FRIENDS";
}
