﻿using System.Collections.Generic;

public class Subject
{
    List<Observer> observers = new List<Observer>();

    //Send notifications if something has happened
    public void Notify(int id, int amount)
    {
        int len = observers.Count;

        for (int i = 0; i < len; i++) {
            observers[i].OnNotify(id, amount);
        }
    }

    public void SetUpUI(int id)
    {
        int len = observers.Count;
        
        for (int i = 0; i < len; i++) {
            observers[i].OnSetUpUI(id);
        }
    }

    //Add observer to the list
    public void AddObserver(Observer observer)
    {
        observers.Add(observer);
    }
}