﻿public abstract class Observer
{
    public abstract void OnNotify(int id, int amount);
    public abstract void OnSetUpUI(int id);
}