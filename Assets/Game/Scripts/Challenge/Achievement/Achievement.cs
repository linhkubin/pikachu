﻿using UnityEngine;
public class Achievement : Observer
{
    public int id;
    public string title;
    public string description;
    public int level;
    public int maxLevel;
    public int currentProgress;
    public int maxProgress;
    public int rewardNumber;

    public Achievement(int id)
    {
        this.id = id;
    }

    public void SetupProgress(string title, string des, int level, int currentProgress, int maxProgress, int rewardNumber)
    {
        this.title = title;
        this.description = des;
        this.level = level;
        this.currentProgress = currentProgress;
        this.maxProgress = maxProgress;
        this.rewardNumber = rewardNumber;
    }

    public override void OnNotify(int id, int amount)
    {

    }

    public override void OnSetUpUI(int id)
    {

    }

    public virtual bool IsCompleted()
    {
        return currentProgress >= maxProgress ? true : false;
    }
}

public class AchievementLevel : Achievement
{
    public AchievementLevel(int id) : base(id)
    {
        maxLevel = 3;
        // id = AchievementConstant.ACHIEVEMENT_LEVEL_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 3)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.lvTitle.text = title.ToString();
                UIAchievementManager.Instance.lvDes.text = description.ToString();
                UIAchievementManager.Instance.lvRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.lvClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.lvRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.lvClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.lvRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[4];
                UIAchievementManager.Instance.lvTitle.text = AchievementConstant.ACHIEVEMENT_LEVEL200_TITLE.ToString();
                UIAchievementManager.Instance.lvDes.text = AchievementConstant.ACHIEVEMENT_LEVEL200_DES.ToString();
                UIAchievementManager.Instance.lvReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.lvCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement Level", false);
            }
        }   
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("LevelProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("LevelLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("LevelLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[4];
                UIAchievementManager.Instance.lvReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.lvCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement Level", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.lvProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.lvProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.lvClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.lvRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.lvProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.lvProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.lvClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.lvRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.lvBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("LevelRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);
        // Debug.Log(rewardNumber);
        switch(level)
        {
            case 1:
            {
                maxProgress = 50;
                PlayerPrefs.SetInt("LevelRewardNumber", 1);
                UIAchievementManager.Instance.lvRewardNumber.text = "+" + PlayerPrefs.GetInt("LevelRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 100;
                PlayerPrefs.SetInt("LevelRewardNumber", 2);
                UIAchievementManager.Instance.lvRewardNumber.text = "+" + PlayerPrefs.GetInt("LevelRewardNumber");

                PlayerPrefs.SetInt("LevelMaxProgress", maxProgress);

                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[3];

                UIAchievementManager.Instance.lvTitle.text = AchievementConstant.ACHIEVEMENT_LEVEL100_TITLE.ToString();
                title = UIAchievementManager.Instance.lvTitle.text;
                PlayerPrefs.SetString("LevelTitle", title);

                UIAchievementManager.Instance.lvDes.text = AchievementConstant.ACHIEVEMENT_LEVEL100_DES.ToString();
                description = UIAchievementManager.Instance.lvDes.text;
                PlayerPrefs.SetString("LevelDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 200;
                PlayerPrefs.SetInt("LevelRewardNumber", 3);
                UIAchievementManager.Instance.lvRewardNumber.text = "+" + PlayerPrefs.GetInt("LevelRewardNumber");

                PlayerPrefs.SetInt("LevelMaxProgress", maxProgress);

                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[5];

                UIAchievementManager.Instance.lvTitle.text = AchievementConstant.ACHIEVEMENT_LEVEL200_TITLE.ToString();
                title = UIAchievementManager.Instance.lvTitle.text;
                PlayerPrefs.SetString("LevelTitle", title);

                UIAchievementManager.Instance.lvDes.text = AchievementConstant.ACHIEVEMENT_LEVEL200_DES.ToString();
                description = UIAchievementManager.Instance.lvDes.text;
                PlayerPrefs.SetString("LevelDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[1];
                    break;
                }

                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[3];
                    break;
                }

                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[5];
                    break;
                }

                UIAchievementManager.Instance.lvImage.sprite = UIAchievementManager.Instance.arcLevelImage[4];
                break;
            }
        }
    }
}

public class Achievement3Stars : Achievement
{
    public Achievement3Stars(int id) : base(id)
    {
        maxLevel = 5;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 5)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.starsTitle.text = title.ToString();
                UIAchievementManager.Instance.starsDes.text = description.ToString();
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.starsClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.starsRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.starsClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.starsRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[8];
                UIAchievementManager.Instance.starsTitle.text = AchievementConstant.ACHIEVEMENT_STARS200_TITLE.ToString();
                UIAchievementManager.Instance.starsDes.text = AchievementConstant.ACHIEVEMENT_STARS200_DES.ToString();
                UIAchievementManager.Instance.starsReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.starsCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement 3Stars", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("3StarsProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("3StarsLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("3StarsLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[8];
                UIAchievementManager.Instance.starsReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.starsCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement 3Stars", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.starsProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.starsProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.starsClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.starsRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
            
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.starsProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.starsProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.starsClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.starsRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.starsBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("3StarsRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);
        
        switch(level)
        {
            case 1:
            {
                maxProgress = 10;
                PlayerPrefs.SetInt("3StarsRewardNumber", 1);
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + PlayerPrefs.GetInt("3StarsRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 30;
                PlayerPrefs.SetInt("3StarsRewardNumber", 2);
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + PlayerPrefs.GetInt("3StarsRewardNumber");

                PlayerPrefs.SetInt("3StarsMaxProgress", maxProgress);

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[3];
                UIAchievementManager.Instance.starsTitle.text = AchievementConstant.ACHIEVEMENT_STARS30_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_STARS30_TITLE.ToString();
                PlayerPrefs.SetString("3StarsTitle", title);

                UIAchievementManager.Instance.starsDes.text = AchievementConstant.ACHIEVEMENT_STARS30_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_STARS30_DES.ToString();
                PlayerPrefs.SetString("3StarsDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 60;
                PlayerPrefs.SetInt("3StarsRewardNumber", 3);
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + PlayerPrefs.GetInt("3StarsRewardNumber");

                PlayerPrefs.SetInt("3StarsMaxProgress", maxProgress);

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[5];

                UIAchievementManager.Instance.starsTitle.text = AchievementConstant.ACHIEVEMENT_STARS60_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_STARS60_TITLE.ToString();
                PlayerPrefs.SetString("3StarsTitle", title);

                UIAchievementManager.Instance.starsDes.text = AchievementConstant.ACHIEVEMENT_STARS60_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_STARS60_DES.ToString();
                PlayerPrefs.SetString("3StarsDes", description);
                break;
            }
            case 4:
            {
                maxProgress = 100;
                PlayerPrefs.SetInt("3StarsRewardNumber", 4);
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + PlayerPrefs.GetInt("3StarsRewardNumber");

                PlayerPrefs.SetInt("3StarsMaxProgress", maxProgress);

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[7];

                UIAchievementManager.Instance.starsTitle.text = AchievementConstant.ACHIEVEMENT_STARS100_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_STARS100_TITLE.ToString();
                PlayerPrefs.SetString("3StarsTitle", title);

                UIAchievementManager.Instance.starsDes.text = AchievementConstant.ACHIEVEMENT_STARS100_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_STARS100_DES.ToString();
                PlayerPrefs.SetString("3StarsDes", description);
                break;
            }
            case 5:
            {
                maxProgress = 200;
                PlayerPrefs.SetInt("3StarsRewardNumber", 5);
                UIAchievementManager.Instance.starsRewardNumber.text = "+" + PlayerPrefs.GetInt("3StarsRewardNumber");

                PlayerPrefs.SetInt("3StarsMaxProgress", maxProgress);

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[9];

                UIAchievementManager.Instance.starsTitle.text = AchievementConstant.ACHIEVEMENT_STARS200_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_STARS200_TITLE.ToString();
                PlayerPrefs.SetString("3StarsTitle", title);

                UIAchievementManager.Instance.starsDes.text = AchievementConstant.ACHIEVEMENT_STARS200_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_STARS200_DES.ToString();
                PlayerPrefs.SetString("3StarsDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[1];
                    break;
                }

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[3];
                    break;
                }

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[5];
                    break;
                }

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[4];
                break;
            }
            case 4:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[7];
                    break;
                }

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[6];
                break;
            }
            case 5:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[9];
                    break;
                }

                UIAchievementManager.Instance.starsImage.sprite = UIAchievementManager.Instance.arc3StarsImage[8];
                break;
            }
        }
    }
}

public class AchievementBooster : Achievement
{
    public AchievementBooster(int id) : base(id)
    {
        maxLevel = 5;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 5)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.boosterTitle.text = title.ToString();
                UIAchievementManager.Instance.boosterDes.text = description.ToString();
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.boosterClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.boosterRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.boosterClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.boosterRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[8];
                UIAchievementManager.Instance.boosterTitle.text = AchievementConstant.ACHIEVEMENT_BOOSTER200_TITLE.ToString();
                UIAchievementManager.Instance.boosterDes.text = AchievementConstant.ACHIEVEMENT_BOOSTER200_DES.ToString();
                UIAchievementManager.Instance.boosterReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.boosterCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement Booster", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("BoosterProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("BoosterLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("BoosterLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[8];
                UIAchievementManager.Instance.boosterReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.boosterCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement Booster", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.boosterProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.boosterProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.boosterClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.boosterRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
            
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.boosterProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.boosterProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.boosterClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.boosterRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.boosterBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("BoosterRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);

        switch(level)
        {
            case 1:
            {
                maxProgress = 5;
                PlayerPrefs.SetInt("BoosterRewardNumber", 1);
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + PlayerPrefs.GetInt("BoosterRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 20;
                PlayerPrefs.SetInt("BoosterRewardNumber", 2);
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + PlayerPrefs.GetInt("BoosterRewardNumber");

                PlayerPrefs.SetInt("BoosterMaxProgress", maxProgress);

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[3];

                UIAchievementManager.Instance.boosterTitle.text = AchievementConstant.ACHIEVEMENT_BOOSTER20_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_BOOSTER20_TITLE.ToString();
                PlayerPrefs.SetString("BoosterTitle", title);

                UIAchievementManager.Instance.boosterDes.text = AchievementConstant.ACHIEVEMENT_BOOSTER20_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_BOOSTER20_DES.ToString();
                PlayerPrefs.SetString("BoosterDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 50;
                PlayerPrefs.SetInt("BoosterRewardNumber", 3);
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + PlayerPrefs.GetInt("BoosterRewardNumber");

                PlayerPrefs.SetInt("BoosterMaxProgress", maxProgress);

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[3];

                UIAchievementManager.Instance.boosterTitle.text = AchievementConstant.ACHIEVEMENT_BOOSTER50_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_BOOSTER50_TITLE.ToString();
                PlayerPrefs.SetString("BoosterTitle", title);

                UIAchievementManager.Instance.boosterDes.text = AchievementConstant.ACHIEVEMENT_BOOSTER50_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_BOOSTER50_DES.ToString();
                PlayerPrefs.SetString("BoosterDes", description);
                break;
            }
            case 4:
            {
                maxProgress = 100;
                PlayerPrefs.SetInt("BoosterRewardNumber", 4);
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + PlayerPrefs.GetInt("BoosterRewardNumber");

                PlayerPrefs.SetInt("BoosterMaxProgress", maxProgress);

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[3];

                UIAchievementManager.Instance.boosterTitle.text = AchievementConstant.ACHIEVEMENT_BOOSTER100_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_BOOSTER100_TITLE.ToString();
                PlayerPrefs.SetString("BoosterTitle", title);

                UIAchievementManager.Instance.boosterDes.text = AchievementConstant.ACHIEVEMENT_BOOSTER100_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_BOOSTER100_DES.ToString();
                PlayerPrefs.SetString("BoosterDes", description);
                break;
            }
            case 5:
            {
                maxProgress = 200;
                PlayerPrefs.SetInt("BoosterRewardNumber", 5);
                UIAchievementManager.Instance.boosterRewardNumber.text = "+" + PlayerPrefs.GetInt("BoosterRewardNumber");

                PlayerPrefs.SetInt("BoosterMaxProgress", maxProgress);

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[3];

                UIAchievementManager.Instance.boosterTitle.text = AchievementConstant.ACHIEVEMENT_BOOSTER200_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_BOOSTER200_TITLE.ToString();
                PlayerPrefs.SetString("BoosterTitle", title);

                UIAchievementManager.Instance.boosterDes.text = AchievementConstant.ACHIEVEMENT_BOOSTER200_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_BOOSTER200_DES.ToString();
                PlayerPrefs.SetString("BoosterDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[1];
                    break;
                }

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[3];
                    break;
                }

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[5];
                    break;
                }

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[4];
                break;
            }
            case 4:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[7];
                    break;
                }

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[6];
                break;
            }
            case 5:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[9];
                    break;
                }

                UIAchievementManager.Instance.boosterImage.sprite = UIAchievementManager.Instance.arcBoosterImage[8];
                break;
            }
        }
    }
}

public class Achievement30Days : Achievement
{
    public Achievement30Days(int id) : base(id)
    {
        maxLevel = 1;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 1)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.checkInTitle.text = title.ToString();
                UIAchievementManager.Instance.checkInDes.text = description.ToString();
                UIAchievementManager.Instance.checkInRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.checkInClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.checkInRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.checkInClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.checkInRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.checkInImage.sprite = UIAchievementManager.Instance.arc30DaysImage[0];
                UIAchievementManager.Instance.checkInTitle.text = AchievementConstant.ACHIEVEMENT_30DAYS_TITLE.ToString();
                UIAchievementManager.Instance.checkInDes.text = AchievementConstant.ACHIEVEMENT_30DAYS_DES.ToString();
                UIAchievementManager.Instance.checkInReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.checkInCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement 30 Days", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("30DaysProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("30DaysLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("30DaysLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.checkInImage.sprite = UIAchievementManager.Instance.arc30DaysImage[0];
                UIAchievementManager.Instance.checkInReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.checkInCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement 30 Days", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.checkInProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.checkInProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.checkInClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.checkInRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.checkInProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.checkInProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.checkInClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.checkInRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.checkInBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("30DaysRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);

        switch(level)
        {
            case 1:
            {
                maxProgress = 30;
                PlayerPrefs.SetInt("30DaysRewardNumber", 1);
                UIAchievementManager.Instance.checkInRewardNumber.text = "+" + PlayerPrefs.GetInt("30DaysRewardNumber");
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.checkInImage.sprite = UIAchievementManager.Instance.arc30DaysImage[1];
                    break;
                }

                UIAchievementManager.Instance.checkInImage.sprite = UIAchievementManager.Instance.arc30DaysImage[0];
                break;
            }
        }
    }
}

public class AchievementScore : Achievement
{
    public AchievementScore(int id) : base(id)
    {
        maxLevel = 5;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 5)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.scoreTitle.text = title.ToString();
                UIAchievementManager.Instance.scoreDes.text = description.ToString();
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.scoreClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.scoreRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.scoreClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.scoreRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[8];
                UIAchievementManager.Instance.scoreTitle.text = AchievementConstant.ACHIEVEMENT_SCORE50K_TITLE.ToString();
                UIAchievementManager.Instance.scoreDes.text = AchievementConstant.ACHIEVEMENT_SCORE50K_DES.ToString();
                UIAchievementManager.Instance.scoreReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.scoreCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement Score", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("ScoreProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("ScoreLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("ScoreLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[8];
                UIAchievementManager.Instance.scoreReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.scoreCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement Score", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.scoreProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.scoreProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.scoreClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.scoreRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
            
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.scoreProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.scoreProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.scoreClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.scoreRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.scoreBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("ScoreRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);

        switch(level)
        {
            case 1:
            {
                maxProgress = 10000;
                PlayerPrefs.SetInt("ScoreRewardNumber", 1);
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + PlayerPrefs.GetInt("ScoreRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 15000;
                PlayerPrefs.SetInt("ScoreRewardNumber", 2);
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + PlayerPrefs.GetInt("ScoreRewardNumber");

                PlayerPrefs.SetInt("ScoreMaxProgress", maxProgress);

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[3];

                UIAchievementManager.Instance.scoreTitle.text = AchievementConstant.ACHIEVEMENT_SCORE15K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_SCORE15K_TITLE.ToString();
                PlayerPrefs.SetString("ScoreTitle", title);

                UIAchievementManager.Instance.scoreDes.text = AchievementConstant.ACHIEVEMENT_SCORE15K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_SCORE15K_DES.ToString();
                PlayerPrefs.SetString("ScoreDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 20000;
                PlayerPrefs.SetInt("ScoreRewardNumber", 3);
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + PlayerPrefs.GetInt("ScoreRewardNumber");

                PlayerPrefs.SetInt("ScoreMaxProgress", maxProgress);

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[5];

                UIAchievementManager.Instance.scoreTitle.text = AchievementConstant.ACHIEVEMENT_SCORE20K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_SCORE20K_TITLE.ToString();
                PlayerPrefs.SetString("ScoreTitle", title);

                UIAchievementManager.Instance.scoreDes.text = AchievementConstant.ACHIEVEMENT_SCORE20K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_SCORE20K_DES.ToString();
                PlayerPrefs.SetString("ScoreDes", description);
                break;
            }
            case 4:
            {
                maxProgress = 30000;
                PlayerPrefs.SetInt("ScoreRewardNumber", 4);
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + PlayerPrefs.GetInt("ScoreRewardNumber");

                PlayerPrefs.SetInt("ScoreMaxProgress", maxProgress);

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[7];

                UIAchievementManager.Instance.scoreTitle.text = AchievementConstant.ACHIEVEMENT_SCORE30K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_SCORE30K_TITLE.ToString();
                PlayerPrefs.SetString("ScoreTitle", title);

                UIAchievementManager.Instance.scoreDes.text = AchievementConstant.ACHIEVEMENT_SCORE30K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_SCORE30K_DES.ToString();
                PlayerPrefs.SetString("ScoreDes", description);
                break;
            }
            case 5:
            {
                maxProgress = 50000;
                PlayerPrefs.SetInt("ScoreRewardNumber", 5);
                UIAchievementManager.Instance.scoreRewardNumber.text = "+" + PlayerPrefs.GetInt("ScoreRewardNumber");

                PlayerPrefs.SetInt("ScoreMaxProgress", maxProgress);

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[9];

                UIAchievementManager.Instance.scoreTitle.text = AchievementConstant.ACHIEVEMENT_SCORE50K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_SCORE50K_TITLE.ToString();
                PlayerPrefs.SetString("ScoreTitle", title);

                UIAchievementManager.Instance.scoreDes.text = AchievementConstant.ACHIEVEMENT_SCORE50K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_SCORE50K_DES.ToString();
                PlayerPrefs.SetString("ScoreDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[1];
                    break;
                }

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[3];
                    break;
                }

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[5];
                    break;
                }

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[4];
                break;
            }
            case 4:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[7];
                    break;
                }

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[6];
                break;
            }
            case 5:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[9];
                    break;
                }

                UIAchievementManager.Instance.scoreImage.sprite = UIAchievementManager.Instance.arcScoreImage[8];
                break;
            }
        }
    }
}

public class AchievementFree : Achievement
{
    public AchievementFree(int id) : base(id)
    {
        maxLevel = 5;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 5)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.freeTitle.text = title.ToString();
                UIAchievementManager.Instance.freeDes.text = description.ToString();
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.freeClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.freeRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.freeClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.freeRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[8];
                UIAchievementManager.Instance.freeTitle.text = AchievementConstant.ACHIEVEMENT_FREE10K_TITLE.ToString();
                UIAchievementManager.Instance.freeDes.text = AchievementConstant.ACHIEVEMENT_FREE10K_DES.ToString();
                UIAchievementManager.Instance.freeReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.freeCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement Free", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("FreeProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("FreeLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("FreeLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[8];
                UIAchievementManager.Instance.freeReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.freeCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement Free", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.freeProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.freeProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.freeClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.freeRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
            
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.freeProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.freeProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.freeClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.freeRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.freeBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("FreeRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);

        switch(level)
        {
            case 1:
            {
                maxProgress = 100;
                PlayerPrefs.SetInt("FreeRewardNumber", 1);
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + PlayerPrefs.GetInt("FreeRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 500;
                PlayerPrefs.SetInt("FreeRewardNumber", 2);
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + PlayerPrefs.GetInt("FreeRewardNumber");

                PlayerPrefs.SetInt("FreeMaxProgress", maxProgress);

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[3];

                UIAchievementManager.Instance.freeTitle.text = AchievementConstant.ACHIEVEMENT_FREE500_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_FREE500_TITLE.ToString();
                PlayerPrefs.SetString("FreeTitle", title);

                UIAchievementManager.Instance.freeDes.text = AchievementConstant.ACHIEVEMENT_FREE500_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_FREE500_DES.ToString();
                PlayerPrefs.SetString("FreeDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 2000;
                PlayerPrefs.SetInt("FreeRewardNumber", 3);
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + PlayerPrefs.GetInt("FreeRewardNumber");

                PlayerPrefs.SetInt("FreeMaxProgress", maxProgress);

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[5];

                UIAchievementManager.Instance.freeTitle.text = AchievementConstant.ACHIEVEMENT_FREE2K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_FREE2K_TITLE.ToString();
                PlayerPrefs.SetString("FreeTitle", title);

                UIAchievementManager.Instance.freeDes.text = AchievementConstant.ACHIEVEMENT_FREE2K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_FREE2K_DES.ToString();
                PlayerPrefs.SetString("FreeDes", description);
                break;
            }
            case 4:
            {
                maxProgress = 5000;
                PlayerPrefs.SetInt("FreeRewardNumber", 4);
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + PlayerPrefs.GetInt("FreeRewardNumber");

                PlayerPrefs.SetInt("FreeMaxProgress", maxProgress);

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[7];

                UIAchievementManager.Instance.freeTitle.text = AchievementConstant.ACHIEVEMENT_FREE5K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_FREE5K_TITLE.ToString();
                PlayerPrefs.SetString("FreeTitle", title);

                UIAchievementManager.Instance.freeDes.text = AchievementConstant.ACHIEVEMENT_FREE5K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_FREE5K_DES.ToString();
                PlayerPrefs.SetString("FreeDes", description);
                break;
            }
            case 5:
            {
                maxProgress = 10000;
                PlayerPrefs.SetInt("FreeRewardNumber", 5);
                UIAchievementManager.Instance.freeRewardNumber.text = "+" + PlayerPrefs.GetInt("FreeRewardNumber");

                PlayerPrefs.SetInt("FreeMaxProgress", maxProgress);

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[9];

                UIAchievementManager.Instance.freeTitle.text = AchievementConstant.ACHIEVEMENT_FREE10K_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_FREE10K_TITLE.ToString();
                PlayerPrefs.SetString("FreeTitle", title);

                UIAchievementManager.Instance.freeDes.text = AchievementConstant.ACHIEVEMENT_FREE10K_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_FREE10K_DES.ToString();
                PlayerPrefs.SetString("FreeDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[1];
                    break;
                }

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[3];
                    break;
                }

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[5];
                    break;
                }

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[4];
                break;
            }
            case 4:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[7];
                    break;
                }

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[6];
                break;
            }
            case 5:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[9];
                    break;
                }

                UIAchievementManager.Instance.freeImage.sprite = UIAchievementManager.Instance.arcFreeImage[8];
                break;
            }
        }
    }
}

public class AchievementInvite : Achievement
{
    public AchievementInvite(int id) : base(id)
    {
        maxLevel = 5;
        // id = AchievementConstant.ACHIEVEMENT_3STARS_ID;
    }
    public override void OnSetUpUI(int id)
    {
        if(this.id == id)
        {   
            if(level <= 5)
            {
                SetProgressBar();
                SetAchievementImage();
                UIAchievementManager.Instance.inviteTitle.text = title.ToString();
                UIAchievementManager.Instance.inviteDes.text = description.ToString();
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + rewardNumber.ToString();

                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.completedBackground;
                    UIAchievementManager.Instance.inviteClaimButton.gameObject.SetActive(true);
                    UIAchievementManager.Instance.inviteRewardNumber.gameObject.SetActive(false);
                }
                else
                {
                    UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                    UIAchievementManager.Instance.inviteClaimButton.gameObject.SetActive(false);
                    UIAchievementManager.Instance.inviteRewardNumber.gameObject.SetActive(true);
                }
            }
            else
            {
                SetProgressBar();
                UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[8];
                UIAchievementManager.Instance.inviteTitle.text = AchievementConstant.ACHIEVEMENT_INVITE40_TITLE.ToString();
                UIAchievementManager.Instance.inviteDes.text = AchievementConstant.ACHIEVEMENT_INVITE40_DES.ToString();
                UIAchievementManager.Instance.inviteReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.inviteCompleted.gameObject.SetActive(true);
            }

            if(level > maxLevel)
            {
                UIAchievementManager.Instance.PushDownCompletedArc("Achievement Invite", false);
            }
        }
    }

    public void IncreaseProgress(int amount)
    {
        currentProgress += amount;
        PlayerPrefs.SetInt("InviteProgress", currentProgress);
    }

    public override void OnNotify(int id, int amount)
    {
        if(this.id == id)
        {
            IncreaseProgress(amount);
            
            SetProgressBar();

            SetAchievementImage();
        }
    }

    public void LevelUp()
    {
        if(level < maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("InviteLevel", level);
                
                UpdateAchievement();

                SetAchievementImage();

                SetProgressBar();
            }
        }

        else if(level == maxLevel)
        {
            if(currentProgress >= maxProgress)
            {
                level++;
                PlayerPrefs.SetInt("InviteLevel", level);
                
                UpdateAchievement();
                UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[8];
                UIAchievementManager.Instance.inviteReward.gameObject.SetActive(false);
                UIAchievementManager.Instance.inviteCompleted.gameObject.SetActive(true);
            }
        }

        if(level > maxLevel)
        {
            UIAchievementManager.Instance.PushDownCompletedArc("Achievement Invite", true);
        }
    }

    private void SetProgressBar()
    {
        float barLength;
        if(!IsCompleted())
        {
            barLength = (float)currentProgress / (float)maxProgress;
            UIAchievementManager.Instance.inviteProgressText.text = currentProgress + "/" + maxProgress;
            UIAchievementManager.Instance.inviteProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.inviteClaimButton.gameObject.SetActive(false);
            UIAchievementManager.Instance.inviteRewardNumber.gameObject.SetActive(true);
            UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.unCompletedBackground;
            
        }
        else
        {
            barLength = 1f;
            UIAchievementManager.Instance.inviteProgressText.text = maxProgress + "/" + maxProgress;
            UIAchievementManager.Instance.inviteProgressBar.transform.localScale = new Vector3((float)barLength, 1f, 1f);
            UIAchievementManager.Instance.inviteClaimButton.gameObject.SetActive(true);
            UIAchievementManager.Instance.inviteRewardNumber.gameObject.SetActive(false);
            UIAchievementManager.Instance.inviteBackground.sprite = UIAchievementManager.Instance.completedBackground;
        }
    }

    private void UpdateAchievement() {
        rewardNumber = PlayerPrefs.GetInt("InviteRewardNumber");
        DataManager.Instance.ChangeGemNumber(rewardNumber);

        switch(level)
        {
            case 1:
            {
                maxProgress = 1;
                PlayerPrefs.SetInt("InviteRewardNumber", 1);
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + PlayerPrefs.GetInt("InviteRewardNumber");
                break;
            }
            case 2:
            {
                maxProgress = 3;
                PlayerPrefs.SetInt("InviteRewardNumber", 2);
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + PlayerPrefs.GetInt("InviteRewardNumber");

                PlayerPrefs.SetInt("InviteMaxProgress", maxProgress);

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[3];

                UIAchievementManager.Instance.inviteTitle.text = AchievementConstant.ACHIEVEMENT_INVITE3_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_INVITE3_TITLE.ToString();
                PlayerPrefs.SetString("InviteTitle", title);

                UIAchievementManager.Instance.inviteDes.text = AchievementConstant.ACHIEVEMENT_INVITE3_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_INVITE3_DES.ToString();
                PlayerPrefs.SetString("InviteDes", description);

                break;
            }
            case 3:
            {
                maxProgress = 10;
                PlayerPrefs.SetInt("InviteRewardNumber", 3);
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + PlayerPrefs.GetInt("InviteRewardNumber");

                PlayerPrefs.SetInt("InviteMaxProgress", maxProgress);

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[5];

                UIAchievementManager.Instance.inviteTitle.text = AchievementConstant.ACHIEVEMENT_INVITE10_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_INVITE10_TITLE.ToString();
                PlayerPrefs.SetString("InviteTitle", title);

                UIAchievementManager.Instance.inviteDes.text = AchievementConstant.ACHIEVEMENT_INVITE10_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_INVITE10_DES.ToString();
                PlayerPrefs.SetString("InviteDes", description);
                break;
            }
            case 4:
            {
                maxProgress = 20;
                PlayerPrefs.SetInt("InviteRewardNumber", 4);
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + PlayerPrefs.GetInt("InviteRewardNumber");

                PlayerPrefs.SetInt("InviteMaxProgress", maxProgress);

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[7];

                UIAchievementManager.Instance.inviteTitle.text = AchievementConstant.ACHIEVEMENT_INVITE20_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_INVITE20_TITLE.ToString();
                PlayerPrefs.SetString("InviteTitle", title);

                UIAchievementManager.Instance.inviteDes.text = AchievementConstant.ACHIEVEMENT_INVITE20_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_INVITE20_DES.ToString();
                PlayerPrefs.SetString("InviteDes", description);
                break;
            }
            case 5:
            {
                maxProgress = 40;
                PlayerPrefs.SetInt("InviteRewardNumber", 5);
                UIAchievementManager.Instance.inviteRewardNumber.text = "+" + PlayerPrefs.GetInt("InviteRewardNumber");

                PlayerPrefs.SetInt("InviteMaxProgress", maxProgress);

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[9];

                UIAchievementManager.Instance.inviteTitle.text = AchievementConstant.ACHIEVEMENT_INVITE40_TITLE.ToString();
                title = AchievementConstant.ACHIEVEMENT_INVITE40_TITLE.ToString();
                PlayerPrefs.SetString("InviteTitle", title);

                UIAchievementManager.Instance.inviteDes.text = AchievementConstant.ACHIEVEMENT_INVITE40_DES.ToString();
                description = AchievementConstant.ACHIEVEMENT_INVITE40_DES.ToString();
                PlayerPrefs.SetString("InviteDes", description);
                break;
            }
        }
    }

    private void SetAchievementImage()
    {
        switch(level)
        {
            case 1:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[1];
                    break;
                }

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[0];
                break;
            }
            case 2:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[3];
                    break;
                }

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[2];
                break;
            }
            case 3:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[5];
                    break;
                }

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[4];
                break;
            }
            case 4:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[7];
                    break;
                }

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[6];
                break;
            }
            case 5:
            {
                if(IsCompleted())
                {
                    UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[9];
                    break;
                }

                UIAchievementManager.Instance.inviteImage.sprite = UIAchievementManager.Instance.arcInviteImage[8];
                break;
            }
        }
    }
}