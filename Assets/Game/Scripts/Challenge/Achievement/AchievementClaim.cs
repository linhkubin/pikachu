﻿using UnityEngine;

public class AchievementClaim : MonoBehaviour
{
    public void AcLvLevelUp()
    {
        DataManager.Instance.achievementLevel.LevelUp();
    }

    public void Ac3StarsLevelUp()
    {
        DataManager.Instance.achievement3Stars.LevelUp();
    }

    public void AcBoosterLevelUp()
    {
        DataManager.Instance.achievementBooster.LevelUp();
    }

    public void Ac30DaysLevelUp()
    {
        DataManager.Instance.achievement30Days.LevelUp();
    }

    public void AcScoreLevelUp()
    {
        DataManager.Instance.achievementScore.LevelUp();
    }

    public void AcFreeLevelUp()
    {
        DataManager.Instance.achievementFree.LevelUp();
    }

    public void AcInviteLevelUp()
    {
        DataManager.Instance.achievementInvite.LevelUp();
    }
}