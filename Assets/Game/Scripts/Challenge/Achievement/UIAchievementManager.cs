﻿using UnityEngine;
using UnityEngine.UI;

public class UIAchievementManager : MonoBehaviour
{
    private static UIAchievementManager instance;
    public static UIAchievementManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<UIAchievementManager>(); return instance; } }

    [Header("Change Level Image")]
    public Sprite[] arcLevelImage;

    [Header("Change 3 Stars Image")]
    public Sprite[] arc3StarsImage;

    [Header("Change Booster Image")]
    public Sprite[] arcBoosterImage;

    [Header("Change 30Days Image")]
    public Sprite[] arc30DaysImage;

    [Header("Change Score Image")]
    public Sprite[] arcScoreImage;

    [Header("Change Free Image")]
    public Sprite[] arcFreeImage;

    [Header("Change Invite Image")]
    public Sprite[] arcInviteImage;

    [Header("Change Achievement Image")]
    public Sprite unCompletedBackground;
    public Sprite completedBackground;

    [Header("Change Level Image")]
    public Sprite[] changedlevelImage;

    [Header("Achievement Level")]
    public Image lvBackground;
    public Image lvImage;
    public Text lvTitle;
    public Text lvDes;
    public Text lvProgressText;
    public GameObject lvProgressBar;
    public Text lvRewardNumber;
    public Button lvClaimButton;
    public GameObject lvReward;
    public GameObject lvCompleted;

    [Header("Achievement 3Stars")]
    public Image starsBackground;
    public Image starsImage;
    public Text starsTitle;
    public Text starsDes;
    public Text starsProgressText;
    public GameObject starsProgressBar;
    public Text starsRewardNumber;
    public Button starsClaimButton;
    public GameObject starsReward;
    public GameObject starsCompleted;

    [Header("Achievement Booster")]
    public Image boosterBackground;
    public Image boosterImage;
    public Text boosterTitle;
    public Text boosterDes;
    public Text boosterProgressText;
    public GameObject boosterProgressBar;
    public Text boosterRewardNumber;
    public Button boosterClaimButton;
    public GameObject boosterReward;
    public GameObject boosterCompleted;

    [Header("Achievement Skin")]
    public Image checkInBackground;
    public Image checkInImage;
    public Text checkInTitle;
    public Text checkInDes;
    public Text checkInProgressText;
    public GameObject checkInProgressBar;
    public Text checkInRewardNumber;
    public Button checkInClaimButton;
    public GameObject checkInReward;
    public GameObject checkInCompleted;

    [Header("Achievement Score")]
    public Image scoreBackground;
    public Image scoreImage;
    public Text scoreTitle;
    public Text scoreDes;
    public Text scoreProgressText;
    public GameObject scoreProgressBar;
    public Text scoreRewardNumber;
    public Button scoreClaimButton;
    public GameObject scoreReward;
    public GameObject scoreCompleted;

    [Header("Achievement Free")]
    public Image freeBackground;
    public Image freeImage;
    public Text freeTitle;
    public Text freeDes;
    public Text freeProgressText;
    public GameObject freeProgressBar;
    public Text freeRewardNumber;
    public Button freeClaimButton;
    public GameObject freeReward;
    public GameObject freeCompleted;

    [Header("Achievement Invite")]
    public Image inviteBackground;
    public Image inviteImage;
    public Text inviteTitle;
    public Text inviteDes;
    public Text inviteProgressText;
    public GameObject inviteProgressBar;
    public Text inviteRewardNumber;
    public Button inviteClaimButton;
    public GameObject inviteReward;
    public GameObject inviteCompleted;

    [Header("Achievement Content")]
    public GameObject arcContent;

    public void PushDownCompletedArc(string arcName, bool slideDown)
    {
        arcContent.transform.GetChild(arcContent.transform.Find(arcName).GetSiblingIndex()).SetAsLastSibling();

        if(slideDown)
        {
            arcContent.transform.position = new Vector3(arcContent.transform.position.x, 722.4594f, arcContent.transform.position.z);
        }
    }
}