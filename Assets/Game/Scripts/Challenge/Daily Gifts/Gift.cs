﻿using UnityEngine;
using UnityEngine.UI;

public class Gift : MonoBehaviour
{
    public Image background;
    public GameObject light1st;
    public GameObject light2nd;
    public Image reward1st;
    public Image reward2nd;
    public GameObject claim;
    
    public int type;
    public int rewardType1st;
    public int rewardType2nd;
    public int rewardNumber1st;
    public int rewardNumber2nd;
}
