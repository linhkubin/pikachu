﻿using UnityEngine;
using UnityEngine.UI;
using LitJson;
using System.IO;

public class DailyGiftsManager : MonoBehaviour
{
    public static DailyGiftsManager instance;
    public static DailyGiftsManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<DailyGiftsManager>(); return instance; } }

    public GiftImageToJson[] giftImageToJsons = new GiftImageToJson[30];
    
    [Header("Day gameobjects")]
    public GameObject[] days = new GameObject[30];

    [Header("Day backgrounds")]
    public Sprite resetSingleBG;
    public Sprite unlockSingleBG;
    public Sprite resetDoubleBG;
    public Sprite unlockDoubleBG;

    [Header("Day claim index")]
    public int curentGift;

    [Header("Gift button")]
    public GameObject claimReward;
    public GameObject nextReward;
    public Text nextRewardTimeText;

    [Header("Gift Image List")]
    public Sprite[] giftImage = new Sprite[9];


    public int secondRewardPopup;


    private void Start()
    {
        // WriteToJson();

        // ResetDailyGifts();
        ReadFromJson();   
        if(PlayerPrefs.GetInt("GiftClaim") == 1)
        {
            nextReward.SetActive(false);
            claimReward.SetActive(true);
        }

        SetUpGiftsUI();
    }

    public void RandomSingleGifts(int type, Image rewardImage, GameObject day)
    {
        Debug.Log("Random single gift");
        if(type == 0)
        {
            RandomPreBooster(rewardImage, day);
        }
        else if(type == 1)
        {
            RandomBooster(rewardImage, day);
        }
    }

    public void RandomDoubleGifts(int type, Image rewardImage, GameObject day)
    {
        RandomPreBooster(rewardImage, day);
    }




    public void RandomBooster(Image rewardImage, GameObject day)
    {
        int random = Random.Range(3, 7);
        day.GetComponent<Gift>().rewardType1st = random;
        rewardImage.sprite = DailyGiftsManager.Instance.giftImage[random];
    }

    public void RandomPreBooster(Image rewardImage, GameObject day)
    {
        int random = Random.Range(0, 3);
        day.GetComponent<Gift>().rewardType1st = random;
        rewardImage.sprite = DailyGiftsManager.Instance.giftImage[random];
    }





    public void ResetDailyGifts()
    {
        Debug.Log("Reset Daily Gifts");

        PlayerPrefs.SetInt("CurrentGift", -1);
        PlayerPrefs.SetInt("GiftClaim", 0);
        for(int i = 0; i < 30; i++)
        {
            days[i].GetComponent<Gift>().claim.SetActive(false);

            if(days[i].GetComponent<Gift>().type == 0 || days[i].GetComponent<Gift>().type == 1)
            {
                RandomSingleGifts(days[i].GetComponent<Gift>().type, days[i].GetComponent<Gift>().reward1st, days[i]);
            }
            else if(days[i].GetComponent<Gift>().type == 2)
            {
                RandomDoubleGifts(days[i].GetComponent<Gift>().type, days[i].GetComponent<Gift>().reward1st, days[i]);
            }
        }

        WriteToJson();
    }

    public void ClaimGift()
    {
        nextReward.SetActive(true);
        claimReward.SetActive(false);

        PlayerPrefs.SetInt("CurrentGift", PlayerPrefs.GetInt("CurrentGift") + 1);

        DataManager.Instance.questTime.ResetGiftTimeLeft();

        int rewardType;
        int rewardNumber;

        if(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().type != 2)
        {
            Debug.Log(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber1st);
            rewardType = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardType1st;
            rewardNumber = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber1st;
            DetectGiftsToSave(rewardType, rewardNumber);
        }
        else
        {
            Debug.Log(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber1st);
            rewardType = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardType1st;
            rewardNumber = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber1st;
            DetectGiftsToSave(rewardType, rewardNumber);

            Debug.Log(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber2nd);
            rewardType = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardType2nd;
            rewardNumber = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber2nd;
            DetectGiftsToSave(rewardType, rewardNumber);
        }

        if(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().type != 2)
        {
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().background.sprite = resetSingleBG;
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().light1st.SetActive(false);
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().claim.SetActive(true);
        }
        else
        {
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().background.sprite = resetDoubleBG;
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().light1st.SetActive(false);
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().light2nd.SetActive(false);
            days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().claim.SetActive(true);
        }

        if(days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().type != 2)
        {   
            secondRewardPopup = 0;
        }
        else
        {
            secondRewardPopup = 1;
        }

        CUIManager.Instance.OpenRewardPopup();
        CUIManager.Instance.rewardNumber.text = "+" + days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber1st.ToString();
        CUIManager.Instance.rewardName.text = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().reward1st.GetComponent<Image>().sprite.name;
        CUIManager.Instance.rewardImage.GetComponent<SpriteRenderer>().sprite = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().reward1st.GetComponent<Image>().sprite;

        

        if(PlayerPrefs.GetInt("CurrentGift") > 28)
        {
            DailyGiftsManager.Instance.ResetDailyGifts();
        }

        PlayerPrefs.SetInt("GiftClaim", 0);
    }

    public void OpenSecondRewardPopup()
    {
        CUIManager.Instance.OpenRewardPopup();
        CUIManager.Instance.rewardNumber.text = "+" + days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().rewardNumber2nd.ToString();
        CUIManager.Instance.rewardName.text = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().reward1st.GetComponent<Image>().sprite.name;
        CUIManager.Instance.rewardImage.GetComponent<SpriteRenderer>().sprite = days[PlayerPrefs.GetInt("CurrentGift")].GetComponent<Gift>().reward2nd.GetComponent<Image>().sprite;
    }

    public void DetectGiftsToSave(int gift, int rewardNumber)
    {
        Debug.Log(gift);
        Debug.Log(rewardNumber);
        if(gift == 0)
        {
            PlayerPrefs.SetInt("PreBoosterDoubleLink", PlayerPrefs.GetInt("PreBoosterDoubleLink") + rewardNumber);
            BoosterManager.Instance.booster[0].number = PlayerPrefs.GetInt("PreBoosterDoubleLink");
            BoosterManager.Instance.booster[0].Reset();
        }
        else if(gift == 1)
        {
            PlayerPrefs.SetInt("PreBoosterRemoveType", PlayerPrefs.GetInt("PreBoosterRemoveType") + rewardNumber);
            BoosterManager.Instance.booster[1].number = PlayerPrefs.GetInt("PreBoosterRemoveType");
            BoosterManager.Instance.booster[1].Reset();
        }
        else if(gift == 2)
        {
            PlayerPrefs.SetInt("PreBoosterKey", PlayerPrefs.GetInt("PreBoosterKey") + rewardNumber);
            BoosterManager.Instance.booster[2].number =  PlayerPrefs.GetInt("PreBoosterKey");
            BoosterManager.Instance.booster[2].Reset();
        }
        else if(gift == 3)
        {
            PlayerPrefs.SetInt("BoosterTime", PlayerPrefs.GetInt("BoosterTime") + rewardNumber);
            BoosterManager.Instance.booster[3].number = PlayerPrefs.GetInt("BoosterTime");
            BoosterManager.Instance.booster[3].Reset();
        }
        else if(gift == 4)
        {
            PlayerPrefs.SetInt("BoosterHammer", PlayerPrefs.GetInt("BoosterHammer") + rewardNumber);
            BoosterManager.Instance.booster[4].number = PlayerPrefs.GetInt("BoosterHammer");
            BoosterManager.Instance.booster[4].Reset();
        }
        else if(gift == 5)
        {
            PlayerPrefs.SetInt("BoosterMix", PlayerPrefs.GetInt("BoosterMix") + rewardNumber);
            BoosterManager.Instance.booster[5].number = PlayerPrefs.GetInt("BoosterMix");
            BoosterManager.Instance.booster[5].Reset();
        }
        else if(gift == 6)
        {
            PlayerPrefs.SetInt("BoosterHint", PlayerPrefs.GetInt("BoosterHint") + rewardNumber);
            BoosterManager.Instance.booster[6].number = PlayerPrefs.GetInt("BoosterHint");
            BoosterManager.Instance.booster[6].Reset();
        }
        else if(gift == 7)
        {
            Debug.Log("1 hour immortal");
        }
        else if(gift == 8)
        {
            DataManager.instance.ChangeGemNumber(rewardNumber);
        }
    }

    public void UnlockGift()
    {
        PlayerPrefs.SetInt("GiftClaim", 1);

        if(days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().type != 2)
        {
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().background.sprite = unlockSingleBG;
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().light1st.SetActive(true);
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().claim.SetActive(false);
        }
        else
        {
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().background.sprite = unlockDoubleBG;
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().light1st.SetActive(true);
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().light2nd.SetActive(true);
            days[PlayerPrefs.GetInt("CurrentGift") + 1].GetComponent<Gift>().claim.SetActive(false);
        }
    }

    public void SetUpGiftsUI()
    {
        ReadFromJson();

        if(PlayerPrefs.GetInt("GiftClaim") == 1)
        {
            UnlockGift();
        }

        for(int i = 0; i < PlayerPrefs.GetInt("CurrentGift") + 1; i++)
        {
            if(days[i].GetComponent<Gift>().type != 2)
            {
                days[i].GetComponent<Gift>().background.sprite = resetSingleBG;
                days[i].GetComponent<Gift>().light1st.SetActive(false);
                days[i].GetComponent<Gift>().claim.SetActive(true);
            }
            else
            {
                days[i].GetComponent<Gift>().background.sprite = resetDoubleBG;
                days[i].GetComponent<Gift>().light1st.SetActive(false);
                days[i].GetComponent<Gift>().light2nd.SetActive(false);
                days[i].GetComponent<Gift>().claim.SetActive(true);
            }
        }
    }

    public void ReadFromJson()
    {
        string jsonString;
        JsonData giftData;

        jsonString = File.ReadAllText(Application.dataPath + "/Game/Scripts/Challenge/Daily Gifts/Gift Json/GiftsJson.json");
        giftData = JsonMapper.ToObject(jsonString);
        
        // int gift1stIndex;
        // int gift2ndIndex;

        for(int i = 0; i < days.Length; i++)
        {
            if(days[i].GetComponent<Gift>().type != 2)
            {
                days[i].GetComponent<Gift>().reward1st.GetComponent<Image>().sprite = DailyGiftsManager.Instance.giftImage[(int)giftData[i]["image1stIndex"]];
                days[i].GetComponent<Gift>().rewardType1st = (int)giftData[i]["reward1stIndex"];
            }
            else
            {
                days[i].GetComponent<Gift>().reward1st.GetComponent<Image>().sprite = DailyGiftsManager.Instance.giftImage[(int)giftData[i]["image1stIndex"]];
                days[i].GetComponent<Gift>().reward2nd.GetComponent<Image>().sprite = DailyGiftsManager.Instance.giftImage[(int)giftData[i]["image2ndIndex"]];
                days[i].GetComponent<Gift>().rewardType1st = (int)giftData[i]["reward1stIndex"];
                days[i].GetComponent<Gift>().rewardType2nd = (int)giftData[i]["reward2ndIndex"];
            }
        }
    }

    public void WriteToJson()
    {
        Debug.Log("Write to json");

        JsonData giftJson;
        int gift1stIndex;
        int gift2ndIndex;
        int reward1stIndex;
        int reward2ndIndex;

        for(int i = 0; i < days.Length; i++)
        {
            if(days[i].GetComponent<Gift>().type != 2)
            {
                gift1stIndex = days[i].GetComponent<Gift>().rewardType1st;
                reward1stIndex = days[i].GetComponent<Gift>().rewardType1st;
                giftImageToJsons[i] = new GiftImageToJson(gift1stIndex, 99, reward1stIndex, 99);
            }
            else
            {
                gift1stIndex = days[i].GetComponent<Gift>().rewardType1st;
                gift2ndIndex = days[i].GetComponent<Gift>().rewardType2nd;
                reward1stIndex = days[i].GetComponent<Gift>().rewardType1st;
                reward2ndIndex = days[i].GetComponent<Gift>().rewardType2nd;
                giftImageToJsons[i] = new GiftImageToJson(gift1stIndex, gift2ndIndex, reward1stIndex, reward2ndIndex);
            }
        }

        giftJson = JsonMapper.ToJson(giftImageToJsons);
        File.WriteAllText(Application.dataPath + "/Game/Scripts/Challenge/Daily Gifts/Gift Json/GiftsJson.json", giftJson.ToString());
    }
}

public class GiftImageToJson
{
    public int image1stIndex;
    public int image2ndIndex;
    public int reward1stIndex;
    public int reward2ndIndex;

    public GiftImageToJson(int image1stIndex, int image2ndIndex, int reward1stIndex, int reward2ndIndex)
    {
        this.image1stIndex = image1stIndex;
        this.image2ndIndex = image2ndIndex;
        this.reward1stIndex = reward1stIndex;
        this.reward2ndIndex = reward2ndIndex;
    }
}