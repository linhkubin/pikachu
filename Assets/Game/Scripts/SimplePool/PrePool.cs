﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrePool : MonoBehaviour
{

    public GameObject pacticle_Connected;

    public GameObject pacticle_Selected;

    public GameObject g_bonus;

    public GameObject g_line;

    public GameObject g_lineCollider;

    public GameObject g_startime;

    public GameObject g_electric;

    public GameObject g_key;

    public GameObject g_starNode;

    public GameObject g_spawnNode;

    public GameObject g_starRequest;

    public GameObject g_windRight;

    public GameObject g_windLeft;

    public GameObject g_cloudy;

    public GameObject g_starPrebooster;

    public GameObject g_diamond;

    public GameObject g_diamondBroken;

    public GameObject g_heart;

    public GameObject g_heartBroken;

    public GameObject g_timeInBooster;

    public GameObject g_enemyBullet;

    #region Point Suggest
    [Header("PointSuggest")]

    public GameObject pointArrow;

    #endregion

    #region Machine Tele
    [Header("MachineTele")]
    public GameObject g_beginHole;

    public GameObject g_endHole;

    public GameObject g_bulletTrail;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        InitPool();
    }

    void InitPool()
    {

        //SimplePool.Preload(SpriteManager.Instance.s_shadow, 1, "EmptyNode");

        //GameObject go = SimplePool.Spawn("EmptyNode", Vector3.zero, Quaternion.identity);

        //go = SimplePool.Spawn("EmptyNode", Vector3.zero, Quaternion.identity);

        //SimplePool.Despawn(go);

        SimplePool.Preload(pacticle_Connected, 10, pacticle_Connected.name);

        SimplePool.Preload(pacticle_Selected, 10, "Selected");

        SimplePool.Preload(g_line, 4, "Line");

        SimplePool.Preload(g_lineCollider, 6, "LineCollider");

        SimplePool.Preload(g_startime, 10, "StarTime");

        SimplePool.Preload(g_electric, 10, "Electric");

        SimplePool.Preload(g_key, 10, "Key");

        SimplePool.Preload(g_starNode, 6, "StarNode");

        SimplePool.Preload(g_spawnNode, 6, "SpawnNode");

        SimplePool.Preload(g_starRequest, 4, "StarRequest");

        SimplePool.Preload(g_windRight, 2, "WindRight");

        SimplePool.Preload(g_windLeft, 2, "WindLeft");

        SimplePool.Preload(g_cloudy, 2, "Cloudy");

        SimplePool.Preload(g_starPrebooster, 5, "StarPrebooster");

        SimplePool.Preload(g_timeInBooster, 5, "TimeInBooster");

        #region Point Suggest
        SimplePool.Preload(pointArrow, 2, "pointArrow");
        #endregion

        #region Click Buy
        SimplePool.Preload(g_diamond, 1, "Diamond");
        SimplePool.Preload(g_diamondBroken, 1, "DiamondBroken");
        SimplePool.Preload(g_heart, 1, "Heart");
        SimplePool.Preload(g_heartBroken, 1, "HeartBroken");
        #endregion

        #region Machine Tele
        SimplePool.Preload(g_beginHole, 2, "TeleHoleBegin");
        SimplePool.Preload(g_endHole, 2, "TeleHoleEnd");
        SimplePool.Preload(g_bulletTrail, 2, "BulletTrail");
        #endregion

        #region EnemyBullet
        SimplePool.Preload(g_enemyBullet, 2, "EnemyBullet");
        #endregion
        //enemy
        for (int i = 0; i < EnemyManager.Instance.g_enemyPrefabs.Length; i++)
        {
            SimplePool.Preload(EnemyManager.Instance.g_enemyPrefabs[i].enemy, 1, "Enemy"+i);
        }
        //bonus

        SimplePool.Preload(g_bonus, 4, "100");
        //base node
        // name = "1" "2" ---- "10"

        for (int i = 0; i < SpriteManager.Instance.s_baseNode.Length; i++)
        {
            SimplePool.Preload(SpriteManager.Instance.s_baseNode[i].anim, 1, SpriteManager.Instance.s_baseNode[i].number.ToString());
        }

        
        // name wall[1] = "10" "11" ---- "14" wall[2] = "15" ---

        for (int i = 0; i < SpriteManager.Instance.wall.Length; i++)
        {

            //wall node

            for (int j = 0; j < SpriteManager.Instance.wall[i].s_nodeMap.Length; j++)
            {
                SimplePool.Preload(SpriteManager.Instance.wall[i].s_nodeMap[j].anim, 1, SpriteManager.Instance.wall[i].s_nodeMap[j].number.ToString());
            }

            //wall barrier

            for (int j = 0; j < SpriteManager.Instance.wall[i].s_barrier.Length; j++)
            {
                SimplePool.Preload(SpriteManager.Instance.wall[i].s_barrier[j].clear, 1, SpriteManager.Instance.wall[i].s_barrier[j].clear.name);
            }

            //wall box  

            for (int j = 0; j < SpriteManager.Instance.wall[i].s_box.Length; j++)
            {
                SimplePool.Preload(SpriteManager.Instance.wall[i].s_box[j].clear, 1, SpriteManager.Instance.wall[i].s_box[j].clear.name);
            }

        }

        // node type is special

        SimplePool.Preload(SpriteManager.Instance.g_box, 1, SpriteManager.Instance.g_box.name);
        SimplePool.Preload(SpriteManager.Instance.g_bonus, 4, SpriteManager.Instance.g_bonus.name);
        SimplePool.Preload(SpriteManager.Instance.g_freeze.anim, 1, SpriteManager.Instance.g_freeze.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_lock.anim, 1, SpriteManager.Instance.g_lock.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_magnet.anim, 1, SpriteManager.Instance.g_magnet.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_jump.anim, 1, SpriteManager.Instance.g_jump.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_cloud.anim, 1, SpriteManager.Instance.g_cloud.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_link.anim, 1, SpriteManager.Instance.g_link.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_boom.anim, 1, SpriteManager.Instance.g_boom.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_flood.anim, 1, SpriteManager.Instance.g_flood.anim.name);
        SimplePool.Preload(SpriteManager.Instance.g_hat.anim, 1, SpriteManager.Instance.g_hat.anim.name);

        //effect type
        SimplePool.Preload(SpriteManager.Instance.g_freeze.clear, 1, SpriteManager.Instance.g_freeze.clear.name);
        SimplePool.Preload(SpriteManager.Instance.g_link.clear, 1, SpriteManager.Instance.g_link.clear.name);
        SimplePool.Preload(SpriteManager.Instance.g_lock.clear, 1, SpriteManager.Instance.g_lock.clear.name);
        SimplePool.Preload(SpriteManager.Instance.g_cloud.clear, 1, SpriteManager.Instance.g_cloud.clear.name);
    }
}
