﻿using UnityEngine;
using System.Collections;
using System;

public delegate void RefillCallback();
public class TimeRefillUnit
{
    public int m_TimeForRefill = 20 * 60; // 20 minutes
    public int m_MaxVlue = 1;
    public int m_CurrentValue = 10;
    public int m_DefaultValue = 10;
    public DateTime m_LastTimeChange = DateTime.Now;
    public int m_TimeToNextAdd = 0;
    public string m_KeyValue = "LifeTime";
    public string m_KeyTime = "LastTimeAdd";
    public string m_EventTriggerName = "UpdateLife";
    public string m_FullfillEventTriggerName = "UpdateLife";
    private RefillCallback m_RefillCallback = null;
    public TimeRefillUnit()
    {

    }
    public TimeRefillUnit(int refillTime, int maxValue, int defaultValue, string key, string eventTriggerName, string fullfillEventTriggerName = "")
    {
        m_TimeForRefill = refillTime;
        m_MaxVlue = maxValue;
        m_CurrentValue = defaultValue;
        m_DefaultValue = defaultValue;
        m_KeyValue = key;
        m_KeyTime = "LastTimeAdd" + key;
        m_EventTriggerName = eventTriggerName;
        m_FullfillEventTriggerName = fullfillEventTriggerName;
    }
    public void InitNew()
    {
        m_CurrentValue = m_DefaultValue;
    }
    public void Reload()
    {

    }
    public void Load()
    {
        m_CurrentValue = PlayerPrefs.GetInt(m_KeyValue, m_DefaultValue);
        string now = DateTime.Now.ToString();
        m_LastTimeChange = DateTime.Parse(PlayerPrefs.GetString(m_KeyTime, now));
        PreloadData();
    }
    void PreloadData()
    {
        DateTime dateTimeNow = DateTime.Now;
        TimeSpan span = dateTimeNow - m_LastTimeChange;
        double totalSecs = span.TotalSeconds;
        while (m_CurrentValue < m_MaxVlue && totalSecs >= m_TimeForRefill)
        {
            m_CurrentValue++;
            totalSecs -= m_TimeForRefill;
            m_LastTimeChange += TimeSpan.FromSeconds(m_TimeForRefill);
            Save();
            if (m_RefillCallback != null) m_RefillCallback();
        }
        if (m_CurrentValue < m_MaxVlue)
        {
            int val = m_TimeForRefill - (int)totalSecs;
            if (m_TimeToNextAdd != val)
            {
                m_TimeToNextAdd = val;
            }
        }
    }
    public void Save()
    {
        PlayerPrefs.SetInt(m_KeyValue, m_CurrentValue);
        PlayerPrefs.SetString(m_KeyTime, m_LastTimeChange.ToString());
    }
    public void Reset()
    {
        PlayerPrefs.SetInt(m_KeyValue, m_DefaultValue);
        m_CurrentValue = PlayerPrefs.GetInt(m_KeyValue, m_DefaultValue);
        string now = DateTime.Now.ToString();
        PlayerPrefs.SetString(m_KeyTime, now);
        m_LastTimeChange = DateTime.Parse(PlayerPrefs.GetString(m_KeyTime, now));
    }
    void UpdateLastTimeChanged()
    {
        m_LastTimeChange = DateTime.Now;
    }
    public bool Add(int amount)
    {
        if (m_CurrentValue >= m_MaxVlue) return false;
        m_CurrentValue += amount;
        if (m_CurrentValue >= m_MaxVlue)
        {
            m_CurrentValue = m_MaxVlue;
            EventManager.TriggerEvent(m_FullfillEventTriggerName);
        }
        EventManager.TriggerEvent(m_EventTriggerName);
        return true;
    }
    public void Consume(int amount = 1)
    {
        if (m_CurrentValue == m_MaxVlue)
        {
            UpdateLastTimeChanged();
        }
        m_CurrentValue -= amount;
        if (m_CurrentValue < 0)
        {
            m_CurrentValue = 0;
        }
        EventManager.TriggerEvent(m_EventTriggerName);
        Save();
    }
    public void SetupRefillTime(int refillTime)
    {
        m_TimeForRefill = refillTime;
        Save();
    }
    public void SetRefillCallback(RefillCallback callback)
    {
        m_RefillCallback = callback;
    }
    public int GetCurrentValue()
    {
        return m_CurrentValue;
    }
    public string GetTimeToNextAdd(int type, string _defValue)
    {
        if (type == 4)
        {
            string ret = _defValue;
            if (m_CurrentValue <= m_MaxVlue)
            {
                int min = m_TimeToNextAdd / 60;
                int sec = m_TimeToNextAdd % 60;
                int hour = min / 60;
                int day = hour / 24;
                min = min % 60;
                hour = hour % 24;
                ret = (day >= 10 ? day.ToString() : "0" + day.ToString()) + "D:" + (hour >= 10 ? hour.ToString() : "0" + hour.ToString()) + "H:" + (min >= 10 ? min.ToString() : "0" + min.ToString()) + "M:" + (sec >= 10 ? sec.ToString() : "0" + sec.ToString()) + "S";
            }
            return ret;
        }
        else if (type == 3)
        {
            string ret = _defValue;
            if (m_CurrentValue < m_MaxVlue)
            {
                int min = m_TimeToNextAdd / 60;
                int sec = m_TimeToNextAdd % 60;
                int hour = min / 60;
                min = min % 60;
                ret = (hour >= 10 ? hour.ToString() : "0" + hour.ToString()) + ":" + (min >= 10 ? min.ToString() : "0" + min.ToString()) + ":" + (sec >= 10 ? sec.ToString() : "0" + sec.ToString());
            }
            return ret;
        }
        else if (type == 2)
        {
            string ret = _defValue;
            if (m_CurrentValue < m_MaxVlue)
            {
                int min = m_TimeToNextAdd / 60;
                int sec = m_TimeToNextAdd % 60;
                ret = (min >= 10 ? min.ToString() : "0" + min.ToString()) + ":" + (sec >= 10 ? sec.ToString() : "0" + sec.ToString());
            }
            return ret;
        }
        else
        {
            return null;
        }
    }
    public void Update()
    {
        if (m_CurrentValue < m_MaxVlue)
        {
            DateTime now = DateTime.Now;
            TimeSpan span = now - m_LastTimeChange;
            double totalSecs = span.TotalSeconds;
            while (m_CurrentValue < m_MaxVlue && totalSecs >= m_TimeForRefill)
            {
                m_CurrentValue++;
                totalSecs -= m_TimeForRefill;
                m_LastTimeChange += TimeSpan.FromSeconds(m_TimeForRefill);
                EventManager.TriggerEvent(m_EventTriggerName);
                Save();
                if (m_RefillCallback != null) m_RefillCallback();
            }
            if (m_CurrentValue < m_MaxVlue)
            {
                int val = m_TimeForRefill - (int)totalSecs;
                if (m_TimeToNextAdd != val)
                {
                    m_TimeToNextAdd = val;
                }
            }
            else
            {
                m_TimeToNextAdd = 0;
            }

            DataManager.Instance.BuyLives(m_MaxVlue, m_CurrentValue);
            CUIManager.Instance.SetNotFullLivesBtn(m_MaxVlue, m_CurrentValue);
        }
        else
        {
            CUIManager.Instance.SetFullLivesBtn();
        }
    }
}