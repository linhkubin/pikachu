﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapJson
{
    public int map;

    public int numberWall;

    public int timePlay;

    public Star star;

    public int numberTypeMain;

    public int numberTypeMap;

    public int numberTypeRand;

    public MapModeRequest modeRequest;

    public MapModeFindStar modeFindStar;

    public List<Tile> tileList = new List<Tile>();

    public List<EnviAffect> EAList = new List<EnviAffect>();

}


[Serializable]
public class Tile
{
    public int Id;
    public int number;
    public int numType;

    public Tile(int Id, int number, int numType)
    {
        this.Id = Id;
        this.number = number;
        this.numType = numType;
    }
}

[Serializable]
public class EnviAffect
{
    public int type;
    public int timeStart;
    public int timeRemain;

    public EnviAffect(int type, int timeStart, int timeRemain)
    {
        this.type = type;
        this.timeStart = timeStart;
        this.timeRemain = timeRemain;
    }
}

[Serializable]
public class Star
{
    public int first;
    public int second;
    public int third;

    public Star(int first, int second, int third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}

[Serializable]
public class MapModeRequest
{
    public bool modeRequest;
    public RequestNode first;
    public RequestNode second;
    public RequestNode third;
}

[Serializable]
public class RequestNode
{
    public int numberNode;
    public int numberRequest;

    public RequestNode(int number,int request)
    {
        numberNode = number;
        numberRequest = request;
    }
}

[Serializable]
public class MapModeFindStar
{
    public bool modeFindStar;
    public MapModeFindStar(bool isStar)
    {
        modeFindStar = isStar;
    }
}

