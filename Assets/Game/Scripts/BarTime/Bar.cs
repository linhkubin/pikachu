﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
{
    private float fillAmount;

    [SerializeField]
    private GameObject loadingTime;

    private Transform tf_loading;
    private Transform GetTransformLoading()
    {
        if (tf_loading == null)
        {
            tf_loading = loadingTime.transform;
        }
        return tf_loading;
    }

    [SerializeField]
    private float lerpSpeed = 2;

    [SerializeField]
    private Image content;

    [SerializeField]
    private Text text;

    public float MaxValue { get; set; }

    private float f_value;

    public float Value
    {
        set
        {
            fillAmount = Map(value, 0, MaxValue, 0, 1);
            f_value = value;
        }
    }
    // Update is called once per frame
    void Update()
    {
        HandleBar();
    }

    private void HandleBar()
    {
        if (fillAmount != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
            text.text = f_value.ToString("F0");
            if (loadingTime != null)
            {
                loadingTime.transform.localPosition = new Vector3(630 * (content.fillAmount - 0.5f), 0, 0);
            }
        }

    }

    private float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}
