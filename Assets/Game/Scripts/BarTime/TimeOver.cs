﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TimeOver 
{
    [SerializeField]
    private Bar bar;

    private float maxTimer;

    private float currentTimer;

    public float MaxVal
    {
        get
        {
            return maxTimer;
        }

        set
        {
            this.maxTimer = value;
            bar.MaxValue = maxTimer;
        }
    }

    public float CurrentVal
    {
        get
        {
            return currentTimer;
        }

        set
        {
            this.currentTimer = Mathf.Clamp(value, 0, maxTimer);
            bar.Value = currentTimer;
        }
    }

    public void Initialize()
    {
        this.MaxVal = Constant.MAX_TIME_PLAY;
        this.CurrentVal = Constant.MAX_TIME_PLAY;
    }

}
