﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weather : MonoBehaviour
{

    public Sprite[] weatherNotice;

    public GameObject[] image;

    private List<int> weatherNumber = new List<int>();

    int i = 0;

    public void NewNotice(int weather)
    {
        if (weatherNumber.Count > 0)
        {
            for (int check = 0; check < i + 1; check++)
            {
                Debug.Log(weatherNumber[check] + " == " + weather);

                if (weatherNumber[check] == weather)
                {
                    return;
                }
            }
        }

        image[i].GetComponent<Image>().sprite = weatherNotice[weather];
        weatherNumber.Add(weather);
        image[i].SetActive(true);
    }

    public void Reset()
    {
        i = 0;
        weatherNumber.Clear();
        image[0].SetActive(false);
        image[1].SetActive(false);
        image[2].SetActive(false);
    }
}
