﻿using UnityEngine;
using System.Collections;

public class ThunderEffect : MonoBehaviour {

    Vector3 m_StartPos;
    Vector3 m_EndPos;

    public GameObject m_StartObject;
    public GameObject m_EndObject;
    public GameObject m_LineObject;
    public LineRenderer m_LineRenderer;

    public Sprite[] m_SpriteList;

    float m_Timer = 0.0f;
    int currentIndex = 0;

    void Start () {
        Setup(m_StartObject.transform.position, m_EndObject.transform.position);
	}
	void Update () {
        m_Timer -= Time.deltaTime;
        m_LineRenderer.SetPosition(0,m_StartObject.transform.position);
        m_LineRenderer.SetPosition(1,m_EndObject.transform.position);
        if (m_Timer <= 0) {
            m_Timer = 0.05f;
            currentIndex += Random.Range(1, 3);
            if (currentIndex > 2) {
                currentIndex -= 3;                
            }
            m_LineRenderer.material.SetTexture("_MainTex", m_SpriteList[currentIndex].texture);
        }
	}
    public void SetOrderLayer(string layerName, int z) {
        m_LineRenderer.sortingLayerName = layerName;
        m_LineRenderer.sortingOrder  =z;
    }
    public void Setup(Vector3 start, Vector3 end) {
        m_StartObject.transform.position = start;
        m_EndObject.transform.position = end;
        m_LineObject.transform.position = (start + end) / 2;
        Vector3 d = end - start;
        float angle = Vector2.Angle(Vector2.up, new Vector2(d.x, d.y));
        Vector3 cross = Vector3.Cross(Vector2.up, new Vector2(d.x, d.y));
        if (cross.z > 0) {
            angle = 360 - angle;
        }
        m_LineObject.transform.rotation = Quaternion.Euler(0, 0, 90 - angle);
        float distance = d.magnitude;
        m_LineObject.transform.localScale = new Vector3((distance * 100.0f / (350.0f)), 1, 1);
        m_Timer = 0.05f;
        currentIndex = Random.Range(0, 3);
        m_LineRenderer.material.SetTexture("_MainTex", m_SpriteList[currentIndex].texture);
        m_LineRenderer.sortingLayerName = "Effect";
        m_LineRenderer.sortingOrder = 5;
        //StartCoroutine(coFadeIn());
    }
    public void ShowStartPoint(bool isActive) {
        m_StartObject.SetActive(isActive);
    }
    public void ShowEndPoint(bool isActive) {
        m_EndObject.SetActive(isActive);
    }
}
