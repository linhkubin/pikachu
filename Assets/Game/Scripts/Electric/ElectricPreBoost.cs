﻿using UnityEngine;
using System.Collections;

public class ElectricPreBoost : MonoBehaviour
{
    public TrailRenderer trail;

    private float timer;

    public Transform tf_electric;

    private Transform tftarget;

    bool start = false;

    private void Update()
    {
        if (start)
        {
            timer -= 3 * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, tftarget.position, 1f);
            trail.material.mainTextureOffset = new Vector2(timer, 0);
        }
        //CalculatePoints();
       
    }

    public void SetTranform(Transform targetMC)
    {
        timer = 0;
        tftarget = targetMC;
        StartCoroutine(Despawn());

        start = true;
    }

    private IEnumerator Despawn()
    {

        yield return new WaitForSeconds(1.2f);
        start = false;
        SimplePool.Despawn(gameObject);
    }
}
