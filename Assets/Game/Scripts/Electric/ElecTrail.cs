﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElecTrail : MonoBehaviour
{

    public TrailRenderer trail;

    private float timer;

    public GameObject start;
    public GameObject target;

    public Transform tf_electric;

    private Transform tftarget;

    bool active = false;

    // Update is called once per frame
    void Update()
    {
        timer += 3 * Time.deltaTime;
        trail.material.mainTextureOffset = new Vector2(timer, 0);

        if (active)
        {
            tf_electric.transform.position = Vector3.MoveTowards(tf_electric.position, tftarget.position, 2.5f * timer);
        }
        
    } 

    public void SetTranform(Transform targetMC)
    {
        //transformPointA = gameObject.transform;
        //transformPointB = target;
        active = false;

        timer = 0;

        this.tftarget = targetMC;

        trail.transform.localPosition =  Vector3.zero;

        start.transform.localPosition = Vector3.zero;

        target.transform.position = tftarget.position;

        StartCoroutine(Run());
    }

    private IEnumerator Run()
    {

        yield return new WaitForSeconds(0.6f);
        active = true;
        TimeManager.Instance.SubtractTimeByWeb(15);
        MCManager.Instance.Impact();
        yield return new WaitForSeconds(1f);
        active = false;
        yield return new WaitForSeconds(0.7f);
        SimplePool.Despawn(gameObject);
    }

}
