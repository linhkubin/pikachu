﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCManager : MonoBehaviour
{
    private static MCManager instance;
    public static MCManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<MCManager>(); return instance; } }

    public GameObject effectCaution;

    private Animator m_Animation;
    private Animator GetAnimation()
    {
        if (m_Animation == null)
        {
            m_Animation = gameObject.GetComponent<Animator>();
        }
        return m_Animation;
    }

    public void PlayGame()
    {
        GetAnimation().SetTrigger("welcome");
    }

    public void Normal()
    {

        GetAnimation().ResetTrigger("timeout");
        GetAnimation().ResetTrigger("fail");
        GetAnimation().ResetTrigger("complete");
        GetAnimation().SetTrigger("idle");
    }

    public void UpStar()
    {
        GetAnimation().SetTrigger("upstar");
        SoundManager.instance.PlayFxSound("Star");
    }

    public void Knock()
    {
        //Debug.Log("asleep");
        if (!GetAnimation().GetCurrentAnimatorStateInfo(0).IsTag("timeout") && Random.Range(0, 5) <= 2)
        {
            GetAnimation().SetTrigger("knock");
            SoundManager.instance.PlayFxSound("CountTime");
        }
    }

    public void Fail()
    {
        effectCaution.SetActive(false);
        StopAllCoroutines();
        GetAnimation().ResetTrigger("timeout");
        GetAnimation().SetTrigger("fail");

        SoundManager.instance.PlayFxSound("Lose");
    }

    public void LevelComplete()
    {
        effectCaution.SetActive(false);
        StopAllCoroutines();
        GetAnimation().ResetTrigger("timeout");
        GetAnimation().SetTrigger("complete");

        SoundManager.instance.PlayFxSound("Win");
    }

    public void Booster()
    {
        GetAnimation().SetTrigger("booster");
    }

    public void TimeOut()
    {
        if (!GetAnimation().GetCurrentAnimatorStateInfo(0).IsTag("time out"))
        {
            GetAnimation().SetTrigger("timeout");
            SoundManager.instance.Vibrate(1);
            effectCaution.SetActive(true);
        }
        else
        {
            effectCaution.SetActive(false);
        }
    }

    public void Impact()
    {
        GetAnimation().SetTrigger("impact");
    }


}
