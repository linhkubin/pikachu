﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class Node : MonoBehaviour
{

    public GameObject g_faceId;

    private GameObject anim;

    private GameObject select;

    private GameObject effectClear;

    [SerializeField]
    private int i_id;

    public int I_id {
        get { return i_id; }
        set
        {
            i_id = value;
            //ConfigImage();
        }
    }

    [SerializeField]
    private int i_numberNode;

    public int I_numberNode
    {
        get
        {
            return i_numberNode;
        }
        set
        {
            i_numberNode = value;
            //ConfigImage();
        }
    }

    public int i_index { get; set; }

    private bool b_lockClick;

    public bool B_lockClick { get => b_lockClick; set => b_lockClick = value; }

    public Transform target;

    public bool Connecting = false;

    private AnimNode m_AnimNode;
    public AnimNode GetAnimNode()
    {
        if (anim == null)
        {
            return null;
        }
        if (m_AnimNode == null)
        {
            m_AnimNode = anim.GetComponent<AnimNode>();
        }
        return m_AnimNode;
    }

    private Transform m_Transform;
    public Transform GetTransform()
    {
        if (m_Transform == null)
        {
            m_Transform = transform;
        }
        return m_Transform;
    }

    private Transform m_AnimTf;
    public Transform GetAnimTf()
    {
        if (anim == null)
        {
            return null;
        }
        if (m_AnimTf == null)
        {
            m_AnimTf = anim.transform;
        }
        return m_AnimTf;
    }

    private SpriteRenderer m_spriteRenderer;
    public SpriteRenderer GetSpriteRenderer()
    {
        if (m_spriteRenderer == null)
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
        }
        return m_spriteRenderer; 
    }

    public int GetCurrentOrder { get { return 10 * (i_index / Constant.NUMBER_ROW) - 5; } }

    public void Start()
    {
        target = transform;
    }

    //-----------------------------------
    public void Update()
    {
        //GetTransform().position = Vector3.MoveTowards(GetTransform().position, target.position, Time.deltaTime * 4f);
    }

    public void InitSelect()
    {
        if (select == null)
        {
            select = SimplePool.Spawn("Selected", new Vector3( GetTransform().position.x, GetTransform().position.y - 0.12f, GetTransform().position.z), Quaternion.Euler(60,0,0));
            //select = Instantiate(GameManager.Instance.g_selectPrefab);
            //select.transform.SetParent(GetTransform(), false);
            //select.transform.rotation = new Quaternion(60f, 0f, 0f,0f);

            if (I_id != Constant.ID_TYPE_NODE_BONUS)
            {
                GetAnimNode().SetLayer(GetCurrentOrder);
            }
        }

    }

    public void RemoveSelect()
    {
        if (anim != null && select != null && I_id != Constant.ID_TYPE_NODE_BONUS)
        {
            GetAnimNode().SetLayer(-GetCurrentOrder);
        }

        if (select != null)
        {
            SimplePool.Despawn(select);
            select = null;
        }

        //Destroy(select);
    }

    public void OnClick()
    {
        if (b_lockClick && !ItemManager.Instance.useHammer)
        {
            return;
        }
        GameManager.Instance.Choose(gameObject);
    }
    
    public void Move(int index, Transform target,bool isUFO = false)
    {
        Connecting = false;
        if (!isUFO)
        {
            this.i_index = index;
        }
        this.target = target;

        StartCoroutine(MoveNode(isUFO));

        if (g_faceId != null)
        {
            g_faceId.GetComponent<AffectNode>().Move(target);
        }

    }

    private IEnumerator MoveNode(bool isUFO = false)
    {
        bool stopMove = false;
        float speed = Time.deltaTime * 4f;

        if (select != null)
        {
            SetAnim("idle");
            RemoveSelect();
        }

        if (isUFO)
        {
            speed = Time.deltaTime * 14f;
            float ran = UnityEngine.Random.Range(0, 0.5f);
            yield return new WaitForSeconds(ran);
        }

        if (GetAnimTf() != null && target != null)
        {
            B_lockClick = true;

            while (!stopMove && gameObject.activeInHierarchy && GetAnimTf() != null)
            {
                Vector2 direction = target.position - GetAnimTf().position;
                GetAnimTf().position = Vector3.MoveTowards(GetAnimTf().position, target.position, speed);
                stopMove = direction.sqrMagnitude < 0.01f;
                yield return null;
            }

            B_lockClick = false;

        }

        yield return new WaitForSeconds(0.3f);

        if (!isUFO)
        {
            GetTransform().position = target.position;
        }

    }

    public bool IsNodeEmpty()
    {
        if (I_id == Constant.ID_TYPE_NODE && I_numberNode == 0)
        {
            return true;
        }
        return false;
    }

    public bool IsNode()
    {
        if (I_id == Constant.ID_TYPE_NODE && I_numberNode > 0 && !B_lockClick && I_id != Constant.ID_TYPE_NODE_BONUS)
        {
            return true;
        }
        return false;
    }

    public virtual void EmptyNode()
    {
        if (Constant.is_MODE_REQUEST)
        {
            ModeRequest.Instance.CheckRequest(GetTransform().position, I_numberNode);
        }

        RemoveSelect();
        RemoveFace();
        GetSpriteRenderer().color = Color.clear;
        I_id = Constant.ID_TYPE_NODE;
        I_numberNode = 0;
        B_lockClick = true;
        //StopAllCoroutines();
        if (anim != null)
        {
            SimplePool.Despawn(anim);
            anim = null;
            m_AnimTf = null;
            m_AnimNode = null;
        }
    }

    public void RemoveNode()
    {
        StartCoroutine(Empty());
        
    }

    public void SetClear(GameObject face)
    {
        effectClear = face;
    }

    public void SetFace(GameObject face)
    {
        g_faceId = SimplePool.Spawn(face.name, GetTransform().position, Quaternion.identity);
    }

    public void NewNode(int number)
    {
        I_id = Constant.ID_TYPE_NODE;
        I_numberNode = number;
        SetAnim(number);
        SetClear(SpriteManager.Instance.connect);
        B_lockClick = false;
    }


    public void SetAnim(int number = 0)
    {
        anim = SimplePool.Spawn(number.ToString(), GetTransform().position, Quaternion.identity);
    }
    

    public void RemoveFace()
    {
        if (I_id == Constant.ID_TYPE_NODE_BONUS)
        {
            GetAnimNode().SetLayer(-GetCurrentOrder);
        }
        if (i_id == Constant.ID_KEY)
        {
            I_id = Constant.ID_TYPE_NODE;
            g_faceId.GetComponent<Key>().Despawn();
            g_faceId = null;
        }
        else
        if (g_faceId != null )
        {
            StartCoroutine(EffectRemoveFace());
            if (anim != null)
            {
                SetSpeedAnim(1);
            }
            B_lockClick = false;
            I_id = Constant.ID_TYPE_NODE;
            SimplePool.Despawn(g_faceId);
            g_faceId = null;
        }

    }

    private IEnumerator Empty()
    {
        B_lockClick = true;
        if (I_id == Constant.ID_TYPE_NODE || I_id == Constant.ID_TYPE_NODE_MAGNET || I_id == Constant.ID_TYPE_NODE_BONUS)
        {
            Connecting = true;
        }
        yield return new WaitForSeconds(Constant.TIME_REMOVE_NODE);
        StartCoroutine(EffectRemove());
        Connecting = false;
        if (anim != null)
        {
            GetAnimNode().SetIdle();
        }
        yield return new WaitForSeconds(0.05f);
        EmptyNode();

    }

    //public void ResetState()
    //{
    //    effectClear = null;
    //    EmptyNode();
    //    GetSpriteRenderer().color = Color.white;
    //    gameObject.transform.parent.GetComponent<SpriteRenderer>().color = Color.white;
    //}

    private IEnumerator EffectRemove()
    {
        if (effectClear != null)
        {
            //GameObject connect = Instantiate(effectClear);
            GameObject connect = SimplePool.Spawn(effectClear.name, GetTransform().position, Quaternion.identity);

            //Debug.Log(effectClear.name);

            //connect.transform.position = Vector3.zero;

            //connect.transform.SetParent(GameManager.Instance.L_position[i_index].transform, false);

            yield return new WaitForSeconds(0.75f);


            //Debug.Log(connect.transform.position);

            SimplePool.Despawn(connect);

            effectClear = null;

            //Destroy(connect);
        }
    }

    private IEnumerator EffectRemoveFace()
    {
        
        if (g_faceId.GetComponent<AffectNode>().clear != null)
        {
            GameObject connect = SimplePool.Spawn(g_faceId.GetComponent<AffectNode>().clear.name, GetTransform().position, Quaternion.identity);

            yield return new WaitForSeconds(0.75f);

            SimplePool.Despawn(connect);
            //Destroy(connect);
        }
    }

    public void SetAnim(string anim)
    {
        if (this.anim == null )
        {
            return;
        }

        switch (anim)
        {
            case "idle":
                {
                    GetAnimNode().SetIdle();
                    break;
                }
            case "idle2":
                {
                    GetAnimNode().SetIdle2();
                    break;
                }
            case "select":
                {
                    GetAnimNode().SetSelect();
                    break;
                }
            case "windwest":
                {
                    GetAnimNode().SetWindWest();
                    break;
                }
            case "windeast":
                {
                    GetAnimNode().SetWindEast();
                    break;
                }
            default:
                Debug.Log("NULL anim");
                break;
        }
    }

    public void SetSpeedAnim(float speed)
    {
        if (anim != null )
        {
            GetAnimNode().SetSpeed(speed);
        }
        //anim.GetComponent<Animator>().speed = speed;
    }

    public void SetLayerObstacle(int order)
    {
        GetSpriteRenderer().sortingOrder = order;
    }

    public void ResetState()
    {

        if (g_faceId != null && i_id != Constant.ID_TYPE_NODE_BONUS)
        {
            SimplePool.Despawn(g_faceId);
            g_faceId = null;
        }
        b_lockClick = true;
        effectClear = null;

        if (select != null && anim != null)
        {
            GetAnimNode().SetLayer(-GetCurrentOrder);
            SimplePool.Despawn(select);
            select = null;
        }

        if (anim != null)
        {
            GetAnimNode().SetIdle();
            SimplePool.Despawn(anim);
            m_AnimTf = null;
            m_AnimNode = null;
            anim = null;

        }

        GetSpriteRenderer().color = Color.white;
        GetSpriteRenderer().sprite = null;
        gameObject.transform.parent.GetComponent<SpriteRenderer>().color = Color.white;

        I_id = Constant.ID_TYPE_NODE;
        I_numberNode = 0;

        GetSpriteRenderer().sortingLayerName = "Default";
    }

}
