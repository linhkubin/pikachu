﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimNode : MonoBehaviour
{

    private Animator m_AnimNode;
    private Animator GetAnimNode()
    {
        if (m_AnimNode == null)
        {
            m_AnimNode = GetComponent<Animator>();
        }
        return m_AnimNode;
    }

    //public Animator MyAnimator { get; set; }

    public SpriteMeshInstance[] sortingLayer;

    // Start is called before the first frame update
    //void Start()
    //{
    //    MyAnimator = GetComponent<Animator>();
    //}

    public void SetSelect()
    {
        ResetAnim();
        GetAnimNode().SetTrigger("select");
    }

    public void SetIdle()
    {
        ResetAnim();
        GetAnimNode().SetTrigger("idle");
    }

    public void SetIdle2()
    {
        ResetAnim();
        GetAnimNode().SetTrigger("idle2");
    }

    public void SetWindWest()
    {
        GetAnimNode().SetTrigger("windwest");
    }

    public void SetWindEast()
    {
        GetAnimNode().SetTrigger("windeast");
    }

    public void SetLayer(int orderNumber)
    {
        //Debug.Log("setlayer");
        if (sortingLayer[0].sortingOrder + orderNumber < 0)
        {
            return;
        }
        for (int i = 0; i < sortingLayer.Length; i++)
        {
            sortingLayer[i].sortingOrder += orderNumber;
        }

    }

    public void SetSpeed(float speed)
    {
        GetAnimNode().speed = speed;
    }

    public void ResetAnim()
    {
        GetAnimNode().ResetTrigger("idle");
        GetAnimNode().ResetTrigger("select");
        GetAnimNode().ResetTrigger("windwest");
        GetAnimNode().ResetTrigger("windeast");
    }
}
