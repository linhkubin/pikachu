﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatTextManager : MonoBehaviour
{
    private static CombatTextManager instance;

    public static CombatTextManager Instance { get { if (instance == null) instance = GameObject.FindObjectOfType<CombatTextManager>(); return instance; } }

    public GameObject textPrefab;

    public RectTransform canvasTranform;

    public float speed = 1;

    public Vector3 direction;

    public float fadeTime = 2;

    public void CreateText(Vector3 position, string text , Color color)
    {
        GameObject crt = (GameObject)Instantiate(textPrefab, position, Quaternion.identity);

        crt.transform.SetParent(canvasTranform);

        crt.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        crt.GetComponent<CombatText>().Initialize(speed, direction, fadeTime);

        crt.GetComponent<Text>().text = text;

        crt.GetComponent<Text>().color = color;

    }
}
