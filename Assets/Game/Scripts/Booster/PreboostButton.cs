﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreboostButton : Booster
{
    private Button m_button;

    private Button GetButton()
    {
        if (m_button == null)
        {
            m_button = gameObject.GetComponent<Button>();
        }

        return m_button;
    }


    private void Start()
    {
        GetButton().onClick.AddListener(Choose);
    }

    public override void Choose()
    {

        if (number <= 0 && !choose)
        {
            // purchase
            Debug.Log("popup mua");

            UIManager.Instance.OpenGetBoost(id, "PREBOOSTER", icon, descripsion, price);
        }
        if (!choose && number > 0)
        {
            choose = true;
            number--;
            SetState(false, false, true);
            return;
        }

        if (choose)
        {
            choose = false;
            number++;
            img_number.transform.GetChild(0).GetComponent<Text>().text = number.ToString();
            SetState(true, false, false);
        }

    }

    public override void Reset()
    {
        choose = false;
        BoosterManager.Instance.UI_boosts[id].number = number;
        if (block)
        {
            SetState(false, false, false, true);
            GetButton().interactable = false;
        }
        else
        {
            GetButton().interactable = true;

            if (number <= 0)
            {
                SetState(false, true, false);
            }
            else
            {
                SetState(true, false, false);
                img_number.transform.GetChild(0).GetComponent<Text>().text = number.ToString();
            }
        }
       
    }

    public override void SetState(bool number,bool add,bool choose,bool block = false)
    {
        img_number.SetActive(number);
        img_add.SetActive(add);
        if (img_choose != null)
        {
            img_choose.SetActive(choose);
        }
        img_block.SetActive(block);
    }

    public override void SetNumber(int id,int number, bool block, int price, Sprite icon)
    {
        this.id = id;
        this.block = block;
        this.number = number;
        this.price = price;
        this.icon = icon;
        SetDescripsion(id);
        Reset();
    }

    private void SetDescripsion(int id)
    {
        if (id == 0)
        {
            descripsion = " Khi ăn 1 cặp node sẽ ngẫu nhiên ăn 1 cặp node khác ";
        }
        if (id == 1)
        {
            descripsion = " Giải cứu ngẫu nhiên tất cả node cùng type ";
        }
        if (id == 2)
        {
            descripsion = " Hóa giải toàn bộ trạng thái bất lợi trên các node trên map ";
        }
    }

    public override void Use()
    {
        if (id == 0)
        {
            TimeManager.Instance.b_pause = true;
            ItemManager.Instance.ItemDoubleLink();
            PlayerPrefs.SetInt("PreBoosterDoubleLink", PlayerPrefs.GetInt("PreBoosterDoubleLink") - 1);
            Reset();
        }
        if (id == 1)
        {
            TimeManager.Instance.b_pause = true;
            ItemManager.Instance.DenyTypeNode();
            PlayerPrefs.SetInt("PreBoosterRemoveType", PlayerPrefs.GetInt("PreBoosterRemoveType") - 1);
            Reset();
        }
        if (id == 2)
        {
            TimeManager.Instance.b_pause = true;
            ItemManager.Instance.KeyRemoveAdverse();
            PlayerPrefs.SetInt("PreBoosterKey", PlayerPrefs.GetInt("PreBoosterKey") - 1);
            Reset();
        }
    }

    public override void Active()
    {
        GetButton().interactable = true;
    }

    public override void InActive()
    {
        GetButton().interactable = false;
    }

    public override IEnumerator TimerActive(float time = 3f)
    {
        yield return null;
    }

    public override void PickChoose(bool onShadow = true)
    {

    }

    public override void ResetPickChoose()
    {

    }
}
