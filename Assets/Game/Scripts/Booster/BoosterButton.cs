﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterButton : Booster
{
    [SerializeField]
    private SpriteRenderer spr_icon;

    public GameObject g_chooseEffect;

    private Button m_button;

    private Button GetButton()
    {
        if (m_button == null)
        {
            m_button = gameObject.GetComponent<Button>();
        }

        return m_button;
    }

    private void Start()
    {
        GetButton().onClick.AddListener(Choose);
    }

    public override void Choose()
    {

        if (number <= 0)
        {
            if (ItemManager.Instance.useHammer)
            {
                number--;
                Use();
                return;
            }
            // purchase
            Debug.Log("popup mua");
            UIManager.Instance.OpenGetBoost(id, "BOOSTER", icon, descripsion, price);

        }
        if (number > 0)
        {
            number--;
            Use();
            return;
        }
    }

    public override void Reset()
    {

        BoosterManager.Instance.UI_boosts[id].number = number;
        if (block)
        {
            SetState(false, false, false, true);
            GetButton().interactable = false;
        }
        else
        {
            if (number <= 0)
            {
                SetState(false, true, false);
            }
            else
            {
                SetState(true, false, false);
                img_number.transform.GetChild(0).GetComponent<Text>().text = number.ToString();
            }
        }

    }

    public override void SetState(bool number, bool add, bool choose, bool block = false)
    {
        img_number.SetActive(number);
        img_add.SetActive(add);
        if (img_choose != null)
        {
            img_choose.SetActive(choose);
        }
        img_block.SetActive(block);
    }

    public override void SetNumber(int id, int number, bool block, int price, Sprite icon)
    {
        this.id = id;
        this.block = block;
        this.number = number;
        this.price = price;
        this.icon = icon;
        SetDescripsion(id);
        Reset();
    }

    private void SetDescripsion(int id)
    {
        if (id == 3)
        {
            descripsion = " Tăng thời gian màn chơi thêm 10s ";
        }
        if (id == 4)
        {
            descripsion = " Phá địa hình size 3x3 ";
        }
        if (id == 5)
        {
            descripsion = " Xáo trộn toàn bộ node trên map ";
        }
        if (id == 6)
        {
            descripsion = " Gợi ý người chơi sau 5s ko ăn node nào ";
        }
    }

    public override void Use()
    {
        if (id == 3)
        {
            ItemManager.Instance.ItemTime();
            PickChoose();
            BoosterManager.Instance.InActiveButton();
            PlayerPrefs.SetInt("BoosterTime", PlayerPrefs.GetInt("BoosterTime") - 1);
            Reset();
        }
        if (id == 4)
        {

            if (ItemManager.Instance.useHammer)
            {
                ResetPickChoose();
                number += 2;
                BoosterManager.Instance.ActiveButton();
                StartCoroutine(TimerActive(1f));
            }
            else
            {
                BoosterManager.Instance.OnlyActive(4);
                PickChoose();
            }
            StartCoroutine(TimerActive(1.4f));

            ItemManager.Instance.ItemHammer();
            Reset();
        }
        if (id == 5)
        {
            ItemManager.Instance.ItemMix();
            PickChoose(false);
            BoosterManager.Instance.InActiveButton();
            PlayerPrefs.SetInt("BoosterMix", PlayerPrefs.GetInt("BoosterMix") - 1);
            Reset();
        }
        if (id == 6)
        {
            ItemManager.Instance.Hint();
            PickChoose();
            BoosterManager.Instance.InActiveButton();
            PlayerPrefs.SetInt("BoosterHint", PlayerPrefs.GetInt("BoosterHint") - 1);
            Reset();
        }
    }

    public override void Active()
    {
        GetButton().interactable = true;
        if (id == 4)
        {
            ResetPickChoose();
        }
    }

    public override void InActive()
    {
        GetButton().interactable = false;
    }

    public override IEnumerator TimerActive(float time = 3f)
    {
        GetButton().interactable = false;
        yield return new WaitForSeconds(time);
        GetButton().interactable = true;
    }

    public override void ResetPickChoose()
    {
        SpriteManager.Instance.g_shadow.SetTrigger("off");
        g_chooseEffect.SetActive(false);
        spr_icon.sortingLayerName = "Default";
        if (id == 4)
        {
            ItemManager.Instance.InActiveBarrier();
        }
        StartCoroutine(ResetTriggerShadow());
    }

    public override void PickChoose(bool onShadow = true)
    {
        g_chooseEffect.SetActive(true);
        spr_icon.sortingLayerName = "Barier";
        if (id == 4)
        {
            ItemManager.Instance.ActiveBarrier();
            SpriteManager.Instance.g_shadow.SetTrigger("quick");
        }
        else if(onShadow)
        {
            SpriteManager.Instance.g_shadow.SetTrigger("on");
        }
    }

    public IEnumerator ResetTriggerShadow()
    {
        yield return new WaitForSeconds(0.5f);
        SpriteManager.Instance.g_shadow.ResetTrigger("off");
        SpriteManager.Instance.g_shadow.ResetTrigger("on");
        SpriteManager.Instance.g_shadow.ResetTrigger("quick");

    }
}