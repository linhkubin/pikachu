﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Booster : MonoBehaviour
{

    public GameObject img_number;

    public GameObject img_add;

    public GameObject img_choose;

    public GameObject img_block;

    public int number = 2;

    public int id;

    public bool choose = false;

    public bool block = false;

    public string descripsion;

    public int price;

    public Sprite icon;

    public abstract void Choose();

    public abstract void Reset();

    public abstract void SetState(bool number, bool add, bool choose, bool block = false);

    public abstract void SetNumber(int id, int number, bool block,int price, Sprite icon);

    public abstract void Use();

    public abstract void Active();

    public abstract void InActive();

    public abstract IEnumerator TimerActive(float time = 3f);

    public abstract void PickChoose(bool onShadow = true);

    public abstract void ResetPickChoose();

}
