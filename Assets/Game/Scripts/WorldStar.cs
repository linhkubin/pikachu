﻿using UnityEngine;
using UnityEngine.UI;

public class WorldStar : MonoBehaviour
{
    public Image background;
    public int brickIndex;
    public Map[] stage;
    public int maxStage;
    public int minStage;

    private void Start()
    {

        SetTotalWorldStar();

        // currentWorld = PlayerPrefs.GetInt("CurrentWorld");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Brick Trigger")
        {
            CUIManager.Instance.DisableBrick();

            if(brickIndex == 0)
            {
                background.sprite = CUIManager.Instance.worldBG[brickIndex];
                Brick.Instance.worlds[brickIndex + 1].background.sprite = CUIManager.Instance.worldBG[brickIndex + 1];

                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex]);
                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex + 1]);
            }
            else if(brickIndex == Brick.Instance.worlds.Length - 1)
            {
                background.sprite = CUIManager.Instance.worldBG[brickIndex];
                Brick.Instance.worlds[brickIndex - 1].background.sprite = CUIManager.Instance.worldBG[brickIndex - 1];

                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex]);
                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex - 1]);
            }
            else
            {
                background.sprite = CUIManager.Instance.worldBG[brickIndex];
                Brick.Instance.worlds[brickIndex + 1].background.sprite = CUIManager.Instance.worldBG[brickIndex + 1];
                Brick.Instance.worlds[brickIndex - 1].background.sprite = CUIManager.Instance.worldBG[brickIndex - 1];

                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex]);
                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex + 1]);
                UnlockLevelInBrick(Brick.Instance.worlds[brickIndex - 1]);
            }
        }
    }

    public void UnlockLevelInBrick(WorldStar world)
    {
        for(int i = 0; i < world.stage.Length; i++)
        {
            if(world.stage[i].GetComponent<Map>().numberMap <= PlayerPrefs.GetInt("CurrentLevel"))
            {
                // world.stage[i].gameObject.SetActive(true);
                world.stage[i].GetComponent<Image>().sprite = CUIManager.Instance.unlockedLevel;
                world.stage[i].GetComponent<Button>().interactable = true;
            }   

            if(world.stage[i].GetComponent<Map>().numberMap == PlayerPrefs.GetInt("CurrentLevel"))
            {
                CUIManager.Instance.currentLvParticle.transform.position = world.stage[i].gameObject.transform.position;
                CUIManager.Instance.currentLvParticle.transform.parent = world.stage[i].gameObject.transform;
            }         
        }
    }

    public void SetBackground()
    {

    }

    public void SetTotalWorldStar()
    {

    }
}