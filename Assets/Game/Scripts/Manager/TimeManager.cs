﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private static TimeManager instance;
    public static TimeManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<TimeManager>(); return instance; } }

    [SerializeField]
    private Transform start;
   
    public Transform target;

    public TimeOver cl_timeOver;

    public Transform tf_MC;

    public bool b_gameStart = false;

    public bool b_gameOver = false;

    public bool b_pause = false;

    #region Text game
    [Header("Text Ingame")]
    public GameObject ready;
    public GameObject complete;
    public GameObject failed;
    public GameObject great;
    public GameObject perfect;
    public GameObject amazing;
    #endregion

    /// <summary>
    /// reslove subtime by web
    /// </summary>
    private bool firstSub = false;

    public void InitTimer()
    {
        b_gameStart = false;
        b_gameOver = false;
        b_pause = false;
        cl_timeOver.Initialize();
    }

    public float GetTime()
    {
        return cl_timeOver.MaxVal - cl_timeOver.CurrentVal;
    }

    public void SubtractTime(float subTime = 30)
    {
        cl_timeOver.CurrentVal -= subTime;

        CombatTextManager.Instance.CreateText(gameObject.transform.position,"- "+ subTime + "s",Color.red);
    }

    public void AddTime(float addTime = 30)
    {
        cl_timeOver.CurrentVal += addTime;

        CombatTextManager.Instance.CreateText(gameObject.transform.position, "+ " + addTime + "s", Color.red);
    }

    public void SubtractTimeByWeb(float subTime = 10)
    {
        if (!firstSub)
        {
            firstSub = true;

            cl_timeOver.CurrentVal -= subTime;

            CombatTextManager.Instance.CreateText(gameObject.transform.position, "- " + subTime + "s", Color.red);

            StartCoroutine(ResetSubByWeb());
        }
    }

    public void SubtractTimeByAlien()
    {
        if (!firstSub)
        {
            firstSub = true;

            cl_timeOver.CurrentVal -= cl_timeOver.CurrentVal;

            CombatTextManager.Instance.CreateText(gameObject.transform.position, "- " + cl_timeOver.CurrentVal + "s", Color.red);

            StartCoroutine(ResetSubByWeb());
        }
    }

    private IEnumerator ResetSubByWeb()
    {
        yield return new WaitForSeconds(0.7f);
        firstSub = false;
    }

    public IEnumerator TimeToScore()
    {
        GameObject go;
        while (cl_timeOver.CurrentVal > 0)
        {
            cl_timeOver.CurrentVal -= 10;
            yield return new WaitForSeconds(0.2f);

            go = SimplePool.Spawn("StarTime", start.position, Quaternion.identity);
            go.GetComponent<StarTest>().SetTarget(target, "Connected", 1f, 0.2f, true);
        }
    }

    public void ActiveState(string text)
    {
        switch (text)
        {
            case "ready":
                {
                    StartCoroutine(StartActiveState(ready));
                    break;
                }
            case "complete":
                {
                    StartCoroutine(StartActiveState(complete));
                    break;
                }
            case "fail":
                {
                    StartCoroutine(StartActiveState(failed));
                    break;
                }
            case "great":
                {
                    StartCoroutine(StartActiveState(great));
                    break;
                }
            case "perfect":
                {
                    StartCoroutine(StartActiveState(perfect));
                    break;
                }
            case "amazing":
                {
                    StartCoroutine(StartActiveState(amazing));
                    break;
                }
            default:
                Debug.Log("err state");
                break;
        }
    }

    private IEnumerator StartActiveState(GameObject go)
    {
        yield return new WaitForSeconds(0.3f);
        go.SetActive(true);
        yield return new WaitForSeconds(1f);
        go.SetActive(false);
    }
}
