﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<UIManager>(); return instance; } }

    //---------------------prepare--------------------------
    [SerializeField]
    private GameObject g_prePlayPanel;

    [SerializeField]
    private Text txt_stage;

    [SerializeField]
    private Text txt_content;

    [SerializeField]
    private GameObject g_prePlayImage;

    //---------------------pause--------------------------
    [SerializeField]
    private GameObject g_pausePanel;

    [SerializeField]
    private GameObject g_pauseImage;

    [SerializeField]
    private Button g_pauseButton;

    //---------------------win--------------------------
    //--------------star----
    //[SerializeField]
    //private GameObject oneStar;

    //[SerializeField]
    //private GameObject twoStar;

    //[SerializeField]
    //private GameObject threeStar;

    //--------------

    [SerializeField]
    private GameObject g_winPanel;

    [SerializeField]
    private TextMeshProUGUI txt_scoreWin;

    [SerializeField]
    private Text txt_timeWin;

    [SerializeField]
    private Text txt_highScore;

    [SerializeField]
    private GameObject g_winImage;

    //---------------------lose--------------------------

    [SerializeField]
    private Animator anim_levelFail;

    //--------------------retry with boots---------------

    [SerializeField]
    private GameObject g_retryPanel;

    [SerializeField]
    private GameObject g_losePanel;

    [SerializeField]
    private TextMeshProUGUI txt_scoreLose;


    [SerializeField]
    private GameObject g_loseImage;
    //---------------------Spin---------------------------

    [SerializeField]
    private GameObject g_spinPanel;
    
    [SerializeField]
    private GameObject g_spinPanelReward;

    public bool spinRewardAppear = false;
    public bool spinIsStarted = false;

    //---------------------------------------------------
    public GameObject inGame;

    public GameObject processMap;

    //----------------------Win button-------------------

    [SerializeField]
    private GameObject g_winPanelButtons;

    [SerializeField]
    private Animator anim_win;

    [SerializeField]
    private Animator anim_levelComplete;

    //---------------------Reward light-------------------

    [SerializeField]
    private GameObject g_rewardLeftLight;

    [SerializeField]
    private GameObject g_rewardRightLight;
    //------------------get more booster-----------------

    [SerializeField]
    private GameObject g_getBoostPanel;

    [SerializeField]
    private Text txt_title;

    [SerializeField]
    private Image s_icon;

    [SerializeField]
    private Text txt_descripsion;

    [SerializeField]
    private Text txt_price;

    //--------------------Toggle - Music and Sound-------

    [SerializeField]
    private Slider toggleMusic;

    [SerializeField]
    private Slider toggleSound;

    [SerializeField]
    private GameObject toggleMusicSprite;

    [SerializeField]
    private GameObject toggleSoundSprite;

    [SerializeField]
    private Sprite[] OnOffSpriteToggle = new Sprite[2];

    //---------------------------------------------------

    public Text txt_MapLevel;

    private void Start()
    {
        Time.timeScale = 1;
        AnimateRewardLeftLight(g_rewardLeftLight.GetComponent<Animator>());
        AnimateRewardRightLight(g_rewardRightLight.GetComponent<Animator>());
    }

    //---------------------pre play-----------------------------
    public void UpdateStage()
    {
        txt_MapLevel.text = GameManager.MAP_LEVEL.ToString();
        txt_stage.text = "LEVEL " + GameManager.MAP_LEVEL.ToString();
    }

    public void OpenPrePlayPanel()
    {
        SoundManager.instance.PlayMusic(UnityEngine.Random.Range(0, 3));

        CUIManager.Instance.SetDefaultSettingIcons();
        MCManager.Instance.Normal();

        g_prePlayPanel.SetActive(true);
        //Time.timeScale = 0;
        PrePlayButton();

        AnimateOpenPopup(g_prePlayImage.GetComponent<Animator>());

        DataManager.Instance.achievementManager.Notify(2, 500000);
        // DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_ADS_ID, 1);
        // DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_ADS_ID, 1);
    }

    public void ClosePrePlayPanel()
    {
        // DataManager.Instance.Consume(1);
        MCManager.Instance.PlayGame();
        StartCoroutine(AnimateClosePopup(g_prePlayImage.GetComponent<Animator>(), g_prePlayPanel));
        TimeManager.Instance.b_gameStart = true;
        // Time.timeScale = 1;
        GameManager.Instance.InitAffect();

        TimeManager.Instance.ActiveState("ready");
        // DataManager.Instance.achievementManager.Notify(0, 5000);
    }

    //---------------------pause-----------------------------
    public void OpenPausePanel()
    {
        TimeManager.Instance.b_pause = true;
        g_pausePanel.SetActive(true);
        // Time.timeScale = 0;
        StartCoroutine(AnimateOpenPopup(g_prePlayImage.GetComponent<Animator>(), 1f, 0f));
    }

    public void ClosePausePanel()
    {
        StartCoroutine(AnimateClosePopup(g_pauseImage.GetComponent<Animator>(), g_pausePanel));
        //-> set time
        // Time.timeScale = 1;
        TimeManager.Instance.b_pause = false;

    }

    //---------------------win-----------------------------

    public void NextLevel()
    {
        if(DataManager.Instance.GetCurrentEnergy() > 0)
        {
            GameManager.MAP_LEVEL++;
            
            // DataManager.Instance.currentLevel = GameManager.MAP_LEVEL;
            //GameManager.Instance.NUMBER_WALL = (GameManager.MAP_LEVEL - 1) / 20;
            TryAgain();
        }
        else
        {
            GoCampaign();
            CUIManager.Instance.OpenEnergyPack();
        }
    }

    public IEnumerator OpenWinPanel(float time, bool spin)
    {
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel") + 1);

        //----------------------Unlock next level------------------------------
        EndPlayButton();
        BonusManager.Instance.DisappearPopup(0);

        TimeManager.Instance.ActiveState("complete");

        MCManager.Instance.LevelComplete();

        //---------------------------------------------------------------------

        float timeRemain = TimeManager.Instance.cl_timeOver.CurrentVal;


        if (spin)
        {
            yield return new WaitForSeconds(1.5f);

            g_winPanelButtons.SetActive(false);
        }
        else
        {
            g_winPanelButtons.SetActive(true);
        }
        
        StartCoroutine(TimeManager.Instance.TimeToScore());

        yield return new WaitForSeconds(1.4f);

        ScoreManager.Instance.AddScoreTime(timeRemain);

        yield return new WaitForSeconds(2.5f);

        //----------------------High Score------------------------------

        g_winPanel.SetActive(true);

        int deltaStar = ScoreManager.Instance.GetStar() - PlayerPrefs.GetInt("StarsLv" + GameManager.MAP_LEVEL) ;

        if (deltaStar > 0)
        {
            DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_EARNSTAR_ID, deltaStar);
        }

        switch (ScoreManager.Instance.GetStar())
        {
            case 1:
                {
                    anim_levelComplete.ResetTrigger("3star");
                    anim_levelComplete.ResetTrigger("2star");
                    anim_levelComplete.SetTrigger("1star");
                    break;
                }
            case 2:
                {
                    anim_levelComplete.ResetTrigger("1star");
                    anim_levelComplete.ResetTrigger("3star");
                    anim_levelComplete.SetTrigger("2star");
                    break;
                }
            case 3:
                {
                    anim_levelComplete.ResetTrigger("1star");
                    anim_levelComplete.ResetTrigger("2star");
                    anim_levelComplete.SetTrigger("3star");

                    DataManager.Instance.questManager.QuestNotify(QuestConstant.QUEST_3STARS_ID, 1);

                    break;
                }
            default:
                break;
        }

        yield return new WaitForSeconds(1.3f);

        anim_win.SetTrigger("run");


        //text_scoreWin.text = ScoreManager.Instance.F_score.ToString();

        StartCoroutine(UpdateScore());


        //txt_scoreWin.SetText(_convertToSprite(_toPrettyString((int)ScoreManager.Instance.F_score)));

        if (time > 0)
        {
            txt_timeWin.text = Math.Round(time, 2).ToString() + "s";
        }

        AnimateOpenPopup(g_prePlayImage.GetComponent<Animator>());

        if(spin)
        {
            yield return new WaitForSeconds(3.5f);
            
            g_winPanel.SetActive(false);
            OpenSpinPanel();
        }
    }

    private IEnumerator UpdateScore()
    {
        int prev = (int)ScoreManager.Instance.F_score - 2000;
        int index = (int)ScoreManager.Instance.F_score;

        float f_t = 0f;

        yield return new WaitForSeconds(0.2f);

        for (int i = 0; i < 40; i++)
        {
            if (f_t < 1.0f)
            {
                yield return new WaitForSeconds(0.03f);
                f_t += 2 * Time.deltaTime;
                //txt_score.text = Mathf.SmoothStep(prev, index, f_t);
                txt_scoreWin.SetText(_convertToSprite(_toPrettyString((int)Mathf.SmoothStep(prev, index, f_t))));
            }
        }

    }

    //private void DefaultStar()
    //{
    //    oneStar.SetActive(false);
    //    twoStar.SetActive(false);
    //    threeStar.SetActive(false);
    //}

    public string _toPrettyString(int number)
    {
        string current = "";
        if (number != 0)
        {
            current = string.Format("{0:0,0}", number);
        }
        else
        {
            current = "0";
        }
        return current.Trim();

    }

    public string _convertToSprite(string number)
    {
        string sprite = "";

        for (int i = 0; i < number.Length; i++)
        {
            switch (number[i])
            {
                case '0':
                    {
                        sprite += "<sprite=0>";
                        break;
                    }
                case '1':
                    {
                        sprite += "<sprite=1>";
                        break;
                    }
                case '2':
                    {
                        sprite += "<sprite=2>";
                        break;
                    }
                case '3':
                    {
                        sprite += "<sprite=3>";
                        break;
                    }
                case '4':
                    {
                        sprite += "<sprite=4>";
                        break;
                    }
                case '5':
                    {
                        sprite += "<sprite=5>";
                        break;
                    }
                case '6':
                    {
                        sprite += "<sprite=6>";
                        break;
                    }
                case '7':
                    {
                        sprite += "<sprite=7>";
                        break;
                    }
                case '8':
                    {
                        sprite += "<sprite=8>";
                        break;
                    }
                case '9':
                    {
                        sprite += "<sprite=9>";
                        break;
                    }
                case ',':
                    {
                        sprite += "<sprite=10>";
                        break;
                    }
            }
        }

        return sprite;
    }

    public void CloseWinPanel()
    {
        StartCoroutine(AnimateClosePopup(g_pauseImage.GetComponent<Animator>(), g_winPanel));
        // g_winPanel.SetActive(false);
        //SceneManager.LoadScene("Campaign");
        //-> set time
    }

    //---------------------retry-----------------------------

    public IEnumerator OpenRetryPanel()
    {
        TimeManager.Instance.ActiveState("fail");

        MCManager.Instance.Fail();

        EndPlayButton();

        yield return new WaitForSeconds(1f);

        StartCoroutine(ClearNode());

        yield return new WaitForSeconds(2f);

        g_retryPanel.SetActive(true);

        AnimateOpenPopup(g_prePlayImage.GetComponent<Animator>());
    }

    private IEnumerator ClearNode()
    {
        List<Node> list = GameManager.Instance.m_Node;

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].I_numberNode > 0)
            {
                list[i].RemoveNode();
                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    public void CloseRetryPanel()
    {
        StartCoroutine(AnimateClosePopup(g_pauseImage.GetComponent<Animator>(), g_retryPanel));
        StartCoroutine(OpenLosePanel(ScoreManager.Instance.F_score));
        // g_retryPanel.SetActive(false);
    }

    public void WatchNow()
    {
        Debug.Log("watch");
    }
    //---------------------lose-----------------------------

    public IEnumerator OpenLosePanel(float score)
    {

        DataManager.Instance.Consume(1);
        yield return new WaitForSeconds(0.5f);
        g_losePanel.SetActive(true);

        anim_levelFail.SetTrigger("run");

        txt_scoreLose.SetText(_convertToSprite(_toPrettyString((int)score)));

        //text_scoreLose.text = ScoreManager.Instance.F_score.ToString();
        //g_pausePanel.GetComponentInChildren<Text>().text = mess;

        AnimateOpenPopup(g_prePlayImage.GetComponent<Animator>());
    }

    public void CloseLosePanel()
    {
        StartCoroutine(AnimateClosePopup(g_pauseImage.GetComponent<Animator>(), g_losePanel));
        // g_losePanel.SetActive(false);
        //SceneManager.LoadScene("Campaign");
        //-> set time
    }

    //-----------------------Spin----------------------------

    public void OpenSpinPanel()
    {
        g_spinPanel.SetActive(true);
        LightManager.Instance.StartCoroutine("Blink");
    }

    public void CloseSpin()
    {
        if(Arrow.Instance.itemName.Equals("Heart"))
        {
            DataManager.Instance.AddEnergy(Arrow.Instance.itemNumber);
        }
        else if(Arrow.Instance.itemName.Equals("Gem"))
        {
            DataManager.Instance.ChangeGemNumber(Arrow.Instance.itemNumber);
        }
        else if(Arrow.Instance.itemName.Equals("Booster1"))
        {
            PlayerPrefs.SetInt("BoosterTime", PlayerPrefs.GetInt("BoosterTime") + Arrow.Instance.itemNumber);
            BoosterManager.Instance.booster[3].number = PlayerPrefs.GetInt("BoosterTime");
            BoosterManager.Instance.booster[3].Reset();
        }
        else if(Arrow.Instance.itemName.Equals("Booster2"))
        {
            PlayerPrefs.SetInt("BoosterHammer", PlayerPrefs.GetInt("BoosterHammer") + Arrow.Instance.itemNumber);
            BoosterManager.Instance.booster[4].number = PlayerPrefs.GetInt("BoosterHammer");
            BoosterManager.Instance.booster[4].Reset();
        }
        else if(Arrow.Instance.itemName.Equals("Booster3_02"))
        {
            PlayerPrefs.SetInt("BoosterMix", PlayerPrefs.GetInt("BoosterMix") + Arrow.Instance.itemNumber);
            BoosterManager.Instance.booster[5].number = PlayerPrefs.GetInt("BoosterMix");
            BoosterManager.Instance.booster[5].Reset();
        }
        else if(Arrow.Instance.itemName.Equals("Hint"))
        {
            PlayerPrefs.SetInt("BoosterHint", PlayerPrefs.GetInt("BoosterHint") + Arrow.Instance.itemNumber);
            BoosterManager.Instance.booster[6].number = PlayerPrefs.GetInt("BoosterHint");
            BoosterManager.Instance.booster[6].Reset();
        }
        else if(Arrow.Instance.itemName.Equals("Ticket"))
        {
            Debug.Log("Gem");
        }

        UIManager.Instance.spinRewardAppear = false;
        UIManager.Instance.spinIsStarted = false;
        g_winPanel.SetActive(true);
        anim_win.SetTrigger("full");
        g_winPanelButtons.SetActive(true);
        g_spinPanel.SetActive(false);
        g_spinPanelReward.SetActive(false);

        switch (ScoreManager.Instance.GetStar())
        {
            case 1:
                {
                    anim_levelComplete.SetTrigger("default1");
                    break;
                }
            case 2:
                {
                    anim_levelComplete.SetTrigger("default2");
                    break;
                }
            case 3:
                {
                    anim_levelComplete.SetTrigger("default3");
                    break;
                }
            default:
                break;
        }
    }

    public void OpenSpinRewardPanel()
    {
        g_spinPanelReward.SetActive(true);
    }

    //-----------------------purchase boost----------------------------
    int id_boost;

    public void OpenGetBoost(int id, string title, Sprite icon, string descripsion, int price)
    {
        id_boost = id;
        g_getBoostPanel.SetActive(true);
        txt_title.text = title;
        s_icon.sprite = icon;
        txt_descripsion.text = descripsion;
        txt_price.text = price.ToString();

        DataManager.Instance.boosterId = id;
    }

    public void CloseGetBoost()
    {
        g_getBoostPanel.SetActive(false);
    }

    public void ConfirmGetBoost()
    {

    }

    //-------------------------------------------------------


    public void GoCampaign()
    {
        TimeManager.Instance.b_gameStart = false;
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            GameManager.Instance.L_listNode[i].GetComponent<Node>().ResetState();
        }
        processMap.SetActive(true);

        ResetUI();

        SoundManager.instance.PlayMusic(UnityEngine.Random.Range(3, 6));

    }

    private void ResetUI()
    {
        g_prePlayPanel.SetActive(false);
        g_pausePanel.SetActive(false);
        g_winPanel.SetActive(false);
        g_losePanel.SetActive(false);
        g_retryPanel.SetActive(false);

    }

    public void TryAgain()
    {
        if(DataManager.Instance.GetCurrentEnergy() > 0)
        {
            GameManager.Instance.ResetStage();
            ResetUI();
            OpenPrePlayPanel();
        }
        else
        {
            GoCampaign();
            CUIManager.Instance.OpenEnergyPack();
        }
    }

    private void AnimateOpenPopup(Animator anim, float scaleTime)
    {
        // Time.timeScale = scaleTime;
        anim.SetTrigger("open");
    }

    private void AnimateOpenPopup(Animator anim)
    {
        anim.SetTrigger("open");
    }

    private IEnumerator AnimateOpenPopup(Animator anim, float timeBeforeAnim, float timeAfterAnim)
    {
        anim.ResetTrigger("close");
        // Time.timeScale = timeBeforeAnim;
        yield return new WaitForSeconds(1f);
        // Time.timeScale = timeAfterAnim;
    }

    private IEnumerator AnimateClosePopup(Animator anim, GameObject panel)
    {
        // Time.timeScale = 1;
        anim.SetTrigger("close");

        yield return new WaitForSeconds(0.2f);

        panel.SetActive(false);

        //g_pausePanel.SetActive(false);
    }

    private void AnimateRewardLeftLight(Animator anim)
    {
        // Time.timeScale = 1;
        anim.SetTrigger("Left");
    }

    private void AnimateRewardRightLight(Animator anim)
    {
        // Time.timeScale = 1;
        anim.SetTrigger("Right");
    }

    public IEnumerator Delay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
    }

    //----------------------------Music and Sound-------------------------------

    public void EnableToggle(Slider toggle)
    {
        if (toggle.name.Equals("Slider - Vibrate"))
        {
            switch (toggle.value)
            {
                case 0:
                    toggleMusicSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[0];
                    SoundManager.instance.muteVibrate = true;
                    break;
                case 1:
                    toggleMusicSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[1];
                    SoundManager.instance.muteVibrate = false;
                    break;
            }
        }
        if(toggle.name.Equals("Slider - Music"))
        {
            switch(toggle.value)
            {
                case 0:
                    toggleMusicSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[0];
                    SoundManager.instance.MuteMusic();
                    break;
                case 1:
                    toggleMusicSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[1];
                    SoundManager.instance.StartMusic();
                    break;
            }
        }
        else if(toggle.name.Equals("Slider - Sound"))
        {
            switch(toggle.value)
            {
                case 0:
                    SoundManager.instance.MuteFx();
                    toggleSoundSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[0];
                    break;
                case 1:
                    SoundManager.instance.StartFx();
                    toggleSoundSprite.GetComponent<Image>().sprite = OnOffSpriteToggle[1];
                    break;
            }
        }

    }

    public void PrePlayButton()
    {
        BoosterManager.Instance.ActiveButton();

        g_pauseButton.interactable = true;
    }

    public void EndPlayButton()
    {
        BoosterManager.Instance.booster[4].GetComponent<BoosterButton>().g_chooseEffect.SetActive(false);

        ItemManager.Instance.ResetHint();

        BoosterManager.Instance.SuggestHammer(false);

        BoosterManager.Instance.InActiveButton();

        g_pauseButton.interactable = false;

    }
}