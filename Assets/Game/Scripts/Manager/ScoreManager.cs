﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //0 - 1: 100
    //2: 150
    //3: 200
    //4: 250
    //5: 300
    //6: 350
    //7 trở lên: 500

    private static ScoreManager instance;
    public static ScoreManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<ScoreManager>(); return instance; } }

    //------------------score-----------------------
    private float f_score = 0;

    public float F_score { get { return f_score; } set { f_score = value; } }

    [SerializeField]
    private Text txt_score;
    
    [SerializeField]
    private int i_lerpSpeed = 2;

    private float f_prevScore = 0.0f;
    private float f_t = 0.0f;

    //-----------------combo score------------------
    int i_combo = 0;

    float f_comboTime = 0;

    // float COMBO_TIMER = 3;

    //------------------bonus score------------------

    public int i_bonusScore = 1;
    //------------------star score------------------
    public Star star = new Star(0, 0, 0);

    public TimeOver cl_firstStar;

    public TimeOver cl_secondStar;

    public TimeOver cl_thirdStar;

    Star anim;

    public Animator anim_updateStar;

    public int countStar;

    //-----------------------------------------------


    private void Update()
    {
        f_comboTime -= Time.deltaTime;

        if (f_t < 1.0f)
        {
            f_t += i_lerpSpeed * Time.deltaTime;
            txt_score.text = Mathf.SmoothStep(f_prevScore, F_score, f_t).ToString("0.");
        }
    }

    public void AddScoreTime(float time)
    {
        F_score += (int)time * 50;
        f_t = 0;
        UpdateStar(F_score);
    }

    public void AddScore(int distance, Transform point1, Transform point2)
    {

        float a_score = ConvertToScore(distance) * i_bonusScore;
        f_prevScore = F_score;
        F_score += a_score;
        f_t = 0;

        //CombatTextManager.Instance.CreateText(new Vector3(point1.position.x - 0.2f, point1.position.y, point1.position.z), "+"+ (a_score/2).ToString(), Color.black);
        //CombatTextManager.Instance.CreateText(new Vector3(point2.position.x - 0.2f, point2.position.y, point2.position.z), "+"+ (a_score/2).ToString(), Color.black);

        if (f_comboTime > 0)
        {
            if (i_combo < 4)
            {
                i_combo++;
            }

            if (i_combo > 0)
            {
                F_score += (i_combo - 1) * a_score;

                // great->

            }


            //CombatTextManager.Instance.CreateText(new Vector3(point1.position.x + 0.2f, point1.position.y, point1.position.z),"+" + (i_combo * 100/2).ToString(), Color.red);
            //CombatTextManager.Instance.CreateText(new Vector3(point2.position.x + 0.2f, point2.position.y, point2.position.z),"+" + (i_combo * 100/2).ToString(), Color.red);

            f_comboTime = Constant.COOLDOWN_COMBO_TIMER;
        }
        else
        {
            f_comboTime = Constant.COOLDOWN_COMBO_TIMER;
            i_combo = 0;
        }

        if (!Constant.is_MODE_FIND_STAR)
        {
            UpdateStar(F_score);
        }
    }

    private int ConvertToScore(int distance)
    {
        switch (distance)
        {
            case 0:
            case 1:
                {
                    return 50;
                }
            case 2:
                {
                    return 100;
                }
            case 3:
                {
                    return 150;
                }
            case 4:
                {
                    return 200;
                }
            case 5:
                {
                    return 250;
                }
            case 6:
                {
                    return 300;
                }
            case 7:
                {
                    return 400;
                }
            case 8:
                {
                    return 500;
                }
            case 9:
                {
                    return 600;
                }
            default:
                {
                    return 600;
                }
        }
    }

    public void InitStar()
    {
        cl_firstStar.Initialize();
        cl_secondStar.Initialize();
        cl_thirdStar.Initialize();

        F_score = 0;
        txt_score.text = "0";

        cl_firstStar.CurrentVal = 0;
        cl_secondStar.CurrentVal = 0;
        cl_thirdStar.CurrentVal = 0;

    }

    public void SetScoreStar(int first, int second, int third)
    {
        countStar = 0;
        if (Constant.is_MODE_FIND_STAR)
        {
            anim = new Star(0, 0, 0);
            star = new Star(first, second, third);
            cl_firstStar.MaxVal = 1;
            cl_secondStar.MaxVal = 2;
            cl_thirdStar.MaxVal = 3;
        }
        else
        {
            anim = new Star(0, 0, 0);
            star = new Star(first, second, third);
            cl_firstStar.MaxVal = first;
            cl_secondStar.MaxVal = second - first;
            cl_thirdStar.MaxVal = third - second;
        }
    }

    
    private void UpdateStar(float score)
    {

        if (score < star.first)
        {
            cl_firstStar.CurrentVal = score;
        }
        else if (score < star.second)
        {
            if (anim.first == 0)
            {
                MCManager.Instance.UpStar();
                anim.first++;
                anim_updateStar.SetTrigger("1star");
                countStar = 1;
            }
            cl_firstStar.CurrentVal = cl_firstStar.MaxVal;
            cl_secondStar.CurrentVal = score - star.first;
        }
        else if (score < star.third)
        {
            if (anim.first == 0)
            {
                MCManager.Instance.UpStar();
                anim.first++;
                anim_updateStar.SetTrigger("1star");
            }

            if (anim.second == 0)
            {
                MCManager.Instance.UpStar();
                anim.second++;
                anim_updateStar.SetTrigger("2star");
                countStar = 2;
            }
            cl_firstStar.CurrentVal = cl_firstStar.MaxVal;
            cl_secondStar.CurrentVal = cl_secondStar.MaxVal;
            cl_thirdStar.CurrentVal = score - star.second;
        }
        else 
        {
            if (anim.second == 0)
            {
                if (!TimeManager.Instance.b_gameOver)
                {
                    MCManager.Instance.UpStar();
                }
                anim.second++;
                anim_updateStar.SetTrigger("2star");
            }

            if (anim.third == 0)
            {
                if (!TimeManager.Instance.b_gameOver)
                {
                    MCManager.Instance.UpStar();
                }
                anim.third++;
                anim_updateStar.SetTrigger("3star");
                countStar = 3;
            }
            cl_firstStar.CurrentVal = cl_firstStar.MaxVal;
            cl_secondStar.CurrentVal = cl_secondStar.MaxVal;
            cl_thirdStar.CurrentVal = cl_thirdStar.MaxVal;
        }
    }

    public int GetStar()
    {
        return countStar;
    }

    public void UpdateStarInMode()
    {
        countStar++;

        if (!TimeManager.Instance.b_gameOver)
        {
            MCManager.Instance.UpStar();
        }
        if (countStar >= 1)
        {
            cl_firstStar.CurrentVal = cl_firstStar.MaxVal; ;
        }
        if (countStar >= 2)
        {
            cl_secondStar.CurrentVal = cl_secondStar.MaxVal; 
        }
        if (countStar >= 3)
        {
            countStar = 3;
            cl_thirdStar.CurrentVal = cl_thirdStar.MaxVal;
            TimeManager.Instance.b_gameOver = true;
        }
    }

}
