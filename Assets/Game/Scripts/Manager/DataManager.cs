﻿using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;
    public static DataManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<DataManager>(); return instance; } }

    public int boosterId;

    public bool unlock = true;
    public int isStart = 0;

    //------------------------------Worlds------------------------------------



    //--------------------------Energy and Gem--------------------------------

    public TimeRefillUnit energyRefill = new TimeRefillUnit(30, 5,1,"life","UpdateLife");

    public int addEnergy;
    public int consumeGem;

    //--------------------------Achievement--------------------------------
    public Subject achievementManager = new Subject();
    public AchievementLevel achievementLevel = new AchievementLevel(AchievementConstant.ACHIEVEMENT_LEVEL_ID);
    public Achievement3Stars achievement3Stars = new Achievement3Stars(AchievementConstant.ACHIEVEMENT_3STARS_ID);
    public AchievementBooster achievementBooster = new AchievementBooster(AchievementConstant.ACHIEVEMENT_BOOSTER_ID);
    public Achievement30Days achievement30Days = new Achievement30Days(AchievementConstant.ACHIEVEMENT_30DAYS_ID);
    public AchievementScore achievementScore = new AchievementScore(AchievementConstant.ACHIEVEMENT_SCORE_ID);
    public AchievementFree achievementFree = new AchievementFree(AchievementConstant.ACHIEVEMENT_FREE_ID);
    public AchievementInvite achievementInvite = new AchievementInvite(AchievementConstant.ACHIEVEMENT_INVITE_ID);

    //----------------------------Quest------------------------------
    public SubjectQuest questManager = new SubjectQuest();
    public QuestAds questAds;
    public Quest3Stars quest3Stars;
    public QuestBooster questBooster;
    public QuestHighScore questHighscore;
    public QuestInvite questInvite;
    public QuestSpin questSpin;
    public QuestEarnStar questEarnStar;

    //----------------------------Quest time-----------------------------
    public QuestTime questTime = new QuestTime();
    // public GiftTime giftTime = new GiftTime();

    private void Start()
    {
        PlayerPrefs.SetInt("CurrentLevel", 1);

        Debug.Log(PlayerPrefs.GetInt("CurrentLevel"));



        // Debug.Log(PlayerPrefs.GetInt("NewestLevel"));

        Load();


        // InitLevelData();

        // PlayerPrefs.DeleteKey("isStart");

        // if(!PlayerPrefs.HasKey("isStart"))
        // {
        //     Debug.Log("not start");
        // }

        if(!PlayerPrefs.HasKey("isStart"))
        {
            InitPreBoosterData();
            InitBoosterData();
            InitLevelData();

            InitAchievementData();
            PlayerPrefs.SetInt("isStart", 1);
        }

        //-------------------------Setup Booster------------------------------

        SetBoosterText();
        SetPreBoosterText();

        //-------------------------Setup Level--------------------------------



        //--------------------------Achievement Setup--------------------------------
        
        SetupAchievement();

        //--------------------------Quest Setup--------------------------------
        
        SetupQuest();
    }

    //----------------------------Setup Level---------------------------------


    //---------------------------Setup Achievement-----------------------------

    public void SetupAchievement()
    {
        achievementLevel.SetupProgress(PlayerPrefs.GetString("LevelTitle"), PlayerPrefs.GetString("LevelDes"), PlayerPrefs.GetInt("LevelLevel"), PlayerPrefs.GetInt("LevelProgress"), PlayerPrefs.GetInt("LevelMaxProgress"),  PlayerPrefs.GetInt("LevelRewardNumber"));
        achievement3Stars.SetupProgress(PlayerPrefs.GetString("3StarsTitle"), PlayerPrefs.GetString("3StarssDes"), PlayerPrefs.GetInt("3StarsLevel"), PlayerPrefs.GetInt("3StarsProgress"), PlayerPrefs.GetInt("3StarsMaxProgress"),  PlayerPrefs.GetInt("3StarsRewardNumber"));
        achievementBooster.SetupProgress(PlayerPrefs.GetString("BoosterTitle"), PlayerPrefs.GetString("BoosterDes"), PlayerPrefs.GetInt("BoosterLevel"), PlayerPrefs.GetInt("BoosterProgress"), PlayerPrefs.GetInt("BoosterMaxProgress"),  PlayerPrefs.GetInt("BoosterRewardNumber"));
        achievement30Days.SetupProgress(PlayerPrefs.GetString("30DaysTitle"), PlayerPrefs.GetString("30DaysDes"), PlayerPrefs.GetInt("30DaysLevel"), PlayerPrefs.GetInt("30DaysProgress"), PlayerPrefs.GetInt("30DaysMaxProgress"),  PlayerPrefs.GetInt("30DaysRewardNumber"));
        achievementScore.SetupProgress(PlayerPrefs.GetString("ScoreTitle"), PlayerPrefs.GetString("ScoreDes"), PlayerPrefs.GetInt("ScoreLevel"), PlayerPrefs.GetInt("ScoreProgress"), PlayerPrefs.GetInt("ScoreMaxProgress"),  PlayerPrefs.GetInt("ScoreRewardNumber"));
        achievementFree.SetupProgress(PlayerPrefs.GetString("FreeTitle"), PlayerPrefs.GetString("FreeDes"), PlayerPrefs.GetInt("FreeLevel"), PlayerPrefs.GetInt("FreeProgress"), PlayerPrefs.GetInt("FreeMaxProgress"),  PlayerPrefs.GetInt("FreeRewardNumber"));
        achievementInvite.SetupProgress(PlayerPrefs.GetString("InviteTitle"), PlayerPrefs.GetString("InviteDes"), PlayerPrefs.GetInt("InviteLevel"), PlayerPrefs.GetInt("InviteProgress"), PlayerPrefs.GetInt("InviteMaxProgress"),  PlayerPrefs.GetInt("InviteRewardNumber"));

        achievementManager.AddObserver(achievementLevel);
        achievementManager.AddObserver(achievement3Stars);
        achievementManager.AddObserver(achievementBooster);
        achievementManager.AddObserver(achievement30Days);
        achievementManager.AddObserver(achievementScore);
        achievementManager.AddObserver(achievementFree);
        achievementManager.AddObserver(achievementInvite);

        for(int i = 0; i < 7; i++)
        {
            achievementManager.SetUpUI(i);
        }
    }

    public void SetupQuest()
    {
        questAds = new QuestAds(QuestConstant.QUEST_ADS_ID);
        quest3Stars = new Quest3Stars(QuestConstant.QUEST_3STARS_ID);
        questBooster = new QuestBooster(QuestConstant.QUEST_BOOSTER_ID);
        questHighscore = new QuestHighScore(QuestConstant.QUEST_HIGHSCORE_ID);
        questInvite = new QuestInvite(QuestConstant.QUEST_INVITE_ID);
        questSpin = new QuestSpin(QuestConstant.QUEST_SPIN_ID);
        questEarnStar = new QuestEarnStar(QuestConstant.QUEST_EARNSTAR_ID);

        InitQuestData();

        questAds.SetupProgress(PlayerPrefs.GetString("QuestAdsTitle"), PlayerPrefs.GetInt("QuestAdsProgress"), PlayerPrefs.GetInt("QuestAdsMaxProgress"), PlayerPrefs.GetInt("QuestAdsRewardNumber"));
        questAds.OnSetUpUI(QuestConstant.QUEST_ADS_ID);
        quest3Stars.SetupProgress(PlayerPrefs.GetString("Quest3StarsTitle"), PlayerPrefs.GetInt("Quest3StarsProgress"), PlayerPrefs.GetInt("Quest3StarsMaxProgress"), PlayerPrefs.GetInt("Quest3StarsRewardNumber"));
        quest3Stars.OnSetUpUI(QuestConstant.QUEST_3STARS_ID);
        questBooster.SetupProgress(PlayerPrefs.GetString("QuestBoosterTitle"), PlayerPrefs.GetInt("QuestBoosterProgress"), PlayerPrefs.GetInt("QuestBoosterMaxProgress"), PlayerPrefs.GetInt("QuestBoosterRewardNumber"));
        questBooster.OnSetUpUI(QuestConstant.QUEST_BOOSTER_ID);
        questHighscore.SetupProgress(PlayerPrefs.GetString("QuestHighScoreTitle"), PlayerPrefs.GetInt("QuestHighScoreProgress"), PlayerPrefs.GetInt("QuestHighScoreMaxProgress"), PlayerPrefs.GetInt("QuestHighScoreRewardNumber"));
        questHighscore.OnSetUpUI(QuestConstant.QUEST_HIGHSCORE_ID);
        questInvite.SetupProgress(PlayerPrefs.GetString("QuestInviteTitle"), PlayerPrefs.GetInt("QuestInviteProgress"), PlayerPrefs.GetInt("QuestInviteMaxProgress"), PlayerPrefs.GetInt("QuestInviteRewardNumber"));
        questInvite.OnSetUpUI(QuestConstant.QUEST_INVITE_ID);
        questSpin.SetupProgress(PlayerPrefs.GetString("QuestSpinTitle"), PlayerPrefs.GetInt("QuestSpinProgress"), PlayerPrefs.GetInt("QuestSpinMaxProgress"), PlayerPrefs.GetInt("QuestSpinRewardNumber"));
        questSpin.OnSetUpUI(QuestConstant.QUEST_SPIN_ID);
        questEarnStar.SetupProgress(PlayerPrefs.GetString("QuestEarnStarTitle"), PlayerPrefs.GetInt("QuestEarnStarProgress"), PlayerPrefs.GetInt("QuestEarnStarMaxProgress"), PlayerPrefs.GetInt("QuestEarnStarRewardNumber"));
        questEarnStar.OnSetUpUI(QuestConstant.QUEST_EARNSTAR_ID);

        questManager.AddQuestObserver(questAds);
        questManager.AddQuestObserver(quest3Stars);
        questManager.AddQuestObserver(questBooster);
        questManager.AddQuestObserver(questHighscore);
        questManager.AddQuestObserver(questInvite);
        questManager.AddQuestObserver(questSpin);
        questManager.AddQuestObserver(questEarnStar);
       
        questTime.Start();
        // giftTime.Start();
    }

    //----------------------------------------------------------------------------

    private void Update() 
    {
        energyRefill.Update();
        questTime.Update();
    }

    public void ChangeGemNumber(int gemNumberAdd)
    {
        if((PlayerPrefs.GetInt("GemNumber") + gemNumberAdd) >= 0)
        {
            PlayerPrefs.SetInt("GemNumber", PlayerPrefs.GetInt("GemNumber") + gemNumberAdd);

            if (gemNumberAdd >= 5)
            {
                CUIManager.instance.BuyDiamod(gemNumberAdd);
            }
            else
            {
                CUIManager.Instance.gemNumberText.text = PlayerPrefs.GetInt("GemNumber").ToString();
            }
        }
    }

    //----------------------------Level manager--------------------------------------

    public void InitLevelData()
    {
        PlayerPrefs.SetInt("CurrentLevel", 1);
    }

    //----------------------------Energy refill--------------------------------------

    public void AddEnergy()
    {
        Debug.Log("Add Energy called");
        if(GetCurrentEnergy() < 5 && PlayerPrefs.GetInt("GemNumber") >= 5)
        {
            ChangeGemNumber(consumeGem);
            energyRefill.Add(addEnergy);
            energyRefill.Save();
            CUIManager.instance.BuyHeart(addEnergy);
        }
    }

    public void AddEnergy(int addEnergy)
    {
        energyRefill.Add(addEnergy);
        energyRefill.Save();
    }

    public int GetCurrentEnergy()
    {
        return energyRefill.GetCurrentValue();
    }

    public string GetNextLifeTime()
    {
        return energyRefill.GetTimeToNextAdd(2,"Full");
    }

    public void Consume(int amount)
    {
        energyRefill.Consume(amount);
    } 

    public void Reset()
    {
        energyRefill.Reset();
    }

    public void Load()
    {
        energyRefill.Load();
    }

    //----------------------------Set level star--------------------------------------


    //--------------------------Save Star-----------------------------
    public void DeleteAllData()
    {
        PlayerPrefs.DeleteAll();
    }

    //-------------------------Init Data-----------------------------

    public void SetBoosterText()
    {
        BoosterManager.Instance.booster[3].number = PlayerPrefs.GetInt("BoosterTime");
        BoosterManager.Instance.booster[3].Reset();
        BoosterManager.Instance.booster[4].number = PlayerPrefs.GetInt("BoosterHammer");
        BoosterManager.Instance.booster[4].Reset();
        BoosterManager.Instance.booster[5].number = PlayerPrefs.GetInt("BoosterMix");
        BoosterManager.Instance.booster[5].Reset();
        BoosterManager.Instance.booster[6].number = PlayerPrefs.GetInt("BoosterHint");
        BoosterManager.Instance.booster[6].Reset();
    }

    public void InitBoosterData()
    {
        PlayerPrefs.SetInt("BoosterTime", 1);
        PlayerPrefs.SetInt("BoosterHammer", 1);
        PlayerPrefs.SetInt("BoosterMix", 1);
        PlayerPrefs.SetInt("BoosterHint", 1);
    }

    public void SetPreBoosterText()
    {
        BoosterManager.Instance.booster[0].number = PlayerPrefs.GetInt("PreBoosterDoubleLink");
        BoosterManager.Instance.booster[0].Reset();
        BoosterManager.Instance.booster[1].number = PlayerPrefs.GetInt("PreBoosterRemoveType");
        BoosterManager.Instance.booster[1].Reset();
        BoosterManager.Instance.booster[2].number = PlayerPrefs.GetInt("PreBoosterKey");
        BoosterManager.Instance.booster[2].Reset();

    }

    

    public void InitPreBoosterData()
    {
        PlayerPrefs.SetInt("PreBoosterDoubleLink", 1);
        PlayerPrefs.SetInt("PreBoosterRemoveType", 1);
        PlayerPrefs.SetInt("PreBoosterKey", 1);
    }

    public void InitAchievementData()
    {
        Debug.Log("Init achievement");
        InitLevelAchievement();

        Init3StarsAchievement();
        
        InitBoosterAchievement();
        
        Init30DaysAchievement();
        
        InitScoreAchievement();
        
        InitFreeAchievement();
        
        InitInviteAchievement();
    }

    public void InitLevelAchievement()
    {
        PlayerPrefs.SetInt("LevelLevel", 1);
        PlayerPrefs.SetString("LevelTitle", AchievementConstant.ACHIEVEMENT_LEVEL50_TITLE);
        PlayerPrefs.SetString("LevelDes", AchievementConstant.ACHIEVEMENT_LEVEL50_DES);
        PlayerPrefs.SetInt("LevelProgress", 0);
        PlayerPrefs.SetInt("LevelMaxProgress", 20);
        PlayerPrefs.SetInt("LevelRewardNumber", 1);
    }
    public void Init3StarsAchievement()
    {
        PlayerPrefs.SetInt("3StarsLevel", 1);
        PlayerPrefs.SetString("3StarsTitle", AchievementConstant.ACHIEVEMENT_STARS10_TITLE);
        PlayerPrefs.SetString("3StarsDes", AchievementConstant.ACHIEVEMENT_STARS10_DES);
        PlayerPrefs.SetInt("3StarsProgress", 0);
        PlayerPrefs.SetInt("3StarsMaxProgress", 10);
        PlayerPrefs.SetInt("3StarsRewardNumber", 1);
    }
    public void InitBoosterAchievement()
    {
        PlayerPrefs.SetInt("BoosterLevel", 1);
        PlayerPrefs.SetString("BoosterTitle", AchievementConstant.ACHIEVEMENT_BOOSTER5_TITLE);
        PlayerPrefs.SetString("BoosterDes", AchievementConstant.ACHIEVEMENT_BOOSTER5_DES);
        PlayerPrefs.SetInt("BoosterProgress", 0);
        PlayerPrefs.SetInt("BoosterMaxProgress", 5);
        PlayerPrefs.SetInt("BoosterRewardNumber", 1);
    }
    public void Init30DaysAchievement()
    {
        Debug.Log("Init 30days achievement");
        PlayerPrefs.SetInt("30DaysLevel", 1);
        PlayerPrefs.SetString("30DaysTitle", AchievementConstant.ACHIEVEMENT_30DAYS_TITLE);
        PlayerPrefs.SetString("30DaysDes", AchievementConstant.ACHIEVEMENT_30DAYS_DES);
        PlayerPrefs.SetInt("30DaysProgress", 0);
        PlayerPrefs.SetInt("30DaysMaxProgress", 30);
        PlayerPrefs.SetInt("30DaysRewardNumber", 1);
    }
    public void InitScoreAchievement()
    {
        PlayerPrefs.SetInt("ScoreLevel", 1);
        PlayerPrefs.SetString("ScoreTitle", AchievementConstant.ACHIEVEMENT_SCORE10K_TITLE);
        PlayerPrefs.SetString("ScoreDes", AchievementConstant.ACHIEVEMENT_SCORE10K_DES);
        PlayerPrefs.SetInt("ScoreProgress", 0);
        PlayerPrefs.SetInt("ScoreMaxProgress", 10000);
        PlayerPrefs.SetInt("ScoreRewardNumber", 1);
    }
    public void InitFreeAchievement()
    {
        PlayerPrefs.SetInt("FreeLevel", 1);
        PlayerPrefs.SetString("FreeTitle", AchievementConstant.ACHIEVEMENT_FREE100_TITLE);
        PlayerPrefs.SetString("FreeDes", AchievementConstant.ACHIEVEMENT_FREE100_DES);
        PlayerPrefs.SetInt("FreeProgress", 0);
        PlayerPrefs.SetInt("FreeMaxProgress", 100);
        PlayerPrefs.SetInt("FreeRewardNumber", 1);
    }
    public void InitInviteAchievement()
    {
        PlayerPrefs.SetInt("InviteLevel", 1);
        PlayerPrefs.SetString("InviteTitle", AchievementConstant.ACHIEVEMENT_INVITE1_TITLE);
        PlayerPrefs.SetString("InviteDes", AchievementConstant.ACHIEVEMENT_INVITE1_DES);
        PlayerPrefs.SetInt("InviteProgress", 0);
        PlayerPrefs.SetInt("InviteMaxProgress", 1);
        PlayerPrefs.SetInt("InviteRewardNumber", 1);
    }

    //--------------------------Quest--------------------------------

    public void InitQuestData()
    {
        InitQuestAdsData();
        InitQuest3StarsData();
        InitQuestBoosterData();
        InitQuestHighScoreData();
        InitQuestInviteData();
        InitQuestSpinData();
        InitQuestEarnStarData();
    }

    public void InitQuestAdsData()
    {
        questAds.maxProgress = questAds.randomProgress[Random.Range(0, questAds.randomProgress.Length)];
        
        if(questAds.maxProgress == 1)
        {
            PlayerPrefs.SetString("QuestAdsTitle", "Watch ads " + questAds.maxProgress + " time");
        }
        else
        {
            PlayerPrefs.SetString("QuestAdsTitle", "Watch ads " + questAds.maxProgress + " times");
        }

        PlayerPrefs.SetInt("QuestAdsProgress", 0);
        PlayerPrefs.SetInt("QuestAdsMaxProgress", questAds.maxProgress);
        PlayerPrefs.SetInt("QuestAdsRewardNumber", 2);
        PlayerPrefs.SetInt("QuestAdsCanClaim", 0);
        PlayerPrefs.SetInt("QuestAdsCompleted", 0);
    }

    public void InitQuest3StarsData()
    {
        quest3Stars.maxProgress = quest3Stars.randomProgress[Random.Range(0, quest3Stars.randomProgress.Length)];
        
        if(quest3Stars.maxProgress == 1)
        {
            PlayerPrefs.SetString("Quest3StarsTitle", "Complete " + quest3Stars.maxProgress + " level with 3 stars");
        }
        else
        {
            PlayerPrefs.SetString("Quest3StarsTitle", "Complete " + quest3Stars.maxProgress + " levels with 3 stars");
        }

        PlayerPrefs.SetInt("Quest3StarsProgress", 0);
        PlayerPrefs.SetInt("Quest3StarsMaxProgress", quest3Stars.maxProgress);
        PlayerPrefs.SetInt("Quest3StarsRewardNumber", 1);
        PlayerPrefs.SetInt("Quest3StarsCanClaim", 0);
        PlayerPrefs.SetInt("Quest3StarsCompleted", 0);
    }

    public void InitQuestBoosterData()
    {
        string booster1;
        string booster2;
        string booster3;
    
        questBooster.randomProgress[0] = Random.Range(3, 7);

        do
        {
            questBooster.randomProgress[1] = Random.Range(3, 7);
        } while(questBooster.randomProgress[1] == questBooster.randomProgress[0]);
        
        do
        {
            questBooster.randomProgress[2] = Random.Range(3, 7);
        } while (questBooster.randomProgress[2] == questBooster.randomProgress[1] || questBooster.randomProgress[2] == questBooster.randomProgress[0]);
    
        booster1 = questBooster.RandomBooster(questBooster.randomProgress[0]);
        booster2 = questBooster.RandomBooster(questBooster.randomProgress[1]);
        booster3 = questBooster.RandomBooster(questBooster.randomProgress[2]);

        PlayerPrefs.SetString("QuestBoosterTitle", "Use 3 boosters " + booster1 + ", " + booster2 + ", " + booster3 + " in level");

        PlayerPrefs.SetInt("QuestBoosterProgress", 0);
        PlayerPrefs.SetInt("QuestBoosterMaxProgress", questBooster.maxProgress);
        PlayerPrefs.SetInt("QuestBoosterRewardNumber", 1);
        PlayerPrefs.SetInt("QuestBoosterCanClaim", 0);
        PlayerPrefs.SetInt("QuestBoosterCompleted", 0);
    }

    public void InitQuestHighScoreData()
    {
        PlayerPrefs.SetString("QuestHighScoreTitle", "Break high score level " + questHighscore.level);

        PlayerPrefs.SetInt("QuestHighScoreProgress", 0);
        PlayerPrefs.SetInt("QuestHighScoreMaxProgress", questHighscore.maxProgress);
        PlayerPrefs.SetInt("QuestHighScoreRewardNumber", 1);
        PlayerPrefs.SetInt("QuestHighScoreCanClaim", 0);
        PlayerPrefs.SetInt("QuestHighScoreCompleted", 0);
    }

    public void InitQuestInviteData()
    {
        PlayerPrefs.SetString("QuestInviteTitle", "Invite a friend");

        PlayerPrefs.SetInt("QuestInviteProgress", 0);
        PlayerPrefs.SetInt("QuestInviteMaxProgress", questHighscore.maxProgress);
        PlayerPrefs.SetInt("QuestInviteRewardNumber", 1);
        PlayerPrefs.SetInt("QuestInviteCanClaim", 0);
        PlayerPrefs.SetInt("QuestInviteCompleted", 0);
    }

    public void InitQuestSpinData()
    {
        questSpin.maxProgress = questSpin.randomProgress[Random.Range(0, questSpin.randomProgress.Length)];
        
        if(questAds.maxProgress == 1)
        {
            PlayerPrefs.SetString("QuestSpinTitle", "Spin " + questSpin.maxProgress + " time");
        }
        else
        {
            PlayerPrefs.SetString("QuestSpinTitle", "Spin " + questSpin.maxProgress + " times");
        }

        PlayerPrefs.SetInt("QuestSpinProgress", 0);
        PlayerPrefs.SetInt("QuestSpinMaxProgress", questSpin.maxProgress);
        PlayerPrefs.SetInt("QuestSpinRewardNumber", 2);
        PlayerPrefs.SetInt("QuestSpinCanClaim", 0);
        PlayerPrefs.SetInt("QuestSpinCompleted", 0);
    }

    public void InitQuestEarnStarData()
    {
        questEarnStar.maxProgress = questEarnStar.randomProgress[Random.Range(0, questEarnStar.randomProgress.Length)];
        
        PlayerPrefs.SetString("QuestEarnStarTitle", "Earn " + questEarnStar.maxProgress + " stars");

        PlayerPrefs.SetInt("QuestEarnStarProgress", 0);
        PlayerPrefs.SetInt("QuestEarnStarMaxProgress", questEarnStar.maxProgress);
        PlayerPrefs.SetInt("QuestEarnStarRewardNumber", 2);
        PlayerPrefs.SetInt("QuestEarnStarCanClaim", 0);
        PlayerPrefs.SetInt("QuestEarnStarCompleted", 0);
    }

    //----------------------------Init world stars-------------------------------
    
    //----------------------------Cheat--------------------------------

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();

        InitPreBoosterData();
        InitBoosterData();
        // InitLevelData();
        InitAchievementData();

        //-------------------------------Reset level-------------------------------


        //-------------------------------Reset achievement and quest-------------------------------

        SetupAchievement();
        SetupQuest();

        //-------------------------------Reset Stars-------------------------------


    }

    bool unlocked = false;
    public void Unlock()
    {
        if(unlocked == false)
        {
            for(int i = 0; i < CUIManager.Instance.bricks.Length; i++)
            {
                for (int j = 0; j < CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage.Length; j++)
                {
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Image>().sprite = CUIManager.Instance.unlockedLevel;
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Button>().interactable = true;
                }
                CUIManager.Instance.bricks[i].GetComponent<WorldStar>().background.sprite = CUIManager.Instance.worldBG[i];    
            }

            unlocked = true;
        }
        else
        {
            for(int i = 0; i < CUIManager.Instance.bricks.Length; i++)
            {
                for (int j = 0; j < CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage.Length; j++)
                {
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Image>().sprite = CUIManager.Instance.lockedLevel;
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Button>().interactable = false;
                }    
            }

            unlocked = false;

            for(int i = 0; i < CUIManager.Instance.bricks.Length; i++)
            {
                for (int j = 0; j < PlayerPrefs.GetInt("CurrentLevel"); j++)
                {
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Image>().sprite = CUIManager.Instance.unlockedLevel;
                    CUIManager.Instance.bricks[i].GetComponent<WorldStar>().stage[j].GetComponent<Button>().interactable = true;
                }    
            }
        }
    }

    //-------------------------Booster-----------------------------

    public void BuyBooster()
    {
        if(boosterId == 0)
        {
            PlayerPrefs.SetInt("PreBoosterDoubleLink", PlayerPrefs.GetInt("PreBoosterDoubleLink") + 1);
            BoosterManager.Instance.booster[0].number = PlayerPrefs.GetInt("PreBoosterDoubleLink");
            BoosterManager.Instance.booster[0].Reset();
        }
        else if(boosterId == 1)
        {
            PlayerPrefs.SetInt("PreBoosterRemoveType", PlayerPrefs.GetInt("PreBoosterRemoveType") + 1);
            BoosterManager.Instance.booster[1].number = PlayerPrefs.GetInt("PreBoosterRemoveType");
            BoosterManager.Instance.booster[1].Reset();
        }
        else if(boosterId == 2)
        {
            PlayerPrefs.SetInt("PreBoosterKey", PlayerPrefs.GetInt("PreBoosterKey") + 1);
            BoosterManager.Instance.booster[2].number =  PlayerPrefs.GetInt("PreBoosterKey");
            BoosterManager.Instance.booster[2].Reset();
        }
        else if(boosterId == 3)
        {
            PlayerPrefs.SetInt("BoosterTime", PlayerPrefs.GetInt("BoosterTime") + 1);
            BoosterManager.Instance.booster[3].number = PlayerPrefs.GetInt("BoosterTime");
            BoosterManager.Instance.booster[3].Reset();
        }
        else if(boosterId == 4)
        {
            PlayerPrefs.SetInt("BoosterHammer", PlayerPrefs.GetInt("BoosterHammer") + 1);
            BoosterManager.Instance.booster[4].number = PlayerPrefs.GetInt("BoosterHammer");
            BoosterManager.Instance.booster[4].Reset();
        }
        else if(boosterId == 5)
        {
            PlayerPrefs.SetInt("BoosterMix", PlayerPrefs.GetInt("BoosterMix") + 1);
            BoosterManager.Instance.booster[5].number = PlayerPrefs.GetInt("BoosterMix");
            BoosterManager.Instance.booster[5].Reset();
        }
        else if(boosterId == 6)
        {
            PlayerPrefs.SetInt("BoosterHint", PlayerPrefs.GetInt("BoosterHint") + 1);
            BoosterManager.Instance.booster[6].number = PlayerPrefs.GetInt("BoosterHint");
            BoosterManager.Instance.booster[6].Reset();
        }

        UIManager.Instance.CloseGetBoost();
    }

    public void BuyLives(int m_MaxVlue, int m_CurrentValue)
    {
        addEnergy = m_MaxVlue - m_CurrentValue;
        consumeGem = -(m_MaxVlue - m_CurrentValue);
    }

    //-------------------------Daily Gift-----------------------------

    public void InitDailyGiftData()
    {
        PlayerPrefs.SetInt("GiftClaim", 1);
        PlayerPrefs.SetInt("CurrentGift", -1);
    }
}