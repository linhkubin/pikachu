﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterManager : MonoBehaviour
{
    private static BoosterManager instance;
    public static BoosterManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<BoosterManager>(); return instance; } }

    public Boost[] UI_boosts;
    public Booster[] booster;

    private GameObject suggestHammer;

    private void Start()
    {
        for (int i = 0; i < UI_boosts.Length; i++)
        {
            booster[i].SetNumber( UI_boosts[i].id, UI_boosts[i].number, UI_boosts[i].block,UI_boosts[i].price, UI_boosts[i].icon);
        }

        suggestHammer = booster[4].GetComponent<BoosterButton>().g_chooseEffect;
    }

    public void InActiveButton()
    {
        for (int i = 3; i < 7; i++)
        {
            booster[i].InActive();
        }
        
    }

    public void ActiveButton()
    {
        for (int i = 3; i < 7; i++)
        {
            booster[i].Active();
        }

    }

    public void OnlyActive(int index)
    {
        for (int i = 3; i < 7; i++)
        {
            if (i != index)
            {
                booster[i].InActive();
            }
            
        }
    }

    public void SuggestHammer(bool logic)
    {
        suggestHammer.SetActive(logic);
    }

}

[Serializable]
public class Boost
{
    public string name;
    public int id;
    public int number;
    public bool block;
    public int price;
    public Sprite icon;
}