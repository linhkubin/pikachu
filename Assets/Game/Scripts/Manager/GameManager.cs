﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{ 
    private static GameManager instance;
    public static GameManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<GameManager>(); return instance; } }

    //------------------Node in Cell----------------

    public List<GameObject> L_listNode = new List<GameObject>();

    public List<Node> m_Node = new List<Node>();
    private Node GetNode(int index)
    {
        if (index >= m_Node.Count)
        {
            m_Node.Add(L_listNode[index].GetComponent<Node>());
        }
        return m_Node[index];
    }

    public List<GameObject> L_position = new List<GameObject>();
    public List<SpriteRenderer> L_spritePosition = new List<SpriteRenderer>();

    //------------------line + select------------------------
    //[SerializeField]
    //private GameObject g_linePrefabs;

    private List<GameObject> l_listLine = new List<GameObject>();
    //-----------position parent list---------------
    [SerializeField]
    private Transform tf_board;

    //-------------------choice---------------------
    private List<GameObject> l_listChoose = new List<GameObject>();

    //-----------------list number pokemon----------
    // list contain a number of number
    // 10 node each number
    private int[] i_initNumber = new int[30]; 

    int i_numberOfEachPokemon = ((Constant.NUMBER_ROW - 2) * (Constant.NUMBER_COLUNM - 2)) / Constant.NUMBER_TYPE_POKEMON;
    //------------------sugget----------------------
    public int i_suggest1 = 0;

    public int i_suggest2 = 0;

    const float f_timeSuggest = 5.5f;

    public int itemHint = 0;

    //------------------time over-------------------

    public float f_timeClick = 0;

    //-----------------wink-------------------------

    private float f_timeEyeWink = 0;

    private float f_timeEyeWinkDuration = 0;

    //------------------bonus-----------------------
    //if destroy 3 a couple of node , it will generate a couple bonus node
    private int i_valueBonus = 0;

    private int i_bonus = 1;

    //----------------------------------------------

    public List<GameObject> l_lock;


    //------------------json-----------------
    //public MapJson mapManager = new MapJson();

    //----------------Wall-------------------

    public int NUMBER_WALL = 0;

    //--------------------load Map------------------

    public static int MAP_LEVEL = 0;

    //---------------select------------------------

    public GameObject g_selectPrefab;

    //----------------empty prefabs----------------
    [SerializeField]
    private GameObject g_nodeEmpty;

    //----------------back ground ingame-----------

    [SerializeField]
    private Image img_background;

    [SerializeField]
    private Image img_MCstand;


    //----------------spăn ------------------------

    public MachineSpawn machineSpawn;
    
    //---------------------------------------------

    /* 
     * init 9x12 node
     * play 7x10 node
     * 7 type pokemon
     * 10 node / type
     */
    public void Init()
    {
        //Debug.Log("map __ " + MAP_LEVEL);
        InitNodeEmpty();
    }

    public void ResetStage()
    {
        ClearStage();

        ModeRequest.Instance.Reset();

        //-------------
        MapJson mapManager = new MapJson();
        ////
        TextAsset assets = Resources.Load(MAP_LEVEL.ToString()) as TextAsset;

        //Debug.Log(assets);

        mapManager = JsonUtility.FromJson<MapJson>(assets.text);

        NUMBER_WALL = mapManager.numberWall;

        int numTypeMain = mapManager.numberTypeMain;

        int numTypeExtra = mapManager.numberTypeMap;

        Constant.is_MODE_REQUEST = mapManager.modeRequest.modeRequest;

        if (Constant.is_MODE_REQUEST)
        {
            if (mapManager.modeRequest.first.numberNode !=0 && mapManager.modeRequest.first.numberRequest != 0)
            {
                ModeRequest.Instance.SetRequest(mapManager.modeRequest.first.numberNode, mapManager.modeRequest.first.numberRequest);
            }

            if (mapManager.modeRequest.second.numberNode != 0 && mapManager.modeRequest.second.numberRequest != 0)
            {
                ModeRequest.Instance.SetRequest(mapManager.modeRequest.second.numberNode, mapManager.modeRequest.second.numberRequest);
            }

            if (mapManager.modeRequest.third.numberNode != 0 && mapManager.modeRequest.third.numberRequest != 0)
            {
                ModeRequest.Instance.SetRequest(mapManager.modeRequest.third.numberNode, mapManager.modeRequest.third.numberRequest);
            }
        }

        Constant.is_MODE_FIND_STAR = mapManager.modeFindStar.modeFindStar;

        if (Constant.is_MODE_FIND_STAR)
        {
            ElementManager.Instance.weather.NewNotice(2);
            ModeFindStar.Instance.Reset();
        }


        ////init sprite
        SpriteManager.Instance.InitListType(numTypeMain, numTypeExtra);


        Constant.NUMBER_TYPE_POKEMON = numTypeMain + numTypeExtra;

        int numRand = mapManager.numberTypeRand;

        // init time + start

        Constant.MAX_TIME_PLAY = mapManager.timePlay;

        TimeManager.Instance.InitTimer();

        //Debug.Log("time play : " + Constant.MAX_TIME_PLAY);

        //Debug.Log(" * : " + mapManager.star.first + "_" + mapManager.star.second + "_" + mapManager.star.third);

        //------------------reset ingame------------
        ChangeBackground();

        //ClearStage();

        UIManager.Instance.UpdateStage();

        ScoreManager.Instance.SetScoreStar(mapManager.star.first, mapManager.star.second, mapManager.star.third);

        //----------------------
        //khoi tao list node binh thuong
        InitNumberOfNode(Constant.NUMBER_TYPE_POKEMON, numRand);
        int prev_tempNum = 0;// giam ty le 2 con canh nhau


        //doc list node
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            int id = 0;
            int number = 0;
            
            if (mapManager.tileList[i].number == -1)
            {
                //random gia tri node
                int tempNum = Random.Range(1, Constant.NUMBER_TYPE_POKEMON + 1);
                while (i_initNumber[tempNum] <= 0)
                {
                    tempNum--;
                    if (tempNum == prev_tempNum && Random.Range(0, 2) == 0)
                    {
                        tempNum--;
                    }
                    if (tempNum <= 0)
                    {
                        tempNum = Constant.NUMBER_TYPE_POKEMON;
                    }
                }

                prev_tempNum = tempNum;
                i_initNumber[tempNum]--;
                //-------

                number = tempNum;
            }
            else
            {
                number = mapManager.tileList[i].number;
            }

            id = mapManager.tileList[i].Id;

            InitNodeOrigin(i, id, number, mapManager.tileList[i].numType);

        }


        // doc environment affect
        for (int i = 0; i < mapManager.EAList.Count; i++)
        {
            
            if (mapManager.EAList[i].type == 0)
            {
                ElementManager.Instance.weather.NewNotice(3);
                tempEA = SimplePool.Spawn("WindRight", Vector3.zero, Quaternion.identity);
                tempEA.GetComponent<WindEast>().SetTime(mapManager.EAList[i].timeStart, mapManager.EAList[i].timeRemain);

                //tempEA.transform.SetParent(tf_wind, false);
            }

            if (mapManager.EAList[i].type == 1)
            {
                ElementManager.Instance.weather.NewNotice(3);

                tempEA = SimplePool.Spawn("WindLeft", Vector3.zero, Quaternion.identity);
                tempEA.GetComponent<WindWest>().SetTime(mapManager.EAList[i].timeStart, mapManager.EAList[i].timeRemain);

                //tempEA.transform.SetParent(tf_wind, false);

            }

            if (mapManager.EAList[i].type == 2)
            {
                ElementManager.Instance.weather.NewNotice(3);
                tempEA = SimplePool.Spawn("Cloudy", Vector3.up * 15.5f , Quaternion.identity);
                tempEA.GetComponent<Cloudy>().SetTime(mapManager.EAList[i].timeStart);
            }

            ElementManager.Instance.Add(tempEA);
        }

        //Debug.Log(L_listNode.Count);


        ////for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        ////{
        ////    Debug.Log("__index : " + i + "______id : " + L_listNode[i].GetComponent<Node>().I_id + "____ number : " + L_listNode[i].GetComponent<Node>().I_numberNode + "____ lock : " + L_listNode[i].GetComponent<Node>().B_lockClick);
        ////}
        ///

        if (Constant.is_MODE_FIND_STAR)
        {
            ModeFindStar.Instance.StartMode();
        }
    }

    GameObject tempEA;

    private void  ChangeBackground()
    {
        img_background.sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_background;
        img_MCstand.sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_MCstand;
    }

    //clear stage
    public void ClearStage()
    {
        f_timeClick = 0;

        itemHint = 0;

        ItemManager.Instance.useHammer = false;

        mixException = 0;

        ClearChoose();

        i_bonus = 0;

        i_suggest1 = 0;

        ItemManager.Instance.ResetHint();


        ScoreManager.Instance.InitStar();

        EnemyManager.Instance.InitEnemy();

        ElementManager.Instance.InitEAffect();

        checkedPreboost = false;

        BonusManager.Instance.bonusDoubleConect = false;

        BonusManager.Instance.popUPCaution.SetActive(false);

        Constant.have_SPAWN_MACHINE = false;

        ElementManager.Instance.weather.Reset();

        // reset mode
    }

    //khoi tao so node random
    private void InitNumberOfNode(int numberPokemon,int totalPokemon)
    {

        if (totalPokemon % 2 == 1)
        {
            Debug.Log("ERROR total pokemen % 2 == 1");
        }

        for (int i = 1; i <= numberPokemon; i++)
        {
            i_initNumber[i] = ((totalPokemon / numberPokemon) / 2) * 2;
        }

        totalPokemon -= i_initNumber[1] * numberPokemon;

        for (int i = 1; i <= numberPokemon; i++)
        {
            if (totalPokemon > 0)
            {
                i_initNumber[i] += 2;
                totalPokemon -= 2;
            }
            else break;
        }

        //for (int i = 1; i <= numberPokemon + 2; i++)
        //{
        //    Debug.Log(i + "_" + i_initNumber[i]);
        //}

    }

    //khoi tao node trong
    private void InitNodeEmpty()
    {

        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            GameObject position;
            position = Instantiate(SpriteManager.Instance.s_shadow);
            position.transform.SetParent(tf_board, false);


            if (i % 2 == (i / Constant.NUMBER_ROW) % 2)
            {
                position.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.Squares;
            }

            L_position.Add(position);
            L_spritePosition.Add(position.GetComponent<SpriteRenderer>());

            GameObject node;
            node = Instantiate(g_nodeEmpty);
            node.transform.SetParent(position.transform, false);
            node.GetComponent<Node>().i_index = i;

            L_listNode.Add(node);

        }

    }

    //khoi tao node base

    private void InitNodeOrigin(int index, int id, int number = 0, int numType = 0)
    {
        GetNode(index).ResetState();

        L_listNode[index].transform.localScale = new Vector3(Mathf.Abs(L_listNode[index].transform.localScale.x), L_listNode[index].transform.localScale.y, L_listNode[index].transform.localScale.z);
        GetNode(index).SetLayerObstacle(1 + 10 * (index / Constant.NUMBER_ROW));

        if (number > 0)
        {

            GetNode(index).SetAnim(SpriteManager.Instance.l_spritesType[number - 1].number);

            GetNode(index).SetSpeedAnim((float)((number) % 3) / 10 + 1);

            GetNode(index).SetClear(SpriteManager.Instance.l_spritesType[number - 1].clear);

            //GetNode(index).GetSpriteRenderer().color = Color.clear;
            //L_listNode[index].GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.l_spritesType[number - 1].sprite;
            number = SpriteManager.Instance.l_spritesType[number - 1].number;
            GetNode(index).B_lockClick = false;

        }
        else if (id == Constant.ID_TYPE_BARRIER)
        {
            if (numType == 3)// tree
            {
                GetNode(index).SetLayerObstacle(3 + 10 *( index / Constant.NUMBER_ROW));
            }
            GetNode(index).SetClear(SpriteManager.Instance.wall[NUMBER_WALL].s_barrier[numType].clear);
            GetNode(index).GetSpriteRenderer().sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_barrier[numType].sprite;
            int ran = Random.Range(0, 2) == 0 ? 1 : -1;
            L_listNode[index].transform.localScale = new Vector3(L_listNode[index].transform.localScale.x * ran, L_listNode[index].transform.localScale.y, L_listNode[index].transform.localScale.z);
        }
        else if (id == Constant.ID_TYPE_ENVIRONMENT && numType > 0)
        {
            L_spritePosition[index].color = Color.clear;
            GetNode(index).SetLayerObstacle(3);
            GetNode(index).GetSpriteRenderer().sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_environment[numType - 1].sprite;
        }
        else if (id == Constant.ID_TYPE_NODE_WATER)
        {
            GetNode(index).SetLayerObstacle(2);
            GetNode(index).GetSpriteRenderer().sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_water[numType].sprite;
        }
        else if (id == Constant.ID_TYPE_BOX)
        {
            GetNode(index).SetClear(SpriteManager.Instance.wall[NUMBER_WALL].s_box[numType].clear);
            GetNode(index).GetSpriteRenderer().color = Color.clear;
            GetNode(index).SetFace(SpriteManager.Instance.g_box);
            GetNode(index).g_faceId.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[NUMBER_WALL].s_box[numType].sprite;
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        //nothing
        if (id == Constant.ID_NOTHING)
        {
            //GetNode(index).GetSpriteRenderer().color = Color.clear;
            //L_listNode[index].transform.parent.GetComponent<SpriteRenderer>().color = Color.clear;
            L_spritePosition[index].color = Color.clear;
        }
        //bound
        if (id == Constant.ID_TYPE_ENVIRONMENT && numType == 0)
        {
            //GetNode(index).GetSpriteRenderer().color = Color.clear;
            number = -1;
            //L_listNode[index].transform.parent.GetComponent<SpriteRenderer>().color = Color.clear;
            L_spritePosition[index].color = Color.clear;
        }

        //empty
        if (id == Constant.ID_TYPE_NODE && number == 0)
        {
            //L_listNode[index].GetComponent<SpriteRenderer>().color = Color.clear;
        }

        //enemy
        if (id == Constant.ID_ENEMY)
        {
            //L_listNode[index].GetComponent<SpriteRenderer>().color = Color.clear;
            id = Constant.ID_TYPE_NODE;
            GameObject enemy = SimplePool.Spawn("Enemy" + numType, Vector3.zero, Quaternion.identity);
            enemy.GetComponent<Enemy>().SetPosition(index);
            EnemyManager.Instance.Add(enemy);
        }

        if (id == Constant.ID_TYPE_NODE_FREEZE)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_freeze.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_TYPE_NODE_LINK)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_link.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_TYPE_NODE_LOCK)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_lock.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_TYPE_NODE_MAGNET)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_magnet.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_TYPE_NODE_BOOM)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_boom.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_TYPE_NODE_FLOOD)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_flood.anim);
        }

        if (id == Constant.ID_TYPE_NODE_JUMP)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_jump.anim);
        }

        if (id == Constant.ID_TYPE_NODE_CLOUD)
        {
            GetNode(index).SetFace(SpriteManager.Instance.g_cloud.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
        }

        if (id == Constant.ID_HAT)
        {
            number = 0; 
            //L_listNode[index].GetComponent<SpriteRenderer>().color = Color.clear;
            GetNode(index).SetFace(SpriteManager.Instance.g_hat.anim);
            GetNode(index).g_faceId.GetComponent<AffectNode>().SetIndex(index);
            ModeFindStar.Instance.newHat(GetNode(index).g_faceId);
        }
        

        GetNode(index).I_id = id;
        GetNode(index).I_numberNode = number;

    }

    // draw Line
    private IEnumerator CreateLine()
    {
        List<GameObject> line = new List<GameObject>();
        for (int i = 0; i < l_listLine.Count; i++)
        {
            line.Add(l_listLine[i]);
        }

        GameObject newLine = SimplePool.Spawn("Line", Vector3.zero, Quaternion.identity);
        LineRenderer lRend = newLine.GetComponent<LineRenderer>();

        if (!CheckStraight(line[0].GetComponent<Node>().i_index, line[1].GetComponent<Node>().i_index))
        {
            GameObject temp = line[1];
            line[1] = line[2];
            line[2] = temp;
        }

        lRend.positionCount = line.Count;

        for (int i = 0; i < line.Count; i++)
        {
            lRend.SetPosition(i, new Vector3(line[i].transform.position.x, line[i].transform.position.y, -1));
            if (i + 1 < line.Count)
            {
                StartCoroutine(AddColliderToLine(line[i].transform.position, line[i + 1].transform.position));
            }
        }

        l_listLine.Clear();
        while (l_listChoose.Count > 0)
        {
            l_listChoose[0].GetComponent<Node>().SetAnim("idle");
            l_listChoose.RemoveAt(0);
        }

        yield return new WaitForSeconds(0.15f);//0.15

        SimplePool.Despawn(newLine);

        yield return new WaitForSeconds(Constant.TIME_REMOVE_NODE/2);//0.7



    }

    private IEnumerator AddColliderToLine( Vector3 startPoint, Vector3 endPoint)
    {

        BoxCollider2D lineCollider = SimplePool.Spawn("LineCollider", Vector3.zero, Quaternion.identity).GetComponent<BoxCollider2D>();
        //lineCollider.transform.parent = line.transform;

        float lineWidth = 0.12f;// line.endWidth;
        float lineLength = Vector3.Distance(startPoint, endPoint);
        lineCollider.size = new Vector3(lineLength, lineWidth, 1f);
        Vector3 midPoint = (startPoint + endPoint) / 2;
        lineCollider.transform.position = midPoint;

        lineCollider.gameObject.tag = "Line";

        if (startPoint.x == endPoint.x)
        {
            lineCollider.transform.Rotate(0, 0, 90);
        }

        yield return new WaitForSeconds(.15f);

        SimplePool.Despawn(lineCollider.gameObject);

        //lineCollider.gameObject.AddComponent<Rigidbody2D>();
    }

    //---------

    // search suggest

    public void RenewSuggest()
    {
        i_suggest1 = 0;
        SearchSuggest();
        ClearChoose();
        //l_listLine.Clear();
        //l_listChoose.Clear();
    }
    private void CheckSuggest()
    {
        if (GetNode(i_suggest1).I_numberNode <= 0 || GetNode(i_suggest2).I_numberNode <= 0 || !CheckValue(i_suggest1,i_suggest2))
        {
            SearchSuggest();
            ClearChoose();
        }
    }
    private bool SearchSuggest()
    {
        //Debug.Log("search");
        int ran = Random.Range(Constant.NUMBER_ROW + 1, Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW - 1);
        //check follow position
        for (int i = ran; i < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW -2 ; i++)
        {
            if (CantConnect(i))
            {
                continue;
            }
            for (int j = i + 1; j < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW; j++)
            {
                if (CantConnect(j) || !CheckValue(i,j) )
                {
                    continue;
                }

                if (CheckCompatibility(i,j) >= 0)
                {
                    i_suggest1 = i;
                    i_suggest2 = j;

                    //Debug.Log("search i + j : " + i + "__" + j );
                    //mixException = 9;

                    return true;
                }
                
            }
        }

        for (int i = Constant.NUMBER_ROW + 1; i < ran; i++)
        {
            if (CantConnect(i))
            {
                continue;
            }
            for (int j = i + 1; j < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW ; j++)
            {
                if (CantConnect(j) || !CheckValue(i, j) )
                {
                    continue;
                }

                if (CheckCompatibility(i, j) >= 0)
                {
                    i_suggest1 = i;
                    i_suggest2 = j;

                    //Debug.Log("search i + j : " + i + "__" + j);
                    return true;
                }

            }
        }
        //Debug.Log("NULLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");

        if (!TimeManager.Instance.b_gameOver && mixException < 9)
        {
            //if (mixException == 0 && connect)
            //{
            //    StartCoroutine(Mix(false));
            //}
            //else
            //{
            //    MixAllNode();
            //}

          
            MixAllNode();
            
        }

        return false;
    }

    private bool CantConnect(int index)
    {
        if (GetNode(index).I_numberNode <= 0 || GetNode(index).I_id == Constant.ID_TYPE_NODE_LOCK || GetNode(index).I_id == Constant.ID_TYPE_NODE_FLOOD || GetNode(index).I_id == Constant.ID_TYPE_NODE_FREEZE || GetNode(index).I_id == Constant.ID_TYPE_NODE_LINK || GetNode(index).I_id == Constant.ID_TYPE_NODE_BONUS )
        {
            return true;
        }

        return false;
    }
    //---------

    public void Choose(GameObject node, bool bonus = false)
    {
        if (ItemManager.Instance.useHammer)
        {
            ItemManager.Instance.ChooseHammerIndex(node.GetComponent<Node>().i_index);
            return;
        }


        int index = node.GetComponent<Node>().i_index;
        //Debug.Log(":" + index + "___value : " + L_listNode[index].GetComponent<Node>().I_numberNode);


        l_listChoose.Add(node);
        node.GetComponent<Node>().SetAnim("select");

        //----------------------
        node.GetComponent<Node>().InitSelect();
        //----------------------

        if (l_listLine.Count == 0)
        {
            l_listLine.Add(node);
        }

        if (l_listChoose.Count == 2)
        {
            if (!CheckValue(l_listChoose[0].GetComponent<Node>().i_index, l_listChoose[1].GetComponent<Node>().i_index))
            {
                
                Debug.Log("!= value");
                ClearChoose();
                Choose(node);
                return;
            }

            if (l_listChoose[0] == l_listChoose[1])
            {
                Debug.Log("===");
                ClearChoose();
                return;
            }

            //check 2 node
            int checkCompatibility = CheckCompatibility(l_listChoose[0].GetComponent<Node>().i_index, l_listChoose[1].GetComponent<Node>().i_index);

            if (checkCompatibility >= 0)
            {
                mixException = 0;
                f_timeClick = 0;
                // check hieu ung node
                // check Bonus
                CheckBonus(l_listChoose[0].GetComponent<Node>().I_numberNode);

                if (l_listChoose[0].GetComponent<Node>().I_id != Constant.ID_TYPE_NODE_BONUS)
                {
                     ScoreManager.Instance.AddScore(checkCompatibility, l_listChoose[0].transform, l_listChoose[1].transform);
                }

                if (l_listLine.Contains(l_listChoose[1]))
                {
                    l_listLine.Add(l_listChoose[0]);
                }
                else
                {
                    l_listLine.Add(l_listChoose[1]);
                }

                FixRemove();
                StartCoroutine(CreateLine());

                if (BonusManager.Instance.bonusDoubleConect && !bonus)
                {
                    StartCoroutine(BonusManager.Instance.AutoLink());
                }

                if (machineSpawn != null && machineSpawn.gameObject.activeInHierarchy )
                {
                    machineSpawn.SubTimerSpawn();
                }

            }
            else
            {
                ClearChoose();
                Choose(node);
            }

            CheckSuggest();

        }

        SoundManager.instance.PlayFxSound("SelectBooster");

    }

    public void ClearChoose()
    {
        if (l_listChoose.Count > 0)
        {
            l_listChoose[0].GetComponent<Node>().RemoveSelect();
        }
        
        while (l_listChoose.Count > 0)
        {
            //Debug.Log("clear : " + l_listChoose[0]);

            l_listChoose[0].GetComponent<Node>().SetAnim("idle");
            l_listChoose.RemoveAt(0);
        }
        l_listLine.Clear();
    }

    private void FixRemove()
    {
        if (l_listChoose[1].GetComponent<Node>().I_id == Constant.ID_TYPE_NODE_BONUS)
        {
            BonusManager.Instance.NewBonus();
        }
        

        RemoveNode(l_listChoose[0].GetComponent<Node>().i_index);
        RemoveNode(l_listChoose[1].GetComponent<Node>().i_index);
    }

    private void RemoveNode(int point)
    {
        GetNode(point).RemoveNode();
        
    }
    //check straight row or colunm
    private bool CheckStraight(int point1, int point2)
    {
        return ((point1 % Constant.NUMBER_ROW) == (point2 % Constant.NUMBER_ROW) || (point1 / Constant.NUMBER_ROW) == (point2 / Constant.NUMBER_ROW));
    }

    //check thich hop tra ve -1 neu k thich hop, tra ve khoang cach 2 o neu tihch hop
    private int CheckCompatibility(int point1, int point2)
    {
        if ( !CheckValue(point1,point1) )
        {
            return -1;
        }

        if (CheckLineX(point1, point2) || CheckLineY(point1, point2))
        {
            //Debug.Log("line : " + Distance(point1, point2));
            return Distance(point1, point2);
        }

        if (CheckRect(point1, point2))
        {
            //Debug.Log("rect : "+ Distance(point1, point2));
            return Distance(point1, point2);
        }

        if (CheckConner(point1, point2))
        {
            //Debug.Log("conner : "+ Distance(point1, point2));
            return Distance(point1, point2);
        }

        int checkU = CheckU(point1, point2);
        if (checkU > 0)
        {
            //Debug.Log("Uuuuuu : " + (checkU *2 + Distance(point1, point2)));
            return Distance(point1, point2) + 2 * checkU;
        }

        return -1;
    }
    //check gia tri
    private bool CheckValue(int point1, int point2)
    {
        return GetNode(point1).I_numberNode == GetNode(point2).I_numberNode ? true : false;
    }
    //check theo hang ngang X
    private bool CheckLineX(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        if ((max/ Constant.NUMBER_ROW) != (min/ Constant.NUMBER_ROW))
        {
            return false;
        }

        for (int i = min + 1; i < max; i++)
        {
            if (IsBlock(i))
            {
                return false;
            }
        }
        return true;
    }
    //check theo cot Y
    private bool CheckLineY(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        if ((max - min) % Constant.NUMBER_ROW != 0)
        {
            return false;
        }

        for (int i = min + Constant.NUMBER_ROW; i < max; i += Constant.NUMBER_ROW)
        {
            if (IsBlock(i))
            {
                return false;
            }
        }
        return true;
    }

    //check vuong goc
    private bool CheckConner(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        if ((min % Constant.NUMBER_ROW) < (max % Constant.NUMBER_ROW))
        {
            if (CheckConnerMaxYX( min, max) || CheckConnerMinXY(min, max))
            {
                return true;
            }
        }
        else
        {
            if (CheckConnerMinYX(min, max) || CheckConnerMaxXY(min, max))
            {
                return true;
            }
        }

        return false;
    }

    private bool CheckConnerMinXY(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        int conner = ConnerXmin(min, max);

        if (!IsBlock(conner) && CheckLineX(min, conner) && CheckLineY(conner, max))
        {
            l_listLine.Add(L_listNode[conner]);
            return true;
        }
        return false;
    }
    private bool CheckConnerMinYX(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        int conner = ConnerXmin(min, max);

        if (!IsBlock(conner) && CheckLineY(conner, max) && CheckLineX(conner, min))
        {
            l_listLine.Add(L_listNode[conner]);
            return true;
        }
        return false;
    }
    private bool CheckConnerMaxXY(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        int conner = ConnerXmax(min, max);

        if (!IsBlock(conner) && CheckLineX(max, conner) && CheckLineY(min, conner))
        {
            l_listLine.Add(L_listNode[conner]);
            return true;
        }
        return false;
    }
    private bool CheckConnerMaxYX(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        int conner = ConnerXmax(min, max);

        if (!IsBlock(conner) && CheckLineY(min, conner) && CheckLineX(conner, max))
        {
            l_listLine.Add(L_listNode[conner]);
            return true;
        }
        return false;
    }
    //----------

    //check trong hinh vuong
    private bool CheckRect(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);

        if ((min % Constant.NUMBER_ROW) < (max % Constant.NUMBER_ROW))
        {
            for (int i = min + 1; i < ConnerXmin(min, max); i++)
            {
                if (IsBlock(i))
                {
                    break;
                }
                if (CheckConnerMaxYX(i, max))
                {
                    l_listLine.Add(L_listNode[i]);
                    return true;
                }
            }

            for (int i = min + Constant.NUMBER_ROW; i < ConnerXmax(min, max); i += Constant.NUMBER_ROW)
            {
                if (IsBlock(i))
                {
                    break;
                }
                if (CheckConnerMinXY(i, max))
                {
                    l_listLine.Add(L_listNode[i]);
                    return true;
                }
            }
        }
        else
        {
            for (int i = max + 1; i < ConnerXmax(min, max); i++)
            {
                if (IsBlock(i))
                {
                    break;
                }
                if (CheckConnerMinYX(i, min))
                {
                    l_listLine.Add(L_listNode[i]);
                    return true;
                }
            }

            for (int i = max - Constant.NUMBER_ROW; i > ConnerXmin(min, max); i -= Constant.NUMBER_ROW)
            {
                if (IsBlock(i))
                {
                    break;
                }
                if (CheckConnerMaxXY(i, min))
                {
                    l_listLine.Add(L_listNode[i]);
                    return true;
                }
            }
        }


        return false;
    }

    //check U -- tra ve khoang cach node gan nhat voi day
    private int CheckU(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);
        int connerMax = ConnerXmax(min, max);
        int connerMin = ConnerXmin(min, max);

        int len = Mathf.Max(Constant.NUMBER_ROW, Constant.NUMBER_COLUNM) - 2;

        //cung cot
        if ((max-min) % Constant.NUMBER_ROW == 0)
        {
            int i1 = 1;
            int i2 = -1;
            for (int i = 1; i < len; i++)
            {
                if (i1 != 0)
                {
                    int t1 = i * i1 + min;

                    if (!IsBlock(t1) && !IsBlock(max + i * i1) && CheckLineY(t1, max + i * i1))
                    {
                        l_listLine.Add(L_listNode[t1]);
                        l_listLine.Add(L_listNode[max + i * i1]);
                        return i;
                    }

                    if (IsBlock(t1) || IsBlock(i * i1 + max))
                    {
                        i1 = 0;
                    }
                }

                if (i2 != 0)
                {
                    int t2 = i * i2 + min;

                    if (!IsBlock(t2) && !IsBlock(max + i * i2) && CheckLineY(t2, max + i * i2))
                    {
                        l_listLine.Add(L_listNode[t2]);
                        l_listLine.Add(L_listNode[max + i * i2]);
                        return i;
                    }

                    if (IsBlock(t2) || IsBlock(max + i * i2))
                    {
                        i2 = 0;
                    }
                }
            }
        }else
        //cung dong
        if ((max / Constant.NUMBER_ROW) == (min / Constant.NUMBER_ROW))
        {
            int i1 = Constant.NUMBER_ROW;
            int i2 = -Constant.NUMBER_ROW;
            for (int i = 1; i < len; i++)
            {
                if (i1 != 0)
                {
                    int t1 = i * i1 + min;
                    if (t1 < Constant.TOTAL_ITEM_NUMBER)
                    {
                        if (!IsBlock(t1) && !IsBlock(max + i * i1) && CheckLineX(t1, max + i * i1))
                        {
                            l_listLine.Add(L_listNode[t1]);
                            l_listLine.Add(L_listNode[max + i * i1]);
                            return i;
                        }

                        if (IsBlock(t1) || IsBlock(i * i1 + max))
                        {
                            i1 = 0;
                        }
                    }
                    else
                    {
                        i1 = 0;
                    }
                    
                    
                }

                if (i2 != 0)
                {
                    int t2 = i * i2 + min;

                    if (t2 >= 0)
                    {
                        if (!IsBlock(t2) && !IsBlock(max + i * i2) && CheckLineX(t2, max + i * i2))
                        {
                            l_listLine.Add(L_listNode[t2]);
                            l_listLine.Add(L_listNode[max + i * i2]);
                            return i;
                        }

                        if (IsBlock(t2) || IsBlock(max + i * i2))
                        {
                            i2 = 0;
                        }
                    }
                    else
                    {
                        i2 = 0;
                    }
                    
                }
            }
        }else
        //o vuong
        if ((min % Constant.NUMBER_ROW) < (max % Constant.NUMBER_ROW))
        {
            int i1 = 1;
            int i2 = -1;
            int i3 = Constant.NUMBER_ROW;
            int i4 = -Constant.NUMBER_ROW;

            if (IsBlock(connerMax) || !CheckLineX(max, connerMax))
            {
                i2 = 0;
            }

            if (IsBlock(connerMax) || !CheckLineY(min, connerMax))
            {
                i3 = 0;
            }

            if (IsBlock(connerMin) || !CheckLineX(min, connerMin))
            {
                i1 = 0;
            }

            if (IsBlock(connerMin) || !CheckLineY(max, connerMin))
            {
                i4 = 0;
            }

            for (int i = 1; i < len; i++)
            {
                if (i2 != 0)
                {
                    int t2 = i * i2 + min;

                    if (!IsBlock(t2) && !IsBlock(connerMax + i * i2) && CheckLineY(t2, connerMax + i * i2))
                    {
                        l_listLine.Add(L_listNode[t2]);
                        l_listLine.Add(L_listNode[connerMax + i * i2]);
                        return i;
                    }

                    if (IsBlock(t2) || IsBlock(connerMax + i * i2))
                    {
                        i2 = 0;
                    }
                }

                if (i3 != 0)
                {
                    int t3 = i * i3 + connerMax;

                    if (t3 < Constant.TOTAL_ITEM_NUMBER)
                    {

                        if (!IsBlock(t3) && !IsBlock(max + i * i3) && CheckLineX(t3, max + i * i3))
                        {
                            l_listLine.Add(L_listNode[t3]);
                            l_listLine.Add(L_listNode[max + i * i3]);
                            return i;
                        }

                        if (IsBlock(t3) || IsBlock(max + i * i3))
                        {
                            i3 = 0;
                        }
                    }
                    else
                    {
                        i3 = 0;
                    }
                }

                if (i1 != 0)
                {
                    int t1 = i * i1 + connerMin;

                    if (!IsBlock(t1) && !IsBlock(max + i * i1) && CheckLineY(t1, max + i * i1))
                    {
                        l_listLine.Add(L_listNode[t1]);
                        l_listLine.Add(L_listNode[max + i * i1]);
                        return i;
                    }

                    if (IsBlock(t1) || IsBlock(max + i * i1))
                    {
                        i1 = 0;
                    }
                }

                if (i4 != 0)
                {
                    int t4 = i * i4 + connerMin;

                    if (t4 >= 0)
                    {
                        if (!IsBlock(t4) && !IsBlock(min + i * i4) && CheckLineX(t4, min + i * i4))
                        {
                            l_listLine.Add(L_listNode[t4]);
                            l_listLine.Add(L_listNode[min + i * i4]);
                            return i;
                        }

                        if (IsBlock(t4) || IsBlock(min + i * i4))
                        {
                            i4 = 0;
                        }
                    }
                    else
                    {
                        i4 = 0;
                    }
                }

            }
        }
        else
        {
                int i1 = 1;
                int i2 = -1;
                int i3 = Constant.NUMBER_ROW;
                int i4 = -Constant.NUMBER_ROW;

                if (IsBlock(connerMax) || !CheckLineY(min, connerMax))
                {
                    i3 = 0;
                }

                if (IsBlock(connerMin) || !CheckLineX(min, connerMin))
                {
                    i2 = 0;
                }

                if (IsBlock(connerMax) || !CheckLineX(max, connerMax))
                {
                    i1 = 0;
                }

                if (IsBlock(connerMin) || !CheckLineY(max, connerMin))
                {
                    i4 = 0;
                }
            
            for (int i = 1; i < len; i++)
            {
                if (i2 != 0)
                {
                    int t2 = i * i2 + connerMin;

                    if (!IsBlock(t2) && !IsBlock(max + i * i2) && CheckLineY(t2, max + i * i2))
                    {
                        l_listLine.Add(L_listNode[t2]);
                        l_listLine.Add(L_listNode[max + i * i2]);
                        return i;
                    }

                    if (IsBlock(t2) || IsBlock(max + i * i2))
                    {
                        i2 = 0;
                    }
                }

                if (i3 != 0)
                {
                    int t3 = i * i3 + connerMax;

                    if (t3 < Constant.TOTAL_ITEM_NUMBER)
                    {
                        if (!IsBlock(t3) && !IsBlock(max + i * i3) && CheckLineX(t3, max + i * i3))
                        {
                            l_listLine.Add(L_listNode[t3]);
                            l_listLine.Add(L_listNode[max + i * i3]);
                            return i;
                        }

                        if (IsBlock(t3) || IsBlock(max + i * i3))
                        {
                            i3 = 0;
                        }
                    }
                    else
                    {
                        i3 = 0;
                    }

                    
                }

                if (i1 != 0)
                {
                    int t1 = i * i1 + connerMax;

                    if (!IsBlock(t1) && !IsBlock(min + i * i1) && CheckLineY(t1, min + i * i1))
                    {
                        l_listLine.Add(L_listNode[t1]);
                        l_listLine.Add(L_listNode[min + i * i1]);
                        return i;
                    }

                    if (IsBlock(t1) || IsBlock(min + i * i1))
                    {
                        i1 = 0;
                    }
                }

                if (i4 != 0)
                {
                    int t4 = i * i4 + connerMin;

                    if (t4 >= 0)
                    {

                        if (!IsBlock(t4) && !IsBlock(min + i * i4) && CheckLineX(t4, min + i * i4))
                        {
                            l_listLine.Add(L_listNode[t4]);
                            l_listLine.Add(L_listNode[min + i * i4]);
                            return i;
                        }

                        if (IsBlock(t4) || IsBlock(min + i * i4))
                        {
                            i4 = 0;
                        }
                    }
                    else
                    {
                        i4 = 0;
                    }
                }

            }
        }
        return 0;
        
    }

    //check co phai block k
    private bool IsBlock(int point)
    {
        if (GetNode(point).I_id == Constant.ID_TYPE_BARRIER || GetNode(point).I_numberNode > 0 || GetNode(point).I_id == Constant.ID_NOTHING || GetNode(point).I_id == Constant.ID_TYPE_BOX || GetNode(point).I_id == Constant.ID_HAT)
        {
            return true;
        }
        return false;
    }

    // khoang cach 2 node
    private int Distance(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);
        int conner = ConnerXmax(min, max);

        return (Mathf.Abs(max - conner) + (conner - min) / Constant.NUMBER_ROW - 1);
    }

    //tra ve vi tri hang ngang tuong duong min//diem x tren
    private int ConnerXmin(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);
        int temp = (max / Constant.NUMBER_ROW) * Constant.NUMBER_ROW + min % Constant.NUMBER_ROW;
        return max - (temp - min);
    }

    //tra ve vi tri hang ngang tuong duong max//diem x duoi
    private int ConnerXmax(int point1, int point2)
    {
        int min = Mathf.Min(point1, point2);
        int max = Mathf.Max(point1, point2);
        int temp = (max / Constant.NUMBER_ROW) * Constant.NUMBER_ROW + min % Constant.NUMBER_ROW;
        return min + (temp - min);
    }

    //truong hop k co node nao co the an -> tron tat ca cac node con lai
    public int mixException = 0;
    public void MixAllNode(bool fan = false)
    {
        Debug.Log("mix : "+ mixException);

        if (mixException > 8)
        {
            return;
        }
        

        int i = Constant.NUMBER_ROW;
        int j = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW;

        bool gameover = true;
        bool mixed = false;

        for (i = Constant.NUMBER_ROW; i < Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW; i++)
        {
            if (IsNode(i))
            {
                break;
            }
        }

        for (j = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW; j > i ; j--)
        {
            if (IsNode(j))
            {
                break;
            }
        }
     
        //Debug.Log("i;j : " + i + "--" + j);

        int nodeI = i;
        int nodeJ = j;

        while (i < j)
        {
            if (GetNode(i).I_numberNode > 0 || GetNode(j).I_numberNode > 0)
            {
                gameover = false;
            }

            if (GetNode(i).IsNodeEmpty() || GetNode(i).I_id != Constant.ID_TYPE_NODE)
            {
                i++;
                continue;
            }

            if (GetNode(j).IsNodeEmpty() || GetNode(j).I_id != Constant.ID_TYPE_NODE)
            {
                j--;
                continue;
            }
            
            gameover = false;

            if ((GetNode(i).I_numberNode != GetNode(j).I_numberNode) )
            {
                if (Random.Range(0, 2) == 0)
                {
                    mixed = true;
                    SwapNodeByUFO(i, j);
                }
            }

            if (Random.Range(0, 2) == 0)
            {
                i++;
            }
            else j--;
        }

        mixException++;
        Debug.Log("mixException : " + mixException);

        //Debug.Log("value : " + GetNode(nodeI).I_numberNode +  " :_: "+ GetNode(nodeJ).I_numberNode);

        if (mixed && SearchSuggest())
        {
            StartCoroutine(Mix(fan));
        }
        
        if ( mixException > 8 && (gameover == false && mixed == false))
        {
            if (CheckCompatibility(nodeI, nodeJ) >= 0 && GetNode(nodeI).I_id == Constant.ID_TYPE_NODE && GetNode(nodeJ).I_id == Constant.ID_TYPE_NODE )
            {
                return;
            }

            SetSuggestHammer();
            return;
        }

        if ((Constant.is_MODE_REQUEST || Constant.is_MODE_FIND_STAR) && Constant.have_SPAWN_MACHINE)
        {
            gameover = false;
        }

        TimeManager.Instance.b_gameOver = gameover;

        //Debug.Log("__gameover : "+gameover + "__mix : " + mixed );

        return;
    }

    public IEnumerator Mix(bool fan = false)
    {
        StartCoroutine(ItemManager.Instance.ActiveG_mix());

        yield return new WaitForSeconds(0.7f);

        for (int i = Constant.NUMBER_ROW;   i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (GetNode(i).IsNode())
            {
                GetNode(i).Move(0, tf_ufoTarget, true);
            }
        }

        yield return new WaitForSeconds(1f);

        for (int i = Constant.NUMBER_ROW; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (GetNode(i).I_numberNode > 0 && GetNode(i).I_id == Constant.ID_TYPE_NODE)
            {
                GetNode(i).Move(GetNode(i).i_index, GetNode(i).transform, true);
            }
        }
        l_listLine.Clear();
    }

    private bool IsNode(int i)
    {
        if (GetNode(i).I_numberNode > 0 && GetNode(i).I_id != Constant.ID_TYPE_NODE_BONUS)
        {
            return true;
        }
        return false;
    }

    //con node nhung bi can k an dc-->>>>
    private void SetSuggestHammer()
    {
        BoosterManager.Instance.SuggestHammer(true);
    }

    private void GenerateBonus()
    {
        int ran = Random.Range(Constant.NUMBER_ROW + 1, (Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) / 2);

        while (!GetNode(ran).IsNodeEmpty())
        {
            ran--;
            if (ran <= Constant.NUMBER_ROW)
            {
                ran = Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW;
            }
        }
        //L_listNode[ran].GetComponent<Node>().I_id = id;

        GetNode(ran).SetFace(SpriteManager.Instance.g_bonus);
        GetNode(ran).I_id = Constant.ID_TYPE_NODE_BONUS;
        GetNode(ran).g_faceId.GetComponent<Bonus>().SetIndex(ran);

        ran = Random.Range((Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW) / 2, Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW);

        while (!GetNode(ran).IsNodeEmpty())
        {
            ran++;
            if (ran >= Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW)
            {
                ran = Constant.NUMBER_ROW;
            }
        }

        GetNode(ran).SetFace(SpriteManager.Instance.g_bonus);
        GetNode(ran).I_id = Constant.ID_TYPE_NODE_BONUS;
        GetNode(ran).g_faceId.GetComponent<Bonus>().SetIndex(ran);

        //RenewSuggest();
    }

    private void CheckBonus(int value)
    {
        
        if (value == i_valueBonus)
        {
            i_bonus++;
        } else
        {
            i_valueBonus = value;
            i_bonus = 1;
        }

        if (i_bonus == 3)
        {
            Debug.Log("bonus");
            GenerateBonus();
            i_bonus = 1;
        }
    }
    
    //-----

    // doi gia tri 2 node
    public void SwapNode(int point1, int point2)
    {
        GetNode(point1).Move(point2, L_position[point2].transform);
        GetNode(point2).Move(point1, L_position[point1].transform);

        SwapObject(point1, point2);


        GetNode(point1).transform.SetParent(L_position[point1].transform);
        GetNode(point2).transform.SetParent(L_position[point2].transform);
    }

    // doi gia tri 2 node
    public Transform tf_ufoTarget;
    public void SwapNodeByUFO(int point1, int point2)
    {

        Debug.Log("SwapNodeByUFO");
        Vector3 tf_temp = GetNode(point1).GetTransform().position;
        GetNode(point1).GetTransform().position = GetNode(point2).GetTransform().position;
        GetNode(point1).i_index = point2;

        GetNode(point2).GetTransform().position = tf_temp;
        GetNode(point2).i_index = point1;

        SwapObject(point1, point2);

        GetNode(point1).transform.SetParent(L_position[point1].transform);
        GetNode(point2).transform.SetParent(L_position[point2].transform);
    }

    // only chance position in list
    public void SwapObject(int point1, int point2)
    {
        var temp = L_listNode[point1];
        var tempNode = m_Node[point1];

        L_listNode[point1] = L_listNode[point2];

        L_listNode[point2] = temp;

        m_Node[point1] = m_Node[point2];

        m_Node[point2] = tempNode;
    }

    //check preboost
    public IEnumerator CheckPreboost()
    {
        TimeManager.Instance.b_gameStart = false;

        yield return new WaitForSeconds(0.5f);
        
        if (BoosterManager.Instance.booster[1].choose)
        {
            yield return new WaitForSeconds(0.5f);
            BoosterManager.Instance.booster[1].Use();
            yield return new WaitForSeconds(2.8f);
        }

        if (BoosterManager.Instance.booster[2].choose)
        {
            yield return new WaitForSeconds(0.5f);
            BoosterManager.Instance.booster[2].Use();
            yield return new WaitForSeconds(2f);
        }

        if (BoosterManager.Instance.booster[0].choose)
        {
            yield return new WaitForSeconds(0.5f);
            BoosterManager.Instance.booster[0].Use();
            yield return new WaitForSeconds(3.1f);
        }

        checkedPreboost = true;
        TimeManager.Instance.b_gameStart = true;

    }

    //-----

    // add target 
    private void ClickTarget()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())//khong bat su kien khac
        {
            //Debug.Log(LayerMask.GetMask("Node"));
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 256);

            if (hit.collider != null) //if (hit.collider.tag == "node")
            {
                hit.transform.GetComponent<Node>().OnClick();
            }
        }
    }

    void Start()
    {
        Init();

        ////test state node
        //L_listNode[19].GetComponent<Node>().I_id = Constant.ID_TYPE_BARRIER;
        //L_listNode[20].GetComponent<Node>().I_id = Constant.ID_TYPE_BARRIER;
        //L_listNode[21].GetComponent<Node>().I_id = Constant.ID_TYPE_BARRIER;
        //L_listNode[22].GetComponent<Node>().I_id = Constant.ID_TYPE_BARRIER;
        //L_listNode[23].GetComponent<Node>().I_id = Constant.ID_TYPE_BARRIER;

        //L_listNode[73].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;
        //L_listNode[74].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;
        //L_listNode[75].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;
        //L_listNode[76].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;



        // L_listNode[10].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_JUMP;
        //L_listNode[11].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_HIDE;
        //L_listNode[59].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;
        //L_listNode[58].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_MAGNET;
        //L_listNode[88].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_FLOOD;
        //L_listNode[12].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_LOCK;
        //L_listNode[25].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_LINK;
        //L_listNode[67].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_BOOM;
        //L_listNode[68].GetComponent<Node>().I_id = Constant.ID_TYPE_NODE_FREEZE;
        //L_listNode[69].GetComponent<Node>().I_id = Constant.ID_TYPE_BOX;
        //EnvironManager.Instance.CreateWindPoint(106);
        //EnvironManager.Instance.CreateWindPoint(90);
        //EnvironManager.Instance.CreateWindPoint(1);
        //EnvironManager.Instance.CreateWindPoint(17);

        //EnvironManager.Instance.CreateRandomCloud();

        //FixSprite();
        
    }

    public void InitAffect()
    {
        //Debug.Log("___ " + l_lock.Count);

        for (int i = 0; i < l_lock.Count; i++)
        {
            if (l_lock[i] == null)
            {
                l_lock.RemoveAt(i);
                i--;
                continue;
            }
            l_lock[i].GetComponent<AffectNode>().Attach();
        }
    }

    bool checkedPreboost = false;

    private void Update()
    {


        //test
        if (TimeManager.Instance.b_gameStart)
        {
            ClickTarget();

            if (!checkedPreboost)
            {
          
                //L_listNode[11].GetComponent<Animator>().;

                //Instantiate(EnAffectManager.Instance.g_windWestPrefabs);
                //Instantiate(machineSpawn);
                //spi = Instantiate(spider);
                // wor = Instantiate(worm);
                // robot = Instantiate(robotManager);
                // laser = Instantiate(laserManager);
                // holeAlienManager = Instantiate(holeAlien);
                // alien = Instantiate(marineAlien);

                //obstacleEnemyManager = Instantiate(obstacleEnemy);
                // chain = Instantiate(chainManager);
                StartCoroutine(CheckPreboost());
            }
            
            
            CheckSuggest();

            if (!TimeManager.Instance.b_pause)
            {
                TimeManager.Instance.cl_timeOver.CurrentVal -= Time.deltaTime;
                f_timeClick += Time.deltaTime;
            }

            //win
            if (TimeManager.Instance.cl_timeOver.CurrentVal > 0 && TimeManager.Instance.b_gameOver)
            {
                //Debug.Log("Win panel open");
                StartCoroutine(UIManager.Instance.OpenWinPanel(TimeManager.Instance.GetTime(), true));
                // UIManager.Instance.OpenSpinPanel();
                TimeManager.Instance.b_gameStart = false;

                
            }

            // anim time out
            if (TimeManager.Instance.cl_timeOver.CurrentVal < 20f && TimeManager.Instance.cl_timeOver.CurrentVal > 2f)
            {
                MCManager.Instance.TimeOut();
            }

            //thua retry
            if (TimeManager.Instance.cl_timeOver.CurrentVal <= 0 && !TimeManager.Instance.b_gameOver)
            {
                TimeManager.Instance.b_gameOver = true;
                StartCoroutine(UIManager.Instance.OpenRetryPanel());
            }


            if (f_timeClick > f_timeSuggest && !TimeManager.Instance.b_gameOver)
            {
                MCManager.Instance.Knock();

                f_timeClick = 0;

                if (itemHint > 0)
                {
                    ItemManager.Instance.DrawSuggest(i_suggest1, i_suggest2);
                    itemHint--;
                    if (itemHint == 0)
                    {
                        ItemManager.Instance.ResetHint();
                    }
                    else
                    {
                        ItemManager.Instance.SetNumberHint(itemHint);
                    }
                }

                //Choose(i_suggest1);
                //Choose(i_suggest2);
            }

        }
    }

}