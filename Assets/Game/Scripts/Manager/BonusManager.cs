﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusManager : MonoBehaviour
{
    private static BonusManager instance;
    public static BonusManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<BonusManager>(); return instance; } }

    //-----------------------pre booster-----------------------

    [SerializeField]
    private GameObject g_key;

    [SerializeField]
    private GameObject g_deny;

    [SerializeField]
    private GameObject g_doubleLink;

    [SerializeField]
    private GameObject g_x2Score;

    //---------------------------------------------------------

    public GameObject popUPCaution;

    [SerializeField]
    private Animator anim_chatbox; 

    [SerializeField]
    private Image icon;

    [SerializeField]
    private TimeOver bar;

    [SerializeField]
    private GameObject g_smallChatbox;

    [SerializeField]
    private GameObject g_txtSmallChatbox;
    
    public bool bonusDoubleConect = false;

    public Sprite s_x2Score;

    public Sprite s_bonusTime;

    public Sprite s_doubleLink;

    public Sprite s_bonusMix;

    public Sprite s_denyNode;

    public Sprite s_hint;

    public Sprite s_hammer;

    public GameObject g_linePrefabs;

    List<Node> l_listLine = new List<Node>();

    public Sprite s_key;

    //---------------IE--------------------
    private Coroutine c_score;

    private Coroutine c_dLink;

    private Coroutine c_cooldown;

    public GameObject shineMC;

    List<Node> list;

    private void Start()
    {
        list = GameManager.Instance.m_Node;
    }

    public void NewBonus()
    {
        anim_chatbox.ResetTrigger("disappear");

        popUPCaution.SetActive(false);

        shineMC.SetActive(true);

        MCManager.Instance.Booster();

        int ran = Random.Range(0,120);
        ran = 99;

        if (ran < 20)
        {
            //Debug.Log("bonus score");
            if (c_score != null)
            {
                StopCoroutine(c_score);
                StopCoroutine(c_cooldown);
            }
            c_score = StartCoroutine(BonusScore());
        }
        else if (ran < 40)
        {
            //Debug.Log("bonus time");
            StartCoroutine(BonusTime());
        }
        else if (ran < 60)
        {
            //Debug.Log("double link");
            if (c_dLink != null)
            {
                StopCoroutine(c_dLink);
                StopCoroutine(c_cooldown);
            }
            CombatTextManager.Instance.CreateText(transform.position, "Double Link x2", Color.black);
            c_dLink = StartCoroutine(BonusLink());
        }
        else if (ran < 80)
        {
            //Debug.Log("bonus mix");
            BonusMixNode();
        }
        else if (ran < 100)
        {
            //Debug.Log("bonus deny");
            BonusDenyNode();
        }
        else
        {
            //Debug.Log("bonus remove adverse");
            BonusRemoveAdverse();
        }
    }

    // x2 or x3 score
    private IEnumerator BonusScore()
    {
        icon.sprite = s_x2Score;
        AppearPopup();

        g_smallChatbox.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        g_txtSmallChatbox.SetActive(true);
        g_txtSmallChatbox.GetComponent<Text>().text = "10";

        int t = Random.Range(0, 20);
        ScoreManager.Instance.i_bonusScore = t == 0 ? 3 : 2;

        g_x2Score.SetActive(true);

        yield return new WaitForSeconds(1.1f);

        g_x2Score.SetActive(false);

        c_cooldown =  StartCoroutine(CoolDownTimer(10));
        yield return new WaitForSeconds(10);

        ScoreManager.Instance.i_bonusScore = 1;


    }
    
    // + 15s
    private IEnumerator BonusTime()
    {
        icon.sprite = s_bonusTime;
        AppearPopup();
        StartCoroutine(DisappearPopup());

        CombatTextManager.Instance.CreateText(transform.position, "BONUS Timer + 15s", Color.yellow);

        yield return new WaitForSeconds(0.2f);

        TimeManager.Instance.b_pause = true;

        ItemManager.Instance.g_time.SetActive(true);
        GameObject go;

        //-> check numer contain
        TimeManager.Instance.AddTime(15f);
        //-> anim
        if (TimeManager.Instance.cl_timeOver.CurrentVal > 20)
        {
            MCManager.Instance.Normal();
        }

        yield return new WaitForSeconds(0.8f);

        for (int i = 0; i < 5; i++)
        {
            go = SimplePool.Spawn("StarTime", Vector3.zero, Quaternion.identity);
            yield return new WaitForSeconds(0.07f);
            go.GetComponent<StarTest>().SetTargetNoFix(ItemManager.Instance.tf_target, "Connected", 1.2f, 0, true);
        }

        yield return new WaitForSeconds(0.5f);

        ItemManager.Instance.g_time.SetActive(false);

        TimeManager.Instance.b_pause = false;

    }

    // link 5 node
    public IEnumerator BonusLink()
    {
        icon.sprite = s_doubleLink;
        AppearPopup();

        g_smallChatbox.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        g_txtSmallChatbox.SetActive(true);
        g_txtSmallChatbox.GetComponent<Text>().text = "10";

        g_doubleLink.SetActive(true);

        yield return new WaitForSeconds(3f);

        g_doubleLink.SetActive(false);

        bonusDoubleConect = true;

        c_cooldown = StartCoroutine(CoolDownTimer(10));
        yield return new WaitForSeconds(10 + 1);

        bonusDoubleConect = false;

    }

    public IEnumerator AutoLink()
    {
        yield return new WaitForSeconds(Constant.TIME_REMOVE_NODE + 0.2f);

        if (!TimeManager.Instance.b_gameOver)
        {
            GameManager.Instance.Choose(GameManager.Instance.L_listNode[GameManager.Instance.i_suggest1],true);
        	GameManager.Instance.Choose(GameManager.Instance.L_listNode[GameManager.Instance.i_suggest2],true);
        }

    }

    //mis node
    private void BonusMixNode()
    {
        icon.sprite = s_bonusMix;
        AppearPopup();
        StartCoroutine(DisappearPopup());

        //StartCoroutine(GameManager.Instance.Mix());
        GameManager.Instance.MixAllNode();

        CombatTextManager.Instance.CreateText(transform.position, "BONUS Mix", Color.black);
    }

    private void BonusDenyNode()
    {
        icon.sprite = s_denyNode;
        AppearPopup();
        StartCoroutine(DisappearPopup());

        StartCoroutine(DenyNode());

        CombatTextManager.Instance.CreateText(transform.position, "BONUS Remove", Color.black);

    }

    public IEnumerator DenyNode()
    {
        g_deny.SetActive(true);

        List<Node> list = GameManager.Instance.m_Node;
        int indexNumber = 0;

        if (!TimeManager.Instance.b_gameOver)
        {
            int ran = Random.Range(Constant.NUMBER_ROW, Constant.TOTAL_ITEM_NUMBER);
            for (int i = ran; i < Constant.TOTAL_ITEM_NUMBER; i++)
            {
                if (list[i].I_numberNode > 0 && list[i].I_id != Constant.ID_TYPE_NODE_BONUS && list[i].I_id != Constant.ID_TYPE_NODE_QUEST)
                {
                    indexNumber = i;
                    break;
                }
            }

            if (indexNumber == 0)
            {
                for (int i = 0; i < ran; i++)
                {
                    if (list[i].I_numberNode > 0 && list[i].I_id != Constant.ID_TYPE_NODE_BONUS && list[i].I_id != Constant.ID_TYPE_NODE_QUEST)
                    {
                        indexNumber = i;
                        break;
                    }
                }
            }
            indexNumber = list[indexNumber].I_numberNode;

            //-----------------

            if (indexNumber == 0)
            {
                yield return null;
            }

            for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
            {
                if (list[i].I_numberNode == indexNumber)
                {
                    l_listLine.Add(list[i]);
                }
            }

            StartCoroutine(CreateLine());

        }
    }

    private IEnumerator CreateLine()
    {
        GameObject go;

        yield return new WaitForSeconds(1.6f);

        for (int i = 0; i < l_listLine.Count; i++)
        {
            go = SimplePool.Spawn("Electric", Vector3.zero, Quaternion.identity);
            go.GetComponent<ElectricPreBoost>().SetTranform(l_listLine[i].transform);

            l_listLine[i].RemoveFace();
            l_listLine[i].SetAnim("select");
            l_listLine[i].SetSpeedAnim(1);
            l_listLine[i].B_lockClick = true;

        }

        yield return new WaitForSeconds(2f);//0.15


        for (int i = 0; i < l_listLine.Count; i++)
        {
            
            l_listLine[i].RemoveNode();
            yield return new WaitForSeconds(0.05f);
        }


        ScoreManager.Instance.AddScoreTime(l_listLine.Count * 2);

        l_listLine.Clear();
        //Destroy(lRend);
        //Destroy(newLine);
        yield return new WaitForSeconds(0.5f);

        TimeManager.Instance.b_pause = false;
        g_deny.SetActive(false);

    }


    //xoa trrang thai xau
    private void BonusRemoveAdverse()
    {
        icon.sprite = s_key;
        AppearPopup();
        StartCoroutine(DisappearPopup(3.5f));

        CombatTextManager.Instance.CreateText(transform.position, "BONUS Remove Adverse", Color.yellow);

        StartCoroutine(RemoveAdverse());
    }

    public IEnumerator RemoveAdverse()
    {

        g_key.SetActive(true);

        yield return new WaitForSeconds(1.7f);

        GameObject go;

        for (int i = Constant.NUMBER_ROW; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (list[i].I_numberNode > 0 && list[i].g_faceId != null)
            {
                go = SimplePool.Spawn("StarPrebooster", Vector3.zero, Quaternion.identity);
                //list[i].GetComponent<Node>().RemoveAffect();
                go.GetComponent<StarTest>().SetTargetPrebooster(list[i].gameObject.transform, list[i], 1f, Random.Range(0,0.3f), true);
                //StartCoroutine(RemoveAffect(list[i].gameObject));
            }
        }

        yield return new WaitForSeconds(1f);

        g_key.SetActive(false);

        TimeManager.Instance.b_pause = false;
    }

    private IEnumerator RemoveAffect(GameObject obj)
    {
        float ran = (float)Random.Range(0, 10)/10 + 5f;
        yield return new WaitForSeconds(ran);
        obj.GetComponent<Node>().RemoveFace();

    }

    public IEnumerator DisappearPopup(float time = 2)
    {
        yield return new WaitForSeconds(time);
        shineMC.SetActive(false);
        anim_chatbox.SetTrigger("disappear");
        g_smallChatbox.GetComponent<Animator>().SetTrigger("disappear");
        g_txtSmallChatbox.SetActive(false);
        yield return new WaitForSeconds(0.4f);
        popUPCaution.SetActive(false);
        //popUPCaution.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = 1;
        g_smallChatbox.SetActive(false);
    }

    private void AppearPopup()
    {
        bar.Initialize();
        popUPCaution.SetActive(true);
    }

    private IEnumerator CoolDownTimer(int timeDuration)
    {
        
        bar.MaxVal = timeDuration;
        bar.CurrentVal = timeDuration;

        while (bar.CurrentVal > 0.2f)
        {
            bar.CurrentVal = bar.CurrentVal - 0.2f;
            yield return new WaitForSeconds(0.2f);

            if (TimeManager.Instance.b_gameOver)
            {
                shineMC.SetActive(false);

                popUPCaution.SetActive(false);
                g_smallChatbox.SetActive(false);
                g_txtSmallChatbox.SetActive(false);
            }
        }

        yield return new WaitForSeconds(0.5f);
        StartCoroutine(DisappearPopup(0));

    }

}
