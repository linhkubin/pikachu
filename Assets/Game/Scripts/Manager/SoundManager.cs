﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public bool muteFx = false;

    public bool muteSound = false;

    public bool muteVibrate = false;

    //Sound Name
    public static string THAT_WRONG = "That’s wrong";
    public static string INCORRECT = "Incorrect";
    public static string TRY_AGAIN = "Try again";
    public static string EXCELLENT = "Excellent";
    public static string GOOD_JOB = "Good job";
    public static string GREAT = "Great";
    public static string SUPER = "Super";
    public static string WELL_DONE = "Well done";

    public AudioSource fxSource;
    public AudioSource musicSource;

    public AudioClip[] clipFx;
    public AudioClip[] clipMusic;

    // Use this for initialization
    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void PlayMusic(int indexClip)
    {
        if (!muteSound)
        {
            musicSource.clip = clipMusic[indexClip];
            musicSource.Play();
        }
    }

    public void NewMusicInGame()
    {
        if (!muteSound)
        {
            PlayMusic(Random.Range(0, 3));
        }
    }

    public void MuteMusic()
    {
        musicSource.Stop();
        muteSound = true;
    }

    public void MuteFx()
    {
        fxSource.Stop();
        muteFx = true;
    }

    public void StartMusic()
    {
        musicSource.Play();
        muteSound = false;
    }

    public void StartFx()
    {
        fxSource.Play();
        muteFx = false;
    }

    public void PlayFxSound(string nameClip, float volume = 1)
    {
        if (!muteFx)
        {
            for (int i = 0; i < clipFx.Length; i++)
            {
                if (clipFx[i].name == nameClip)
                {
                    fxSource.volume = volume;
                    fxSource.PlayOneShot(clipFx[i]);
                    break;
                }
            }
        }

    }

    public void PlayFxTrue(string name = "")
    {
        if (!muteFx)
        {
            PlayFxSound("right");
            if (name != "")
            {
                StartCoroutine(PlayTrueByName(name));
            }
            else
            {
                Invoke("PlayTrue", 0.3f);
            }
        }
    }

    IEnumerator PlayTrueByName(string name)
    {
        yield return new WaitForSeconds(0.3f);
        PlayFxSound(name);
    }

    public void PlayFxFalse(string name = "")
    {
        PlayFxSound("wrong");
        if (name != "")
        {
            StartCoroutine(PlayFalseByName(name));
        }
        else
        {
            Invoke("PlayFalse2", 0.3f);
        }
    }

    IEnumerator PlayFalseByName(string name)
    {
        yield return new WaitForSeconds(0.3f);
        PlayFxSound(name);
    }

    private void PlayTrue()
    {
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                PlayFxSound(EXCELLENT);
                break;
            case 1:
                PlayFxSound(GOOD_JOB);
                break;
            case 2:
                PlayFxSound(GREAT);
                break;
            case 3:
                PlayFxSound(SUPER);
                break;
            case 4:
                PlayFxSound(WELL_DONE);
                break;
        }
    }


    public void PlayFxFalse()
    {
        PlayFxSound("wrong", 0.5f);
        Invoke("PlayFalse", 0.3f);
    }

    //private void PlayFalse()
    //{
    //    int random = 0;
    //    if (APIRequest.currentClass == "1")
    //    {
    //        if (PrepareSource.instance.listPlatform[GameController.instance.indexVideoHelp] == (int)PlatformID.LISTEN_AND_REPEAT)
    //        {
    //            random = Random.Range(0, 2);
    //        }
    //        else
    //        {
    //            random = Random.Range(0, 3);
    //        }
    //    }
    //    else if(APIRequest.currentClass == "2")
    //    {
    //        random = Random.Range(0, 2);
    //    }
    //    else
    //    {
    //        random = Random.Range(0, 3);
    //    }
    //    switch (random)
    //    {
    //        case 0:
    //            PlayFxSound(THAT_WRONG);
    //            break;
    //        case 1:
    //            PlayFxSound(INCORRECT);
    //            break;
    //        case 2:
    //            PlayFxSound(TRY_AGAIN);
    //            break;
    //    }
    //}

    public void PlayTrueById(int id)
    {
        Debug.Log("Star Number: " + id);
        switch (id)
        {
            case 5:
                PlayFxSound(EXCELLENT);
                break;
            case 2:
                PlayFxSound(GOOD_JOB);
                break;
            case 3:
                PlayFxSound(GREAT);
                break;
            case 4:
                PlayFxSound(SUPER);
                break;
            case 1:
                PlayFxSound(WELL_DONE);
                break;
        }
    }

    public void PlayFxFalse2()
    {
        PlayFxSound("wrong", 0.5f);
        Invoke("PlayFalse2", 0.3f);
    }

    private void PlayFalse2()
    {
        int random = 0;
        random = Random.Range(0, 3);
        switch (random)
        {
            case 0:
                PlayFxSound(THAT_WRONG);
                break;
            case 1:
                PlayFxSound(INCORRECT);
                break;
            case 2:
                PlayFxSound(TRY_AGAIN);
                break;
        }
    }

    public void Vibrate(int volume = 1)
    {
        if (muteVibrate)
            return;
    #if UNITY_ANDROID || UNITY_IOS
            switch (volume)
            {
                case 1:
                    // MMVibrationManager.Haptic(HapticTypes.Selection);
                    break;
                case 2:
                    // MMVibrationManager.Haptic(HapticTypes.MediumImpact);
                    break;
            }

    #endif
    }
}
