﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : MonoBehaviour
{
    private static SpriteManager instance;
    public static SpriteManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<SpriteManager>(); return instance; } }

    public Animator g_shadow;

    public GameObject connect;

    public Sprite Squares;

    public GameObject s_shadow;

    public GameObject s_bound;

    public Character[] s_baseNode;

    public Wall[] wall;

    public GameObject g_box;

    public GameObject g_bonus;

    public Character g_freeze;

    public Character g_lock;

    public Character g_magnet;

    public Character g_jump;

    public Character g_cloud;
    
    public Character g_link;

    public Character g_boom;

    public Character g_flood;

    public Character g_hat;

    private int numberType = Constant.NUMBER_TYPE_POKEMON;

    public List<Character> l_spritesType = new List<Character>();

    public void InitListType(int numTypeMain, int numTypeExtra)
    {
        //Debug.Log(numTypeMain + "___" + numTypeExtra);

        l_spritesType.Clear();

        if (Constant.is_MODE_REQUEST)
        {

            for (int i = 0; i < ModeRequest.Instance.findNode.Count; i++)
            {
                if (ModeRequest.Instance.findNode[i].numberNode <= 10)
                {
                    ModeRequest.Instance.UI_findNodes[i].ui.SetActive(true);
                    ModeRequest.Instance.UI_findNodes[i].image.sprite = s_baseNode[ModeRequest.Instance.findNode[i].numberNode - 1].sprite;
                    ModeRequest.Instance.UI_findNodes[i].numberRequest.text = ModeRequest.Instance.findNode[i].numbeRequest.ToString();
                    l_spritesType.Add(s_baseNode[ModeRequest.Instance.findNode[i].numberNode-1]);
                    numTypeMain--;
                }
                else
                {
                    for (int j = 0; j < wall[GameManager.Instance.NUMBER_WALL].s_nodeMap.Length; j++)
                    {
                        if (wall[GameManager.Instance.NUMBER_WALL].s_nodeMap[j].number == ModeRequest.Instance.findNode[i].numberNode)
                        {
                            ModeRequest.Instance.UI_findNodes[i].ui.SetActive(true);
                            ModeRequest.Instance.UI_findNodes[i].image.sprite = wall[GameManager.Instance.NUMBER_WALL].s_nodeMap[j].sprite;
                            ModeRequest.Instance.UI_findNodes[i].numberRequest.text = ModeRequest.Instance.findNode[i].numbeRequest.ToString();
                            l_spritesType.Add(wall[GameManager.Instance.NUMBER_WALL].s_nodeMap[j]);
                            numTypeExtra--;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < numTypeMain; i++)
        {
            int ran = UnityEngine.Random.Range(0, s_baseNode.Length);
            while (l_spritesType.Contains(s_baseNode[ran]))
            {
                ran++;
                if (ran >= s_baseNode.Length)
                {
                    ran = 0;
                }
            }
            l_spritesType.Add(s_baseNode[ran]);
        }

        for (int i = 0; i < numTypeExtra; i++)
        {

            int ran = UnityEngine.Random.Range(0, wall[GameManager.Instance.NUMBER_WALL].s_nodeMap.Length);//----> check name map
            while (l_spritesType.Contains(wall[GameManager.Instance.NUMBER_WALL].s_nodeMap[ran]))
            {

                ran++;
                if (ran >= wall[GameManager.Instance.NUMBER_WALL].s_nodeMap.Length)
                {
                    ran = 0;
                }
            }
            l_spritesType.Add(wall[GameManager.Instance.NUMBER_WALL].s_nodeMap[ran]);
        }

        //for (int i = 0; i < spritesType.Count; i++)
        //{
        //    Debug.Log("length : " + spritesType[i].name);
        //}
    }




}

[Serializable]
public class Wall
{
    public string name;

    public Sprite s_MCstand;

    public Sprite s_background;

    public Character[] s_nodeMap;

    public Character[] s_barrier;

    public Character[] s_environment;

    public Character[] s_box;

    public Character[] s_water;
}

[Serializable]
public class Character
{
    public int number;
    public Sprite sprite;
    public GameObject anim;
    public GameObject clear;
}


[Serializable]
public class EnemySprite
{
    public Sprite sprite;
    public GameObject enemy;
}