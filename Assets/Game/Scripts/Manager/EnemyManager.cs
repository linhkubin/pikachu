﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    private static EnemyManager instance;
    public static EnemyManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<EnemyManager>(); return instance; } }

    public List<GameObject> l_enemy = new List<GameObject>();

    public EnemySprite[] g_enemyPrefabs;

    public void InitEnemy()
    {
        for (int i = 0; i < l_enemy.Count; i++)
        {
            SimplePool.Despawn(l_enemy[i]);
        }

        l_enemy.Clear();
    }

    public void Add(GameObject enemy)
    {
        l_enemy.Add(enemy);
    }
}
