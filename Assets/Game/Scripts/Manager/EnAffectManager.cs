﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnAffectManager : MonoBehaviour
{

    private static EnAffectManager instance;
    public static EnAffectManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<EnAffectManager>(); return instance; } }

    public GameObject g_windEastPrefabs;

    public GameObject g_windWestPrefabs;

    public GameObject g_cloudy;

    public bool windLeft = false;

    List<GameObject> g_listWind = new List<GameObject>();

    public void Add(GameObject wind)
    {
        g_listWind.Add(wind);
    }

    public void InitEAffect()
    {
        while (g_listWind.Count > 0)
        {
            SimplePool.Despawn(g_listWind[0]);
            g_listWind.RemoveAt(0);
        }
    }

    public void CreateRandomCloud()
    {
        CreateCloud(Random.Range(Constant.NUMBER_ROW,Constant.TOTAL_ITEM_NUMBER - Constant.NUMBER_ROW));
    }

    public void CreateCloud(int point)
    {
        List<GameObject> list = GameManager.Instance.L_listNode;

        int ran = Random.Range(2, 4);
        for (int i = point; i <= point + ran; i++)
        {
            if (list[i].GetComponent<Node>().I_numberNode > 0 && list[i].GetComponent<Node>().I_numberNode <= Constant.NUMBER_TYPE_POKEMON)
            {
                //list[i].GetComponent<Node>().Flood();
            }

            for (int j = i + Constant.NUMBER_ROW; j <= i + ran * Constant.NUMBER_ROW; j += Constant.NUMBER_ROW)
            {
                if (list[j].GetComponent<Node>().I_numberNode > 0 && list[j].GetComponent<Node>().I_numberNode <= Constant.NUMBER_TYPE_POKEMON)
                {
                    //list[j].GetComponent<Node>().Flood();
                }
            }
        }
    }

    public void Storm()
    {

    }

    public void Mix2x2(int point)
    {
        GameManager.Instance.SwapNode(point, point + Constant.NUMBER_ROW + Random.Range(0, 2));
        GameManager.Instance.SwapNode(point + 1, point + Constant.NUMBER_ROW + Random.Range(0, 2));
    }


    //Water enviroment
    public void CreateWater3x3(List<GameObject> waterList)
    {
        for(int i = 0; i < waterList.Count; i++)
        {
            waterList[i].GetComponent<Node>().I_id = Constant.ID_TYPE_ENVIRONMENT;
            waterList[i].GetComponent<Node>().I_numberNode = 0;
            //waterList[i].GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[0].s_environment[3].GetComponent<SpriteRenderer>().sprite;
        }
    }
}