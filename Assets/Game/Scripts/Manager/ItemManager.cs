﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    private static ItemManager instance;
    public static ItemManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<ItemManager>(); return instance; } }

    //-------------------------------------

    [SerializeField]
    private GameObject g_linePrefabs;

    private List<Node> list;
    //-------------------timer---------------------

    [SerializeField]
    private Transform tf_start;

    public GameObject g_time;

    public Transform tf_target;

    //--------------item double link----------------
    private int timeDoubleLink = 10;

    public bool useHammer = false;

    //----------------------hammer-------------------------
    public GameObject g_hammer;

    public Transform tf_hammer;

    //----------------------hint-------------------------
    public GameObject g_hint;

    public GameObject g_txtHint;

    public Text txt_hint;

    //---------------------mix-----------------------
    public GameObject g_mix;

    public GameObject g_fan;

    //-----------------------------------------------

    private void Start()
    {
        list = GameManager.Instance.m_Node;

    }
    //----------------------time
    public void ItemTime()
    {
        StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer()
    {

        TimeManager.Instance.b_pause = true;
        yield return new WaitForSeconds(0.5f);
        g_time.SetActive(true);
        GameObject go;

        //-> check numer contain
        //-> anim
        if (TimeManager.Instance.cl_timeOver.CurrentVal > 20)
        {
            MCManager.Instance.Normal();
        }

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 5; i++)
        {
            go = SimplePool.Spawn("TimeInBooster", tf_start.position, Quaternion.identity);
            yield return new WaitForSeconds(0.07f);
            go.GetComponent<StarTest>().SetTargetNoFix(tf_target, "Connected", 1.2f, 0f, true);
        }

        yield return new WaitForSeconds(0.9f);

        TimeManager.Instance.AddTime(15f);


        BoosterManager.Instance.ActiveButton();

        BoosterManager.Instance.booster[3].ResetPickChoose();

        g_time.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        TimeManager.Instance.b_pause = false;
    }
    //----------------------hammer

    public IEnumerator PickItemHammer(int index)
    {
        useHammer = false;

        g_hammer.SetActive(true);

        tf_hammer.position = GameManager.Instance.L_position[index].transform.position;

        StartCoroutine(BoosterManager.Instance.booster[4].TimerActive(1f));

        ////-> check number contain
        ///
        yield return new WaitForSeconds(.6f);

        //int index = ChooseHoeIndex();
        for (int i = index - 1; i <= index + 1; i++)
        {
            for (int j = i - Constant.NUMBER_ROW; j <= i + Constant.NUMBER_ROW; j += Constant.NUMBER_ROW)
            {
                if (j < 0 || j >= Constant.TOTAL_ITEM_NUMBER)
                {
                    continue;
                }
                if ((index % Constant.NUMBER_ROW) == 0 && (j + 1) % Constant.NUMBER_ROW == 0 )
                {
                    continue;
                }
                if (((index + 1) % Constant.NUMBER_ROW) == 0 && j % Constant.NUMBER_ROW == 0)
                {
                    continue;
                }

                if (CanBreak(list[j].I_id))
                {
                    if (list[j].I_id == Constant.ID_TYPE_NODE_FREEZE || list[j].I_id == Constant.ID_TYPE_NODE_LOCK || list[j].I_id == Constant.ID_TYPE_NODE_LINK)
                    {
                        list[j].RemoveFace();
                    }
                    else
                    {
                        list[j].RemoveNode();
                    }
                }
            }
        }


        yield return new WaitForSeconds(0.5f);

        g_hammer.SetActive(false);

        BoosterManager.Instance.booster[4].ResetPickChoose();

        BoosterManager.Instance.ActiveButton();

        PlayerPrefs.SetInt("BoosterHammer", PlayerPrefs.GetInt("BoosterHammer") - 1);

    }

    public void ItemHammer()
    {
        useHammer = !useHammer;
    }

    public void ChooseHammerIndex(int index)
    {

        if (CanBreak(list[index].I_id))
        {
            StartCoroutine(PickItemHammer(index));
        }

        Debug.Log(index);
    }

    private bool CanBreak(int id)
    {
        if (id == Constant.ID_TYPE_BOX || id == Constant.ID_TYPE_BARRIER || id == Constant.ID_TYPE_NODE_FREEZE || id == Constant.ID_TYPE_NODE_LOCK || id == Constant.ID_TYPE_NODE_LINK)
        {
            return true;
        }

        return false;
    }

    public void ActiveBarrier()
    {
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (list[i].I_id == Constant.ID_TYPE_BARRIER)
            {
                list[i].GetSpriteRenderer().sortingLayerName = "Barier";
            }
            if (list[i].I_id == Constant.ID_TYPE_NODE_FREEZE || list[i].I_id == Constant.ID_TYPE_BOX || list[i].I_id == Constant.ID_TYPE_NODE_LOCK || list[i].I_id == Constant.ID_TYPE_NODE_LINK)
            {
                list[i].g_faceId.GetComponent<SpriteRenderer>().sortingLayerName = "Barier";
            }
        }
    }

    public void InActiveBarrier()
    {
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (list[i].I_id == Constant.ID_TYPE_BARRIER)
            {
                list[i].GetSpriteRenderer().sortingLayerName = "Default";
            }
            if (list[i].I_id == Constant.ID_TYPE_NODE_FREEZE || list[i].I_id == Constant.ID_TYPE_BOX || list[i].I_id == Constant.ID_TYPE_NODE_LOCK || list[i].I_id == Constant.ID_TYPE_NODE_LINK)
            {
                list[i].g_faceId.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            }
        }

    }
    //----------------------mix

    public void ItemMix()
    {
        StartCoroutine(StartMix());
    }

    private IEnumerator StartMix()
    {
        yield return new WaitForSeconds(0.5f);
        //StartCoroutine(GameManager.Instance.Mix());
        GameManager.Instance.MixAllNode();
        yield return new WaitForSeconds(1f);
        yield return new WaitForSeconds(1.5f);
        BoosterManager.Instance.booster[5].ResetPickChoose();
        yield return new WaitForSeconds(1f);
        BoosterManager.Instance.ActiveButton();
        GameManager.Instance.mixException = 0;
    }

    public IEnumerator ActiveG_mix(bool fan = false)
    {
        TimeManager.Instance.b_pause = true;
        //if (!fan)
        //{
        //    //g_fan.SetActive(false);
        //}
        g_mix.SetActive(true);
        TimeManager.Instance.b_gameStart = false;
        yield return new WaitForSeconds(3f);
        g_mix.SetActive(false);
        //g_fan.SetActive(true);
        TimeManager.Instance.b_pause = false;
        TimeManager.Instance.b_gameStart = true;
    }
    //----------------------x2 link

    public void ItemDoubleLink()
    {
        if (BonusManager.Instance.bonusDoubleConect)
        {
            return;
        }
        StartCoroutine(StartDoubleLink());
    }

    private IEnumerator StartDoubleLink()
    {
        SpriteManager.Instance.g_shadow.SetTrigger("on");
        yield return new WaitForSeconds(0.4f);

        StartCoroutine(BonusManager.Instance.BonusLink());

        yield return new WaitForSeconds(3.2f);

        SpriteManager.Instance.g_shadow.SetTrigger("off");

        TimeManager.Instance.b_pause = false;

    }

    //---------------------key
    public void KeyRemoveAdverse()
    {
        StartCoroutine(StartKey());
    }

    private IEnumerator StartKey()
    {
        SpriteManager.Instance.g_shadow.SetTrigger("on");
        yield return new WaitForSeconds(0.4f);

        StartCoroutine(BonusManager.Instance.RemoveAdverse());

        yield return new WaitForSeconds(3.2f);

        SpriteManager.Instance.g_shadow.SetTrigger("off");

        TimeManager.Instance.b_pause = false;
    }

    //---------------------denytype
    public void DenyTypeNode()
    {
        StartCoroutine(StartDeny());
    }

    private IEnumerator StartDeny()
    {
        SpriteManager.Instance.g_shadow.SetTrigger("on");
        yield return new WaitForSeconds(0.4f);

        StartCoroutine(BonusManager.Instance.DenyNode());

        yield return new WaitForSeconds(2f);

        SpriteManager.Instance.g_shadow.SetTrigger("off");

        TimeManager.Instance.b_pause = false;
    }

    //---------------------hint
    public void Hint()
    {
        StartCoroutine(StartHint());
    }

    private IEnumerator StartHint()
    {
        yield return new WaitForSeconds(0.5f);
        g_hint.SetActive(true);
        yield return new WaitForSeconds(1f);
        BoosterManager.Instance.booster[6].ResetPickChoose();
        yield return new WaitForSeconds(1f);
        g_hint.SetActive(false);
        GameManager.Instance.itemHint = 5;
        g_txtHint.SetActive(true);
        GameManager.Instance.f_timeClick = 10;
        BoosterManager.Instance.ActiveButton();
        BoosterManager.Instance.booster[6].InActive();
    }

    public void ResetHint()
    {
        BoosterManager.Instance.booster[6].Active();
        g_txtHint.SetActive(false);
        txt_hint.text = "5";
    }

    public void SetNumberHint(int num)
    {
        txt_hint.text = num.ToString();
    }

    private IEnumerator DrawPointSuggest(int point)
    {
        list[point].SetAnim("select");
        GameObject go = SimplePool.Spawn("pointArrow", list[point].GetTransform().position, Quaternion.identity);
        yield return new WaitForSeconds(1.6f);
        list[point].SetAnim("idle");
        SimplePool.Despawn(go);
    }

    public void DrawSuggest(int point1,int point2)
    {
        StartCoroutine(DrawPointSuggest(point1));
        StartCoroutine(DrawPointSuggest(point2));
    }

    //---------------------

}
