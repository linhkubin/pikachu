
Machine.png
size: 376,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Control
  rotate: true
  xy: 253, 60
  size: 129, 71
  orig: 131, 73
  offset: 1, 1
  index: -1
as
  rotate: false
  xy: 2, 337
  size: 273, 173
  orig: 275, 175
  offset: 1, 1
  index: -1
bb
  rotate: false
  xy: 201, 60
  size: 4, 4
  orig: 6, 6
  offset: 1, 1
  index: -1
bc
  rotate: true
  xy: 257, 199
  size: 8, 26
  orig: 10, 28
  offset: 1, 1
  index: -1
bep
  rotate: true
  xy: 277, 317
  size: 193, 75
  orig: 195, 77
  offset: 1, 1
  index: -1
bi
  rotate: true
  xy: 201, 8
  size: 50, 118
  orig: 52, 120
  offset: 1, 1
  index: -1
ffffff
  rotate: true
  xy: 354, 346
  size: 164, 20
  orig: 164, 20
  offset: 0, 0
  index: -1
fgh
  rotate: false
  xy: 2, 14
  size: 197, 50
  orig: 199, 52
  offset: 1, 1
  index: -1
gc
  rotate: true
  xy: 321, 2
  size: 27, 39
  orig: 29, 41
  offset: 1, 1
  index: -1
jkl
  rotate: true
  xy: 326, 31
  size: 176, 39
  orig: 176, 39
  offset: 0, 0
  index: -1
l
  rotate: true
  xy: 257, 209
  size: 106, 104
  orig: 108, 106
  offset: 1, 1
  index: -1
noi
  rotate: false
  xy: 2, 66
  size: 249, 123
  orig: 251, 125
  offset: 1, 1
  index: -1
rc
  rotate: true
  xy: 354, 321
  size: 23, 13
  orig: 25, 15
  offset: 1, 1
  index: -1
s
  rotate: false
  xy: 257, 321
  size: 14, 14
  orig: 16, 16
  offset: 1, 1
  index: -1
shadow
  rotate: false
  xy: 2, 191
  size: 253, 144
  orig: 255, 146
  offset: 1, 1
  index: -1
