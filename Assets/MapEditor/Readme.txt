* sửa đường dẫn link st trong EditorManager

* màn hình campain
	+ màu trắng -> chưa có map
	+ màu vàng  -> đã có map
	+ ô bên trên nhập số để tìm map tắt
	+ mỗi màn hình hiển thị 20 map. bắt đầu từ map 0
* màn hình Editor
	+ top  
		- số thứ tự của map
		- số nhân vật base hiển thị trong map này
		- số nhân vật phụ < theo Wall > hiển thị trong map này
	+ các ô chọn lựa
		- ô Node base:  node rỗng, node random, các node nhân vật base
		- ô Node phụ: các node phụ theo Wall
		- ô Vật cản - box - môi trường : Xóa node<xóa khỏi vùng inGame> , node Bound<viền bao xung quanh>, 
			các vật chắn<cố định>, box, môi trường<các noed có thể link qua nhưng k thể đặt vào> -> theo Wall
		- ô hiệu ứng: đóng băng, khóa 2 bên, nam châm, nhảy, ẩn, khôá 4 bên, bom, ngậm nước
	+ button
		- số màu đỏ : số các node nhân vật trong map
		- back : trở lại màn hình campain
		- hide : ẩn hiện viền bao xung quanh
		- enemy : <update> - chọn và đặt vị trí enemy
		- environ : <update> - chọn và đặt hiệu ứng của wall vào map
		- lưu
	