﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorSprite : MonoBehaviour
{
    private static EditorSprite instance;
    public static EditorSprite Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<EditorSprite>(); return instance; } }

    public Sprite emptyCell;

    public Sprite randomCell;

    public Sprite bound;

    public Sprite[] s_mainNode;

    public WallSprite[] wall;

    public Sprite[] s_effectNode;

    public Sprite[] s_enviAffect;

    public Sprite[] s_enemy;

    public List<Sprite> s_listNode = new List<Sprite>();

    private int numberType = Constant.NUMBER_TYPE_POKEMON;

    private int numberTypeMain = 10;

    private int numberTypeExtra = Constant.NUMBER_TYPE_POKEMON - 10;

    private void Awake()
    {

        for (int i = 0; i < SpriteManager.Instance.s_baseNode.Length; i++)
        {
            s_listNode.Add(SpriteManager.Instance.s_baseNode[i].sprite);
        }

        for (int i = 0; i < SpriteManager.Instance.wall.Length; i++)
        {
            for (int j = 0; j < SpriteManager.Instance.wall[i].s_nodeMap.Length; j++)
            {
                s_listNode.Add(SpriteManager.Instance.wall[i].s_nodeMap[j].sprite);
            }
        }
    }


}
