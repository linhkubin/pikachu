﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorConstant : MonoBehaviour
{
    private static EditorConstant instance;
    public static EditorConstant Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<EditorConstant>(); return instance; } }

    public GameObject g_modeRequest;
    public bool modeRequest;

    public RequestCell oneRQ;
    public RequestCell towRQ;
    public RequestCell threeRQ;

    public GameObject g_modeFindStar;
    public bool modeFindStar = false;

    public GameObject g_have_enemy;
    public bool have_enemy;

    public GameObject g_have_element;
    public bool have_element;

    public void UpdateModeRequest(bool have)
    {
        modeRequest = have;
        g_modeRequest.SetActive(have);

        if (have)
        {
            UpdateFindStar(false);
        }

    }

    public void UpdateFindStar(bool have)
    {
        modeFindStar = have;
        g_modeFindStar.SetActive(have);
        if (have)
        {
            UpdateModeRequest(false);
            oneRQ.Reset();
            towRQ.Reset();
            threeRQ.Reset();
        }
        else
        {
            
        }

    }

    public void UpdateEnemy(bool have)
    {
        have_enemy = have;
        g_have_enemy.SetActive(have);

    }

    public void UpdateElement(bool have)
    {
        have_element = have;
        g_have_element.SetActive(have);

    }

    public void Reset()
    {
        UpdateModeRequest(false);
        UpdateFindStar(false);
        UpdateEnemy(false);
        UpdateElement(false);
    }

    public MapModeRequest GetRequest()
    {
        MapModeRequest newRequest = new MapModeRequest();
        
        newRequest.modeRequest = this.modeRequest;
        newRequest.first = new RequestNode(oneRQ.numberNode,oneRQ.GetNumberRequest());
        newRequest.second = new RequestNode(towRQ.numberNode, towRQ.GetNumberRequest());
        newRequest.third = new RequestNode(threeRQ.numberNode, threeRQ.GetNumberRequest());

        return newRequest;
    }
}

