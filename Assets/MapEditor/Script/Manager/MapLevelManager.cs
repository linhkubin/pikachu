﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapLevelManager : MonoBehaviour
{

    public static int index = 0;

    public int number = 0;

    public Text enterIndex;

    public Text[] textLevel;


    // Start is called before the first frame update
    void Start()
    {
        Config();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            Debug.Log("enter");

            EnterIndex();
        }


    }

    public void Next()
    {
        index += 20;
        Config();
    }


    public void Prev()
    {
        if (index >= 20)
        {
            index -= 20;
        }
        Config();
    }

    public void EnterIndex()
    {
        index = (int.Parse(enterIndex.text) / 20) * 20;
        Config();
    }

    //ECDB4B

    public void Config()
    {
        number = index;

        for (int i = 0; i < 20; i++)
        {
            number = index + i;

            if (File.Exists(Application.dataPath + "/Game/Resources/" + (index + i).ToString()+".json"))
            {
                textLevel[i].GetComponentInParent<Image>().color = new Color(0.9f, 0.9f, 0.3f, 1);
            }
            else
            {
                textLevel[i].GetComponentInParent<Image>().color = Color.white;
            }

            textLevel[i].text = (index + i).ToString();
            
        }
    }

    public void TestGame()
    {
        SceneManager.LoadScene("PlayScene");
    }

}
