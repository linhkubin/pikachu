﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIEManager : MonoBehaviour
{
    private static UIEManager instance;
    public static UIEManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<UIEManager>(); return instance; } }

    public GameObject alertPanel;

    public GameObject savePanel;

    public GameObject buttonOK;

    public GameObject environPanel;

    public GameObject enemyPanel;

    public GameObject modeRequestPanel;

    public GameObject modeFindStarPanel;

    public GameObject initWallPanel;

    public InputField txtNumberWall;

    public Text contentSavePanel;

    public void OpenAlertPanel()
    {
        alertPanel.SetActive(true);
    }

    public void CloseAlertPanel()
    {
        alertPanel.SetActive(false);
    }

    public void OpenIntroPanel(string mess, bool err = false)
    {
        contentSavePanel.text = mess;
        savePanel.SetActive(true);

        if (err == true)
        {
            StartCoroutine(DisappearPanel(savePanel, buttonOK));
        }

    }

    private IEnumerator DisappearPanel(GameObject panel, GameObject button)
    {
        button.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        panel.SetActive(false);
        button.SetActive(true);
    }

    public void CloseIntroPanel()
    {
        savePanel.SetActive(false);
    }

    public void GoCampaign()
    {
        SceneManager.LoadScene("MapCampain");
    }

    public void SaveJson()
    {
        EditorManager.Instance.WriteToJson();
    }

    public void OpenEvinronPanel()
    {
        environPanel.SetActive(!environPanel.activeInHierarchy);
        EditorConstant.Instance.UpdateElement(EditorManager.Instance.L_listEA.Count > 0);
    }

    public void OpenEnemyPanel()
    {
        enemyPanel.SetActive(!enemyPanel.activeInHierarchy);
        
    }

    public void CloseEnvironPanel()
    {
        environPanel.SetActive(false);
        if (modeRequestPanel.activeInHierarchy)
        {
            modeRequestPanel.SetActive(false);
        }
    }

    public void CloseEnemyPanel()
    {
        enemyPanel.SetActive(false);
    }

    public void OpenRequestPanel()
    {
        modeRequestPanel.SetActive(!modeRequestPanel.activeInHierarchy);
    }

    public void OpenFindStarPanel()
    {
        modeFindStarPanel.SetActive(!modeFindStarPanel.activeInHierarchy);
        bool hat = false;
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (EditorManager.Instance.L_listCell[i].GetComponent<Cell>().Id == Constant.ID_HAT)
            {
                hat = true;
                break;
            }
        }
        EditorConstant.Instance.UpdateFindStar(hat);
    }

    public void OpenInitNumberWallPanel()
    {
        initWallPanel.SetActive(true);
    }

    public void CloseInitNumberWallPanel()
    {
        EditorManager.numWall = int.Parse(txtNumberWall.text);

        if (EditorManager.numWall != 0)
        {
            EditorManager.Instance.txt_typeExtra.text = "5";
        }
        else
        {
            EditorManager.Instance.txt_typeExtra.text = "0";
        }

        EditorManager.Instance.InitButton();

        EditorManager.Instance.InitBound();

        initWallPanel.SetActive(false);
    }

    public void ChangeWall()
    {
        EditorManager.numWall = int.Parse(EditorManager.Instance.txt_numberWall.text);
        EditorManager.Instance.InitButton();
        for (int i = 0; i < 120; i++)
        {
            EditorManager.Instance.L_listCell[i].GetComponent<Cell>().ConfigImage();
        }
        EditorManager.Instance.txt_typeExtra.text = "5";
    }
}
