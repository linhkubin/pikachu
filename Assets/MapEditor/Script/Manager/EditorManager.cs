﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditorManager : MonoBehaviour
{
    private static EditorManager instance;
    public static EditorManager Instance
    { get { if (instance == null) instance = GameObject.FindObjectOfType<EditorManager>(); return instance; } }

    public static int numWall = 0;

    public static int NUM_MAP = 0;

    public Text txt_numNode;

    public Text txt_numberWall;

    public Text txt_numberMap;

    public Transform tf_board;

    public GameObject g_nodeEmpty;

    public GameObject g_image;

    public int tempId = 4;
    public int tempNumber;
    public int tempNumberType;

    public List<GameObject> L_listCell = new List<GameObject>();

    public List<GameObject> L_listEA = new List<GameObject>();

    public List<GameObject> L_listEnemy = new List<GameObject>();

    //--------------------number-------------------------

    public int numTypeMain = 5;

    public int numTypeExtra = 0;

    public Text txt_typeMain;

    public Text txt_typeExtra;


    //---------------tranform----------------------------

    public GameObject g_buttonPrefab;

    public Transform tf_mainType;

    public Transform tf_extraType;

    public Transform tf_barrier_box_environment;
    //----------------time _ star-------------------------------
    public Text txt_timePlay;

    public Text txt_firstStar;

    public Text txt_secondStar;

    public Text txt_thirdStar;

    //----------------Environ Affect-------------------------------
    public GameObject g_EAPrefabs;

    public Transform contentEA;

    public int tempEA_type = 0;

    //----------------Enemy-------------------------------
    public GameObject g_enemyPrefabs;

    public Transform tf_enemy;

    public int tempEnemy_type = 0;

    //------------------mode request---------------------
    //public MapModeRequest modeRequest;

    //---------------------------------------------------
    void Awake()
    {
        if (numWall >= SpriteManager.Instance.wall.Length)
        {
            numWall = 0;
        }
        Init();
    }

    private void Start()
    {
        
        txt_timePlay.transform.parent.gameObject.GetComponent<InputField>().text = "90";
        ReadMapFromJson();
        CheckNode();
    }

    // Update is called once per frame
    void Update()
    {
        ClickTarget();

        //Debug.Log(txt_timePlay.text);

        //Debug.Log("__id :"+ tempCell.GetComponent<Cell>().Id);
        //Debug.Log("__Number :"+ tempCell.GetComponent<Cell>().NumberNode);
        //Debug.Log("__NumberType :"+ tempCell.GetComponent<Cell>().NumberType);
    }

    // add target 
    private void ClickTarget()
    {

        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())//khong bat su kien khac
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 256);

            if (hit.collider != null) //if (hit.collider.tag == "node")
            {
                hit.transform.GetComponent<Cell>().OnClick();
                CheckNode();
            }
        }
    }

    private void CheckNode()
    {
        int num = 0;

        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (L_listCell[i].GetComponent<Cell>().NumberNode != 0)
            {
                num++;
            }
        }

        txt_numNode.text = num.ToString();
    }

    private void Init()
    {
        InitNodeEmpty();
        //InitButton();

    }

    //khoi tao node trong
    private void InitNodeEmpty()
    {
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            GameObject cell;
            cell = Instantiate(g_nodeEmpty);
            cell.GetComponent<Cell>().index = i;
            cell.GetComponent<Cell>().Id = 4;
            cell.GetComponent<Cell>().ConfigImage();
            cell.transform.SetParent(tf_board, false);
            //position 
            L_listCell.Add(cell);
        }

    }


    List<GameObject> listButton = new List<GameObject>();
    public void InitButton()
    {
        txt_numberWall.transform.parent.GetComponent<InputField>().text = numWall.ToString();

        for (int i = listButton.Count-1; i >= 0;  i--)
        {
            Destroy(listButton[i]);
        }

        listButton.Clear();

        //main Node
        for (int i = 0; i < SpriteManager.Instance.s_baseNode.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.s_baseNode[i].sprite;
            button.transform.SetParent(tf_mainType, false);
            button.GetComponent<ButtonSprite>().SetResource( Constant.ID_TYPE_NODE, i + 1, 0);
            listButton.Add(button);
        }

        //extra Node
        for (int i = 0; i < SpriteManager.Instance.wall[numWall].s_nodeMap.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.wall[numWall].s_nodeMap[i].sprite;
            button.transform.SetParent(tf_extraType, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_TYPE_NODE, 10 + i + 1 + (numWall -1)* 5, 0);
            listButton.Add(button);
        }

        //barrier Node
        for (int i = 0; i < SpriteManager.Instance.wall[numWall].s_barrier.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.wall[numWall].s_barrier[i].sprite;
            button.transform.SetParent(tf_barrier_box_environment, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_TYPE_BARRIER, 0, i);
            listButton.Add(button);
        }

        //box Node
        for (int i = 0; i < SpriteManager.Instance.wall[numWall].s_box.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.wall[numWall].s_box[i].sprite;
            button.transform.SetParent(tf_barrier_box_environment, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_TYPE_BOX, 0, i);
            listButton.Add(button);
        }

        //envi Node
        for (int i = 0; i < SpriteManager.Instance.wall[numWall].s_environment.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.wall[numWall].s_environment[i].sprite;
            button.transform.SetParent(tf_barrier_box_environment, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_TYPE_ENVIRONMENT, 0, i + 1);
            listButton.Add(button);
        }

        //water Node
        for (int i = 0; i < SpriteManager.Instance.wall[numWall].s_water.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = SpriteManager.Instance.wall[numWall].s_water[i].sprite;
            button.transform.SetParent(tf_barrier_box_environment, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_TYPE_NODE_WATER, 0, i);
            listButton.Add(button);
        }

        //enemy
        for (int i = 0; i < EnemyManager.Instance.g_enemyPrefabs.Length; i++)
        {
            GameObject button = Instantiate(g_buttonPrefab);
            button.GetComponent<Image>().sprite = EnemyManager.Instance.g_enemyPrefabs[i].sprite;
            button.transform.SetParent(tf_enemy, false);
            button.GetComponent<ButtonSprite>().SetResource(Constant.ID_ENEMY, 0, i);
            listButton.Add(button);
        }

    }

    public void ChooseNumber(int number)
    {
        tempNumber = number;
    }

    public void ChooseId(int id)
    {
        tempId = id;
    }

    public void ChooseNumberType(int numberType)
    {
        tempNumberType = numberType;
    }

    public bool IsSpecialCell(int id)
    {
        switch (id)
        {
            case 5 : 
            case 6 :
            case 7 :
            case 8 :
            case 9 :
            case 10 :
            case 11 :
            case 12 : return true;

            default: break;
        }

        return false;
    }

    public MapJson mapManager = new MapJson();

    public void WriteToJson()
    {
        if (!CheckSave())
        {
            return;
        }
        
        mapManager.map = NUM_MAP;

        mapManager.numberWall = int.Parse(txt_numberWall.text); 

        mapManager.timePlay = int.Parse(txt_timePlay.text);

        Debug.Log(txt_firstStar.text+ txt_secondStar.text+ txt_thirdStar.text);

        Star star = new Star(int.Parse(txt_firstStar.text), int.Parse(txt_secondStar.text), int.Parse(txt_thirdStar.text));

        mapManager.star = star;

        mapManager.numberTypeMain = int.Parse(txt_typeMain.text);
        //mapManager.numberTypeMain = numTypeMain;

        mapManager.numberTypeMap = int.Parse(txt_typeExtra.text);
        //mapManager.numberTypeMap = numTypeExtra;

        mapManager.numberTypeRand = 0;

        mapManager.modeRequest = EditorConstant.Instance.GetRequest();

        mapManager.modeFindStar = new MapModeFindStar(EditorConstant.Instance.modeFindStar);

        //-----------------------------------------------------------------------------
        Debug.Log("_______________" + L_listCell.Count);

        for (int i = 0; i < L_listCell.Count; i++)
        {
            Tile tile = new Tile(L_listCell[i].GetComponent<Cell>().Id, L_listCell[i].GetComponent<Cell>().NumberNode, L_listCell[i].GetComponent<Cell>().NumberType);

            mapManager.tileList.Add(tile);

            if (L_listCell[i].GetComponent<Cell>().NumberNode == -1)
            {
                mapManager.numberTypeRand++;
            }

        }

        for (int i = 0; i < L_listEA.Count; i++)
        {
            EnviAffect ea = new EnviAffect(L_listEA[i].GetComponent<EAffect>().type, L_listEA[i].GetComponent<EAffect>().timeStart, L_listEA[i].GetComponent<EAffect>().timeRemain);

            mapManager.EAList.Add(ea);

        }

        string json = JsonUtility.ToJson(mapManager, true);


        File.WriteAllText(Application.dataPath + "/Game/Resources/"+ NUM_MAP.ToString() + ".json", json);
        
        Debug.Log("CONVERT JSON OK !!!");


        mapManager.EAList.Clear();
        mapManager.tileList.Clear();

        UIEManager.Instance.OpenIntroPanel("complete save");
    }

    private bool CheckSave()
    {
        bool hat = false;
        for (int i = 0; i < Constant.TOTAL_ITEM_NUMBER; i++)
        {
            if (EditorManager.Instance.L_listCell[i].GetComponent<Cell>().Id == Constant.ID_HAT)
            {
                hat = true;
                break;
            }
        }
        EditorConstant.Instance.UpdateFindStar(hat);

        if (int.Parse(txt_timePlay.text) <= 0)
        {
            UIEManager.Instance.OpenIntroPanel("Time error",true);
            return false;
        }

        if (int.Parse(txt_firstStar.text) <= 0)
        {
            UIEManager.Instance.OpenIntroPanel("Star error", true);
            return false;
        }

        if (int.Parse(txt_numNode.text) % 2 == 1)
        {
            UIEManager.Instance.OpenIntroPanel("Number %2 =1", true);
            return false;
        }

        return true;
    }

    public void ReadMapFromJson()
    {
        EditorConstant.Instance.Reset();
        txt_numberMap.text = NUM_MAP.ToString();

        //Doc Json 
        if (!File.Exists(Application.dataPath + "/Game/Resources/" + NUM_MAP.ToString() + ".json"))
        {
            Debug.Log("null");


            txt_typeMain.text = numTypeMain.ToString();

            txt_typeExtra.text = "0";

            UIEManager.Instance.OpenInitNumberWallPanel();

            txt_firstStar.transform.parent.GetComponent<InputField>().text = "2000";
            txt_secondStar.transform.parent.GetComponent<InputField>().text = "5000";
            txt_thirdStar.transform.parent.GetComponent<InputField>().text = "9000";

            return;
        }

        TextAsset assets = Resources.Load(NUM_MAP.ToString()) as TextAsset;

        Debug.Log(assets);

        mapManager = JsonUtility.FromJson<MapJson>(assets.text);

        numWall = mapManager.numberWall;

        InitButton();

        numTypeMain = mapManager.numberTypeMain;

        numTypeExtra = mapManager.numberTypeMap;

        //----------------------
        //time play
        txt_timePlay.transform.parent.GetComponent<InputField>().text = mapManager.timePlay.ToString();

        //star
        txt_firstStar.transform.parent.GetComponent<InputField>().text = mapManager.star.first.ToString();
        txt_secondStar.transform.parent.GetComponent<InputField>().text = mapManager.star.second.ToString();
        txt_thirdStar.transform.parent.GetComponent<InputField>().text = mapManager.star.third.ToString();

        //type Main + map 
        txt_typeMain.text = numTypeMain.ToString();

        txt_typeExtra.text = numTypeExtra.ToString();

        //------------------------------mode request-----------------------------------
        EditorConstant.Instance.UpdateModeRequest(mapManager.modeRequest.modeRequest);

        if (mapManager.modeRequest.modeRequest)
        {
            EditorConstant.Instance.oneRQ.SetValue(mapManager.modeRequest.first.numberNode, mapManager.modeRequest.first.numberRequest);
            EditorConstant.Instance.towRQ.SetValue(mapManager.modeRequest.second.numberNode, mapManager.modeRequest.second.numberRequest);
            EditorConstant.Instance.threeRQ.SetValue(mapManager.modeRequest.third.numberNode, mapManager.modeRequest.third.numberRequest);
        }

        //------------------------------mode find star-----------------------------------

        //-----------------------------------------------------------------------------
        Debug.Log("_______________" + mapManager.tileList.Count);
        //node
        for (int i = 0; i < mapManager.tileList.Count; i++)
        {
            L_listCell[i].GetComponent<Cell>().Id = mapManager.tileList[i].Id;
            L_listCell[i].GetComponent<Cell>().NumberNode = mapManager.tileList[i].number;
            L_listCell[i].GetComponent<Cell>().NumberType = mapManager.tileList[i].numType;
            L_listCell[i].GetComponent<Cell>().ConfigImage();
        }

        L_listEA.Clear();
        //Eaffect
        for (int i = 0; i < mapManager.EAList.Count; i++)
        {
            GameObject ea = Instantiate(g_EAPrefabs);
            ea.transform.SetParent(contentEA, false);
            ea.GetComponent<EAffect>().SetResource(mapManager.EAList[i].type, mapManager.EAList[i].timeStart, mapManager.EAList[i].timeRemain);
            L_listEA.Add(ea);

        }

        Debug.Log("READ JSON OK !!!");

        mapManager.tileList.Clear();
        mapManager.EAList.Clear();
    }

    bool hideBound = false;

    public void HideBound()
    {

        for (int i = 0; i < L_listCell.Count; i++)
        {
            if (L_listCell[i].GetComponent<Cell>().Id == 0 && L_listCell[i].GetComponent<Cell>().NumberNode == 0 && L_listCell[i].GetComponent<Cell>().NumberType == 0)
            {
                if (!hideBound)
                {
                    L_listCell[i].GetComponent<SpriteRenderer>().color = Color.clear;
                }
                else
                {
                    L_listCell[i].GetComponent<SpriteRenderer>().color = Color.white;
                }
            }
        }

        hideBound = !hideBound;
    }

    public void AddNumMain()
    {
        numTypeMain++;
        if (numTypeMain > 10)
        {
            numTypeMain = 0;
        }

        txt_typeMain.text = numTypeMain.ToString();
    }

    public void AddNumExtra()
    {
        numTypeExtra++;
        if (numTypeExtra > 5)
        {
            numTypeExtra = 0;
        }

        txt_typeExtra.text = numTypeExtra.ToString();
    }

    public void AddEA()
    {
        GameObject ea = Instantiate(g_EAPrefabs);
        ea.transform.SetParent(contentEA, false);

        L_listEA.Add(ea);
    }

    public void SaveEA()
    {
        for (int i = 0; i < L_listEA.Count; i++)
        {
            L_listEA[i].GetComponent<EAffect>().ConfigImage();
        }
    }

    public void InitBound()
    {
        for (int i = 0; i < 10; i++)
        {
            L_listCell[i].GetComponent<Cell>().Id = 0;
            L_listCell[i].GetComponent<Cell>().NumberNode = 0;
            L_listCell[i].GetComponent<Cell>().NumberType = 0;
            L_listCell[i].GetComponent<Cell>().ConfigImage();
        }

        for (int i = 110; i < 120; i++)
        {
            L_listCell[i].GetComponent<Cell>().Id = 0;
            L_listCell[i].GetComponent<Cell>().NumberNode = 0;
            L_listCell[i].GetComponent<Cell>().NumberType = 0;
            L_listCell[i].GetComponent<Cell>().ConfigImage();
        }

        for (int i = 0; i < 120; i+= 10)
        {
            L_listCell[i].GetComponent<Cell>().Id = 0;
            L_listCell[i].GetComponent<Cell>().NumberNode = 0;
            L_listCell[i].GetComponent<Cell>().NumberType = 0;
            L_listCell[i].GetComponent<Cell>().ConfigImage();
        }

        for (int i = 9; i < 120; i += 10)
        {
            L_listCell[i].GetComponent<Cell>().Id = 0;
            L_listCell[i].GetComponent<Cell>().NumberNode = 0;
            L_listCell[i].GetComponent<Cell>().NumberType = 0;
            L_listCell[i].GetComponent<Cell>().ConfigImage();
        }
    }
}

[Serializable]
public class WallSprite
{
    public string name;

    public Sprite[] s_nodeMap;

    public Sprite[] s_barrier;

    public Sprite[] s_environment;

    public Sprite[] s_box;
}