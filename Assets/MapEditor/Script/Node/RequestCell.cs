﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequestCell : MonoBehaviour
{
    public int numberNode;

    public int numberRequest;

    public Text txtNumberRequest;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(setResource);
    }

    private void setResource()
    {
        if (!EditorConstant.Instance.modeRequest)
        {
            EditorConstant.Instance.UpdateModeRequest(true);
        }
        numberNode = EditorManager.Instance.tempNumber;
        Config();
    }

    private void Config()
    {
        if (numberNode == 0)
        {
            Debug.Log("err");
            return;
        }else
        
        gameObject.GetComponent<Image>().sprite = EditorSprite.Instance.s_listNode[numberNode - 1];
    }

    public int GetNumberRequest()
    {
        return int.Parse(txtNumberRequest.text);
    }

    public void Reset()
    {
        gameObject.GetComponent<Image>().sprite = null;
        numberNode = 0;
        txtNumberRequest.text = "0";
    }

    public void SetValue(int number,int request)
    {
        Debug.Log(number+"__"+request);
        if (!EditorConstant.Instance.modeRequest)
        {
            EditorConstant.Instance.UpdateModeRequest(true);
        }

        txtNumberRequest.transform.parent.GetComponent<InputField>().text = request.ToString();
        this.numberNode = number;
        Config();
    }
}
