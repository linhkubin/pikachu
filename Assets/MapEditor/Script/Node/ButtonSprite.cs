﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSprite : MonoBehaviour
{

    public int id;

    public int numberNode;

    public int numberType;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(getResource);
    }

    public void getResource()
    {
        EditorManager.Instance.tempId = id;

        EditorManager.Instance.tempNumber = numberNode;

        EditorManager.Instance.tempNumberType = numberType;


        //Debug.Log("id : "+ id + "-number : "+ numberNode + "-type : "+ numberType);
    }

    public void SetResource(int id, int numberNode, int numberType)
    {
        this.id = id;
        this.numberNode = numberNode;
        this.numberType = numberType;
    }
}
