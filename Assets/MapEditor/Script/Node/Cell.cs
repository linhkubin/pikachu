﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{

    public int index { get; set; }
    public int NumberNode { get; set; }
    public int Id { get; set; }
    public int NumberType { get; set; }

    public GameObject effectCell;


    public void OnClick()
    {
        if (!CheckCondition())
        {
            return;
        }

        Destroy(effectCell);
        Id = 4;

        Id = EditorManager.Instance.tempId;

        if (!EditorManager.Instance.IsSpecialCell(EditorManager.Instance.tempId))
        {
            NumberNode = EditorManager.Instance.tempNumber;
        }
            
        NumberType = EditorManager.Instance.tempNumberType;


        ConfigImage();

        Debug.Log("Click index : " + index + " ____id : " + Id + " _number : " + NumberNode + " _numType : " + NumberType);
    }

    public bool CheckCondition()
    {
        // true continue;//false break
        if (EditorManager.Instance.IsSpecialCell(EditorManager.Instance.tempId))
        {
            // khong phai la node empty
            if (Id == 4 && NumberNode != 0)
            {
                return true;
            }
            else return false;
        }

        return true;
    }

    public void ConfigImage()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;

        //emty node
        if (Id == 4 && NumberNode == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.emptyCell;
        }

        //random node
        if (Id == 4 && NumberNode == -1)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.randomCell;
        }

        //remove node
        if (Id == -2)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.emptyCell;
            gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
        }
        else gameObject.GetComponent<SpriteRenderer>().color = Color.white;

        //bound
        if (Id == 0 && NumberNode == 0 && NumberType == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.bound;
        }

        //barrier
        if (Id == Constant.ID_TYPE_BARRIER)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[EditorManager.numWall].s_barrier[NumberType].sprite;
        }

        //box
        if (Id == Constant.ID_TYPE_BOX)
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
            Destroy(effectCell);
            effectCell = Instantiate(EditorManager.Instance.g_image);
            effectCell.transform.SetParent(gameObject.transform, false);

            //effectCell.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.s_effectNode[Id - 5];//-->fix
            effectCell.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[EditorManager.numWall].s_box[NumberType].sprite;
            effectCell.transform.localScale = new Vector3(200, 200, 1);
        }

        //environment
        if (Id == Constant.ID_TYPE_ENVIRONMENT && NumberType != 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[EditorManager.numWall].s_environment[NumberType - 1].sprite;
        }

        //water
        if (Id == Constant.ID_TYPE_NODE_WATER)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = SpriteManager.Instance.wall[EditorManager.numWall].s_water[NumberType].sprite;
        }

        if (EditorManager.Instance.IsSpecialCell(Id))
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.randomCell;
            Destroy(effectCell);
            effectCell = Instantiate(EditorManager.Instance.g_image);
            effectCell.transform.SetParent(gameObject.transform, false);

            effectCell.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.s_effectNode[Id - 5];//-->fix
        }

        //enemy
        if (Id == Constant.ID_ENEMY)
        {
            EditorConstant.Instance.UpdateEnemy(true);
            gameObject.GetComponent<SpriteRenderer>().sprite = EnemyManager.Instance.g_enemyPrefabs[NumberType].sprite;
            Destroy(effectCell);
        }

        //hat
        if (Id == Constant.ID_HAT)
        {
            EditorConstant.Instance.UpdateFindStar(true);
            gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
            Destroy(effectCell);
            effectCell = Instantiate(EditorManager.Instance.g_image);
            effectCell.transform.SetParent(gameObject.transform, false);

            //effectCell.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.s_effectNode[Id - 5];//-->fix
            effectCell.GetComponent<SpriteRenderer>().sprite = EditorSprite.Instance.s_effectNode[8];
            //effectCell.transform.localScale = new Vector3(200, 200, 1);
            NumberNode = 0;
        }
    }
}
