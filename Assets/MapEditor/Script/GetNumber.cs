﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GetNumber : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInParent<Button>().onClick.AddListener(getNumber);
    }

    public void getNumber()
    {

        Debug.Log( gameObject.GetComponentInChildren<Text>().text);

        EditorManager.NUM_MAP = int.Parse(gameObject.GetComponentInChildren<Text>().text);

        //int temp = (EditorManager.NUM_MAP - 1) / 20;

        //Debug.Log(temp);

        //EditorManager.numWall =  temp;

        SceneManager.LoadScene("EditorScene");
    }

}
