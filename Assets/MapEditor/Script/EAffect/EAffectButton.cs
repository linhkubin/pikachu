﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EAffectButton : MonoBehaviour
{

    public int type;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(getResource);
    }

    public void getResource()
    {
        EditorManager.Instance.tempEA_type = type;
        Debug.Log(" click"+EditorManager.Instance.tempEA_type);
    }

}
