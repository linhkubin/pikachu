﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EAffect : MonoBehaviour
{

    public int type;

    public int timeStart;

    public int timeRemain;

    // Start is called before the first frame update
    void Start()
    {

        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        type = EditorManager.Instance.tempEA_type;
        ConfigImage();
    }

    public void ConfigImage()
    {
        if (type == -1)
        {
            EditorManager.Instance.L_listEA.RemoveAt(EditorManager.Instance.L_listEA.IndexOf(gameObject));
            Destroy(gameObject);
        }
        else
        {
            if (type == 0)
            {
                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = EditorSprite.Instance.s_enviAffect[0];
            }

            if (type == 1)
            {
                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = EditorSprite.Instance.s_enviAffect[1];
            }

            if (type == 2)
            {
                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = EditorSprite.Instance.s_enviAffect[2];
            }

        }

        



        

        timeStart = int.Parse(gameObject.transform.GetChild(1).GetChild(0).GetComponent<InputField>().text);
        timeRemain = int.Parse(gameObject.transform.GetChild(2).GetChild(0).GetComponent<InputField>().text);

        Debug.Log(timeStart + " + "+ timeRemain);
    }

    public void SetResource(int type, int timeStart, int timeRemain)
    {
        this.type = type;

        this.timeStart = timeStart;

        this.timeRemain = timeRemain;

        if (type == 0)
        {
            gameObject.transform.GetChild(0).GetComponent<Image>().sprite = EditorSprite.Instance.s_enviAffect[0];
        }

        if (type == 1)
        {
            gameObject.transform.GetChild(0).GetComponent<Image>().sprite = EditorSprite.Instance.s_enviAffect[1];
        }

        gameObject.transform.GetChild(1).GetChild(0).GetComponent<InputField>().text = timeStart.ToString();
        gameObject.transform.GetChild(2).GetChild(0).GetComponent<InputField>().text = timeRemain.ToString();
    }
}
